# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later


import os
import pytest
import time

@pytest.fixture
def mf_version():
    return pytest.mf_version

def pytest_addoption(parser):
    parser.addoption("--mf-version", default=None,
                     required=True, help="MasterFIP version")

def pytest_configure(config):
    pytest.mf_version = config.getoption("--mf-version")


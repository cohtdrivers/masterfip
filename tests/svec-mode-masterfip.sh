#!/bin/bash

# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

. /usr/local/drivers/scripts/environment.sh

# Restart all masterfip mockturtle CPUs
for DEV in $(/usr/local/bin/lsmockturtle)
do
    APPID=$(cat /sys/class/mockturtle/${DEV}/application_id)
    if [ "${APPID}" = "0x53564d42" ]
    then
        DEV_ID=0x$(echo ${DEV} | cut -d "-" -f 2)
        /usr/local/bin/mockturtle-loader -D ${DEV_ID} -i 0 -f ${FIRMWARE_PATH}/masterfip-rt-ba-v3.0.3.bin
        /usr/local/bin/mockturtle-loader -D ${DEV_ID} -i 1 -f ${FIRMWARE_PATH}/masterfip-rt-cmd-v3.0.3.bin
        /usr/local/bin/mockturtle-cpu-restart -D ${DEV_ID} -i 0 -i 1
    fi
done

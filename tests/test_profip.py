# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later


import os
import pytest
import serial
import stat
import time

filename_pf_tty = "/dev/ttywbu-pf-0"
filename_pfdata_tty = "/dev/ttywbu-pfdata-0"

def write_wbu(ser, data):
    ser.write(data.encode('utf-8'))

def read_wbu(ser):
    outputCharacters = ""
    while 1:
        ch = ser.read(10)
        if len(ch) == 0:
            break
        outputCharacters += ch.decode()
    return outputCharacters

def get_version(resp):
    if len(resp.split("ProFIP ")) >= 2:
        resp = resp.split("ProFIP ",1)[1]
        if len(resp.split(" ")) >= 2: 
            version = resp.split(" ")[0]
            git_ver = resp.split(" ")[1].splitlines()[0]
            
            return version, git_ver
    return "0", "0"

def get_status(resp):
    profip_state = "ERROR"
    masterfip_state = "ERROR"

    if len(resp.split("ProFIP state:    ")) >= 2:
        resp = resp.split("ProFIP state:    ")[1]
        
        if len(resp.split("MasterFip state: ")) >= 2: 
            profip_state = resp.split(" ")[0]
            masterfip_state = resp.split("MasterFip state: ")[1].split("\r")[0]   
    return profip_state, masterfip_state

def get_alarms(resp):
    profip_alarm = "ERROR"

    if len(resp.split(" | ")) >= 6:
        resp = resp.split(" | ")[5]
        profip_alarm = resp.split("\r")[0]

    return profip_alarm

class TestProfip(object):

    def test_tty_pf_exists(self):
        assert os.path.exists(filename_pf_tty) == True

    def test_tty_pfdata_exists(self):
        assert os.path.exists(filename_pfdata_tty) == True

    def test_tty_pf_chardevice(self):
        st = os.stat(filename_pf_tty)
        assert stat.S_ISCHR(st.st_mode) == True

    def test_tty_pfdata_chardevice(self):
        st = os.stat(filename_pfdata_tty)
        assert stat.S_ISCHR(st.st_mode) == True

    def test_tty_pf_permission(self):
        st = os.stat(filename_pf_tty)
        assert st.st_mode & stat.S_IRUSR != 0

    def test_tty_pfdata_permission(self):
        st = os.stat(filename_pfdata_tty)
        assert st.st_mode & stat.S_IRUSR != 0

    def test_reboot(self):
        with serial.Serial(filename_pf_tty, timeout = 1) as ser:
            read_wbu(ser) #clear buffer
            write_wbu(ser, '\nreboot\n')
            time.sleep(3)
            read_wbu(ser) #clear buffer
            write_wbu(ser, '\nstatus\n')
            resp = read_wbu(ser)
            pf_state, mf_state = get_status(resp)
            time.sleep(2)
            assert pf_state == "RUNNING" or pf_state == "CONNECTING" or pf_state == "PROGRAMMING"

    def test_any_version(self):
        with serial.Serial(filename_pf_tty, timeout = 1) as ser:
            read_wbu(ser) #clear buffer
            write_wbu(ser, '\nversion\n')
            resp = read_wbu(ser)
            version, git_version = get_version(resp)
            assert version != "0" and git_version != "0"

    def test_version(self, shell, mf_version):
        with serial.Serial(filename_pf_tty, timeout = 1) as ser:
            read_wbu(ser) #clear buffer
            write_wbu(ser, '\nversion\n')
            resp = read_wbu(ser)
            version, git_version = get_version(resp)
            assert version == mf_version
        
    def test_status(self):
        with serial.Serial(filename_pf_tty, timeout = 1) as ser:
            read_wbu(ser) #clear buffer
            write_wbu(ser, '\nstatus\n')
            resp = read_wbu(ser)
            pf_state, mf_state = get_status(resp)
            assert pf_state == "RUNNING" and mf_state == "RUNNING"

    def test_alarms(self):
        with serial.Serial(filename_pf_tty, timeout = 1) as ser:
            read_wbu(ser) #clear buffer
            write_wbu(ser, '\nalarms\n')
            resp = read_wbu(ser)
            pf_alarm = get_alarms(resp)
            assert pf_alarm == "no active alarms"

#!/bin/bash
#
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

cd /usr/local/drivers/svec
bash install_svec.sh

cd /usr/local/drivers/svec-profip
bash install-profip.sh

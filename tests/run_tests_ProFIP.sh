#!/bin/bash

# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

mf_version='V2.0.3'

# start tests
export TMP_DIR=$(mktemp -d)

source /acc/local/share/python/acc-py/base/pro/setup.sh
acc-py venv $TMP_DIR/venv
source $TMP_DIR/venv/bin/activate
pip install -r ./requirements.txt
pytest --mf-version $mf_version --junitxml=$TMP_DIR/pytest.xml
 
 rm -rf $TMP_DIR

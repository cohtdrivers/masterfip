# SPDX-FileCopyrightText: 2020 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

variables:
  GIT_SUBMODULE_STRATEGY: normal
  EDL_CI_DOC_SRC_PATH: 'doc'
  EDL_CI_DOC_DST_PATH: 'edl_ci_output/doc'
  EDL_CI_EOS_OUTPUT_DIR: 'edl_ci_output'
  
include:
  - project: 'be-cem-edl/evergreen/gitlab-ci'
    ref: master
    file:
      - 'edl-gitlab-ci.yml'

cppcheck:
  stage: analyse
  allow_failure: true
  image:
    name: gitlab-registry.cern.ch/coht/common-containers/static-analysis:latest
  script:
    - make -C software cppcheck

flawfinder:
  stage: analyse
  allow_failure: true
  image:
    name: gitlab-registry.cern.ch/coht/common-containers/static-analysis:latest
  script:
    - make -C software flawfinder

software_build:
  extends: .build_fec_os_sw
  variables:
    EDL_CI_SW_PATHS: software

firmware_build:
  extends: .build_urv_fw
  variables:
    EDL_CI_FW_PATHS: software/rt
    GEN_CORES: ${CI_PROJECT_DIR}/dependencies/general-cores/
  parallel:
    matrix:
      - TARGET_DEV: masterfip
      - TARGET_DEV: profip
        VARIANT: profip

# firmware needs to be in a specific location for our FPGA designs to be able to find it
firmware_deliver_masterfip:
  stage: build
  needs:
  - job: firmware_build
    parallel:
      matrix:
        - TARGET_DEV: masterfip
  variables:
    _FW_ARTIFACT_PATH: '$CI_PROJECT_DIR/$EDL_CI_EOS_OUTPUT_DIR/firmware'
  script:
    - export TMP_DIR=$(mktemp -d)
    - tar xvf $_FW_ARTIFACT_PATH/$CI_PROJECT_NAME-firmware${CI_COMMIT_TAG:+-$CI_COMMIT_TAG}.tar.xz -C $TMP_DIR/ --wildcards "*.bram" --strip-components 3
    - mv $TMP_DIR/masterfip-rt-cmd.bram software/rt/cmd
    - mv $TMP_DIR/masterfip-rt-ba.bram software/rt/ba
    - rm -r $TMP_DIR
  artifacts:
    paths:
      - software/rt/*/*.bram

kernel_build_validation:
  extends: .kernel_build_validation
  variables: 
    EDL_CI_KBUILD_PATHS: software/driver

build_ertec_fw:
  stage: build
  variables:
    ERTEC_FW_PATH: software/ertec
    INSTALL_PREFIX: ertec_fw_builds
    PUBLISH_DIR: ${CI_PROJECT_DIR}/${EDL_CI_EOS_OUTPUT_DIR}/ertec_fw
    DESTDIR: ${CI_PROJECT_DIR}/${INSTALL_PREFIX}
  image: ${EDL_IMAGE_ERTEC_FW_BUILD}:latest
  script:
    - make -C ${ERTEC_FW_PATH} -j8
    - make -C ${ERTEC_FW_PATH} install
    - mkdir -p ${PUBLISH_DIR}
    - tar -cJf ${PUBLISH_DIR}/${CI_PROJECT_NAME}-ertec-fw${CI_COMMIT_TAG:+-$CI_COMMIT_TAG}.tar.xz -C ${DESTDIR} .
  artifacts:
    paths:
      - "${DESTDIR}/"
      - "${PUBLISH_DIR}/*"

fpga_synthesis:
  extends: .synthesis-ise-14-7
  interruptible: true
  needs: ["firmware_deliver_masterfip"]
  parallel:
    matrix:
      - EDL_CI_SYN_SRC_PATH:
        - hdl/syn/masterfip-spec
        - hdl/syn/profip-svec

.. SPDX-License-Identifier: CC0-1.0
..
.. SPDX-FileCopyrightText: 2024 CERN

=========
Changelog
=========

3.0.3 - 2024-05-07
==================

Fixed
-----
- [ci] fixed firmware delivery job

3.0.2 - 2024-05-02
==================
Added
-----
- [ci] added bram to synthesizes job

Fixed
-----
- [sw] bug in masterfip open by lun function

3.0.1 - 2023-03-22
==================
Fixed
-----
- [hdl] clock configuration from FIP driver
- [sw] install missing header file
- [sw] minor fixes

3.0.0 - 2024-02-26
==================
Added
-----
- [ci] support for CI/CD
- [lib] functions to discover masterfip devices
- [lib] function to open by masterfip identifier (same as mockturtle identifier)
- [py] Python wrapper to help
- [tst] basic integration test suite

Removed
-------
- [lib] function to open by fmc-bus id. fmc-bus has been erased from the control system.

Changed
-------
- [tools] discovery mechanism in `diag` following new enumeration schema
- [hdl|sw] upgrade to mockturtle 4.3.0
- [hdl|sw] upgrade to spec-base 3.0.1
- [hdl] internal frequency changed to 62.5 MHz
- [hdl] soft-CPU and shared memory reduced to fit into SPEC 45T

Fixed
-----
- [tools|lib] minor bug fixes
- [tools|lib] minor style fixes

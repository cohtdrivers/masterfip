// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (C) 2019 CERN (www.cern.ch)
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mfd/core.h>
#include <linux/mod_devicetable.h>

enum mstrfip_spec_dev_offsets {
	MSTRFIP_SPEC_TRTL_MEM_START = 0x00020000,
	MSTRFIP_SPEC_TRTL_MEM_END   = 0x00040000
};

/* MFD devices */
enum spec_fpga_mfd_devs_enum {
	MSTRFIP_SPEC_MFD_TRTL = 0
};

static struct resource mstrfip_spec_trtl_res[] = {
	{
		.name = "mock-turtle-mem",
		.flags = IORESOURCE_MEM,
		.start = MSTRFIP_SPEC_TRTL_MEM_START,
		.end = MSTRFIP_SPEC_TRTL_MEM_END,
	}, {
		.name = "mock-turtle-irq_in",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 0,
	}, {
		.name = "mock-turtle-irq_out",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 1,
	}, {
		.name = "mock-turtle-irq_con",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 3,
	}, {
		.name = "mock-turtle-irq_not",
		.flags = IORESOURCE_IRQ | IORESOURCE_IRQ_HIGHLEVEL,
		.start = 2,
	},
};

static const struct mfd_cell mstrfip_spec_mfd_devs[] = {
	[MSTRFIP_SPEC_MFD_TRTL] = {
		.name = "mock-turtle",
		.platform_data = NULL,
		.pdata_size = 0,
		.num_resources = ARRAY_SIZE(mstrfip_spec_trtl_res),
		.resources = mstrfip_spec_trtl_res,
	}
};


static int mstrfip_spec_probe(struct platform_device *pdev)
{
	struct resource *rmem;
	int irq;

	rmem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!rmem) {
		dev_err(&pdev->dev, "Missing memory resource\n");
		return -EINVAL;
	}

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(&pdev->dev, "Missing IRQ number\n");
		return -EINVAL;
	}

	/*
	 * We know that this design uses the HTVIC IRQ controller.
	 * This IRQ controller has a linear mapping, so it is enough
	 * to give the first one as input
	 */

	return mfd_add_devices(&pdev->dev, PLATFORM_DEVID_AUTO,
			       mstrfip_spec_mfd_devs,
			       ARRAY_SIZE(mstrfip_spec_mfd_devs),
			       rmem, irq, NULL);
}

static int mstrfip_spec_remove(struct platform_device *pdev)
{
	mfd_remove_devices(&pdev->dev);

	return 0;
}

/**
 * List of supported platform
 */
enum mstrfip_spec_version {
	MSTRFIP_SPEC_VER = 0,
};

static const struct platform_device_id mstrfip_spec_id_table[] = {
	{
		.name = "mstrfip-urv-spec",
		.driver_data = MSTRFIP_SPEC_VER,
	}, {
		.name = "id:000010DC53504D41",
		.driver_data = MSTRFIP_SPEC_VER,
	}, {
		.name = "id:000010dc53504d41",
		.driver_data = MSTRFIP_SPEC_VER,
	},
	{},
};

static struct platform_driver mstrfip_spec_driver = {
	.driver = {
		.name = "masterfip-spec",
		.owner = THIS_MODULE,
	},
	.id_table = mstrfip_spec_id_table,
	.probe = mstrfip_spec_probe,
	.remove = mstrfip_spec_remove,
};

module_platform_driver(mstrfip_spec_driver);

MODULE_AUTHOR("Piotr Klasa <piotr.klasa@cern.ch>");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
MODULE_DESCRIPTION("Driver for the MasterFip uRV SPEC");
MODULE_DEVICE_TABLE(platform, mstrfip_spec_id_table);

MODULE_SOFTDEP("pre: spec_fmc_carrier mockturtle");

#!/bin/bash

#
# MasterFIP CI must run different macro cycle configuration configuring the FIP
# agent with different payload size.
# To this end we can imagine interleave test case with a command capable to
# reprogram the FIP agent accordingly to the test case


# command to program the FIP with a payload size of 124bytes and ...
# ssh powerfip, run systemctl service

sudo ./software/cmocka-test/mfip-hw-test -D 0xb -t 30 -c 1 && wait
retval=$?
if [ $retval -ne 0 ]; then
    echo "command failed"
    exit $retval
fi

# command to program the FIP with a payload size of 124bytes and ...
# ssh powerfip, run systemctl service

sudo ./software/cmocka-test/mfip-hw-test -D 0xb -t 30 -c 2 && wait
retval=$?
if [ $retval -ne 0 ]; then
    echo "command failed"
    exit $retval
fi

exit $retval

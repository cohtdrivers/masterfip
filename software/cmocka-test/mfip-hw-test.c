#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <cmocka.h>

#include "mfip-mcycle-config.h"
#include "mfiptest.h"

#include <libmockturtle-internal.h>
#include <masterfip/libmasterfip-priv.h>
#include <masterfip/libmasterfip.h>

enum mtrtl_mocked {
	MFIP_SPEED = 0x1,
	MFIP_BA_CYCLE,
};

static struct prog_args prg_args;

static pthread_t main_pid;

static int group_setup(void **state)
{
	struct mft_dev *dev;

	mstrfip_init();

	dev = mft_init(prg_args.devid);
	if (dev == NULL)
		return -1;
	*state = dev;
	return 0;
}

static void reset_current_ba(struct mstrfip_dev *dev)
{
	int res;

	if ((res = mstrfip_ba_stop(dev)) < 0) {
		fprintf(stderr, "Stop BA failed: %s\n",
			mstrfip_strerror(errno));
	}

	if ((res = mstrfip_ba_reset(dev)) < 0) {
		fprintf(stderr, "Reset BA failed: %s\n",
			mstrfip_strerror(errno));
	}

}

static int group_teardown(void **state)
{
	struct mft_dev *mft = (struct mft_dev *)*state;

	reset_current_ba(mft->dev);
	mft_delete_mcycle(mft);

	mstrfip_exit();
	free(*state);
	return 0;
}

#define MAX_PAYLOAD_SZ 125
#define CONS_PAYLAOD_EXTRA_BYTE 1 // for consumed payload nanaofip can add an extra byte
static uint8_t payload_buf[MAX_PAYLOAD_SZ*2];
static int payload_buf_idx = MAX_PAYLOAD_SZ;
static int current_max_payload_sz;

static void dump_payload(uint8_t *buf, char* header)
{
	fprintf(stdout, "%s: %d %d %d %d %d ..... %d %d %d %d %d\n",
		header, buf[0], buf[1], buf[2], buf[3], buf[4],
		buf[current_max_payload_sz-5], buf[current_max_payload_sz-4],
		buf[current_max_payload_sz-3], buf[current_max_payload_sz-2],
		buf[current_max_payload_sz-1]);
}

/*
 * Note that assert should not be used in caalbacks, beacuse callbcaks run in
 * mstrfip thread. In case of error a signal INT is send to the main thread
 * which uses assert to indicate that the test failed
 */
static void prod_var_cb_1(struct mstrfip_dev *dev, struct mstrfip_data *data,
			  struct mstrfip_irq *irq)
{
	int res;

	payload_buf_idx = (payload_buf_idx == current_max_payload_sz) ? 0 : payload_buf_idx + 1;
	memcpy(data->buffer, &payload_buf[payload_buf_idx], current_max_payload_sz);
	res = mstrfip_var_write(dev, data);
	if (res != 0) {
		fprintf(stdout, "Error writing payload variable: %d\n", res);
		pthread_kill(main_pid, SIGINT);
	}
}

/*
 * The consumed variable 0x6 doesn't return the current payload sent in the
 * macro cycle, but the one of the previous cycle. In addition the first
 * acquisition must be thrown because it returns what is in the agent
 * Note that assert should not be used in caalbacks, beacuse callbcaks run in
 * mstrfip thread. In case of error a signal INT is send to the main thread
 * which uses assert to indicate that the test failed
 */
static int first_acq_ignored_flag = 1; /* ignore the first payload read back */
static void cons_var_cb_1(struct mstrfip_dev *dev, struct mstrfip_data *data,
			  struct mstrfip_irq *irq)
{
	int res, idx;

	mstrfip_var_update(dev, data);

	if (first_acq_ignored_flag == 1) {
		first_acq_ignored_flag = 0;
		return;
	}
	switch (data->status) {
	case MSTRFIP_DATA_OK:
		// only the useful part of the payload is compared
		idx = (payload_buf_idx == 0) ? current_max_payload_sz : payload_buf_idx - 1;
		res = memcmp(data->buffer, &payload_buf[idx],
			     current_max_payload_sz);
		if (res) {
			fprintf(stdout, "Read back wrong payload: curr_idx:%d idx:%d\n",
				payload_buf_idx, idx);
			dump_payload(&payload_buf[idx], "Prod");
			dump_payload(data->buffer, "Cons");
			pthread_kill(main_pid, SIGINT);
		}
		break;
	case MSTRFIP_DATA_FRAME_ERROR:
		fprintf(stdout, "status error: %d frame_error: %d\n",
			data->status, data->frame_error);
		break;
	case MSTRFIP_DATA_PAYLOAD_ERROR:
		fprintf(stdout, "status error: %d (Payload Error)\n", data->status);
		break;
	default:
		fprintf(stdout, "status error: %d\n", data->status);
		break;
	}
	if (data->status != MSTRFIP_DATA_OK)
		pthread_kill(main_pid, SIGINT);
}

static void error_cb(struct mstrfip_dev *dev, enum mstrfip_error_list error)
{
}

static void prepare_for_new_mcycle(int payload_sz)
{
	int i;

	for (i=0; i<2*payload_sz; ++i)
		payload_buf[i] = i;
	current_max_payload_sz = payload_buf_idx = payload_sz;

	/* new macro cycle, skip first acquisition */
	first_acq_ignored_flag = 1;
}

static void assert_results(struct mstrfip_report *report,
			   struct expected_results *expected_res)
{
	int min_count, max_count;

	if (report->tx_err > expected_res->tx_err[1]) {
		fprintf(stdout, "TX errors %d\n", report->tx_err);
		assert_in_range(report->tx_err, expected_res->tx_err[0],
		expected_res->tx_err[1]);
	}
	if (report->rx_err > expected_res->rx_err[1]) {
		fprintf(stdout, "RX errors %d\n", report->tx_err);
		assert_in_range(report->rx_err, expected_res->rx_err[0],
		expected_res->rx_err[1]);
	}
	if (report->tx_mps_status_err > expected_res->tx_mps_status[1]) {
		fprintf(stdout, "TX MPS errors %d\n", report->tx_mps_status_err);
		assert_in_range(report->tx_mps_status_err,
		expected_res->tx_mps_status[0], expected_res->tx_mps_status[1]);
	}
	if (report->rx_mps_status_err > expected_res->rx_mps_status[1]) {
		fprintf(stdout, "RX MPS errors %d\n", report->rx_mps_status_err);
		assert_in_range(report->rx_mps_status_err,
		expected_res->rx_mps_status[0], expected_res->rx_mps_status[1]);
	}
	/*
	 * cycle counter: expected results provide an expected number per
	 * second. So the real expected cycle counter must be multiplied by the
	 * execution time
	 */
	min_count = expected_res->cycle_counter[0] * prg_args.exec_time;
	max_count = expected_res->cycle_counter[1] * prg_args.exec_time;
	if ((report->cycles < min_count) || (report->cycles > max_count)) {
		fprintf(stdout, "Wrong cycle counter %d\n", report->cycles);
		assert_in_range(report->cycles, min_count, max_count);
	}
	if ((report->int_sync_pulse_count < min_count) ||
	    (report->int_sync_pulse_count > max_count)) {
		fprintf(stdout, "Wrong interrupt counter %d\n",
		report->int_sync_pulse_count);
		assert_in_range(report->int_sync_pulse_count, min_count,
				max_count);
	}
	if (report->ba_state != MSTRFIP_FSM_INITIAL) {
		fprintf(stdout, "Wrong BA FSM state %d\n", report->ba_state);
		assert_int_equal(report->ba_state, MSTRFIP_FSM_INITIAL);
	}
}

#define TEST_CASE_NB  3
static void test_mcycle(void **state)
{
	struct mft_dev *mft = (struct mft_dev *)*state;
	int res, err, speed_idx, j, count;
	struct timespec ts;
	enum mstrfip_bitrate bitrate;
	struct mstrfip_report report;

	struct test_case {
		int speed;
		struct mcycle_cfg_case test_case[TEST_CASE_NB];
		struct expected_results expected_res[TEST_CASE_NB];
	} test_cases[] = {
	  {
		.speed = MSTRFIP_BITRATE_1000,
		.test_case =
		{
		 	/* 1 Mb/s speed: various payload size and turn around time
		    	 * The wait window duration is computed to the minum for each
		    	 * test case
		   	*/
			{ .var_addr = {0x502, 0x602}, .var_sz = {124, 124},
			  .cb = {&prod_var_cb_1, &cons_var_cb_1},
			  .wait_wind_time = {1250, 2500}, .tr_time = 30,
			  .error_cb = &error_cb,
			},
			{ .var_addr = {0x502, 0x602}, .var_sz = {64, 64},
			  .cb = {&prod_var_cb_1, &cons_var_cb_1},
			  .wait_wind_time = {675, 1368}, .tr_time = 13,
			  .error_cb = &error_cb,
			},
			{ .var_addr = {0x502, 0x602}, .var_sz = {64, 64},
			  .cb = {&prod_var_cb_1, &cons_var_cb_1},
			  .wait_wind_time = {5000, 10000}, .tr_time = 32,
			  .error_cb = &error_cb,
			},
		},
		.expected_res =
		{
			{ .tx_err = {0, 1}, .rx_err = {0, 1}, .tx_mps_status = {0, 0},
			  .rx_mps_status = {0, 1}, .cycle_counter = {1000, 10001},
			  .int_trig_count = {1000, 1001}, .ba_state = MSTRFIP_FSM_INITIAL,
			},
			{ .tx_err = {0, 1}, .rx_err = {0, 1}, .tx_mps_status = {0, 0},
			  .rx_mps_status = {0, 1}, .cycle_counter = {1000, 10001},
			  .int_trig_count = {1000, 1001}, .ba_state = MSTRFIP_FSM_INITIAL,
			},
			{ .tx_err = {0, 1}, .rx_err = {0, 1}, .tx_mps_status = {0, 0},
			  .rx_mps_status = {0, 1}, .cycle_counter = {1000, 10001},
			  .int_trig_count = {1000, 1001}, .ba_state = MSTRFIP_FSM_INITIAL,
			},
		},
	  },
	  {
		.speed = MSTRFIP_BITRATE_2500,
		.test_case =
		{
			/*
		   	 * 2.5 Mb/s speed: various payload size and turn around time
		   	 * The wait window duration is computed to the minum for each
		   	 * test case
		   	*/
			{ .var_addr = {0x502, 0x602}, .var_sz = {124, 124},
			  .cb = {&prod_var_cb_1, &cons_var_cb_1},
			  .wait_wind_time = {600, 1200}, .tr_time = 30,
			  .error_cb = &error_cb,
			},
			{ .var_addr = {0x502, 0x602}, .var_sz = {124, 124},
			  .cb = {&prod_var_cb_1, &cons_var_cb_1},
			  .wait_wind_time = {800, 1600}, .tr_time = 30,
			  .error_cb = &error_cb,
			},
			{ .var_addr = {0x502, 0x602}, .var_sz = {124, 124},
			  .cb = {&prod_var_cb_1, &cons_var_cb_1},
			  .wait_wind_time = {1500, 3000}, .tr_time = 30,
			  .error_cb = &error_cb,
			},
			/* Next use cases require to reprogram the agent with
			   the new payload
			{{0x502, 0x602}, {64, 64}, {&prod_var_cb_1, &cons_var_cb_1},
			 {500, 1000}, 20, &error_cb},
			{{0x502, 0x602}, {64, 64}, {&prod_var_cb_1, &cons_var_cb_1},
			 {1000, 2000}, 30, &error_cb},
			 */
		},
		.expected_res =
		{
			{ .tx_err = {0, 0}, .rx_err = {0, 1}, .tx_mps_status = {0, 1},
			  .rx_mps_status = {0, 0}, .cycle_counter = {830, 835},
			  .int_trig_count = {830, 835}, .ba_state = MSTRFIP_FSM_INITIAL,
			},
			{ .tx_err = {0, 1}, .rx_err = {0, 1}, .tx_mps_status = {0, 1},
			  .rx_mps_status = {0, 1}, .cycle_counter = {623, 628},
			  .int_trig_count = {623, 628}, .ba_state = MSTRFIP_FSM_INITIAL,
			},
			{
			  .tx_err = {0, 1}, .rx_err = {0, 1}, .tx_mps_status = {0, 1},
			  .rx_mps_status = {0, 1}, .cycle_counter = {331, 336},
			  .int_trig_count = {331, 336}, .ba_state = MSTRFIP_FSM_INITIAL,
			},
		},
	  },
	};
	struct mft_config_data mcycle_cfg_data;

	if ((res = mstrfip_hw_speed_get(mft->dev, &bitrate)) < 0) {
		fprintf(stderr, "Get HW Speed: %s\n",
			mstrfip_strerror(errno));
	}
	assert_int_equal(res, 0);
	switch (bitrate) {
	case MSTRFIP_BITRATE_1000:
		speed_idx = 0;
		break;
	case MSTRFIP_BITRATE_2500:
		speed_idx = 1;
		break;
	default:
		fprintf(stderr, "Unsupported FIP bus speed\n");
		assert_int_equal(-1, 0);
		break;
	}

	main_pid = pthread_self();

	fprintf(stdout, "\t\tBus Speed %s\n", mft_speed2str(test_cases[speed_idx].speed));
	count = TEST_CASE_NB;
	j = 0;
	if (prg_args.test_case_no != 0) { /* single test case or the full suite */
		j = prg_args.test_case_no - 1;
		count = prg_args.test_case_no;
	}
	for ( ; j < count; ++j) {
		init_mcycle_config(&test_cases[speed_idx].test_case[j], &mcycle_cfg_data);
		mft->cfg_data = &mcycle_cfg_data;
		fprintf(stdout, "\t\t\tTest case %d\n", j + 1);

		if ((res = mft_load_mcycle(mft, &mcycle_cfg_data)) < 0) {
			fprintf(stderr, "Loading macrocycle failed: %s\n",
				mstrfip_strerror(errno));
		}
		assert_int_equal(res, 0);

		/* Init payload buffer */
		prepare_for_new_mcycle(test_cases[speed_idx].test_case[j].var_sz[0]);

		/* Run the unit test during some time */
		/* This could be a program argument ??? */
		ts.tv_sec = prg_args.exec_time;
		ts.tv_nsec = 0;

		if ((res = mstrfip_ba_start(mft->dev)) < 0) {
			fprintf(stderr, "Start BA failed: %s\n",
				mstrfip_strerror(errno));
			mft_exit(mft);
		}
		assert_int_equal(res, 0);

		err = mft_wait(&ts); /* returns != 0 in case of signal */

		assert_int_equal(err, 0);

		if (err != 0) {/* signal occurred return error */
			fprintf(stderr, "\nA signal has been catched \n");
			return;
		}

		mstrfip_ba_stop(mft->dev);
		reset_current_ba(mft->dev);

		sleep(1);

		mstrfip_report_get(mft->dev, &report);
		assert_results(&report, &test_cases[speed_idx].expected_res[j]);

		mft_delete_mcycle(mft);
		sleep(1);
	}
}

static void prg_help(char *argv[])
{
	fprintf(stdout, "%s -D <PCI dev id> [-t <exec time> -c <test case number>]\n", argv[0]);
	fprintf(stdout, "-D device id in hex format\n");
	fprintf(stdout, "-t test case execution duration in sec\n");
	fprintf(stdout, "-c test case number in the range[1-3]\n");
	fprintf(stdout, "\n");
	exit(1);
}

int main(int argc, char *argv[])
{
	int c;
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_mcycle),
	};

	while ((c = getopt (argc, argv, "h?D:t:c:")) != -1) {
		switch (c) {
		case 'h':
		case '?':
			prg_help(argv);
			break;
		case 'D':
			// PCI device id
			if (strstr(optarg, "0x"))
				sscanf(optarg, "%x", &prg_args.devid);
			break;
		case 't':
			sscanf(optarg, "%d", &prg_args.exec_time);
			break;
		case 'c':
			sscanf(optarg, "%d", &prg_args.test_case_no);
			if ((prg_args.test_case_no > TEST_CASE_NB) ||
			    (prg_args.test_case_no < 1)) {
				prg_help(argv);
				exit(-1);
			}
			break;
		}
	}

	if (prg_args.devid == 0) {
		fprintf(stdout, "Missing arg -D\n");
		prg_help(argv);
		exit(-1);
	}

	if (prg_args.exec_time == 0)
		prg_args.exec_time = EXEC_TIME_DEFAULT;


	return cmocka_run_group_tests(tests, group_setup, group_teardown);
}

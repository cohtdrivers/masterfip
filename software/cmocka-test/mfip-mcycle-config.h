#ifndef __MFIP_MCYCLE_CONFIG
#define __MFIP_MCYCLE_CONFIG

#include "mfiptest.h"

#define EXEC_TIME_DEFAULT 10
struct prog_args {
	int devid;   /* mockturtle PCI device id */
	int exec_time; /* test case execution time in sec */
	int test_case_no; /* test case number */
};

struct expected_results {
	int tx_err[2]; /* range min max accepted errors */
	int rx_err[2]; /* range min max accepted errors */
	int tx_mps_status[2]; /* range min max accepted errors */
	int rx_mps_status[2]; /* range min max accepted errors */
	int cycle_counter[2]; /* range min max accepted errors */
	int int_trig_count[2]; /* range min max accepted errors */
	int ba_state;
};

struct mcycle_cfg_case {
	int var_addr[2];       /* agent variable address */
	int var_sz[2];         /* agent variable payload size */
	mft_data_cb cb[2];     /* periodic variable callback */
	int wait_wind_time[2]; /* wait window end time */
	int tr_time;           /* turn around time */
	mft_error_handler_cb error_cb; /* MFIP error callback */
};

extern void init_mcycle_config(struct mcycle_cfg_case *cfg, struct mft_config_data *mcycle);

#endif // MFIP_MCYCLE_CONFIG

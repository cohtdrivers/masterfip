/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __MASTERFIP_COMMON_H
#define __MASTERFIP_COMMON_H

#include <stdint.h>

/* Frame status: error bit meaning */
enum mstrfip_frame_errors {
	MSTRFIP_FRAME_OK		= 0,
	MSTRFIP_FRAME_TMO 		= (1 << 1),
	MSTRFIP_FRAME_ERR 		= (1 << 2),
	MSTRFIP_FRAME_BAD_CTRL 		= (1 << 3),
	MSTRFIP_FRAME_BAD_PDU 		= (1 << 4),
	MSTRFIP_FRAME_BAD_BSZ 		= (1 << 5),
	MSTRFIP_FRAME_UNKNOWN 		= (1 << 6),
};

enum mstrfip_ba_fsm {
	MSTRFIP_FSM_INITIAL,
	MSTRFIP_FSM_READY,
	MSTRFIP_FSM_RUNNING
};

enum mstrfip_cycle_fsm {
	MSTRFIP_FSM_RUN_WAIT,
	MSTRFIP_FSM_RUN_APER_MSG,
	MSTRFIP_FSM_RUN_APER_VAR,
	MSTRFIP_FSM_RUN_PER_VAR,
	MSTRFIP_FSM_WAIT_TX_END,
	MSTRFIP_FSM_WAIT_RX_PREAMB,
	MSTRFIP_FSM_WAIT_RX_END,
	MSTRFIP_FSM_WAIT_EXT_TRG,
	MSTRFIP_FSM_WAIT_RX_CTRL_BYTE,
	MSTRFIP_FSM_WAIT_MACROCYC_TICKS_COUNTER,
};

/**
 * @enum mstrfip_bitrate
 * All supported FIP bus speed.
 */
enum mstrfip_bitrate {
	MSTRFIP_BITRATE_31 = 0,    /**< 31.25 kb/s */
	MSTRFIP_BITRATE_1000,      /**< 1 Mb/s */
	MSTRFIP_BITRATE_2500,      /**< 2.5 Mb/s */
	MSTRFIP_BITRATE_UNDEFINED, /**< undefined speed */
};

/*
 * FIP data attributes.
 * Note: future flags can be added up to 7 (flag value '1 << 7' is reserved)
 */
enum mstrfip_data_flags {
	MSTRFIP_DATA_FLAGS_PROD =    (1 << 0), /**< produced data*/
	MSTRFIP_DATA_FLAGS_CONS =    (1 << 1), /**< consumed data*/
	MSTRFIP_DATA_FLAGS_ACK =     (1 << 2), /**< ACK transaction (only for msg data) */
	/* new flags cannot exceed the value 1 << 6 */
	MSTRFIP_DATA_FLAGS_MAX_VAL = (1 << 6), /**< max value for fip data flag */
};

struct mstrfip_hw_cfg {
	/**
	 * An external trigger is used to drive the macro cyle.
	 */
	int enable_ext_trig;
	/**
	 * A 50ohms termination is added to the external trigger.
	 */
	int enable_ext_trig_term;
	/**
	 * An internal counter drives the macro cyle.
	 * In conjonction with an external trigger enabled, it allows the
	 * internal macro cycle counter to supply to any external trigger
	 * absence. In this case the system enters in a free run mode, and will
	 * drift regarding the external timing till the external trig is back.
	 * If it is not enabled, in case of absence of external trigger, the macro
	 * cycle execution stops till the external trig is back.
	 * Of course it should be enabled if an external pulse is not used.
	 */
	 int enable_int_trig;
	/**
	 * One can define an appropriate turn around time in us for the master.
	 * The protocol specifies that this value should be inside a range
	 * according to the speed of the bus
	 * 	- 31.25KB/s	tr_min:424us 	tr_max:440us
	 * 	- 1MB/s		tr_min:10us	tr_max:33us
	 * 	- 2.5MB/s	tr_min:13us	tr_max:40us
	 * By default, the masterfip uses the minimum value defined for each
	 * speed as turn around time. Set to 0, means keep default value.
	 */
	uint32_t turn_around_ustime;
};

/**
 * header of an acquisition trtl message send by the mock turtle.
 * A single message can brings several FIP data and optionnaly an irq entry in
 * case the application should be wake-up
 */
struct mstrfip_acq_trtlmsg_desc {
	uint32_t hmq_slot; /**< the hmq_slot which defined the data type packed into
			the trtl msg: APP_PER_VAR, APP_APER_VAR, APP_APER_MSG.. */
	uint32_t nentries; /**< number of entries packed into the trtl msg */
	uint32_t irq_woffset; /**< offset of the irq entry if any */
};
#define MSTRFIP_ACQ_TRTLMSG_DESC_WSZ sizeof(struct mstrfip_acq_trtlmsg_desc) / sizeof(uint32_t)

#define MSTRFIP_DATA_ENTRY_TYPE 0
#define MSTRFIP_IRQ_ENTRY_TYPE 1
/**
 * Each entry in an acq_trtlmsg is encoding according to the TLV rule. This is
 * the header of each entry describing the type and the length of the entry
 */
struct mstrfip_tlv_entry_desc {
	uint32_t type; /**< entry type: acq or irq */
	uint32_t data_wsz; /**< data size in word */
};

/*
 *
 */
struct mstrfip_data_entry_desc {
	uint32_t type;    	/**< entry type: data or irq */
	uint32_t entry_wsz; 	/**< entry data size in words */
	uint32_t key;     	/**< used to lookup mfip data */
	uint32_t payload_bsz;/**< payload size in bytes */
	uint32_t error;   	/**< error status */
};
#define MSTRFIP_DATA_ENTRY_DESC_WSZ sizeof(struct mstrfip_data_entry_desc) / sizeof(uint32_t)

/*
 *
 */
struct mstrfip_irq_entry_desc {
	uint32_t type;      /**< entry type: data or irq */
	uint32_t wsz; 	    /**< data size in word */
	uint32_t key;       /**< key of the fip data having fired the irq */
	uint32_t flags;     /**< fip data flags (mainly direction cons or prod) */
	uint32_t hw_sec;    /**< Mock turtle time counter: second */
	uint32_t hw_ticks;  /**< Mock turtle time counter: ticks */
	uint32_t irq_count; /**< number of irq since the startup */
	uint32_t trtlmsg_count; /**< number of trtlmsg accumulated since the last irq */
};
#define MSTRFIP_IRQ_ENTRY_DESC_WSZ sizeof(struct mstrfip_irq_entry_desc) / sizeof(uint32_t)

struct mstrfip_data_hdr_trtlmsg {
	uint32_t id; /**< message id */
	uint32_t seq; /**< sequence number */
	uint32_t ndata; /**< number of varś header packed into single msg */
};
#define MSTRFIP_DATA_HDR_TRTLMSG_WSZ sizeof(struct mstrfip_data_hdr_trtlmsg) / sizeof(uint32_t)

struct mstrfip_msg_payload {
	uint32_t payload_wsz;
	uint32_t dest;
	uint32_t src;
};
#define MSTRFIP_MSG_PAYLOAD_WSZ sizeof(struct mstrfip_msg_payload) / sizeof(uint32_t)

struct mstrfip_report {
	uint32_t tx_ok;
	uint32_t tx_err;
	uint32_t fd_tx_err;
	uint32_t fd_tx_err_hwtime;
	uint32_t fd_tx_err_cycle;
	uint32_t fd_cd;
	uint32_t fd_tx_watchdog;
	uint32_t fd_tx_watchdog_hwtime;
	uint32_t fd_tx_watchdog_cycle;
	uint32_t rx_ok;
	uint32_t rx_err;
	uint32_t rx_tmo;
	uint32_t rx_mps_status_err;
	uint32_t tx_mps_status_err;
	uint32_t cycles;
	uint32_t ext_sync_pulse_count;
	uint32_t ext_sync_pulse_missed_count;
	uint32_t int_sync_pulse_count;
	uint32_t ba_state; /* enum mstrfip_ba_fsm */
	uint32_t cycle_state; /* enum mstrfip_cycle_fsm */
//	enum mstrfip_ba_fsm ba_state;
//	enum mstrfip_cycle_fsm cycle_state;
	uint32_t temp;
};

/**
 * MFIP version definition
 * Returns the git version (git commit SHA1) of RT application and library
 * and versions in terms of Major.Minor.Patch of gateware, RT app and library.
 * It returns also the gateware id.
 */
struct mstrfip_version {
	uint32_t lib_version; /* lib version */
	uint32_t rt_version; /* RT software version */
	uint32_t fpga_version; /* FPGA version */
	uint32_t fpga_id; /* FPGA identifier expected */
	uint32_t rt_id; /* rt software identifier expected */
	uint32_t lib_git_version; /* git commit SHA1 of the compilation time */
	uint32_t rt_git_version; /* git commit SHA1 of the compilation time */
};

#define MSTRFIP_MAX_AGENT_COUNT 255
struct mstrfip_seg_cfg {
	int present_nb;
	uint32_t resp_nstime[MSTRFIP_MAX_AGENT_COUNT + 1]; /* idx 0 is the master */
};

#ifdef DUMP_TRTLMSG
static inline void dump_trtlmsg(char* topic, struct trtl_msg *msg)
{
	int i;
	fprintf(stdout, "%s\n", topic);
	for (i = 0; i < msg->hdr.len; ++i)
		fprintf(stdout, "0x%x ", (int)msg->data[i]);
	fprintf(stdout, "\n\n");
}
#endif

#endif // __MASTERFIP_COMMON_H

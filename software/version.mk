#MasterFIP version
MFIP_GIT_VERSION=0x$(shell git rev-parse --short=8 HEAD 2>/dev/null)
MFIP_VERSION:=$(shell git describe --tags --abbrev=0 2>/dev/null | tr -d 'v')

ifndef MFIP_VERSION
	MFIP_VERSION:=0.0.0
endif

ifeq ($(MFIP_GIT_VERSION), 0x)
	MFIP_GIT_VERSION=0x00000000
endif

MAJOR_MFIP_VERSION = $(shell echo $(MFIP_VERSION) | cut -d'.' -f1)
MINOR_MFIP_VERSION = $(shell echo $(MFIP_VERSION) | cut -d'.' -f2)
PATH_MFIP_VERSION = $(shell echo $(MFIP_VERSION) | cut -d'.' -f3)

HEX_MFIP_VERSION = $(shell printf '0x%.2x%.2x%.2x' $(MAJOR_MFIP_VERSION) $(MINOR_MFIP_VERSION) $(PATH_MFIP_VERSION))

$(info $$MFIP_GIT_VERSION is [${MFIP_GIT_VERSION}])

#Standart versionning schema encoded into 3 bytes: 0xMajorMinorPatch
# major segment (1 byte)indicates breakage in the API
# minor segment indicates "externally visible" changes but backward compatible
# patch  indicates bug fixes

# version of the library
MFIP_LIB_VERSION=$(HEX_MFIP_VERSION)
# version of the RT software
MFIP_RT_VERSION=$(HEX_MFIP_VERSION)
# version of the FPGA
MFIP_FPGA_VERSION=0x20100
# RT app id: ascii code for MFIP
MSTRFIP_RT_ID=0x4d464950
# FPGA id
MSTRFIP_FPGA_ID=0xC000FFEE 

#define product's name
PRODUCT_NAME=masterfip



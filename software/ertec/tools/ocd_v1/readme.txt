For flashing:

openocd -f flash.cfg

(edit the file and change the path to the CPU image)

Flashing takes quite a long time (5min), the board may not start if the debugger is connected.

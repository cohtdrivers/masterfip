<!--
SPDX-FileCopyrightText: 2024 CERN (home.cern)

SPDX-License-Identifier: LGPL-2.1-or-later
-->

Reprogramming ProFIP with TCP tools

1 # Initialize TCP Reprogramming
    Opt A: using profip serial console 
    Opt B: using TIA portal
        - Add input tag "tcp reprogramming" at pos 20 in CAS module (1byte)
        - Go online
        - Add created tag to "Watch table"
        - change tag value from 0 to 1

2 # check ProFip IP
    Opt A: using profip serial console
        Enter 'i' command
    Opt B: using TIA portal 
        - Go online
        - Open ProFip Device
        - Go To properties
        - Open PROFINET interface [X1] -> Ethernet address

3 # Open console (or WSL)
    - cd tools/tcpfwloader_linux
    - ./tcpfwloader.sh 192.168.20.1

4 # Do not switch power off during reprogramming!!!

#!/bin/bash

# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

address=$1
rm -rf EB200P_Size_EcosNative.bin header.txt header.bin merged.bin
cp ../../sw/appl/EB200P_Size_EcosNative/EB200P_Size_EcosNative.bin .
image_size=$(cksum EB200P_Size_EcosNative.bin)
set -- $image_size
printf '00000000 %.2x%.2x%.2x%.2x %.2x%.2x%.2x%.2x' $(($2 & 0xFF)) $((($2 & 0xFF00) >> 8)) $((($2 & 0xFF0000) >> 16)) $((($2 & 0xFF000000) >> 24)) $(($1 & 0xFF)) $((($1 & 0xFF00) >> 8)) $((($1 & 0xFF0000) >> 16)) $((($1 & 0xFF000000) >> 24)) > header.txt
xxd -r -p header.txt header.bin
cat header.bin EB200P_Size_EcosNative.bin > merged.bin

if [ $? -ne 0 ]; then
    rm -rf EB200P_Size_EcosNative.bin header.txt header.bin merged.bin
    exit 1
fi

echo "programming..."
nc -w 90 $address 999 < merged.bin

if [ $? -eq 0 ]; then
    echo "succeeded"
else
    echo "failed"
fi

rm -rf EB200P_Size_EcosNative.bin header.txt header.bin merged.bin


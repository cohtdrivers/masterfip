param(
    [Parameter(Mandatory=$true)] [string]   $outputFolder,
    [Parameter(Mandatory=$true)] [string]   $ecos,
	[Parameter(Mandatory=$false)] [string]   $proxy_ip_addr,
    [Parameter(Mandatory=$false)] [string]   $proxy_port,
    [bool]     $download_Eclipse = $true,
    [bool]     $add_OpenJDK_PATH = $true,
    [bool]     $download_OpenJDK = $true,
    [bool]     $download_ARM_GCC = $true,
    [bool]     $download_MinGW   = $true,
    [bool]     $copy_ECOS        = $true,
    [bool]     $removeArchives   = $true,
    [bool]     $cleanUp_Temp     = $true
)

$tempFolder = "$outputFolder/temp"

if ( -not ([string]::IsNullOrWhitespace(${proxy_ip_addr})) -and -not([string]::IsNullOrWhitespace(${proxy_port})) )
{
#	Write-Host("proxy_ip_addr and proxy_port are NOT empty")
#	Write-Host("proxy_ip_addr: $proxy_ip_addr ")
#	Write-Host("proxy_port: $proxy_port ")
	$curl_proxy_param = " -x  ${proxy_ip_addr}:${proxy_port} "
}
else
{
#	Write-Host("one of them or both are empty")
#	Write-Host("proxy_ip_addr: $proxy_ip_addr ")
#	Write-Host("proxy_port: $proxy_port ")
	$curl_proxy_param = " "
}

# ---------------------------------------------------------------------------------------------------
# Unzip Archive
# ---------------------------------------------------------------------------------------------------
function Unzip([string] $sourceFile, [string] $destinationFolder, [string] $projectName)
{
    Write-Host("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    Write-Host(" Unzipping $projectName")
    Write-Host("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    # Check if unzipped folder is already available
    if(Test-Path "$destinationFolder/$projectName" -PathType Container)
    {
        Remove-Item "$destinationFolder/$projectName" -Force -Recurse
    }

    # Unzip target
    Expand-Archive -Path "$sourceFile" -DestinationPath "$destinationFolder/$projectName" -ErrorAction Stop -Force

    if($removeArchives -eq $true)
    {
        Remove-Item "$sourceFile" -Force
    }
}

# ---------------------------------------------------------------------------------------------------
# Download Tool via Curl
# ---------------------------------------------------------------------------------------------------
function DownloadTool([string] $projectName, [string] $fileName, [string] $url)
{
    $curl       = "$tempFolder/curl-8.4.0_7-win64-mingw/bin/curl.exe"

    # Check if Curl is available
    # if not then download it
    if(-not (Test-Path $curl -PathType Leaf))
    {
        if(-not (Test-Path $tempFolder -PathType Container))
        {
            New-Item $tempFolder -ItemType Container -ErrorAction Stop
        }
        Write-Host("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        Write-Host(" Downloading Curl")
        Write-Host("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        Start-BitsTransfer -Source "https://curl.se/windows/dl-8.4.0_7/curl-8.4.0_7-win64-mingw.zip" -Destination "$tempFolder/curl.zip" -ErrorAction Stop
        Expand-Archive -Path "$tempFolder/curl.zip" -DestinationPath $tempFolder -Force -ErrorAction Stop
        Remove-Item    -Path "$tempFolder/curl.zip" -Force -ErrorAction Stop
    }

    # Check if File is already available -> delete
    if(Test-Path "$tempFolder/$fileName" -PathType Leaf)
    {
        Remove-Item "$tempFolder/$fileName" -Force -ErrorAction Stop
    }

    Write-Host("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    Write-Host(" Downloading $projectName")
    Write-Host("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    # Download target
    Start-Process -FilePath $curl -ArgumentList " $curl_proxy_param -L $url --output $tempFolder/$fileName" -NoNewWindow -Wait 
}

# ---------------------------------------------------------------------------------------------------
# Add Directory to PATH Variable in Machine Enviroment
# ---------------------------------------------------------------------------------------------------
function Add-PATH([string] $Dir)
{
    Write-Host("-----------------------------------------------------------------")
    Write-Host(" Adding $Dir to PATH")
    Write-Host("-----------------------------------------------------------------")

    if(-not (Test-Path $Dir -PathType Container) )
    {
        Write-warning "Supplied directory was not found!"
    }
    else
    {
        $PATH = [Environment]::GetEnvironmentVariable("PATH", [System.EnvironmentVariableTarget]::Machine)

        if( $PATH -notlike "*"+$Dir+"*" ){
            [Environment]::SetEnvironmentVariable("PATH", "$PATH;$Dir", [System.EnvironmentVariableTarget]::Machine)
        }else{
	        Write-warning("$Dir already in PATH Variable")
        }
    }
}

if($download_Eclipse -eq $true){
    # ---------------------------------------------------------------------------------------------------
    # Download Eclipse
    # ---------------------------------------------------------------------------------------------------
    [string] $projectName    = "Eclipse"
    [string] $fileName       = "eclipse-cpp-2020-03-R-incubation-win32-x86_64.zip"
    [string] $url            = "https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-03/R/eclipse-cpp-2020-03-R-incubation-win32-x86_64.zip"
    DownloadTool -projectName $projectName -fileName $fileName -url $url
    Unzip -sourceFile "$tempFolder/$fileName" -destinationFolder $outputFolder -projectName $projectName
    # ---------------------------------------------------------------------------------------------------
}

if($download_OpenJDK -eq $true){
    # ---------------------------------------------------------------------------------------------------
    # Download OpenJDK
    # ---------------------------------------------------------------------------------------------------
    $projectName    = "openJDK14_openj9"
    $fileName       = "OpenJDK14U-jdk_x64_windows_openj9_14.0.1_7_openj9-0.20.0.zip"
    $url            = 'https://github.com/AdoptOpenJDK/openjdk14-binaries/releases/download/jdk-14.0.1%2B7.1_openj9-0.20.0/OpenJDK14U-jdk_x64_windows_openj9_14.0.1_7_openj9-0.20.0.zip'
    DownloadTool -enabled $download_OpenJDK -projectName $projectName -fileName $fileName -url $url
    Unzip -sourceFile "$tempFolder/$fileName" -destinationFolder $outputFolder -projectName $projectName

    # Adding OpenJDK to PATH
    if($add_OpenJDK_PATH -eq $true)
    {
        Add-PATH -Dir "$outputFolder/$projectName/jdk-14.0.1+7/bin"
    }
    # ---------------------------------------------------------------------------------------------------
}


if($download_ARM_GCC -eq $true){
    # ---------------------------------------------------------------------------------------------------
    # Download ARM_GCC
    # ---------------------------------------------------------------------------------------------------
    $projectName    = "arm_gcc"
    $fileName       = "gcc-arm-none-eabi-8-2019-q3-update-win32.zip"
    $url            = 'https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2019q3/RC1.1/gcc-arm-none-eabi-8-2019-q3-update-win32.zip?revision=2f0fd855-d015-423c-9c76-c953ae7e730b?product=GNU%20Arm%20Embedded%20Toolchain,ZIP,,Windows,8-2019-q3-update'
    DownloadTool $download_ARM_GCC -projectName $projectName -fileName $fileName -url $url
    Unzip -sourceFile "$tempFolder/$fileName" -destinationFolder $outputFolder -projectName $projectName
    # ---------------------------------------------------------------------------------------------------
}


if($download_MinGW -eq $true){
    # ---------------------------------------------------------------------------------------------------
    # Download MinGW
    # ---------------------------------------------------------------------------------------------------
    $projectName    = "MinGW"
    $fileName       = "mingw-get-0.6.3-mingw32-pre-20170905-1-bin.zip"
    $url            = 'https://osdn.net/frs/redir.php?m=xtom_us&f=mingw%2F68260%2Fmingw-get-0.6.3-mingw32-pre-20170905-1-bin.zip'
    DownloadTool -projectName $projectName -fileName $fileName -url $url
    Unzip -sourceFile "$tempFolder/$fileName" -destinationFolder $outputFolder -projectName $projectName

    Write-Host("-----------------------------------------------------------------")
    Write-Host(" Install MinGW packages")
    Write-Host("-----------------------------------------------------------------")
    Start-Process -Wait -FilePath "$outputFolder/$projectName/bin/mingw-get.exe" -ArgumentList "install mingw32-base-bin mingw32-gcc-g++-bin msys-base-bin mingw32-tcl-bin" -ErrorAction Stop

    # Creating CygPath File
    Write-Host("-----------------------------------------------------------------")
    Write-Host(" Adding CygPath File")
    Write-Host("-----------------------------------------------------------------")
    [string] $filepath = "$outputFolder/$projectName/msys/1.0/bin/cygpath"
    "#! /bin/sh" | Out-File $filepath -Encoding ascii
    "echo `$2" | Out-File $filepath -Append -NoNewline -Encoding ascii
    # ---------------------------------------------------------------------------------------------------
}


if($download_ARM_GCC -eq $true -and $download_MinGW -eq $true)
{
    # ---------------------------------------------------------------------------------------------------
    # Copy ARM_GCC into MinGW
    # ---------------------------------------------------------------------------------------------------
    $minGW_Path = "$outputFolder/MinGW"
    $arm_gcc_Path = "$outputFolder/arm_gcc"

    if(Test-Path "$minGW_Path/msys/1.0/" -PathType Container)
    {
        if(Test-Path "$minGW_Path/msys/1.0/local" -PathType Container)
        {
            Remove-Item -Path "$outputFolder/$projectName/msys/1.0/local" -Recurse -Force
        }
        Write-Host("-----------------------------------------------------------------")
        Write-Host(" Copy ARM_GCC into msys")
        Write-Host("-----------------------------------------------------------------")
        Move-Item -Path "$arm_gcc_Path" -Destination "$minGW_Path/msys/1.0/local" -ErrorAction Stop
    }
    # ---------------------------------------------------------------------------------------------------
}


if($copy_ECOS -eq $true -and $download_MinGW -eq $true){
    # ---------------------------------------------------------------------------------------------------
    # Copy ECOS into MinGW
    # ---------------------------------------------------------------------------------------------------
    $minGW_Path = "$outputFolder/MinGW"
    if(Test-Path "$minGW_Path/msys/1.0/")
    {
        if(Test-Path "$minGW_Path/msys/1.0/ecos")
        {
            Remove-Item -Path "$minGW_Path/msys/1.0/ecos" -Recurse -Force
        }
        Write-Host("-----------------------------------------------------------------")
        Write-Host(" Copy ECOS into msys")
        Write-Host("-----------------------------------------------------------------")
        Copy-Item -Path "$ecos" -Destination "$minGW_Path/msys/1.0/ecos" -Recurse -ErrorAction Stop
    }
    # ---------------------------------------------------------------------------------------------------
}


if($cleanUp_Temp -eq $true){
    # ---------------------------------------------------------------------------------------------------
    # Cleanup Temp
    # ---------------------------------------------------------------------------------------------------
    Write-Host("-----------------------------------------------------------------")
    Write-Host(" CleanUp Temp")
    Write-Host("-----------------------------------------------------------------")
    if(Test-Path "$outputFolder/temp" -PathType Container)
    {
        Remove-Item -Path "$outputFolder/temp" -Recurse -Force
    }
    # ---------------------------------------------------------------------------------------------------
}

Write-Host("-----------------------------------------------------------------")
Write-Host(" Completed")
Write-Host("-----------------------------------------------------------------")


    

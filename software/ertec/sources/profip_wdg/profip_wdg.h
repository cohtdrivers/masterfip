// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 * @file profip_wdg.h
 * @brief Watchdog manager
 * 
 * Support for software watchdogs for multiple threads.
 * If at least one thread is not sending life signs, then 
 * the watchdog manager stops triggering the hardware watchdog and the processor is reset.
 * 
 * The watchdog thread runs at the highest possible priority.
 * 
 * @image html diag3.svg
 */


#ifndef PROFIP_WDG_H
#define PROFIP_WDG_H

#include <stdint.h>

enum profip_wdg_module_t
{
	PROFIP_WDG_MODULE_PROFIP = 0,
	PROFIP_WDG_MODULE_MASTERFIP,
	PROFIP_WDG_MODULE_MASTERFIP_DIAG
};

/**
 * @brief Initialize watchdog
*/
uint32_t profip_wdg_init(void);

/**
 * @brief starts supervision for a given module
 * @param module
 * @param timeout_ms the maximum time after which the watchdog should be triggered
*/
void profip_wdg_start(enum profip_wdg_module_t module, uint32_t timeout_ms);

/**
 * @brief trigger watchdog for a given module
 * @param module
*/
void profip_wdg_trigger(enum profip_wdg_module_t module);

#endif

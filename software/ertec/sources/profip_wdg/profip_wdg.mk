# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

SRC_C += $(SRC_ROOT_DIR_APPL)/profip_wdg/profip_wdg.c

INCD += -I$(SRC_ROOT_DIR_APPL)/profip_wdg/
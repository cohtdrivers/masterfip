// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file nf_node.h
 * @author Piotr Klasa (piotr.klasa@cerh.ch)
 * @brief This is a module responsible for storing all information about
 *        nanoFIP nodes that are configured in the Macrocycle. It allows
 *        writing to modules (values are sent to the bus in the next
 *        macrocycle) and reading current node values.
 */

#ifndef NF_NODE_H
#define NF_NODE_H

#include <stdint.h>
#include "ertec-libmasterfip.h"
#include "nf_node_status.h"

#define NANOFIP_IDENT_VAR_IDX	0x10 ///< Var ID for identification
#define NANOFIP_CONS_VAR_IDX	0x06 ///< Var ID for consumed variable (VAR1)
#define NANOFIP_PROD_VAR_IDX	0x05 ///< Var ID for produced variable (VAR3)
#define NANOFIP_RESET_VAR_IDX	0xe0 ///< Var ID for reset variable

/* ADDR =  VARID | AGT_ID */
#define PROD_VAR_ADDR(AGT_IDX)  (((0xFF00 & (NANOFIP_PROD_VAR_IDX << 8)) | (0x00FF & (AGT_IDX))))  ///< Macro for getting address for produced variable
#define CONS_VAR_ADDR(AGT_IDX)  (((0xFF00 & (NANOFIP_CONS_VAR_IDX << 8)) | (0x00FF & (AGT_IDX))))  ///< Macro for getting address for consumed variable
#define RESET_VAR_ADDR(AGT_IDX) (((0xFF00 & (NANOFIP_RESET_VAR_IDX << 8))| (0x00FF & (AGT_IDX))))  ///< Macro for getting address for reset variable
#define IDENT_VAR_ADDR(AGT_IDX) (((0xFF00 & (NANOFIP_IDENT_VAR_IDX << 8))| (0x00FF & (AGT_IDX))))  ///< Macro for getting address for identification variable

#define VAR_IDX(ADDR) ((ADDR >> 8) & 0x00FF)	///< Macro for extracting variable id from address
#define AGT_IDX(ADDR) ((ADDR & 0x00FF)			///< Macro for extracting agent id from address

/* internal error codes */
#define IE_NF_NODE_OK                       0 ///< OK
#define IE_NF_NODE_ERR_CONS_WRONG_SIZE     10
#define IE_NF_NODE_ERR_CONS_VAR_OTHER      11
#define IE_NF_NODE_ERR_PROD_WRONG_SIZE     12
#define IE_NF_NODE_ERR_NO_PROD_VAR         16
#define IE_NF_NODE_ERR_CANT_CREATE_VAR     18
#define IE_NF_NODE_ERR_DUPLICATED_AGT      19

/**
 * @brief Initizlize module
*/
void nf_node_init(void);

/**
 * @brief Creates a produced variable. Reserves memory space for
 *        produced variable fora given node and connects given
 *        slot to the agent_id.
 * @return 0 on succes, otherwise internal error code
*/
int nf_node_add_prod_var(uint16_t slot, uint8_t agt_id, uint32_t bsz, struct mstrfip_macrocycle * mcycle);

/**
 * @brief Creates a consumed variable. Reserves memory space for
 *        consumed variable for a given node and connects given
 *        slot to the agent_id.
 * @return 0 on succes, otherwise internal error code
*/
int nf_node_add_cons_var(uint16_t slot, uint8_t agt_id, uint32_t bsz, struct mstrfip_macrocycle * mcycle);

/**
 * @brief Creates an identity variable.
 * @return internal error code
*/
int nf_node_add_ident_var(uint8_t agt_id, struct mstrfip_macrocycle * mcycle);

/**
 * @brief Creates a reset variable.
 * @return internal error code
*/
int nf_node_add_reset_var(uint8_t agt_id, struct mstrfip_macrocycle * mcycle);

/**
 * @brief Reads current values from MasterFIP Lib for each consumed variable
 *        configured in the macrocycle and saves them. This function shall
 *        be called by MasterFip Lib cons callback.
 * @return 0 on success, otherwise internal error code
*/
int nf_node_update_all_cons_var(struct mstrfip_dev *dev);

/**
 * @brief Reads current values from MasterFIP Lib for each ident variable
 * 	      configured in the macrocycle and saves them. This function shall
 *        be called by MasterFip Lib ident callback.
 * @return 0 on success, otherwise internal error code
*/
int nf_node_update_all_ident_var(struct mstrfip_dev *dev);

/**
 * @brief For each variable produced configured in the macrocycle it writes
 *        its actual values to the MasterFIP library. This function shall
 *        be called by MasterFip Lib cons or prod callback.
 * @return internal error code
*/
int nf_node_send_all_prod_var(struct mstrfip_dev *dev);

/**
 * @brief It clears all internal configuration. After calling this function 
 *        no node is configured. The configuration in MasterFIP Lib
 *        is not deleted. It must be cleaned separately!
*/
int nf_node_remove_all(void);

/**
 * @return Returns sum of all created produced and reset variables.
*/
uint32_t nf_node_nbr_of_prod_var(void);

/**
 * @return Returns number of created consumed variables.
*/
uint32_t nf_node_nbr_of_cons_var(void);

/**
 * @return Returns number of created ident variables.
*/
uint32_t nf_node_nbr_of_ident_var(void);

/**
 * @brief Fills the given prod_varlist with all configured
 *        produced and reset variables.
 * @returns Number of prod variables.
*/
uint32_t nf_node_get_prod_varlist(struct mstrfip_data** prod_varlist, uint32_t prod_varlist_size);

/**
 * @brief Fills the given cons_varlist with all configured 
 *        consumed variables.
 * @returns Number of cons variables.
*/
uint32_t nf_node_get_cons_varlist(struct mstrfip_data** cons_varlist, uint32_t cons_varlist_size);

/**
 * @brief Fills the given ident_varlist with all configured 
 *        ident variables.
 * @returns Number of ident variables.
*/
uint32_t nf_node_get_ident_varlist(struct mstrfip_data** ident_varlist, uint32_t ident_varlist_size);

/**
 * @brief Reads and writes the most current value of a variable to "data"
 *        It writes the fip variable value and status (at the end).
 * @param cons_expired - passed to the module to inform it that the variable
 *                      has not been send to macrocycle for a long time. It
 *                      would probably be better to calculate "expiration" in
 *                      this module, but "expiration" also depends on the state
 *                      of the translator, and this module does not have such
 *                      information.
 * @return 0 on success, otherwise error code
*/
enum nf_node_status nf_node_read_cons_var(uint16_t slot, uint8_t* data, uint32_t size, int cons_expired);

/**
 * @brief It writes a new variable value (passed in "data") into a given module.
 * @param prod_exired - passed to the module to inform it that the variable
 *                      has not been send to macrocycle for a long time. It
 *                      would probably be better to calculate "expiration" in
 *                      this module, but "expiration" also depends on the state
 *                      of the translator, and this module does not have such
 *                      information.
 * @return 0 on success, otherwise error code
*/
enum nf_node_status nf_node_write_prod_var(uint16_t slot, uint8_t* data, uint32_t size, int prod_exired);

/**
 * @brief Puts "agt id" into a given "reset" variable. If a node receives
 *        such a variable, it resets itself. In the next macrocycle,
 *        the reset variable is automatically set to zero so that
 *        the module does not reset again.
 * @return  0 on success, otherwise error code
*/
enum nf_node_status nf_node_write_reset_var(uint16_t slot);

/**
 * @brief Clears all internal errors counters.
*/
void nf_node_clear_err(void);

/**
 * @brief Returns the status for a given slot.
 * @returns 0 on OK, otherwise error code.
 */
enum nf_node_status nf_node_get_status_by_slot(uint16_t slot);

/**
 * @brief Returns a pointer to a string with a status description.
 */
char* nf_node_get_status_name(enum nf_node_status status);

/**
 * @brief Fills the buffer "buff" with identification variable
 *        for a given node.
 */
enum nf_node_status nf_node_get_ident_var(uint32_t slot, uint8_t* buff, uint32_t size);

/**
 * @brief Waits until all nodes receive valid payloads.
 */
int nf_node_wait_for_valid(uint32_t timeout_ms);

/**
 * @brief Prints information about all nanoFip nodes
 *        to the console.
 */
void nf_node_print(void);

/**
 * @brief Prints statuses for all nanofip nodes
 *        to the console.
 */
void nf_node_print_status();

#endif

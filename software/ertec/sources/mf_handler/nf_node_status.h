// SPDX-FileCopyrightText: 2024 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef NF_NODE_STATUS
#define NF_NODE_STATUS

#include "ertec-libmasterfip.h"

/* Extended status for MasterFIP data */
enum nf_node_status {
	NF_NODE_DATA_OK = MSTRFIP_DATA_OK, 							/**< valid FIP data */
	NF_NODE_DATA_FRAME_ERROR = MSTRFIP_DATA_FRAME_ERROR, 		/**< frame fault: CRC, TMO, BAD_NBYTES,..*/
	NF_NODE_DATA_PAYLOAD_ERROR = MSTRFIP_DATA_PAYLOAD_ERROR, 	/**< payload fault: !freshed, !significant..*/
	NF_NODE_DATA_NOT_RECEIVED = MSTRFIP_DATA_NOT_RECEIVED, 		/**< no new data received since last reading */
	NF_NODE_NOT_EXIST,
	NF_NODE_DATA_EXPIRED,
	NF_NODE_WRONG_SIZE
};

#endif

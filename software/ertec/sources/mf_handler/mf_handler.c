// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stddef.h>
#include <stdint.h>
#include <errno.h>

#include "ertec-libmasterfip.h"
#include "ertec-libmasterfip-diag.h"
#include "mf_handler.h"

#include "mfh_helper.h"
#include "profip_log.h"
#include "pf_err.h"
#include "cas.h"
#include "nf_node.h"
#include "os.h"
#include "pf_alarm.h"
#include "mstrfip_ctrl.h"

/* mf_handler errors */
#define MF_H_ERR_PROD_VAR_CBK_WR_SIZE -101
#define MF_H_AGT_NOT_FOUND -102;
#define MF_H_ERR_NO_DEVICE -103;

enum masterfip_state_t
{
	MASTERFIP_STATE_INIT,
	MASTERFIP_STATE_NOT_PROGRAMMED,
	MASTERFIP_STATE_PROGRAMMING,
	MASTERFIP_STATE_PROGRAMMED_AND_RUNNING,
	MASTERFIP_STATE_PROGRAMMED_AND_STOPPED,
	MASTERFIP_STATE_STOPPING,
	MASTERFIP_STATE_ERROR
};


static const char* masterfip_state_names[] =
{
	"INIT",
	"NOT PROGRAMMED",
	"PROGRAMMING",
	"RUNNING",
	"STOPPED",
	"STOPPING",
	"ERROR",
};

static struct mf_h_cfg_t actual_in_cfg;
static struct mf_h_stat_t mf_h_stat;

static uint64_t prod_timestamp;
static uint64_t cons_timestamp;

static enum masterfip_state_t mf_h_state = MASTERFIP_STATE_INIT;

/* masterfip macrocycle & device*/
static struct mstrfip_macrocycle *mcycle = NULL;
static struct mstrfip_dev* mstrfip = NULL;

/* --- static functions declarations --- */

/* masterfip produced variable callback */
static void my_prod_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *pvar, struct mstrfip_irq* irq);

/* masterfip consumed variable callback */
static void my_cons_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *pvar, struct mstrfip_irq* irq);

static void my_ident_var_handler(struct mstrfip_dev *dev, struct mstrfip_irq *irq);

static enum mf_h_rc_t mfh_add_prod_var(uint16_t slot, uint8_t agt_id, uint32_t bsz);

static enum mf_h_rc_t mfh_add_cons_var(uint16_t slot, uint8_t agt_id, uint32_t bsz);

static enum mf_h_rc_t mfh_add_ident_var(uint8_t agt_id);

static enum mf_h_rc_t mfh_add_reset_var(uint8_t agt_id);

/* --- static functions --- */

static void my_ident_var_handler(struct mstrfip_dev *dev,
					struct mstrfip_irq *irq)
{
	nf_node_update_all_ident_var(dev);
}

static enum mf_h_rc_t mfh_add_ident_var(uint8_t agt_id)
{
    if (!mcycle) {
    	return MF_H_RC_NO_MACROCYCLE;
    }

	if (nf_node_add_ident_var(agt_id, mcycle) != IE_NF_NODE_OK)
		return MF_H_RC_CANT_CREATE_VAR;

    return MF_H_RC_OK;
}

static enum mf_h_rc_t mfh_add_reset_var(uint8_t agt_id)
{
    if (!mcycle) {
    	return MF_H_RC_NO_MACROCYCLE;
    }

	if (nf_node_add_reset_var(agt_id, mcycle) != IE_NF_NODE_OK)
		return MF_H_RC_CANT_CREATE_VAR;

    return MF_H_RC_OK;
}

static enum mf_h_rc_t mfh_add_prod_var(uint16_t slot, uint8_t agt_id, uint32_t bsz)
{
	int ret;

    if (!mcycle) {
    	return MF_H_RC_NO_MACROCYCLE;
    }

	ret = nf_node_add_prod_var(slot, agt_id, bsz, mcycle);

	if (ret == IE_NF_NODE_ERR_DUPLICATED_AGT)
		return MF_H_RC_DUPLICATED_AGTS;

	if (ret != 0)
		return MF_H_RC_CANT_CREATE_VAR;

    return MF_H_RC_OK;
}

static enum mf_h_rc_t mfh_add_cons_var(uint16_t slot, uint8_t agt_id, uint32_t bsz)
{
	int ret;

    if (!mcycle) {
    	return MF_H_RC_NO_MACROCYCLE;
    }

	ret = nf_node_add_cons_var(slot, agt_id, bsz, mcycle);

	if (ret == IE_NF_NODE_ERR_DUPLICATED_AGT)
		return MF_H_RC_DUPLICATED_AGTS;

	if (ret != 0)
		return MF_H_RC_CANT_CREATE_VAR;
	
    return MF_H_RC_OK;
}

static void my_prod_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *pvar, struct mstrfip_irq* irq)
{
	prod_timestamp = OsGetTime_us();
	//TODO optionally: add check if all data sent before new data came?
}

static void my_cons_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *pvar, struct mstrfip_irq* irq)
{
	static uint16_t fip_cycle_cnt = 0;

	cons_timestamp = OsGetTime_us();
	
	/* CONSUMED VARIABLES */
	nf_node_update_all_cons_var(dev);

	/* PRODUCED VARIABLES */
	//trigger send all prod variables to masterFip
	nf_node_send_all_prod_var(dev);

	cas_set_fip_cycle_cnt(fip_cycle_cnt++);
}

enum mf_h_rc_t mf_handler_program_macrocycle(struct mf_h_cfg_t* cfg)
{
	int i;
	enum mstrfip_bitrate bus_speed;
	struct mstrfip_hw_cfg hw_cfg = {
		.enable_ext_trig = 0,
		.enable_ext_trig_term = 0,
		.enable_int_trig = 0,
		.turn_around_ustime = 532};
	int res;
	enum mf_h_rc_t rc = MF_H_RC_OK;

	mf_h_state = MASTERFIP_STATE_PROGRAMMING;
	cas_set_masterfip_status(mf_h_state);

	if(mstrfip == NULL)
		mstrfip = mstrfip_open();
	else
		mstrfip_reset(); //this function waits for all threads to stops

	//clear actual configuration
	memset(&actual_in_cfg, 0, sizeof(actual_in_cfg));
	nf_node_remove_all();

	res = mstrfip_rtapp_check_version(mstrfip);

	if(res) {
		if(res == -1) {
			rc =  MF_H_RC_NO_RMQ_COMMUNICATON;
			goto prog_err;
		} else {
			rc = MF_H_RC_WRONG_VERSION;
			goto prog_err;
		}
	}

	// check the FIP bus speed
	res = mstrfip_hw_speed_get(mstrfip, &bus_speed);

	// try twice to get the speed
	if(res)
		res = mstrfip_hw_speed_get(mstrfip, &bus_speed);

	if(0 != res) {
		rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_GET_BUS_SPEED, 0,\
				"Can't get bus speed\n");
		goto prog_err;
	} else {
		PROFIP_DEBUG(__DEBUG__"WorldFIP bus speed is %d\n", bus_speed);
		mf_h_stat.bus_speed = (uint8_t) bus_speed;
	}

	//create macrocycle
	mcycle = mstrfip_macrocycle_create(mstrfip);
	if (mcycle)
	{
		PROFIP_LOG(__OK__"Macrocycle created\n");
	}
	else
	{
		rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_CREATE_MACROCYCLE, 0,\
				"Macrocycle not created\n");
		goto prog_err;
	}

	/* Set turnaround time */
	if (cfg->turnaround_time_us == 0)
		hw_cfg.turn_around_ustime = mfh_helper_calculate_turnaround_time_us(bus_speed);
	else
		hw_cfg.turn_around_ustime = cfg->turnaround_time_us;

	res = mstrfip_hw_cfg_set(mstrfip, &hw_cfg);
	if(0 != res)
	{
		rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_MF_CONFIG_NOT_SET, 0,\
				"Can't set hw config\n");
		goto prog_err;
	}
	else
	{
		PROFIP_LOG(__OK__"HW config set\n");
	}

	mf_h_stat.turnaround_time_us = mstrfip_get_turnaround_ustime(mstrfip);

	for(i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		if(cfg->agts[i].is_initialized)
		{
			if(cfg->agts[i].prod_bsz > 0)
				rc = mfh_add_prod_var(cfg->agts[i].slot, cfg->agts[i].agt_idx, cfg->agts[i].prod_bsz);

			if(rc == MF_H_RC_OK && cfg->agts[i].cons_bsz > 0)
				rc = mfh_add_cons_var(cfg->agts[i].slot, cfg->agts[i].agt_idx, cfg->agts[i].cons_bsz);

			if(rc == MF_H_RC_OK && cfg->agts[i].enable_ident)
				rc = mfh_add_ident_var(cfg->agts[i].agt_idx);

			if(rc == MF_H_RC_OK && cfg->agts[i].enable_reset)
				rc = mfh_add_reset_var(cfg->agts[i].agt_idx);

			if(rc != MF_H_RC_OK)
			{
				rc = pf_err(PF_ERR_MODULE_MF_HANDLER, (int32_t) rc, 0,\
						"Can't add WF variable to macrocycle\n");
				goto prog_err;
			}
		}
	}

	mf_h_stat.prod_var_nbr = nf_node_nbr_of_prod_var();
	mf_h_stat.cons_var_nbr = nf_node_nbr_of_cons_var();

	if(mf_h_stat.prod_var_nbr == 0 && mf_h_stat.cons_var_nbr == 0 && cfg->diag_enabled == 0)
	{
		rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_NO_VARIABLE_DEFINED, 0,\
				"No variable defined\n");
		goto prog_err;
	}

	/*Configure macro cycle*/
	struct mstrfip_per_var_wind_cfg pwind_cfg = {0};

	struct mstrfip_data* prod_varlist[FIP_MAX_NBR_OF_NODES * 2]; // max number of prod vars + reset vars
	struct mstrfip_data* cons_varlist[FIP_MAX_NBR_OF_NODES];
	uint32_t prod_varlist_cnt = 0;
	uint32_t cons_varlist_cnt = 0;

	/*create prod and cons varlists based on configuration*/
	if (mf_h_stat.prod_var_nbr > 0)
	{
		
		prod_varlist_cnt = nf_node_get_prod_varlist(prod_varlist, FIP_MAX_NBR_OF_NODES * 2);

		mstrfip_var_add_data_handler(mcycle, prod_varlist[prod_varlist_cnt - 1], &my_prod_var_handler);

		pwind_cfg.varlist = prod_varlist;
		pwind_cfg.var_count = prod_varlist_cnt;
		res =  mstrfip_per_var_wind_append(mcycle, &pwind_cfg);

		if(res != 0)
		{
			rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_ADD_PERIODIC_VAR_WINDOW, 0,\
					"Cannot append per var window\n\n");
			goto prog_err;
		}
	}

	if (mf_h_stat.cons_var_nbr > 0)
	{

		cons_varlist_cnt = nf_node_get_cons_varlist(cons_varlist, FIP_MAX_NBR_OF_NODES);
		
		mstrfip_var_add_data_handler(mcycle, cons_varlist[cons_varlist_cnt - 1], &my_cons_var_handler);

		pwind_cfg.varlist = cons_varlist;
		pwind_cfg.var_count = cons_varlist_cnt;	

		res =  mstrfip_per_var_wind_append(mcycle, &pwind_cfg);

		if(res != 0)
		{
			rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_ADD_PERIODIC_VAR_WINDOW, 1,\
					"Cannot append per var window\n");
			goto prog_err;
		}
	}

	uint32_t total_time_us = mfh_helper_calculate_pervar_wind_time_us(bus_speed, mf_h_stat.turnaround_time_us,\
				prod_varlist_cnt, cons_varlist_cnt, prod_varlist, cons_varlist);

	/*Add aperiodic wind for diagnostic*/
	if(cfg->diag_enabled == 1U || nf_node_nbr_of_ident_var() > 0)
	{

		if(cfg->diag_enabled == 1U)
		{
			/* Add time for periodic diagnostic var winwis - one prod var and one cosn var with len 2*/
			total_time_us += mfh_helper_calculate_diagpervar_wind_time_us(bus_speed,  mf_h_stat.turnaround_time_us);
		}

		/* Set aper var wind time time */
		if(cfg->aper_var_wind_time_us == 0)
		{
			uint32_t aper_var_wind_time_us_tmp = mfh_helper_calculate_aper_var_wind_time_us(bus_speed);
			total_time_us += aper_var_wind_time_us_tmp;
			mf_h_stat.aper_var_wind_time_us = aper_var_wind_time_us_tmp;
		}
		else
		{
			mf_h_stat.aper_var_wind_time_us = cfg->aper_var_wind_time_us;
			total_time_us += cfg->aper_var_wind_time_us;
		}

		struct mstrfip_aper_var_wind_cfg apwind_cfg;
		apwind_cfg.enable_diag = cfg->diag_enabled;
		apwind_cfg.end_ustime = total_time_us;
		apwind_cfg.ident_var_count = nf_node_nbr_of_ident_var();
		apwind_cfg.ident_varlist = NULL;
		apwind_cfg.mstrfip_ident_var_handler = (nf_node_nbr_of_ident_var() > 0) ? &my_ident_var_handler : NULL;

		int ret = mstrfip_aper_var_wind_append(mcycle, &apwind_cfg);

		if (ret < 0)
		{
			rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_ADD_APER_VAR_WIND, 1,\
					"Can't add wind for aperiodic variables\n");
			goto prog_err;
		}
		else
		{
			PROFIP_LOG(__OK__"Appended window for aperiodic variables: %d us\n", mf_h_stat.aper_var_wind_time_us);
		}
	}
	else
	{
		mf_h_stat.aper_var_wind_time_us = 0;
	}

	/* Calculate wait wind time = periodic variable windows times + 6000 us 
	   In order not to lose frames, the total time of the macrocycle should be
	   no less than 3 x the length of the profinet cycle */
	uint32_t wait_wind_total_time_us = 0;

	if(cfg->wait_wind_time_us == 0)
	{
		total_time_us += 6000;

		if (total_time_us < 10000)
			total_time_us = 10000;

		wait_wind_total_time_us = total_time_us;
	}
	else
	{
		wait_wind_total_time_us = cfg->wait_wind_time_us;
	}

	mf_h_stat.wait_wind_time_us = wait_wind_total_time_us;

	PROFIP_DEBUG(__OK__"Appended wait wind time: %d us\n", wait_wind_total_time_us);

	res = mstrfip_wait_wind_append(mcycle, 1, wait_wind_total_time_us);

	if(res != 0)
	{
		rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_ADD_WAIT_WINDOW, 0,\
				"Cannot append wait window\n");
		goto prog_err;
	}

	/* Load mactocycle */
	res = mstrfip_ba_load(mstrfip, mcycle);

	if(res != 0)
	{
		if(errno == MSTRFIP_BA_INVALID_MACROCYCLE)
		{
			rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_WRONG_WAIT_WIND, 0,\
					"Cannot load macrocycle. Wrong wait window.\n");
			goto prog_err;
		}
		else
		{
			rc = pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_LOAD_MACROCYCLE, 0,\
					"Cannot load macrocycle\n");
			goto prog_err;
		}
	}
	else
	{
		PROFIP_LOG(__OK__"Macrocycle loaded without problems\n");
	}

	/* save actual configuration */
	memcpy(&actual_in_cfg, cfg, sizeof(actual_in_cfg));

	mf_h_state = MASTERFIP_STATE_PROGRAMMED_AND_STOPPED;
	cas_set_masterfip_status(mf_h_state);

	return rc;

prog_err: 
	mf_h_state = MASTERFIP_STATE_ERROR;
	cas_set_masterfip_status(mf_h_state);
	
	//reset masterfip
	mstrfip_reset();

	// reset nanofip nodes
	nf_node_remove_all();
	memset(&mf_h_stat, 0, sizeof(mf_h_stat));

	return rc;
}

int mf_handler_request_ident(void)
{
	struct mstrfip_data* ident_varlist[FIP_MAX_NBR_OF_NODES];
	uint32_t ident_varlist_cnt = 0;

	ident_varlist_cnt = nf_node_get_ident_varlist(ident_varlist, FIP_MAX_NBR_OF_NODES);

	if (mstrfip)
		mstrfip_ident_request(mstrfip, ident_varlist, ident_varlist_cnt);

	return 0;
}

int mf_handler_start_macrocycle(void)
{
	int res = -1;

	if (!mstrfip)
		return -1;

	/* Wait for new data and send initial values to MasterFIP */
	if(nf_node_wait_for_valid(10) != 0)
		pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_PROD_DATA_INVALID, 0,\
				"No new prod data from PLC\n");
	nf_node_send_all_prod_var(mstrfip);

	res = mstrfip_ba_start(mstrfip);

	if(res != 0)
	{
		mf_h_state = MASTERFIP_STATE_ERROR;
		cas_set_masterfip_status(mf_h_state);

		return pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_CANT_START_MACROCYCLE, 0,\
				"Cannot start macrocycle\n");
	}
	else
	{
		prod_timestamp = OsGetTime_us();
		cons_timestamp = OsGetTime_us();

		mf_h_state = MASTERFIP_STATE_PROGRAMMED_AND_RUNNING;
		cas_set_masterfip_status(mf_h_state);

		PROFIP_LOG(__OK__"Macrocycle started without problems\n");
	}

	if(nf_node_nbr_of_ident_var() > 0)
		mf_handler_request_ident();

	return res;
}

int mf_handler_stop_macrocycle(void)
{
	int ret = -1;

	enum masterfip_state_t prev_mf_h_state = mf_h_state;

	if (!mstrfip)
		return -1;

	mf_h_state = MASTERFIP_STATE_STOPPING;
	cas_set_masterfip_status(mf_h_state);

	ret = mstrfip_ba_stop(mstrfip);

	if(prev_mf_h_state == MASTERFIP_STATE_PROGRAMMED_AND_RUNNING)
		mf_h_state = MASTERFIP_STATE_PROGRAMMED_AND_STOPPED;
	else
		mf_h_state = prev_mf_h_state;

	cas_set_masterfip_status(mf_h_state);

	return ret;
}

void mf_handler_reset_masterfip(void)
{
	if (!mstrfip)
		mstrfip = mstrfip_open();
	else
		mstrfip_reset(); //this function waits for all threads to stops

	/* clear actual configuration */
	memset(&actual_in_cfg, 0, sizeof(actual_in_cfg));
	nf_node_remove_all();

	mf_h_state = MASTERFIP_STATE_NOT_PROGRAMMED;
	cas_set_masterfip_status(mf_h_state);
}

int mf_handler_cmp_cfg(struct mf_h_cfg_t* cfg)
{
	return memcmp(&actual_in_cfg, cfg, sizeof(actual_in_cfg));
}

static int get_cons_expired(void)
{
	return ((mf_h_state == MASTERFIP_STATE_PROGRAMMED_AND_RUNNING ||
			mf_h_state == MASTERFIP_STATE_ERROR) &&
			(OsGetTime_us() > (cons_timestamp + 3 * mf_h_stat.wait_wind_time_us))) ? 1 : 0;
}

static int get_prod_expired(void)
{
	return ((mf_h_state == MASTERFIP_STATE_PROGRAMMED_AND_RUNNING ||
			mf_h_state == MASTERFIP_STATE_ERROR) &&
			(OsGetTime_us() > (prod_timestamp + 3 * mf_h_stat.wait_wind_time_us))) ? 1 : 0;
}

enum nf_node_status mf_handler_write_var(uint8_t slot, uint8_t* data, uint32_t size)
{
	static int old_prod_expired = 0;
	int prod_expired = get_prod_expired();

	int status = nf_node_write_prod_var(slot, data, size, prod_expired);

	if (!old_prod_expired && prod_expired)
	{
		pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_MF_NO_DATA, 1,
				"MasterFIP did not send data on time\n");
		mf_h_state = MASTERFIP_STATE_ERROR;
	}

	if (!prod_expired && mf_h_state == MASTERFIP_STATE_ERROR)
		mf_h_state = MASTERFIP_STATE_PROGRAMMED_AND_RUNNING;

	old_prod_expired = prod_expired;

	return status;
}

enum nf_node_status mf_handler_read_var(uint8_t slot, uint8_t* data, uint32_t size)
{
	static int old_cons_expired = 0;
	int cons_expired = get_cons_expired();

	enum nf_node_status status = nf_node_read_cons_var(slot, data, size, cons_expired);

	if (!old_cons_expired && cons_expired)
	{
		pf_err(PF_ERR_MODULE_MF_HANDLER, MF_H_RC_MF_NO_DATA, 0,\
				"MasterFIP did not send data on time\n");
		mf_h_state = MASTERFIP_STATE_ERROR;
	}

	if (!cons_expired && mf_h_state == MASTERFIP_STATE_ERROR)
		mf_h_state = MASTERFIP_STATE_PROGRAMMED_AND_RUNNING;

	old_cons_expired = cons_expired;

	return status;
}

void mf_handler_write_reset_var(uint8_t slot)
{
	nf_node_write_reset_var(slot);
}

int mf_handler_get_ident_var(uint32_t slot, uint8_t* buff, uint32_t size)
{
	return (int) nf_node_get_ident_var(slot, buff, size);
}

void mf_handler_init(void)
{
	mstrfip_ctrl_cpu_disable();

	mf_h_state = MASTERFIP_STATE_INIT;
	cas_set_masterfip_status(mf_h_state);

	mstrfip_ctrl_cpu_enable();

	nf_node_init();
}

struct mf_h_stat_t* mf_handler_get_stat()
{
	return &mf_h_stat;
}

void mf_handler_clear_err(void)
{
	nf_node_clear_err();
}

void mf_handler_print_status(void)
{
	PROFIP_LOG("MasterFIP state:    %s\n", masterfip_state_names[mf_h_state]);
	nf_node_print_status();
}

void mf_handler_print_config(void)
{
	PROFIP_LOG("turnaround time:    %5d us\n", mf_h_stat.turnaround_time_us);
	PROFIP_LOG("wait wind time:     %5d us\n", mf_h_stat.wait_wind_time_us);
	PROFIP_LOG("aper var wind time: %5d us\n", mf_h_stat.aper_var_wind_time_us);
	PROFIP_LOG("bus speed:          %5d\n", mf_h_stat.bus_speed);
	PROFIP_LOG("prod var nbr:       %5d\n", mf_h_stat.prod_var_nbr);
	PROFIP_LOG("cons var nbr:       %5d\n", mf_h_stat.cons_var_nbr);
}

void mf_handler_print_nodes(void)
{
	nf_node_print();
}

void mf_handler_print_slots(void)
{
	int i;

	if (nf_node_nbr_of_cons_var() == 0 &&
			nf_node_nbr_of_prod_var() == 0)
	{
		PROFIP_LOG("no slots configured\n");
		return;
	}

	PROFIP_LOG("slot |  agt | prod | cons |\n");
	PROFIP_LOG("-----+------+------+------+-\n");

	for(i=0; i<FIP_MAX_NBR_OF_NODES; i++)
	{
		struct mf_h_cfg_agts_t *agt = &actual_in_cfg.agts[i];

		if (!agt->is_initialized)
			continue;

		PROFIP_LOG("%4d | 0x%.2x |", agt->slot, agt->agt_idx);

		if(agt->prod_bsz)
			PROFIP_LOG(" %4d |", agt->prod_bsz);
		else
			PROFIP_LOG("    - |");

		if(agt->cons_bsz)
			PROFIP_LOG(" %4d | ", agt->cons_bsz);
		else
			PROFIP_LOG("    - | ");

		if (mf_h_state == MASTERFIP_STATE_PROGRAMMED_AND_STOPPED) {
			PROFIP_LOG("stopped");
		} else {
			enum nf_node_status status = nf_node_get_status_by_slot(agt->slot);

			PROFIP_LOG("%s", nf_node_get_status_name(status));
		}

		if (agt->enable_ident)
			PROFIP_LOG(" +ident_var");

		if (agt->enable_reset)
			PROFIP_LOG(" +res_var");

		PROFIP_LOG("\n");
	}
}

void mf_handler_print_presence_list(void)
{
	mstrfip_print_presence_list();
}

void mf_handler_print_macrocycle(void)
{
	mstrfip_print();
}

void mf_handler_print_version(void)
{
	struct mstrfip_diag_shm* shm = mstrfip_diag_get();

	PROFIP_LOG("RT_ID           %.8x\n", shm->ext.vers.rt_id);
	PROFIP_LOG("FPGA_ID         %.8x\n", shm->ext.vers.fpga_id);
	PROFIP_LOG("RT_VERSION      %.8x\n", shm->ext.vers.rt_version);
	PROFIP_LOG("LIB_VERSION 	%.8x\n", shm->ext.vers.lib_version);
	PROFIP_LOG("FPGA_VERSION    %.8x\n", shm->ext.vers.fpga_version);
	PROFIP_LOG("RT_GIT_VERSION 	%.8x\n", shm->ext.vers.rt_git_version);
	PROFIP_LOG("LIB_GIT_VERSION %.8x\n", shm->ext.vers.lib_git_version);
}

// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "mfh_helper.h"
#include "profip_log.h"

/* constants related to the worldFIP parameters */
static const unsigned MSTRFIP_BUS_SPEEDS[3] = {31250, 1000000, 2500000}; /*bps*/
static const unsigned MSTRFIP_SILENCE_TIME[3] = {4312, 178, 116};        /*us*/

/*Default duration for aperiodic wait wind time based on bus speed*/
#define APER_VAR_WIND_COEFF 1000
static const unsigned MSTRFIP_APER_VAR_WIND_TIME[3] =
	{APER_VAR_WIND_COEFF * 80, APER_VAR_WIND_COEFF * 25 / 10, APER_VAR_WIND_COEFF * 1}; /*us*/

unsigned mfh_helper_calculate_pervar_wind_time_us(unsigned bus_speed, unsigned turn_around_time_us,\
		unsigned prod_varlist_cnt, unsigned cons_varlist_cnt,\
		struct mstrfip_data** prod_varlist, struct mstrfip_data** cons_varlist)
{
	unsigned wait_wind_total_time_us = 0;
	int i;

	for(i = 0; i<prod_varlist_cnt; i++)
	{
		uint32_t id_dat_time_us = (8 * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		uint32_t rp_dat_time_us = ((9 + prod_varlist[i]->bsz) * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		wait_wind_total_time_us += (id_dat_time_us + rp_dat_time_us);
	}
	for(i = 0; i<cons_varlist_cnt; i++)
	{
		uint32_t id_dat_time_us = (8 * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		uint32_t rp_dat_time_us = ((9 + cons_varlist[i]->bsz) * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		wait_wind_total_time_us += (id_dat_time_us + rp_dat_time_us + MSTRFIP_SILENCE_TIME[bus_speed]);
	}
	return wait_wind_total_time_us;
}

unsigned mfh_helper_calculate_turnaround_time_us(unsigned int bus_speed)
{
	switch(bus_speed)
	{
		case 0: /*31.25 Kbps*/
			return 532;
			break;
		case 1: /*1 Mbps*/
			return 32;
			break;
		case 2: /*2.5 Mbps*/
			return 30;
			break;
		default:
			return 532;
			break;
	}
	return 532;
}

unsigned mfh_helper_calculate_aper_var_wind_time_us(unsigned int bus_speed)
{
	return MSTRFIP_APER_VAR_WIND_TIME[bus_speed];
}

unsigned mfh_helper_calculate_diagpervar_wind_time_us(unsigned bus_speed, unsigned turn_around_time_us)
{
	unsigned wait_wind_total_time_us = 0;

	{
		uint32_t id_dat_time_us = (8 * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		uint32_t rp_dat_time_us = ((9 + 2) * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		wait_wind_total_time_us += (id_dat_time_us + rp_dat_time_us);
	}

	{
		uint32_t id_dat_time_us = (8 * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		uint32_t rp_dat_time_us = ((9 + 2) * 8 * 1000000 + (MSTRFIP_BUS_SPEEDS[bus_speed]-1)) / MSTRFIP_BUS_SPEEDS[bus_speed] + turn_around_time_us;
		wait_wind_total_time_us += (id_dat_time_us + rp_dat_time_us + MSTRFIP_SILENCE_TIME[bus_speed]);
	}

	return wait_wind_total_time_us;
}

# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

SRC_C += $(SRC_ROOT_DIR_APPL)/mqospi/mqospi.c
SRC_C += $(SRC_ROOT_DIR_APPL)/mqospi/fr_timer.c
SRC_C += $(SRC_ROOT_DIR_APPL)/mqospi/spi_dma.c

INCD += -I$(SRC_ROOT_DIR_APPL)/mqospi/
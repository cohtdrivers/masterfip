// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file mqospi.h
 * 
 * @brief Mock Turtle's (Remote) Message Queues over SPI. 
 *        
 * Module for exchanging data with Mock turtle's RMQs over SPI. This module has
 * an analogous API to the HMI API that is available in the Mockturtle library.
 * The goal is for the ertec-libmasterfip library to function as closely as
 * possible to the libmasterfip library on FEC.
 * 
 * MQOSPI Protocol
 * 
 * MQOSPI frame consist of MQOSPI header and RMQ message:
 * 	[MQOSPI header][message]
 * 
 * Header consists of 4 bytes: 
 * - direction (Write or Read)
 * - RMQ ID (associated with the RMQ ID and core number,
 *           ID = RMQ_ID + 6*CPU_ID),
 * - Payload Size
 * - reserved (allways 0). 
 * 
 * For the sent data, the module creates a frame consisting
 * of a header and a message and starts the SPI transmission.
 * For received data, the module provides a poll function - 
 * for each queue, one GPIO pin is connected, indicating
 * whether there is data to be sent from the FPGA.
 * If there is the data then, the "recv" function can be called.
 * The module sets the appropriate ID and direction in the header.
 * The size field is not set. The module sends a header and then
 * reads 4 bytes from FPGA, which contains information about
 * the payload length. It then reads the payload according 
 * to the received length.
 */

#ifndef MQOSPI_H
#define MQOSPI_H

#include <stdint.h>

/**
 * @brief Maximum RMQ frame size (in bytes). It must be consistent with the MockTurtle configuration.
 */
#define TRTL_MAX_PAYLOAD_SIZE  128U 

#if TRTL_MAX_PAYLOAD_SIZE != RMQ_WIDTH
#error Wrong TRTL_MAX_PAYLOAD_SIZE
#endif

#define MQOSPI_POLLIN 0x1 

/**
 * RMQ header descriptor. It helps the various software layers to process
 * messages. This header must be the same as the RMQ header to be compatible.
 */
struct mqospi_hmq_header {
	/* word 0 */
	uint16_t rt_app_id;     /** firmware application unique identifier.
                                Used to validate a message against the firmware         
                                that receives it */
	uint8_t flags;          /** flags */
	uint8_t msg_id;         /** It uniquely identify the message type. The
			                    first __TRTL_MSG_ID_MAX_USER are free use */
	/* word 1 */
	uint16_t len;           /** message-length in 32bit words */
	uint16_t sync_id;       /** synchronous identifier */
	/* word 2 */
	uint32_t seq;           /** sequence number (automatically set by the library) */
};

struct mqospi_msg{
    struct mqospi_hmq_header hdr;           ///< header
    uint32_t data[TRTL_MAX_PAYLOAD_SIZE];   ///< payload
};

struct pollmqospi
{
    unsigned int idx_cpu;   ///< CPU index
	unsigned int idx_hmq;   ///< HMQ index
	short events;           ///< like in pollfd poll(2)
	short revents;          ///< like in pollfd poll(2)
};

/**
 * @name mqospi_msg_sync
 * @brief Sends a message to the Mock Turtle, waits, and receives a reply.
 * @param idx_cpu CPU index
 * @param idx_hmq RMQ index
 * @param msg pointer to output and input message
 * @param timeout maximum waiting time for a response from MockTurtle
 * @returns 0 on success, otherwise error code (<0)
 */
int mqospi_msg_sync(unsigned int idx_cpu, unsigned int idx_hmq, struct mqospi_msg *msg, int timeout);	

/**
 * @name mqospi_msg_async_send
 * @brief Sends a message to the Mock Turtle.
 * @param idx_cpu CPU index
 * @param idx_hmq RMQ index
 * @param msg pointer to output message
 * @returns 0 on success, otherwise error code (<0)
 */
int mqospi_msg_async_send(unsigned int idx_cpu, unsigned int idx_hmq, struct mqospi_msg *msg, unsigned int n);

/**
 * @name mqospi_msg_async_recv
 * @brief Receives a message from MockTurtle.
 * @param idx_cpu CPU index
 * @param idx_hmq RMQ index
 * @returns 1 on success, otherwise error code (<0)
 */
int mqospi_msg_async_recv(unsigned int idx_cpu, unsigned int idx_hmq, struct mqospi_msg *msg, unsigned int n);

/**
 * @name mqospi_poll
 * @brief Checks if there are a messages in RMQs
 * @param poll
 * @param n number of entries in poll structure
 * @param timeout not used
 * @return number of entries in poll structure
 */
int mqospi_poll(struct pollmqospi* poll, unsigned int n, int timeout);

/**
 * @name mqospi_init
 * @brief Initialize module
 */
int mqospi_init(void);

/**
 * @name mqospi_purge
 * @brief Clears all messages from RMQ's
 * @return number of removed messages from rmqs
 */
int mqospi_purge(unsigned int idx_cpu, unsigned int idx_hmq);

/**
 * @name mqospi_reset
 */
void mqospi_reset(void);

#endif

// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file fr_timer.h
 * @brief API for measuring time difference with us precision
 */

#ifndef FR_TIMER_H
#define FR_TIMER_H

#include <stdint.h>

struct fr_timer_t
{
	uint32_t start_time_us;
};

/**
 * @name fr_timer_init
 * @brief Call this function before using other functions, it starts the clock
 **/
void fr_timer_init(void);

/**
 * @name fr_timer_start
 * @param timer - variable for storing measurement data
 * @brief Saves the time of the function call to the timer variable
 */
void fr_timer_start(struct fr_timer_t *timer);

/**
 * @name fr_timer_stop
 * @param timer
 * @return Returns the time difference between the value stored in the timer 
 *         and the current time, in microseconds (us)
 */
uint32_t fr_timer_stop(struct fr_timer_t *timer);


#endif

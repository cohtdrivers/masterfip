/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "profip_log.h"
#include "ertec-libmasterfip-priv.h"
#include "masterfip-common-priv.h"
#include "ertec-libmasterfip.h"
#include "mqospi.h"


typedef void (*mstrfip_refresh_t)(struct mstrfip_data *msg, uint8_t *payload,
				  int payload_bsz);

/*
 * Specific treatment for periodic var update:
 * The last byte of the payload is the so called MPS status from which payload
 * error is set in terms of freshness and significance.
 */
static void mstrfip_var_refresh(struct mstrfip_data *var, uint8_t *payload,
					int payload_bsz)
{
	uint8_t *ptr_data;

	/*
	 * FIP frame format (byte operation not supported in mockturtle)
	 * [payload format]:|pdu_type|HWnbytes|byte0|...|byten|MPS_STATUS|
	 * skip PDU + hw_bsz
	 */
	ptr_data = payload + MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
	/*
	 * client payload size: skip EXTRA_BSZ + MPS status byte
	 */
	var->bsz = payload_bsz
			- MSTRFIP_VAR_PAYLOAD_HDR_BSZ
			- MSTRFIP_VAR_MPS_STATUS_BSZ; /* MPS_STATUS */
	memcpy(var->buffer, ptr_data, var->bsz);
	/*
	 * Last byte is MPS status.
	 */
	ptr_data += var->bsz;
	/* translate MPS status into payload status */
	var->payload_error = MSTRFIP_FRAME_PAYLOAD_OK;
	/* compute data status: check Refresh and significance bit */
	if (!(*ptr_data & MSTRFIP_MPS_REFRESH_BIT))
		var->payload_error |= MSTRFIP_FRAME_PAYLOAD_NOT_REFRESH;
	if (!(*ptr_data & MSTRFIP_MPS_SIGNIFICANCE_BIT))
		var->payload_error |= MSTRFIP_FRAME_PAYLOAD_NOT_SIGNIFICANT;
	/* overwrite var's status in case of payload status error */
	if (var->payload_error != MSTRFIP_FRAME_PAYLOAD_OK)
		var->status = MSTRFIP_DATA_PAYLOAD_ERROR;

}

/*
 * Specific treatment for identification var update
 */
static void mstrfip_ident_var_refresh(struct mstrfip_data *var, uint8_t *payload,
					int payload_bsz)
{
	uint8_t *ptr_data;

	/*
	 * FIP frame format (byte operation not supported in mockturtle)
	 * [payload format]:|pdu_type|HWnbytes=0x8|byte0|...|byte7|
	 */
	ptr_data = payload + MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
	var->bsz = payload_bsz - MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
	memcpy(var->buffer, ptr_data, var->bsz);
}

static void mstrfip_msg_refresh(struct mstrfip_data *msg, uint8_t *payload,
					int payload_bsz)
{
	// Aperiodic messages are not supported
	pf_err(PF_ERR_MODULE_LIBMASTERFIP, MSTRFIP_INT_ERR_APER_MSG_REFRESH, 0, "mstrfip_msg_refresh\n");
}

static void mstrfip_refresh_none(struct mstrfip_data *msg, uint8_t *payload,
				 int payload_bsz)
{
	return;
}

/**
 * Lookup table
 */
static const mstrfip_refresh_t mstrfip_refresh[] = {
		[MSTRFIP_PER_VAR] = mstrfip_var_refresh,
		[MSTRFIP_APER_VAR] = mstrfip_refresh_none,
		[MSTRFIP_IDENT_VAR] = mstrfip_ident_var_refresh,
		[MSTRFIP_APER_MSG] = mstrfip_msg_refresh,
		[MSTRFIP_APER_MSG_ACK] = mstrfip_refresh_none,
};

/**
 * Starts running macro cycle
 * @param[in] dev device token
 * @param[in] var the desired periodic variable to update
 */
/* used to tag the data entry already consumed */
#define MSTRFIP_FRAME_CONSUMED 0x10000000
void mstrfip_data_refresh(struct mstrfip_data *data, enum mstrfip_data_type data_type,
				struct mqospi_msg *trtlmsglist, int trtlmsg_count)
{
	struct mstrfip_data_priv *pdata_priv = (struct mstrfip_data_priv *)data->priv;
	struct mstrfip_acq_trtlmsg_desc *acq_msg;
	struct mstrfip_data_entry_desc *data_entry;
	struct mstrfip_tlv_entry_desc *entry;
	int idx1, idx2;
	uint32_t *ptr_32;
	uint8_t *ptr_data;

	/*
	 * The caller expects this FIP var to be updated with the content of its
	 * pending message. First the flag updated is reset and if a
	 * corresponding message is found it will be set and the content will
	 * be updated.
	 */
	data->status = MSTRFIP_DATA_NOT_RECEIVED;
	for (idx1 = 0; idx1 < trtlmsg_count; ++idx1)
    {
		acq_msg = (struct mstrfip_acq_trtlmsg_desc *)&(trtlmsglist[idx1].data);
		ptr_32 = ((uint32_t *)acq_msg + MSTRFIP_ACQ_TRTLMSG_DESC_WSZ);
		for (idx2 = 0;
		     idx2 < acq_msg->nentries;
		     ++idx2, ptr_32 += entry->data_wsz)
        {
			entry = (struct mstrfip_tlv_entry_desc *)ptr_32;
			if (entry->type != MSTRFIP_DATA_ENTRY_TYPE)
				continue;
			data_entry = (struct mstrfip_data_entry_desc *)entry; // it's a data entry

			if (pdata_priv->hdr.key != data_entry->key)
				continue;

			/* Found */
			if (data_entry->error == MSTRFIP_FRAME_CONSUMED)
			{
				PROFIP_DEBUG("MSTRFIP_FRAME_CONSUMED");
				/*
				 * already consumed and not refeshed since
				 * the last call of var_update. Therefore
				 * return not received to the client
				 */
				return;
			}
			/*
			 * data message format:
			 * |ID|seq|hw_err|msg_tag|nbytes|[data]|
			 */
			data->frame_error = data_entry->error;
			/* Skip 16bits header (pdu_type + bytes-length)
			 * from the data msg. Message format:
			 */
			/*
			 * TODO: check buffer size against payload size
			 * to prevent from any overflow: an error
			 * should be reported like "payload size
			 * exceeded"
			 */
			if (data->frame_error == MSTRFIP_FRAME_OK) {
				data->status = MSTRFIP_DATA_OK;
				ptr_data = ((uint8_t *)data_entry) +
					MSTRFIP_DATA_ENTRY_DESC_WSZ * 4;
				mstrfip_refresh[data_type](data,
							   ptr_data,
							   data_entry->payload_bsz);
			}
			else { // invalid data in the buffer
				//printf("data->frame_error %d (%d) %d\n", data->frame_error, data_type, data->id);
				data->status = MSTRFIP_DATA_FRAME_ERROR;
				data->bsz = 0;
				memset(data->buffer, 0xFF,
				       pdata_priv->hdr.max_bsz);
			}
			/*
			 * message is consumed make it invalid by tagging it
			 * not freshed anymore. Done by writing
			 * MSTRFIP_FRAME_NOT_FRESHED the hw_error word status
			 */
			data_entry->error = MSTRFIP_FRAME_CONSUMED;
			return;
		}
	}
}

/**
 * Starts running macro cycle
 * @param[in] dev device token
 * @param[in] var the desired periodic variable to update
 */
void mstrfip_var_update(struct mstrfip_dev *dev, struct mstrfip_data *var)
{
	struct mstrfip_ba_macrocycle *mc =
			((struct mstrfip_desc *)dev)->ba.mcycle;

	return mstrfip_data_refresh(var, MSTRFIP_PER_VAR,
			mc->per_vars.trtl.msglist, mc->per_vars.trtl.msg_count);
}

/**
 * Starts running macro cycle
 * @param[in] dev device token
 * @param[in] varlist the desired list of periodic variable to update
 * @param[in] var_count number of variables in varlist
 * @return the number of periodic vars updated
 */
int mstrfip_varlist_update(struct mstrfip_dev *dev, struct mstrfip_data **varlist,
			int var_count)
{
	struct mstrfip_data *pvar;
	int i, count = 0;

	for (i = 0; i < var_count; ++i) {
		pvar = varlist[i];
		mstrfip_var_update(dev, pvar);
		if (pvar->status != MSTRFIP_DATA_NOT_RECEIVED)
			++ count;
	}
	return count;
}

int mstrfip_var_write(struct mstrfip_dev *dev, struct mstrfip_data *var)
{
	struct mstrfip_data_priv *pvar_priv =
				(struct mstrfip_data_priv *)var->priv;
	int err;
	struct mqospi_msg trtlmsg;
	struct mstrfip_var_payload_trtlmsg *payload_msg =
			(struct mstrfip_var_payload_trtlmsg *)trtlmsg.data;

	payload_msg->trtl_hdr.id = MSTRFIP_CMD_SET_VAR_PAYLOAD;
	payload_msg->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	payload_msg->key = pvar_priv->hdr.key;
	payload_msg->bsz = var->bsz;
	trtlmsg.hdr.len = MSTRFIP_VAR_PAYLOAD_HDR_TRTLMSG_WSZ;
	memcpy((uint8_t *)&trtlmsg.data[trtlmsg.hdr.len],
					var->buffer, var->bsz);
	trtlmsg.hdr.len += (var->bsz / 4) + (!!(var->bsz % 4));

#ifdef DUMP_TRTLMSG
	dump_trtlmsg("mstrfip_var_write (dir:input slot: 1)", &trtlmsg);
#endif

	/* Send the message */

	trtlmsg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	trtlmsg.hdr.flags = 0;
	trtlmsg.hdr.msg_id = MSTRFIP_CMD_SET_VAR_PAYLOAD;

    err = mqospi_msg_async_send(MSTRFIP_CPU1, MSTRFIP_HMQ_IO_CPU1_CMD, &trtlmsg, 1);

	return (err < 0) ? err : 0;
}

void mstrfip_ident_update(struct mstrfip_dev *dev, struct mstrfip_data *var)
{
	struct mstrfip_ba_macrocycle *mc =
			((struct mstrfip_desc *)dev)->ba.mcycle;

	return mstrfip_data_refresh(var, MSTRFIP_IDENT_VAR,
		mc->aper_vars.trtl.msglist, mc->aper_vars.trtl.msg_count);
}

int mstrfip_ident_request(struct mstrfip_dev *dev,
			struct mstrfip_data **identlist, int ident_count)
{
	int err, i, word_idx, bit_idx, agent_addr;
	struct mqospi_msg msg;
	struct mstrfip_ident_request_trtlmsg *ident_msg =
			(struct mstrfip_ident_request_trtlmsg *)msg.data;
	uint32_t ident_request_val[MSTRFIP_IDENT_REQUEST_WSIZE];

	ident_msg->trtl_hdr.id = MSTRFIP_CMD_REQ_IDENT_VAR;
	ident_msg->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	memset(ident_request_val, 0, sizeof(ident_request_val));
	for (i = 0; i < ident_count; ++i) {
		agent_addr = identlist[i]->id & 0xFF;
		word_idx = agent_addr / 32;
		bit_idx = agent_addr % 32;
		ident_request_val[word_idx] |= (1 << bit_idx);
	}
	memcpy(ident_msg->ident_request, ident_request_val,
						sizeof(ident_request_val));
	msg.hdr.len = sizeof(struct mstrfip_ident_request_trtlmsg) / 4;
	/* Send the message */
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_REQ_IDENT_VAR;

	err = mqospi_msg_async_send(MSTRFIP_CPU1, MSTRFIP_HMQ_IO_CPU1_CMD, &msg, 1);

	return (err < 0) ? err : 0;
}

/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "profip_log.h"
#include "mqospi.h"
#include "mstrfip_ctrl.h"
#include "ertec-libmasterfip-priv.h"
#include "masterfip-common-priv.h"
#include "ertec-libmasterfip.h"
#include "pf_err.h"
#include "cas.h"
#include "os.h"

/* time in ns to send one bit according to the 3 supported speed */
const uint32_t mstrfip_bit_nstime[3] = {30769, 1000, 400};
/* turn around time in ns, according to the 3 supported speed */
const uint32_t mstrfip_tr_min_nstime[3] = {424000, 13000, 10000};
const uint32_t mstrfip_tr_max_nstime[3] = {532000, 32000, 30000};
/* silence time(time-out) in ns, according to the 3 supported speed */
const uint32_t mstrfip_ts_nstime[3] = {4312000, 178000, 116000};

/*
 * components version generated at compile time
 */
const uint32_t lib_version = MFIP_LIB_VERSION;
const uint32_t lib_git_version = GIT_VERSION;

/**
 *
 *
 */
int mstrfip_version_get(struct mstrfip_dev *dev, struct mstrfip_version *vers)
{
	struct mqospi_msg msg;
	struct mstrfip_rt_version_trtlmsg *rt_vers;
	int err;

	/*
	 * Sequence: send a sync message to get version
	 */
	rt_vers = (struct mstrfip_rt_version_trtlmsg *)msg.data;
	/* Build the message */
	rt_vers->trtl_hdr.id = MSTRFIP_CMD_GET_RT_VERSION;
	rt_vers->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	msg.hdr.len = MSTRFIP_RT_VERSION_TRTLMSG_WSZ;
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_GET_RT_VERSION;
#ifdef DUMP_TRTLMSG
	dump_trtlmsg("request mstrfip_version_get (dir:input slot:0)", &msg);
#endif
	/* Send the message and get answer */
	err = mqospi_msg_sync(MSTRFIP_CPU1, MSTRFIP_HMQ_IO_CPU1_CMD, &msg, MSTRFIP_DEFAULT_TIMEOUT);	

	memset(vers, 0, sizeof(struct mstrfip_version));
	if (err < 0)
		return -1;
	/* fill in vers output parameter */
	vers->lib_version = lib_version;
	vers->rt_version = rt_vers->rt_version;
	vers->fpga_version = rt_vers->fpga_version;
	vers->fpga_id = rt_vers->fpga_id;
	vers->rt_id = rt_vers->rt_id;
	vers->lib_git_version = lib_git_version;
	vers->rt_git_version = rt_vers->rt_git_version;
	
	return 0;
}

static int mstrfip_check_version_consistency(struct mstrfip_desc *mstrfip)
{
	int res;
	struct mstrfip_version *vers = &mstrfip->vers;
	uint32_t msk;

	res = mstrfip_version_get((struct mstrfip_dev *)mstrfip, vers);

	if (res)
	{
		pf_err(PF_ERR_MODULE_LIBMASTERFIP, MSTRFIP_INVALID_HW_VERS, 0U, "Reading version from HW failed\n");
		mstrfip->current_error = MSTRFIP_INVALID_HW_VERS;
		res = -1;
	}
	/* check first fpga and RT app id */
	if (res == 0 &&  ((vers->rt_id != MSTRFIP_RT_ID) ||
	     (vers->fpga_id != MSTRFIP_FPGA_ID)))
	{
		pf_err(PF_ERR_MODULE_LIBMASTERFIP, MSTRFIP_INVALID_HW_VERS, 1U, "Unexpected bitstream or RT app\n");
		mstrfip->current_error = MSTRFIP_INVALID_HW_VERS;
		res = -2;
	}
	/*
	 * check RT and fpga version against expected versions
	 * patch number is ignored
	 */
	msk = 0xFFFF00; /* ignore patch number */
	if (res == 0 && (((vers->rt_version & msk) != (mstrfip->ref_rt_version & msk)) ||
	     ((vers->fpga_version & msk) != (mstrfip->ref_fpga_version &msk))))
	{
		pf_err(PF_ERR_MODULE_LIBMASTERFIP, MSTRFIP_INVALID_HW_VERS, 2U, "Unexpected HW version\n");
		mstrfip->current_error = MSTRFIP_INVALID_HW_VERS;
		res = -3;
	}

	if(res == 0)
	{
		PROFIP_LOG("\n"__OK__"MasterFip vers. | Expected | Received\n");
		PROFIP_LOG("---------------------+----------+----------\n");
		PROFIP_LOG("MSTRFIP_RT_ID        | %.8x | %.8x\n", MSTRFIP_RT_ID, vers->rt_id);
		PROFIP_LOG("MSTRFIP_FPGA_ID      | %.8x | %.8x\n", MSTRFIP_FPGA_ID, vers->fpga_id);
		PROFIP_LOG("MSTRFIP_RT_VERSION   | %.8x | %.8x\n", mstrfip->ref_rt_version, vers->rt_version);
		PROFIP_LOG("MSTRFIP_FPGA_VERSION | %.8x | %.8x\n", mstrfip->ref_fpga_version, vers->fpga_version);
		PROFIP_LOG("MSTRFIP_GIT_VERSION  |          | %.8x\n\n", vers->rt_git_version);
	}
	else
	{
		PROFIP_ERR("\n"__ERR__"MasterFip vers.| Expected | Received\n");
		PROFIP_ERR("---------------------+----------+----------\n");
		PROFIP_ERR("MSTRFIP_RT_ID        | %.8x | %.8x\n", MSTRFIP_RT_ID, vers->rt_id);
		PROFIP_ERR("MSTRFIP_FPGA_ID      | %.8x | %.8x\n", MSTRFIP_FPGA_ID, vers->fpga_id);
		PROFIP_ERR("MSTRFIP_RT_VERSION   | %.8x | %.8x\n", mstrfip->ref_rt_version, vers->rt_version);
		PROFIP_ERR("MSTRFIP_FPGA_VERSION | %.8x | %.8x\n", mstrfip->ref_fpga_version, vers->fpga_version);	
		PROFIP_ERR("MSTRFIP_GIT_VERSION  |          | %.8x\n\n", vers->rt_git_version);
	}

	cas_set_mfip_git_vers(vers->rt_git_version);

	return res;
}

int mstrfip_rtapp_check_version(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	int err = mstrfip_check_version_consistency(mstrfip);

	if (err)
	{
		errno = mstrfip->current_error;
		pf_err(PF_ERR_MODULE_LIBMASTERFIP, errno, err, "Masterfip version consistency error\n");
	}

	return err;
}

/**
 * Returns the response time for every requested agent.
 * @param[in] dev device token
 * @param[in] agent_count number of requested agents
 * @param[in] agent_addr_list list of agent adresses
 * @param[out] resp_nstime_list response time in ns time for each agent
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_hw_response_time_get(struct mstrfip_dev *dev, int agent_count,
				int agent_addr_list[], uint32_t resp_nstime_list[])
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	int i;

	if (mstrfip->current_error) {
		errno = mstrfip->current_error;
		return -1;
	}

	if (mstrfip->ba.seg.present_nb == 0) {
		/*
		 * Not yet scheduled.
		 * TODO: if the state is not running, we can schedule the job
		 * to satisfy the client request. For the time being, as this
		 * service is meant for internal purpose (macro cycle
		 * validation) it is automatically scheduled when the macro
		 * cycle is loaded.
		 */
		return -1;
	}
	for (i = 0; i < agent_count; i++) {
		resp_nstime_list[i] =
			mstrfip->ba.seg.resp_nstime[agent_addr_list[i]];
	}
	return 0;
}

/**
 * Returns the bus speed
 * @param[in] dev device token
 * @param[out] bitrate bus speed
 */
int mstrfip_hw_speed_get(struct mstrfip_dev *dev, enum mstrfip_bitrate *bitrate)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct mqospi_msg msg;
	struct mstrfip_hw_speed_trtlmsg *hw_speed;
	int err;

	if (mstrfip->current_error) {
		errno = mstrfip->current_error;
		return -1;
	}

	/*
	 * Sequence: send a sync message to get the HW configuration bitrate
	 */
	hw_speed = (struct mstrfip_hw_speed_trtlmsg *)msg.data;
	/* Build the message */
	hw_speed->trtl_hdr.id = MSTRFIP_CMD_GET_BITRATE;
	hw_speed->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	msg.hdr.len = MSTRFIP_HW_SPEED_TRTLMSG_WSZ;
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_BA;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_GET_BITRATE;
#ifdef DUMP_TRTLMSG
	dump_trtlmsg("request mstrfip_hw_speed_get (dir:input slot:0)", &msg);
#endif
	/* Send the message and get answer */
	err = mqospi_msg_sync(MSTRFIP_CPU0, MSTRFIP_HMQ_IO_CPU0_CMD, &msg, MSTRFIP_DEFAULT_TIMEOUT);

	mstrfip->ba.bitrate = (err < 0) ? MSTRFIP_BITRATE_UNDEFINED :
					hw_speed->bitrate;
	*bitrate = mstrfip->ba.bitrate; // set output parameter
	return (err > 0) ? 0 : err;
}

/**
 * Sets the HW configuration in terms of external and internal trigger,
 * termination,...
 * @param[in] dev device token
 * @param[in] cfg expected hardware config
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_hw_cfg_set(struct mstrfip_dev *dev, struct mstrfip_hw_cfg *cfg)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct mqospi_msg msg;
	struct mstrfip_hw_cfg_trtlmsg *cfg_msg;
	int err;
	uint32_t tr_ns;

	if (mstrfip->current_error)
    {
		errno = mstrfip->current_error;
		return -1;
	}

	if (mstrfip->ba.bitrate == MSTRFIP_BITRATE_UNDEFINED) {
		err = mstrfip_hw_speed_get(dev, &mstrfip->ba.bitrate);
		if (err)
        {
			return err;
        }
	}
	/* HW supports the expected bitrate: send HW config */
	/* Init msg and map hw_cfg struct to msg data */
	cfg_msg = (struct mstrfip_hw_cfg_trtlmsg *)msg.data;

	cfg_msg->trtl_hdr.id = MSTRFIP_CMD_SET_HW_CFG;
	cfg_msg->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	/* trigger config  */
	cfg_msg->enb_ext_trig = cfg->enable_ext_trig;
	cfg_msg->enb_ext_trig_term = cfg->enable_ext_trig_term;
	/*
	 * when the external trigger is not used, the internal trigger is
	 * automatically enabled. Otherwise application's wish is taken
	 */
	cfg_msg->enb_int_trig = (cfg->enable_ext_trig == 0) ?
					1 : cfg->enable_int_trig;
	/*turn around time,silence time and bit time in ticks(cpu clock: 10ns)*/
	if (cfg->turn_around_ustime != 0 &&
	    !mstrfip_ba_tr_chk(mstrfip, cfg->turn_around_ustime * 1000))
		tr_ns = cfg->turn_around_ustime * 1000;
	else // not specified or invalid, takes the max limit
		tr_ns = mstrfip_tr_max_nstime[mstrfip->ba.bitrate];
	cfg_msg->tr_ticks = ns_to_ticks(tr_ns, MFIP_CPU_HZ);
	cfg_msg->ts_ticks = ns_to_ticks(mstrfip_ts_nstime[mstrfip->ba.bitrate], MFIP_CPU_HZ);
	cfg_msg->bit_ticks = ns_to_ticks(mstrfip_bit_nstime[mstrfip->ba.bitrate], MFIP_CPU_HZ);
	msg.hdr.len = MSTRFIP_HW_CFG_TRTLMSG_WSZ;
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_BA;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_SET_HW_CFG;

#ifdef DUMP_TRTLMSG
	dump_trtlmsg("mstrfip_hw_cfg_set (dir:input slot:0)", &msg);
#endif
	/* Send the message and get answer */
	err = mqospi_msg_sync(MSTRFIP_CPU0, MSTRFIP_HMQ_IO_CPU0_CMD, &msg, MSTRFIP_DEFAULT_TIMEOUT);	


	if (err < 0)
		return err;

	/* check the answer */
	err = mstrfip_validate_acknowledge(&msg);
	if (!err) {
		mstrfip->ba.hw_cfg = *cfg; /* save requested hw config */
		/*
		 * Store the TR used by the master (in case the default was
		 * selected
		 */
		mstrfip->ba.hw_cfg.turn_around_ustime = tr_ns / 1000;
	}
	return err;
}

/**
 * Set the Software configuration
 *
 * @param[in] dev device token
 * @param[in] cfg expected sw config
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_sw_cfg_set(struct mstrfip_dev *dev, struct mstrfip_sw_cfg *cfg)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;

	/*
	 * Nothing more can be checked until the full config is done. An
	 * ultimate check occurs in ba_start where the full config is supposed
	 * to be done
	 */
	mstrfip->ba.sw_cfg = *cfg; /* save requested sw config */
	return 0;
}

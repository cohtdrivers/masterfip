/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <math.h>

#include "profip_log.h"
#include "ertec-libmasterfip-priv.h"
#include "ertec-libmasterfip.h"

/* return 1 if the action is allowed otherwhise returns 0 and set errno */
/* TODO : review completely this routine */
int mstrfip_is_action_allowed(struct mstrfip_desc *mstrfip, int action)
{
	if (mstrfip->ba.state != MSTRFIP_FSM_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return 0; /* not allowed */
	}
	if (action == __MSTRFIP_CREATE_BA)
		return 1; /* this action doesn't require more checks */

//	if (!mstrfip->ba.max_instr) {/* Max instruction count is null:first, create BA */
//		errno = MSTRFIP_BA_IS_NULL;
//		return 0; /* not allowed */
//	}
	/* TODO: commentted for the time being */
//	if (mstrfip->ba.var_list_count == mstrfip->ba.max_var_list) {
//		errno = MSTRFIP_BA_MAX_VAR_LIST;
//		return 0; /* not allowed */
//	}
	if (action == __MSTRFIP_CONFIG_BA)
		return 1; /* this action doesn't require more checks */

	return 1;
}

/**
 * Check consistency between ba macro cycle instructions and periodic variables
 * requested services
 * Returns 0 in case of inconsistency otherwhise 1
 */
int mstrfip_ba_consistency_chk(struct mstrfip_desc *mstrfip)
{
	return 1;
}

/**
 * this routine computes the theoritical duration using the
 * theoritical turnaround (tr) and silence time (ts) according to the bus speed
 *
 */
uint32_t compute_list_var_length(struct mstrfip_ba *ba,
		struct mstrfip_ba_macrocycle *mc, int start_idx, int stop_idx)
{
	int i;
	struct mstrfip_data_priv *pvar_priv;
	uint64_t rpdat_time, iddat_time;
	uint32_t ustime = 0;
	double vald;
	uint32_t tr_ns, ts_ns, bit_ns;
	/*
	 * TODO:
	 */

//	tr_ns = mstrfip_tr_nstime[ba->bitrate];
	tr_ns = ba->hw_cfg.turn_around_ustime * 1000;
	ts_ns = mstrfip_ts_nstime[ba->bitrate];
	bit_ns = mstrfip_bit_nstime[ba->bitrate];

	for (i = start_idx; i < stop_idx; ++i) {
		pvar_priv = (struct mstrfip_data_priv *)
				mc->per_vars.sorted_varlist[i]->priv;
		/*
		 * rpdat frame is made of nbytes of payload + 9 extra bytes:
		 * header, footer, crc ...
		 */
		rpdat_time = ((pvar_priv->hdr.max_bsz + 9) * 8 * bit_ns) +
								(2 * tr_ns);
		/*
		 * iddat frame is made of 2 payload bytes
		 */
		iddat_time = (2 + 6) * 8 * bit_ns;
		if (pvar_priv->hdr.flags & MSTRFIP_DATA_FLAGS_CONS) { /* consumed var */
			//vald = (ts_ns < rpdat_time) ? rpdat_time : ts_ns;
			vald = iddat_time + rpdat_time + ts_ns;
		}
		else { /*produced var */
			vald = iddat_time + rpdat_time;
		}
		ustime += ceil(vald/1000);
	}

	return ustime;
}

int mstrfip_ba_cyclelength_chk(struct mstrfip_ba *ba,
				struct mstrfip_ba_macrocycle *mc)
{
	int i, start_idx, stop_idx;
	uint32_t cycle_uslength, ticks, us_endtime;

	cycle_uslength = 0;
	for (i = 0; i < mc->instr_count; ++i)
    {
		switch (mc->instrlist[i].code)
        {
		case MSTRFIP_BA_PER_VAR_WIND:
			start_idx = mc->instrlist[i].per_var_wind.start_var_idx;
			stop_idx = mc->instrlist[i].per_var_wind.stop_var_idx;
			cycle_uslength += compute_list_var_length(ba, mc,
							start_idx, stop_idx);
			continue; /* skip the rest and treat next instruction */
			break;
		case MSTRFIP_BA_APER_VAR_WIND:
			ticks = mc->instrlist[i].aper_var_wind.ticks_end_time;
			break;
		case MSTRFIP_BA_WAIT_WIND:
			ticks = mc->instrlist[i].wait_wind.ticks_end_time;
			break;
		default:
			PROFIP_ERR(__ERR__"Wrong instruction code: %d\n", mc->instrlist[i].code);
			continue; /* should never happen */
			break;
		}
		/*
		 * the current cycle duration should be less than the
		 * end time of the aperiodic window or the wait window
		 */
		us_endtime = ticks_to_us(ticks, MFIP_CPU_HZ);
		if (cycle_uslength > us_endtime )
        {
			/*
			 * inconsistent BA: the current cycle length
			 * exceeds the end of ba-wait or aperiodic
			 * window
			 */
			PROFIP_ERR("Wrong macro cycle: periodic traffic overrun the window\n");
			errno = MSTRFIP_BA_INVALID_MACROCYCLE;
			return -1;
		}
		cycle_uslength = us_endtime;
	}
	mc->comp_cycle_ustime = cycle_uslength;

	if (mc->cycle_ustime < cycle_uslength) {
		PROFIP_ERR("periodic ends at us and s ends at us\n");
		errno = MSTRFIP_BA_INVALID_MACROCYCLE;
		return -1;
	}
	return 0;
}

int mstrfip_ba_tr_chk(struct mstrfip_desc *mstrfip, uint32_t tr_nstime)
{
	uint32_t tr_min_ns = mstrfip_tr_min_nstime[mstrfip->ba.bitrate];
	uint32_t tr_max_ns = mstrfip_tr_max_nstime[mstrfip->ba.bitrate];

	if (tr_nstime < tr_min_ns || tr_nstime > tr_max_ns)
		return 1;
	return 0;
}

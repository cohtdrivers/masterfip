/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include "ertec-libmasterfip-priv.h"
#include "ertec-libmasterfip.h"

#include "os.h"
#include "os_taskprio.h"
#include "pnio_types.h"
#include "pnioerrx.h"

#include "profip_log.h"
#include "pf_err.h"
#include "profip_wdg.h"
#include "pf_utils.h"

#define MSTRFIP_DIAG_PERIOD_SEC 5

static int mstrfip_present_list_get(struct mstrfip_desc *mstrfip,
				struct mstrfip_present_list *present)
{
	int err;
	struct mqospi_msg msg;

	/*
	 * Send a sync message to get presence liste
	 */
	/* Build the message */
	msg.data[0] = MSTRFIP_CMD_GET_PRESENT_LIST;
	msg.data[1] = 0; /* sequence number: always 0 for cmd */
	msg.hdr.len = 10; /* dim corresponding to the answer:  */
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_GET_PRESENT_LIST;
	/* Send the message and get answer */
	err = mqospi_msg_sync(MSTRFIP_CPU1, MSTRFIP_HMQ_IO_CPU1_CMD, &msg, MSTRFIP_DEFAULT_TIMEOUT);

	if (err < 0)
		return err;
	if (msg.hdr.len == 10)
		memcpy(present->list, &msg.data[2], 32);
	return 0;
}

/**
 * It sends a synchronous message to get the report variable which gives some
 * statistics used for diagnostic purpose
 * @param[in] dev device token
 * @param[out] report the report variable which is returned
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_report_get(struct mstrfip_dev *dev, struct mstrfip_report *report)
{
	int err;
	struct mqospi_msg msg;
	time_t time_sec;
	static uint32_t prev_tx_err = 0, prev_tx_watchdog = 0;

	/*
	 * Send a sync message to get presence liste
	 */
	/* Build the message */
	msg.data[0] = MSTRFIP_CMD_GET_REPORT;
	msg.data[1] = 0; /* sequence number: always 0 for cmd */
	msg.hdr.len = sizeof(struct mstrfip_report) + 2; /* dim corresponding to the answer:  */
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_GET_REPORT;

	/* Send the message and get answer */
	err = mqospi_msg_sync(MSTRFIP_CPU1, MSTRFIP_HMQ_IO_CPU1_CMD, &msg, MSTRFIP_DEFAULT_TIMEOUT);

	if (err < 0)
		return err;
	memcpy(report, &msg.data[2], sizeof(struct mstrfip_report));
	time_sec = pf_time_get();
	if (report->fd_tx_err && (report->fd_tx_err != prev_tx_err)) {
		report->fd_tx_err_hwtime = time_sec;
		prev_tx_err = report->fd_tx_err;
	}
	if (report->fd_tx_watchdog && (report->fd_tx_watchdog != prev_tx_watchdog)) {
		report->fd_tx_watchdog_hwtime = time_sec;
		prev_tx_watchdog = report->fd_tx_watchdog;
	}
	return 0;
}

static int mstrfip_request_fipdiag_ident(struct mstrfip_desc *mstrfip)
{
	struct mstrfip_data *identlist[1] = {mstrfip->ba.mcycle->diag.ident_var};
	int res;

	res = mstrfip_ident_request((struct mstrfip_dev *)mstrfip, identlist, 1);
	/* TODO : error reporting probably call application error handler */
	return res;
}


static int convert_diag_var_status(int payload_error) {
	int status = 0x0;

	if (!(payload_error & MSTRFIP_FRAME_PAYLOAD_NOT_REFRESH))
		status |= 0x1; /* refresh bit */
	if (!(payload_error & MSTRFIP_FRAME_PAYLOAD_NOT_SIGNIFICANT))
		status |= 0x4; /* significant bit */
	return status;
}

static void read_diag_var(struct mstrfip_desc *mstrfip, time_t time_sec, int agt_absent)
{
	struct mstrfip_diag_shm *shm = &(mstrfip->ba.shm);
	struct mstrfip_data *pvar = mstrfip->ba.mcycle->diag.varlist[0]; /* 0x057F diag var */

	mstrfip_data_refresh(pvar, MSTRFIP_PER_VAR,
			&mstrfip->ba.mcycle->diag.trtl.msglist[0],
			mstrfip->ba.mcycle->diag.trtl.msg_count);
	if (pvar->status != MSTRFIP_DATA_FRAME_ERROR) {
		++shm->com_ok_count;
		if (shm->com_status >= MSTRFIP_STATUS_DIAG_HW_ERR) {
			/* back to a working com: stamp com_first_ok */
			shm->com_first_ok_date = time_sec;
			++shm->com_recover_count;
		}
		/*
		 * for backward compatibility with old FIP tools the var status
		 * published in the shared memory should be compliant with teh
		 * previous definition. This is the purpose of this
		 * status_convert function.
		 */
		shm->var_status =
				convert_diag_var_status(pvar->payload_error);
		if (pvar->payload_error == MSTRFIP_FRAME_PAYLOAD_OK) {
			shm->acq[0] = pvar->buffer[0];
			shm->acq[1] = pvar->buffer[1];
		}
		if (shm->acq[0] != shm->ctrl[0]) {
			++shm->bad_data_count;
			shm->com_status = MSTRFIP_STATUS_DIAG_DATA_ERR;
		}
		else {
			shm->com_status = MSTRFIP_STATUS_DIAG_OK;
			if (agt_absent)
				shm->com_status =
						MSTRFIP_STATUS_AGT_MISS;
		}
	}
	else { /* com error diag agt doesn't reply properly*/

		++shm->com_fault_count;
		if (shm->com_status < MSTRFIP_STATUS_DIAG_HW_ERR) {
			/* back to a working com: stamp com_first_ok */
			shm->com_first_fault_date = time_sec;
		}
		shm->com_status = MSTRFIP_STATUS_DIAG_HW_ERR;
		if (agt_absent > 1) /* trick: skip fip diag unpresence */
			shm->com_status = MSTRFIP_STATUS_DIAG_FAULT1;

	}
}

static void write_diag_var(struct mstrfip_desc *mstrfip, struct mstrfip_data *var)
{
	struct mstrfip_diag_shm *shm = &(mstrfip->ba.shm);

	/* write new value */
	/*
	 * trick: a single byte is compared between read/write values.
	 * Therefore ctrl[0] keeps the previous control value and ctrl[1]
	 * contains the new value which will be sent over the bus next cycle.
	 */
	shm->ctrl[0] = shm->ctrl[1];
	shm->ctrl[1] = (shm->ctrl[1] == 255) ?
		0 : (shm->ctrl[1] + 1);
	var->buffer[0] = shm->ctrl[1];
	var->buffer[1] = shm->ctrl[1];
	var->bsz = 2; /*TODO: define var diag length */
	mstrfip_var_write((struct mstrfip_dev *)mstrfip, var);
}

void mstrfip_diag_ident_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *pvar,
				struct mstrfip_irq *irq)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_diag_shm *shm = &(mstrfip->ba.shm);
	uint32_t model, constructor;
	static int diag_idet_var_error = 0;

	mstrfip_data_refresh(pvar, MSTRFIP_IDENT_VAR,
				&mstrfip->ba.mcycle->diag.trtl.msglist[0],
				mstrfip->ba.mcycle->diag.trtl.msg_count);
	if (pvar->status == MSTRFIP_DATA_OK)
	{
		model = pvar->buffer[4];
		constructor = pvar->buffer[3];
		shm->segment_id = model & 0x1F;
		shm->system_id = (constructor >> 4) & 0x0F;
		shm->building_id = ((model >> 5) & 0x07) +
						((constructor & 0x01) << 3);

		diag_idet_var_error = 0;
	}
	else
	{
		if(diag_idet_var_error == 0)
		{
			pf_err(PF_ERR_MODULE_LIBMASTERFIP, MSTRFIP_INT_ERR_DIAG_IDENT_VAR_ERR_STATUS, \
					pvar->status, "Diag ident var error status\n");
			diag_idet_var_error = 1;
		}
	}
}

/*
 * produced diagnostic var's callback
 */
void mstrfip_diag_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *var,
				struct mstrfip_irq *irq)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_diag_shm *shm = &(mstrfip->ba.shm);
	int i, agt_absent = 0, hw_ustime;
	time_t time_sec;

	++shm->cycle_count; /* increment cycle counter */
	time_sec = pf_time_get();
	shm->last_cycle_date = time_sec; /* stamp the cycle */
	/* compute list of absent: crasy but required!!! */
	for (i = 0, agt_absent = 0; i < 32; ++i) {
		shm->absent_list[i] = shm->present_list[i] ^
					shm->present_theoric_list[i];
		if (shm->absent_list[i] != 0)
			++agt_absent;
	}
	/* time between two diag irq define the macro cycle length */
	hw_ustime = (irq->hw_sec * 1000000) + (irq->hw_ns / 1000);
	shm->comp_cycle_uslength = hw_ustime -
			mstrfip->ba.mcycle->diag.last_hw_ustime;
	mstrfip->ba.mcycle->diag.last_hw_ustime = hw_ustime;

	if (irq->acq_count == 1) {
		read_diag_var(mstrfip, time_sec, agt_absent);
	}
	else { /*expect only one message corresponding to consumed diag var */
		++shm->com_fault_count;

		if (shm->com_status < MSTRFIP_STATUS_DIAG_HW_ERR) {
			/* back to a working com: stamp com_first_ok */
			shm->com_first_fault_date = time_sec;
		}
		shm->com_status = MSTRFIP_STATUS_DIAG_HW_ERR;
	}
	write_diag_var(mstrfip, var);
}

static void mstrfip_diag_run(void)
{
	struct mstrfip_desc *mstrfip = mstrfip_get();

	struct mstrfip_diag_shm *shm = &(mstrfip->ba.shm);
	struct mstrfip_present_list present;
	int first_loop = 1;
	int cycle_counter = 0;
	int cnt = 0;
	struct mstrfip_report report;

	OsWaitOnEnable();

	profip_wdg_start(PROFIP_WDG_MODULE_MASTERFIP_DIAG, 20000 /*ms*/);

	/* init rx_error counter to 0 */
	mstrfip->ba.mcycle->diag.prev_rx_err = 0;

	for (;;) {
		/*
		 * man 7 pthreads: according to POSIX.1-2001 and/or POSIX.1-2008
		 * nanosleep is a cancelation point, so no pint to call
		 * pthread_testcancel to create a cancellation point.
		 */
		profip_wdg_trigger(PROFIP_WDG_MODULE_MASTERFIP_DIAG);

		OsWait_ms(100);
		cnt++;

		if(!mstrfip->ba.diag_run)
		{
			mstrfip->ba.diag_stopped = 1U;
			first_loop = 1U;
			cycle_counter = 0U;
			continue;
		}

		if(cnt%50 == 0)
		{

			mstrfip->ba.diag_stopped = 0U;

			if (cycle_counter == shm->cycle_count) {
				/*
				 * cycle_counter remains fix: seems to indicate that
				 * the RT BA controller is stuck somewhere.
				 */
				shm->com_status = MSTRFIP_STATUS_DIAG_FAULT2;
				shm->comp_cycle_uslength = 0;
				shm->last_cycle_date = pf_time_get(); /*stamp the cycle*/
				memset(shm->present_list, 0, 32);
				mstrfip_report_get((struct mstrfip_dev *)mstrfip, &report);
			}
			else {
				/*
				 * cycle counter got incremented, means the BA
				 * controller is running
				 */
				cycle_counter = shm->cycle_count;
				mstrfip_present_list_get(mstrfip, &present);
				memcpy(shm->present_list, present.list, 32);
				if (first_loop) { /*first iteration:theoric_present = present */
					memcpy(shm->present_theoric_list, present.list, 32);
					/* enforce fip_diag presence in the theoric_list */
					shm->present_theoric_list[15] |= 0x80;
					first_loop = 0;
				}
				mstrfip_report_get((struct mstrfip_dev *)mstrfip, &report);
				mstrfip_request_fipdiag_ident(mstrfip);
				shm->tx_ok_count = report.tx_ok;
				shm->rx_ok_count = report.rx_ok;
				/*
				 * Trick to be compliant with the SHM: bad_count is a
				 * uint16 while rx_err is uint32. Then the rx_err is
				 * truncated which is admissible. If the rx_err didn't
				 * increase since the last update, it is reseted
				 */
				if (mstrfip->ba.mcycle->diag.prev_rx_err == report.rx_err)
					shm->rx_bad_count = 0;
				else {
					mstrfip->ba.mcycle->diag.prev_rx_err = report.rx_err;
					shm->rx_bad_count = report.rx_err;
				}
			}
		}
	}

	return;
}

int mstrfip_diag_start(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *) dev;

  	PNIO_UINT32   TskId_masterfip_diag, Status;

  	if(mstrfip->ba.diag_started && mstrfip->ba.diag_run)
  	{
  		PROFIP_ERR(__ERR__"Diag thread is running\n");
  		return -10;
  	}

	if(mstrfip->ba.diag_started)
	{
		PROFIP_LOG(__OK__"Diag thread started (1)\n");
		mstrfip->ba.diag_run = 1U;
		return 0;
	}

	Status = OsCreateThread (mstrfip_diag_run, (PNIO_UINT8*)"MasterFIP_Diag", TASK_PRIO_LIBMFIP_DIAG_LOW, &TskId_masterfip_diag);
	if(PNIO_OK != Status)
	{
		return -11;
	}

	Status = OsCreateMsgQueue (TskId_masterfip_diag); // install the task message queue
	if(PNIO_OK != Status)
	{
		return -12;
	}

	Status = OsStartThread (TskId_masterfip_diag); // start, after the task message queue has been installed
	if(PNIO_OK != Status)
	{
		return -13;
	}

	mstrfip->ba.diag_started = 1;
	mstrfip->ba.diag_run = 1;

	PROFIP_LOG(__OK__"Diag thread started\n");

	return 0;
}

int mstrfip_diag_shm_create(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;

	/* Zero shared memory structure */
	memset(&mstrfip->ba.shm, 0, sizeof(struct mstrfip_diag_shm));

	/*
	 * Get system time as the start time of the FIP application
	 * usec are ignored, keep just number of seconds since the Epoch
	 */
	mstrfip->ba.shm.start_time = 0;
	mstrfip->ba.shm.lib_vers = 1;
	return 0;
}

struct mstrfip_diag_shm*  mstrfip_diag_get(void)
{
	return &(mstrfip_get()->ba.shm);
}

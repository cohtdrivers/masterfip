/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __LIB_MASTERFIP_DIAG_H
#define __LIB_MASTERFIP_DIAG_H

#include "masterfip-common-priv.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

struct mstrfip_per_var_desc {
	uint32_t var_id;   /* varaiable id */
	uint32_t dir;      /* direction: consumed/produced */
	uint32_t sz;       /* payload size in bytes */
	uint32_t cb;       /* associated callback (yes or no) */
};

/*
 * As it is for diagnostic, we chosse to size this cfg space according to a
 * pseudo max config in order to have a shm with a static size, which
 * simplify the attach to the SHM from client side.
 */
#define MSTRFIP_BA_PER_VAR_CFG_COUNT 1020 /*Max per_var: 255agt * 4var_per_agt */
#define MSTRFIP_BA_INSTR_LIST_COUNT 20 /* Max instr count to compose a macrocycle */
struct mstrfip_ba_cfg {
	uint32_t cycle_uslength;
	uint32_t n_per_var;
	struct mstrfip_per_var_desc per_varlist[MSTRFIP_BA_PER_VAR_CFG_COUNT];
	uint32_t ninstr;
	struct mstrfip_ba_instr instrlist[MSTRFIP_BA_INSTR_LIST_COUNT];
};

struct mstrfip_ext_diag_shm {
	struct mstrfip_version vers;
	struct mstrfip_seg_cfg seg;
	enum mstrfip_bitrate bitrate;
	uint32_t mtrtl_cpu_speed_hz;
	struct mstrfip_hw_cfg hw_cfg;
	struct mstrfip_ba_cfg ba_cfg;
};

/*
 * master fip diagnostic should be compliant with the current FDM diagnostic
 * implementation, because a mstrfip diamon agent should report diagnostic data
 * from a mstrfip segment (CERN) as well as from a FDM segment (ALSTHOM)
 * Therefore all those definitions come from the current implementation and a
 * systemV shared memory is used to export diagnostic data
 */

/* Constants */
#define MSTRFIP_DIAG_SHM_KEY 0x46444730  // Key for WorldFIP Diamon shared memory

/* those status value are used by diamon and cannot be changed */
#define MSTRFIP_STATUS_DIAG_OK 0 /* no error at all with diag var*/
#define MSTRFIP_STATUS_AGT_MISS 1 /* absent agent are detected */
#define MSTRFIP_STATUS_DIAG_DATA_ERR 2 /* mismatch between read/write diag data */
#define MSTRFIP_STATUS_DIAG_HW_ERR 3 /* diag var status is faulty: hw error */
#define MSTRFIP_STATUS_DIAG_FAULT1 4 /* diag var is faulty + agent are missing */
#define MSTRFIP_STATUS_DIAG_FAULT2 5 /* no irq received poll leaves by timeout */

/**
 * replica of the structure mapped into the diagnostic SHM
 * Again this structure is imposed by the previous CERN
 * implementation of the diagnostic on top of Alsthom FIP.
 * Even if some fields are meaningless, we have to publish the FIP diagnostic of
 * the new CERN's master FIP system through this structure.
 * Nevetheless at the end of this structure we have added additional diagnostic
 * allowing new diagnostic tool to provide more information but without
 * breaking old tools.
 */
struct mstrfip_diag_shm {
	uint32_t useless_1[2]; /**< mandatory for backward compatibility */

	uint8_t building_id; /**< retrieved from ident diag var */
	uint8_t system_id; /**< retrieved from ident diag var */
	uint8_t segment_id; /**< retrieved from ident diag var */

	/* statictics on diagnostic's transcation */
	uint32_t com_ok_count; /**< count successful consumed diag var*/
	uint32_t com_fault_count; /**< count failed consumed diag var*/
	uint32_t com_recover_count; /**< count transition between BAD to OK*/
	uint32_t bad_data_count; /**< count data mismatch between read/write*/

	uint32_t cycle_count; /**< cycle counter */

	/* produced/consumed diag var data */
//	uint8_t ctrl; /**< byte send on the current cycle prod diag var*/
//	uint8_t prev_ctrl;/**< byte sent during the previous cycle prod diag var*/
	uint8_t ctrl[2];
	uint8_t acq[2]; /**< data acquired using the consumed diag var*/
	uint8_t var_status; /**< status of the consumed diag var*/

	/* The following date are the seconds since the epoch */
	uint32_t com_first_ok_date; /**< last transition time from FAULT to OK*/
	uint32_t com_first_fault_date; /**< last transition time from OK to FAULT*/
	uint32_t last_cycle_date;/**< last cycle time */
	uint32_t start_time; /**< application start time*/

	/*
	 * various list to determine the list of agent present/absent
	 * Potentially 255 agents can be connected to a unique FIP segment.
	 * So each bit (32*8=256) is used to signal if the corresponding agent
	 * is present(bit raised) or absent(bit lowered).
	 */
	uint8_t present_list[32]; /**< list of agent present*/
	uint8_t present_theoric_list[32]; /**< theoric list of agent present*/
	uint8_t absent_list[32];/**< list of agent absent: why?? seems redondant*/

	/* summary giving an overall of the status of the bus*/
	uint16_t com_status;

	/*number of ok/bad received frames between two consecutives get report*/
	uint16_t tx_ok_count;
	uint16_t tx_bad_count;
	uint16_t rx_bad_count;
	uint16_t rx_ok_count;
	uint16_t ch_status; /* ????????? */

	uint32_t comp_cycle_uslength;

	uint32_t lib_vers;
	uint32_t useless_4;

	uint16_t useless_5;

	/* extended diagnostics */
	struct mstrfip_ext_diag_shm ext;
};

#ifdef __cplusplus
};
#endif

#endif /* __LIB_MASTERFIP_DIAG_H */

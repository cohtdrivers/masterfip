/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <errno.h>

#include "mqospi.h"
#include "ertec-libmasterfip-priv.h"
#include "ertec-libmasterfip.h"

/**
 * It validates the answer of a synchronous message
 */
int mstrfip_validate_acknowledge(struct mqospi_msg *msg)
{
	int res = 0;

	if (msg->hdr.len < 2)
    {
		errno = MSTRFIP_INVALID_ANSWER_ACK;
		return -1;
	}
    
	switch (msg->data[0])
    {
	case MSTRFIP_REP_ACK:
		res = 0;
		break;

	case MSTRFIP_REP_NACK:
		//TODO: to be reviewed
		//data[2] is supposed to contain a clear error code
		res = (msg->hdr.len >= 3) ? msg->data[2] : -1;
		break;
	}
	return res;
}

/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __LIB_MASTERFIP_PRIV_H
#define __LIB_MASTERFIP_PRIV_H

#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <stdio.h>

#include "mqospi.h"
#include "masterfip-common-priv.h"
#include "ertec-libmasterfip.h"
#include "pf_err.h"

#define MSTRFIP_CPU0 (0)
#define MSTRFIP_CPU1 (1)

#define MSTRFIP_APP_ID_BA (0)
#define MSTRFIP_APP_ID_CMD (1)

#define MSTRFIP_DEFAULT_TIMEOUT    10000
#define __MSTRFIP_CREATE_BA 0
#define __MSTRFIP_CONFIG_BA 1

#define MSTRFIP_APP_DOMAIN (1)
#define MSTRFIP_DIAG_DOMAIN (2)

/* time in ns to send one bit according to the 3 supported speed */
extern const uint32_t mstrfip_bit_nstime[3];
/* turn around time in ns (min and max), according to the 3 supported speed */
extern const uint32_t mstrfip_tr_min_nstime[3];
extern const uint32_t mstrfip_tr_max_nstime[3];
/* silence time(time-out) in us, according to the 3 supported speed */
extern const uint32_t mstrfip_ts_nstime[3];

/*
 * private data structure attached to a mstrfip-data object.
 * Contains an object data_hdr shared between the RT app and the library and a
 * callback provided by the final application in case the application wants to
 * be waken-up when this data is sceduled on the bus.
 */
/* forward declaration */
struct mstrfip_irq;
struct mstrfip_data;
struct mstrfip_dev;
struct mstrfip_data_priv {
	struct mstrfip_data_hdr hdr;
	void (* mstrfip_data_handler)(struct mstrfip_dev *dev,
			struct mstrfip_data *data, struct mstrfip_irq *irq);
};

#define MSTRFIP_PRESENCE_BIT(A,k) (A[(k/32)] & (1 << (k%32)))
struct mstrfip_present_list {
	uint32_t list[8]; /* 1bit per agent max 256 agents: 8 32bits words */
	int present_count;
	int absent_count;
};

#define MSTRFIP_TRTL_BUF_MSGLIST_SIZE 64U
struct mstrfip_mqospi_buf {
	struct mqospi_msg msglist[MSTRFIP_TRTL_BUF_MSGLIST_SIZE]; /* allocated messages */
	int msg_count; /* current number of messages */
};


/**
 * masterFIP data buffer
 */
#define MSTRFIP_DATA_BUF_SIZE (1 + 4*FIP_MAX_NBR_OF_NODES) /* at least 4 * number of slots */
struct mstrfip_data_buf {
	struct mstrfip_data list[MSTRFIP_DATA_BUF_SIZE]; /**< data list */
	struct mstrfip_data_priv list_priv[MSTRFIP_DATA_BUF_SIZE]; /**< data list */
	int count; /**< current number of data in the list */
};

struct mstrfip_ba_diag {
	/*
	 * contains the two periodic fip diag variables used to survey the
	 * electrical quality of the bus
	 */
	struct mstrfip_data *varlist[2];
	/*
	 * This fip diag identification variable is scheduled according to the
	 * FIP diag period.
	 */
	struct mstrfip_data *ident_var;
	/*
	 * Upon irq, all pending mockturtle messages since the previous irq
	 * are read from the driver in once and stored into the trtl buffer
	 * till the applicationw consume it.
	 */
	struct mstrfip_mqospi_buf trtl;

	/* bunch of variables required for diagnostic */
	int last_hw_ustime; /*last diag irq hw time in us*/
	int agt_missing; /*set if theoric-present differs from present list*/
	int prev_rx_err; /* Required by old diagnostic */
};

struct mstrfip_ba_per_var_set {
	struct mstrfip_data_buf var;

	/*
	 * sorted list of pointer to periodic var: ordered sequence of periodic
	 * variables, according to the programming of the periodic window.
	 * The number of sorted vars can differed from the created var number,
	 * in case the same var is scheduled several time in the macro cycle.
	 */
	struct mstrfip_data *sorted_varlist[MSTRFIP_DATA_BUF_SIZE];
	int sorted_var_count;

	/*
	 * two additional list of pointers to periodic variable:
	 * one for the produced variables and one for the consumed varaiables.
	 * This is required for efficiency in the RT side, to retrieve
	 * the payload of a produced var in the SMEM without using a costly
	 * lookup, but just using an index in an array of contiguous produced
	 * variables' payloads.
	 * The same for consumed variables when the message is received.
	 */
	struct mstrfip_data *prod_varlist[MSTRFIP_DATA_BUF_SIZE];
	int prod_var_count;

	struct mstrfip_data *cons_varlist[MSTRFIP_DATA_BUF_SIZE];
	int cons_var_count;

	/*
	 * Upon irq, all pending mockturtle messages since the previous irq
	 * are read from the driver in once and stored into the trtl buffer
	 * till the applicationw consume it.
	 */
	struct mstrfip_mqospi_buf trtl;
};

struct mstrfip_ba_aper_var_set {
	/*
	 * list of identification variable which can be scheduled during the
	 * aperiodic window upon request from the application.
	 */
	struct mstrfip_data_buf ident;
	void (* mstrfip_ident_var_handler)(struct mstrfip_dev *dev,
						struct mstrfip_irq *irq);

	/*
	 * Upon irq, all pending mockturtle messages since the previous irq
	 * are read from the driver in once and stored into the trtl buffer
	 * till the applicationw consume it.
	 */
	struct mstrfip_mqospi_buf trtl;
};

enum mstrfip_macrocycle_state {
	MSTRFIP_MACROCYCLE_INITIAL,
	MSTRFIP_MACROCYCLE_VALID,
	MSTRFIP_MACROCYCLE_INVALID,
};

#define MSTRFIP_BA_MACROCYCLE_INSTRLIST_SIZE 10U
struct mstrfip_ba_macrocycle {
	struct mstrfip_desc *dev; /* relationship with the fip device */
	struct mstrfip_ba_per_var_set per_vars; /* periodic variables */

	struct mstrfip_ba_aper_var_set aper_vars; /* aperiodic variables */

	struct mstrfip_ba_diag diag; /* diagnostic configuration */

	/* BA instruction set */
	struct mstrfip_ba_instr instrlist[MSTRFIP_BA_MACROCYCLE_INSTRLIST_SIZE];
	uint32_t instr_count; /* number of instructions inserted */

	uint32_t comp_cycle_ustime; /* computed ba cycle length in us */
	uint32_t cycle_ustime; /* requested ba cycle length in us */

	enum mstrfip_macrocycle_state state; /* initial, valid, invalid */

	int has_registered_cb; /* registered callback (drive start irq_thread) */
};

struct mstrfip_ba {
	/* HW and SW  configuration */
	struct mstrfip_hw_cfg hw_cfg; /* Hardware config */
	struct mstrfip_sw_cfg sw_cfg; /* Software config */

	/* current macro cycle in use */
	struct mstrfip_ba_macrocycle *mcycle;

	struct mstrfip_seg_cfg seg;
	/* ... */
	enum mstrfip_bitrate bitrate;

	struct mstrfip_diag_shm shm;

	enum mstrfip_ba_fsm state; /* ba running or stopped */

	volatile int irq_run;
	volatile int irq_started;
	volatile int irq_stopped;

	volatile int diag_run;
	volatile int diag_started;
	volatile int diag_stopped;
};

struct mstrfip_desc {
	/* version */
	uint32_t ref_rt_version; /* reference rt app version */
	uint32_t ref_fpga_version; /* reference fpga version */
	struct mstrfip_version vers; /* version read from HW */

	/* ba + macro cycle list */
	struct mstrfip_ba ba;
	struct mstrfip_ba_macrocycle mcycle; /* macrocycle */

	/* current error */
	int current_error;
};

extern int mstrfip_is_action_allowed(struct mstrfip_desc *mstrfip, int action);
extern int mstrfip_ba_tr_chk(struct mstrfip_desc *mstrfip, uint32_t tr_nstime);
extern int mstrfip_ba_consistency_chk(struct mstrfip_desc *mstrfip);

extern int mstrfip_ba_cyclelength_chk(struct mstrfip_ba *ba,
					struct mstrfip_ba_macrocycle *mc);

extern void mstrfip_diag_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *var,
					struct mstrfip_irq *irq);
extern void mstrfip_diag_ident_var_handler(struct mstrfip_dev *dev,
				struct mstrfip_data *var, struct mstrfip_irq *irq);

/* Maximum size for dynamic error message */
#define MSTRFIP_DYNAMIC_ERR_MSG_SZ 256
/* error messages */
extern char *mstrfip_errors[__MSTRFIP_MAX_ERROR_CODE - MSTRFIP_ERROR_CODE_OFFSET];
extern void mstrfip_error_init();
extern void mstrfip_error_free();

extern int mstrfip_irq_start(struct mstrfip_desc *mstrfip);
extern int mstrfip_irq_stop(struct mstrfip_desc *mstrfip);

extern int mstrfip_diag_start(struct mstrfip_dev *dev);
extern int mstrfip_diag_stop(struct mstrfip_dev *dev);

extern int mstrfip_diag_shm_create(struct mstrfip_dev *dev);

extern int mstrfip_validate_acknowledge(struct mqospi_msg *msg);

extern void mstrfip_msg_init(struct mqospi_msg *msg, int max_size, int dir);

extern void mstrfip_data_refresh(struct mstrfip_data *var,
		enum mstrfip_data_type data_type, struct mqospi_msg *trtlmsglist,
		int trtlmsg_count);

extern struct mstrfip_desc * mstrfip_get(void);

extern uint32_t compute_list_var_length(struct mstrfip_ba *ba,
		struct mstrfip_ba_macrocycle *mc, int start_idx, int stop_idx);

#endif //__LIB_MASTERFIP_PRIV_H

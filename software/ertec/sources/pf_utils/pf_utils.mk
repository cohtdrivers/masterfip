# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

SRC_C += $(SRC_ROOT_DIR_APPL)/pf_utils/pf_utils.c
SRC_C += $(SRC_ROOT_DIR_APPL)/pf_utils/pf_err.c
SRC_C += $(SRC_ROOT_DIR_APPL)/pf_utils/uart_flash_fw.c
SRC_C += $(SRC_ROOT_DIR_APPL)/pf_utils/data_uart.c
SRC_C += $(SRC_ROOT_DIR_APPL)/pf_utils/data_uart_util.c

INCD += -I$(SRC_ROOT_DIR_APPL)/pf_utils/
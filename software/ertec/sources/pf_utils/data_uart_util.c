// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "data_uart_util.h"

#include <cyg/kernel/kapi.h>

#include "os.h"
#include "os_taskprio.h"
#include "pnio_types.h"
#include "pnioerrx.h"

#include "ertec-libmasterfip.h"
#include "uart_flash_fw.h"
#include "data_uart.h"
#include "profip_log.h"
#include "pf_utils.h"
#include "data_uart_cmd.h"

#define DIAG_SHM_SIZE_EXT 	(sizeof(struct mstrfip_diag_shm))
#define DIAG_SHM_SIZE 		(sizeof(struct mstrfip_diag_shm) - sizeof(struct mstrfip_ext_diag_shm))

static uint8_t tmp_diag_shm[DIAG_SHM_SIZE_EXT];

static void data_uart_util_send_diag(int shm_size)
{
	// copy diag shm
	cyg_interrupt_disable();
	memcpy(tmp_diag_shm, mstrfip_diag_get(), shm_size);
	cyg_interrupt_enable();

	// send sync header
	data_uart_put_u32(CMD_DIAG_SYNC);

	// send shm data
	for(int i=0; i<shm_size; i++)
	{
		data_uart_putc(tmp_diag_shm[i]);
	}

	data_uart_put_u32(posix_crc32(tmp_diag_shm, shm_size));
}

static void data_uart_set_time(void)
{
	OsWait_ms(10);
	uint32_t time_tmp;

	if(data_uart_data_to_read() >= 4)
	{
		if(data_uart_get_u32(&time_tmp) == 0)
		{
			pf_time_set(time_tmp);
		}
	}
}

static void data_uart_util_run(void)
{
	uint32_t cmd = 0;

	OsWaitOnEnable();

	PROFIP_LOG(__OK__"data uart thread started\n");

	for(;;)
	{
		uint8_t c;

		if(data_uart_getc(&c) == 0)
		{
			cmd = (cmd >> 8) | (c << 24);

			switch(cmd)
			{
			case CMD_SEND_DIAG_SHM:
				data_uart_util_send_diag(DIAG_SHM_SIZE);
				break;

			case CMD_SEND_DIAG_SHM_EXT:
				data_uart_util_send_diag(DIAG_SHM_SIZE_EXT);
				break;

			case CMD_FW_UPDATE:
				uart_flash_fw();
				break;

			case CMD_SET_TIME:
				data_uart_set_time();
				break;

			default:
				break;
			}
		}
		else
		{
			OsWait_ms(30);
		}
	}
}

/**
 * @desc Starts data uart thread
 */
int data_uart_util_init(void)
{
	data_uart_init();

	PNIO_UINT32   TskId_data_uart_util, Status;

	Status = OsCreateThread (data_uart_util_run, (PNIO_UINT8*)"Data_uart_util", TASK_PRIO_DATA_UART_UTIL_LOW, &TskId_data_uart_util);
	if(PNIO_OK != Status)
	{
		return -30;
	}

	Status = OsCreateMsgQueue (TskId_data_uart_util); // install the task message queue
	if(PNIO_OK != Status)
	{
		return -31;
	}

	Status = OsStartThread (TskId_data_uart_util); // start, after the task message queue has been installed
	if(PNIO_OK != Status)
	{
		return -32;
	}

	return 0;
}



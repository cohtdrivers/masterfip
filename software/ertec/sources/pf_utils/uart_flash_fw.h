// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file uart_flash_fw.h
 * @name Uart Flash Firmware
 * @brief Module for eeprogramming Ertec over UART
 */

#ifndef UART_FLASH_FW_H
#define UART_FLASH_FW_H

int uart_flash_fw(void);

#endif
// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef DATA_UART_H
#define DATA_UART_H

#include <stdint.h>

void data_uart_init(void);

int data_uart_getc(uint8_t* c);

int data_uart_putc(uint8_t c);

int data_uart_get_u32(uint32_t *data);

int data_uart_put_u32(uint32_t data);

int data_uart_flush(void);

int data_uart_data_to_read(void);

#endif

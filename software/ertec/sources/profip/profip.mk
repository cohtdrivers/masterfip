# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

SRC_C += $(SRC_ROOT_DIR_APPL)/profip/profip_iodapi_event.c
SRC_C += $(SRC_ROOT_DIR_APPL)/profip/profip_usriod_main.c
SRC_C += $(SRC_ROOT_DIR_APPL)/profip/profip.c
SRC_C += $(SRC_ROOT_DIR_APPL)/profip/profip_bsp.c
SRC_C += $(SRC_ROOT_DIR_APPL)/profip/cas.c
SRC_C += $(SRC_ROOT_DIR_APPL)/profip/pf_diag.c

INCD += -I$(SRC_ROOT_DIR_APPL)/profip/
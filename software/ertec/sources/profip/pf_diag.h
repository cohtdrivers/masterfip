// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file pf_diag.h
 * @author Piotr Klasa (piotr.klasa@cerh.ch)
 * @brief Module responsible for preparing diagnostic messages (Diagnostics
 *        module). It reads the shm masterFIP diagnostics and packs them
 *        into a buffer that can be sent via PROFINET.
 * @version 0.1
 */

#ifndef PF_DIAG_H
#define PF_DIAG_H

#include <stdint.h>

/**
 * @brief copies Diagnostic Data (MasterFIP SHM) into "buff". 
 *        The data in "buff" is stored in BigEndiann format.
 * @return IOPS
 */
uint8_t pf_diag_copy_to_pnStack(uint8_t* buff, uint32_t size);

/**
 * @brief for any index it copies Diagnostic Data (MasterFIP
 *        SHM) into "buff". The data in "buff" is stored in
 *        BigEndiann format.
 */
int pf_diag_read_record(uint8_t index, uint8_t* buff, uint32_t size);

#endif

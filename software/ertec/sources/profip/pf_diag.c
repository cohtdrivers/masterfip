// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <string.h>

#include "pf_diag.h"

#include "pniobase.h"
#include "pf_err.h"
#include "pf_utils.h"
#include "ertec-libmasterfip.h"

/**
 * @brief Subslot 1 (All errors) of module Diagnostics
 */
#define PF_DIAG_SUBSLOT_MFIP_DIAG 1

static int copy_mfip_diag(uint8_t* buff, uint32_t size)
{
	char tmp[sizeof(struct mstrfip_diag_shm) - sizeof(struct mstrfip_ext_diag_shm)];
	struct mstrfip_diag_shm *shm_tmp = (struct mstrfip_diag_shm *) &tmp;
	int size_to_cpy = MIN(size, sizeof(tmp));

	shm_tmp->useless_1[0] = mstrfip_diag_get()->useless_1[0];
	shm_tmp->useless_1[1] = mstrfip_diag_get()->useless_1[1];

	shm_tmp->building_id = mstrfip_diag_get()->building_id;
	shm_tmp->system_id = mstrfip_diag_get()->system_id;
	shm_tmp->segment_id = mstrfip_diag_get()->segment_id;

	shm_tmp->com_ok_count = CHANGE_ENDIANESS_32(mstrfip_diag_get()->com_ok_count);
	shm_tmp->com_fault_count = CHANGE_ENDIANESS_32(mstrfip_diag_get()->com_fault_count);
	shm_tmp->com_recover_count = CHANGE_ENDIANESS_32(mstrfip_diag_get()->com_recover_count);
	shm_tmp->bad_data_count = CHANGE_ENDIANESS_32(mstrfip_diag_get()->bad_data_count);

	shm_tmp->cycle_count = CHANGE_ENDIANESS_32(mstrfip_diag_get()->cycle_count);

	memcpy(&shm_tmp->ctrl, &(mstrfip_diag_get()->ctrl), 2);
	memcpy(&shm_tmp->acq, &(mstrfip_diag_get()->acq), 2);
	shm_tmp->var_status = mstrfip_diag_get()->var_status;

	shm_tmp->com_first_ok_date = CHANGE_ENDIANESS_32(mstrfip_diag_get()->com_first_ok_date);
	shm_tmp->com_first_fault_date = CHANGE_ENDIANESS_32(mstrfip_diag_get()->com_first_fault_date);
	shm_tmp->last_cycle_date = CHANGE_ENDIANESS_32(mstrfip_diag_get()->last_cycle_date);
	shm_tmp->start_time = CHANGE_ENDIANESS_32(mstrfip_diag_get()->start_time);

	memcpy(&shm_tmp->present_list, &(mstrfip_diag_get()->present_list), 32);
	memcpy(&shm_tmp->present_theoric_list, &(mstrfip_diag_get()->present_theoric_list), 32);
	memcpy(&shm_tmp->absent_list, &(mstrfip_diag_get()->absent_list), 32);

	shm_tmp->com_status = CHANGE_ENDIANESS_16(mstrfip_diag_get()->com_status);

	shm_tmp->tx_ok_count = CHANGE_ENDIANESS_16(mstrfip_diag_get()->tx_ok_count);
	shm_tmp->tx_bad_count = CHANGE_ENDIANESS_16(mstrfip_diag_get()->tx_bad_count);
	shm_tmp->rx_bad_count = CHANGE_ENDIANESS_16(mstrfip_diag_get()->rx_bad_count);
	shm_tmp->rx_ok_count = CHANGE_ENDIANESS_16(mstrfip_diag_get()->rx_ok_count);
	shm_tmp->ch_status = CHANGE_ENDIANESS_16(mstrfip_diag_get()->ch_status);

	shm_tmp->comp_cycle_uslength = CHANGE_ENDIANESS_32(mstrfip_diag_get()->comp_cycle_uslength);

	shm_tmp->lib_vers = CHANGE_ENDIANESS_32(mstrfip_diag_get()->lib_vers);
	shm_tmp->useless_4 = CHANGE_ENDIANESS_32(mstrfip_diag_get()->useless_4);

	shm_tmp->useless_5 = CHANGE_ENDIANESS_16(mstrfip_diag_get()->useless_5);

	memcpy(buff, ((uint8_t*)shm_tmp), size_to_cpy);
	return size_to_cpy;
}

//return iops
uint8_t pf_diag_copy_to_pnStack(uint8_t* buff, uint32_t size)
{
	copy_mfip_diag(buff, size);

	return PNIO_S_GOOD;
}


int pf_diag_read_record(uint8_t index, uint8_t* buff, uint32_t size)
{
	memset(buff, 0, size);
	return copy_mfip_diag(buff, size);
}


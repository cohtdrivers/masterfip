// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @author Piotr Klasa <piotr.klasa@cern.ch>
 *
 * @file profip_bsp.h
 * @name Profip board support package
 * @brief Module for controlling electronics on ProFIP device
 */

#ifndef PROFIP_BSP_H
#define PROFIP_BSP_H

#include <stdint.h>

/**
 * @brief Starts blinking all leds
*/
void profip_bsp_start_led_blinking(uint32_t freq);

/**
 * @brief Stops blinking all leds
*/
void profip_bsp_stop_led_blinking(void);

#endif

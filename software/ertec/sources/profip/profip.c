// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "profip.h"

#include "profip_log.h"

#include "cas.h"

#include "os.h"
#include "os_taskprio.h"
#include "pniousrd.h"

#include "profip_led.h"

#include "pniobase.h"
#include "pf_err.h"
#include "pf_alarm.h"
#include "profip_wdg.h"

#include "mf_handler.h"
#include "pf_utils.h"

#define SLOT_IDX(slot) (slot - 3) 	///< converts slot index to slot number: slots numbers starts from 3 (3 - FIP_MAX_NBR_OF_NODES)
#define SLOT_NBR(idx) (idx + 3)		///< converts slot numbet to slot index

/* Profip internal error codes */
#define PROFIP_ERR_WRONG_MAGIC_NUMBER 			-2
#define PROFIP_ERR_WF_CONFIG_ALREADY_DONE 		-7
#define PROFIP_ERR_WRONG_PARAM_INDEX_FOR_NODE 	-8
#define PROFIP_ERR_WF_CONFIG_ALREADY_DONE_2 	-10
#define PROFIP_ERR_WRONG_SLOT_NUMBER 			-11
#define PROFIP_ERR_WRONG_AGT_ID 				-13
#define PROFIP_ERR_DISCONNECTED 				-14
#define PROFIP_ERR_NO_PROD_CBK 					-15
#define PROFIP_ERR_NO_CONS_CBK 					-16
#define PROFIP_ERR_WF_CONFIG_WRONG_SIZE			-17

/* parameters for "nanofip node" modules */
#define PROFIP_NANOFIPNODE_PARAM_MAGIC_NUMBER 0x95192912
#define RECORD_INDEX_NANOFIPNODE_PARAM			10
#define RECORD_INDEX_GET_IDENT_VAR				90
#define RECORD_INDEX_RESET_VAR					95

extern void TcpUtilFlashFirmware (void);

/* States for ProFip main state machine */
enum profip_state_t
{
	PROFIP_STATE_INIT, 			///< Initialization
	PROFIP_STATE_CONNECTING,	///< Connecting to ProFinet (collecting parameters etc.)
	PROFIP_STATE_PROGRAMMING,	///< Programming MasterFip network
	PROFIP_STATE_RUNNING,		///< Translation is running
	PROFIP_STATE_STOPPED,		///< Connected, but MasterFip (and translation) is stopped
	PROFIP_STATE_ERROR,			///< Error occured during programming
	PROFIP_STATE_RESET			///< ProFip will be reseted
};

static const char* profip_state_names[] =
{
	"INIT",
	"CONNECTING \e[1;33mO\e[0m",
	"PROGRAMMING \e[1;33mO\e[0m \e[0;32mO\e[0m",
	"RUNNING \e[1;32mO\e[0m",
	"STOPPED \e[0;32mO\e[0m",
	"ERROR \e[1;31mO\e[0m",
	"RESET \e[0;31mO\e[0m",
};

struct profip_config_t
{
    struct mf_h_cfg_t mf_cfg; /* masterFip configuration */

	enum profip_state_t state;
	enum profip_state_t next_state;

	uint8_t flag_disconnected;
	uint8_t flag_config_done;
};

struct profip_config_t pf_config;
volatile uint8_t tmp_flag_disconnected;
volatile uint8_t tmp_flag_config_done;
uint8_t tmp_flag_reset_masterfip;

/* Profip task id*/
PNIO_UINT32   TskId_profip_main;


/* ---- ----- STATIC FUNCTIONS DEFINITIONS ----- ----- */

static void profip_main_task(void);

static int profip_add_wf_node(uint32_t slot, uint8_t* buff, uint32_t size);

static int program_macrocycle(struct mf_h_cfg_t* cfg);

/* ---- ----- STATIC FUNCTIONS IMPLEMENTATIONS ----- ----- */

static int program_macrocycle(struct mf_h_cfg_t* cfg)
{
	int i;

	/* reset all initialization alarms */
	pf_alarm_reset(PF_ALARM_WRONG_FPGA_BITSTREAM, 1);
	pf_alarm_reset(PF_ALARM_WRONG_MF_VERS, 1);
	pf_alarm_reset(PF_ALARM_WR_CFG_NO_SLOTS, 1);
	pf_alarm_reset(PF_ALARM_WR_CFG_WAIT_WIND, 1);
	pf_alarm_reset(PF_ALARM_DUPLICATED_AGTS, 1);
	pf_alarm_reset(PF_ALARM_SW_PROBLEM, 1);

	/* reset all alarms for nf nodes  */
	for(i = 0; i < FIP_MAX_NBR_OF_NODES; i++)
	{
		pf_alarm_reset(PF_ALARM_DATA_FRAME_ERROR, SLOT_NBR(i));
		pf_alarm_reset(PF_ALARM_PAYLOAD_ERROR, SLOT_NBR(i));
		pf_alarm_reset(PF_ALARM_DATA_NOT_RECEIVED, SLOT_NBR(i));
		pf_alarm_reset(PF_ALARM_NO_PROD_DATA, SLOT_NBR(i));
		pf_alarm_reset(PF_ALARM_NO_CONS_DATA, SLOT_NBR(i));
		pf_alarm_reset(PF_ALARM_RUNTIME_SW_PROBLEM, SLOT_NBR(i));
	}

	enum mf_h_rc_t ce = mf_handler_program_macrocycle(cfg);

	if(ce != MF_H_RC_OK)
	{
		switch (ce)
		{
		case MF_H_RC_NO_RMQ_COMMUNICATON:
			pf_alarm_set(PF_ALARM_WRONG_FPGA_BITSTREAM, 1);
			break;
		case MF_H_RC_WRONG_VERSION:
			pf_alarm_set(PF_ALARM_WRONG_MF_VERS, 1);
			break;
		case MF_H_RC_NO_VARIABLE_DEFINED:
			pf_alarm_set(PF_ALARM_WR_CFG_NO_SLOTS, 1);
			break;
		case MF_H_RC_WRONG_WAIT_WIND:
			pf_alarm_set(PF_ALARM_WR_CFG_WAIT_WIND, 1);
			break;
		case MF_H_RC_DUPLICATED_AGTS:
			pf_alarm_set(PF_ALARM_DUPLICATED_AGTS, 1);
			break;
		default:
			pf_alarm_set(PF_ALARM_SW_PROBLEM, 1);
			break;
		}
	}

	cas_set_bus_speed(mf_handler_get_stat()->bus_speed);
	cas_set_wait_wind_time_us(mf_handler_get_stat()->wait_wind_time_us);
	cas_set_turnaround_time_us(mf_handler_get_stat()->turnaround_time_us);
	cas_set_nbr_of_cons_variables(mf_handler_get_stat()->cons_var_nbr);
	cas_set_nbr_of_prod_variables(mf_handler_get_stat()->prod_var_nbr);
	cas_set_aper_var_wind_time_us(mf_handler_get_stat()->aper_var_wind_time_us);

	return (int) ce;
}

static void profip_main_task(void)
{
    int profip_tick_cnt = 0;
    int err;

    OsWaitOnEnable();
    OsWait_ms(50);

    //init mf handler
	mf_handler_init();

    //Init pf alarms
    pf_alarm_init();

    profip_wdg_start(PROFIP_WDG_MODULE_PROFIP, 4000 /*ms*/);

    for(;;)
    {
    	OsWait_ms(10);

    	cas_set_pf_thread_tick((unsigned char)(profip_tick_cnt++));

    	/* copy the flag images to cfg - we don't want the flags to change during loop execution */
    	pf_config.flag_disconnected = tmp_flag_disconnected;
    	tmp_flag_disconnected = 0U;
    	pf_config.flag_config_done = tmp_flag_config_done;
    	tmp_flag_config_done = 0U;

    	uint8_t cmd_reset_masterfip = cas_cmd_reset_masterfip() || tmp_flag_reset_masterfip;
    	tmp_flag_reset_masterfip = 0U;

    	if(pf_config.state != PROFIP_STATE_RESET)
    	{
    		profip_wdg_trigger(PROFIP_WDG_MODULE_PROFIP);
    	}

    	/* resetting the entire system does not depend on the current state */
    	if(cas_cmd_reset())
    	{
    		PROFIP_DEBUG(__USER__"reset ProFip\n");
    		pf_config.next_state = PROFIP_STATE_RESET;
    	}

    	if(pf_config.state != PROFIP_STATE_RESET && pf_config.next_state != PROFIP_STATE_RESET)
    	{
			if(cas_cmd_clear_nodes_faults())
			{
				PROFIP_DEBUG(__USER__"clear node error counters\n");
				mf_handler_clear_err();
			}

			if(cas_cmd_clear_faults())
			{
				PROFIP_DEBUG(__USER__"clear faults\n");
				pf_err_clear();
			}

			if(cas_cmd_tcp_reprogramming())
			{
				/* start reprogramming thread */
				TcpUtilFlashFirmware();
			}

			/* --- MAIN STATE MACHINE --- */

			switch(pf_config.state)
			{
			case PROFIP_STATE_INIT:
				pf_config.next_state = PROFIP_STATE_CONNECTING;
				break;

			case PROFIP_STATE_CONNECTING:
				if(pf_config.flag_config_done)
				{
					pf_config.mf_cfg.diag_enabled 			= cas_get_diag_enabled();
					pf_config.mf_cfg.turnaround_time_us 	= cas_get_turnaround_time_us();
					pf_config.mf_cfg.wait_wind_time_us 		= cas_get_wait_wind_time_us();
					pf_config.mf_cfg.aper_var_wind_time_us 	= cas_get_aper_var_wind_time_us();

					/* Compare actual slot configuration with programmed configuration */
					if(mf_handler_cmp_cfg(&pf_config.mf_cfg) == 0 /* no new config*/)
					{
						PROFIP_LOG(__INFO__"New configuration is the same as the previous one.\n");

						/* trigger resending alarms */
						pf_alarm_trigger();

						if (cas_cmd_stop_macrocycle() == 0)
						{
							if (mf_handler_start_macrocycle() == 0)
								pf_config.next_state = PROFIP_STATE_RUNNING;
							else
								pf_config.next_state = PROFIP_STATE_ERROR;
						}
						else
						{
							pf_config.next_state = PROFIP_STATE_STOPPED;
						}
					}
					else
					{
						PROFIP_LOG(__INFO__"New configuration will be programmed.\n");
						/* Configuration changed. Reprogramming needed. */
						pf_config.next_state = PROFIP_STATE_PROGRAMMING;
					}

					/*clear flag*/
					pf_config.flag_config_done = 0U;
				}

				break;

			case PROFIP_STATE_PROGRAMMING:
				/* Reprogram macrocyccle*/
				err = program_macrocycle(&(pf_config.mf_cfg));

				if (err == 0)
				{
					if(cas_cmd_stop_macrocycle() == 1)
					{
						pf_config.next_state = PROFIP_STATE_STOPPED;
					}
					else
					{
						if (mf_handler_start_macrocycle() == 0)
							pf_config.next_state = PROFIP_STATE_RUNNING;
						else
							pf_config.next_state = PROFIP_STATE_ERROR;
					}
				}
				else
				{
					pf_config.next_state = PROFIP_STATE_ERROR;
				}

				break;

			case PROFIP_STATE_STOPPED:
				if (pf_config.flag_disconnected)
				{
					pf_config.next_state = PROFIP_STATE_CONNECTING;
					pf_config.flag_disconnected = 0;
				}
				else if(cmd_reset_masterfip)
				{
					/* reset masterFip */
					PROFIP_DEBUG(__USER__"reset MasterFip\n");
					pf_config.next_state = PROFIP_STATE_PROGRAMMING;
				}
				else if(cas_cmd_stop_macrocycle() == 0) /* start macrocycle */
				{
					PROFIP_DEBUG(__USER__"start macrocycle\n");

					if (mf_handler_start_macrocycle() == 0)
						pf_config.next_state = PROFIP_STATE_RUNNING;
					else
						pf_config.next_state = PROFIP_STATE_ERROR;
				}

				break;

			case PROFIP_STATE_RUNNING:
				if (pf_config.flag_disconnected)
				{
					/* stop masterFIP*/
					pf_config.next_state = PROFIP_STATE_CONNECTING;
					mf_handler_stop_macrocycle();
					pf_config.flag_disconnected = 0;
				}
				else if(cmd_reset_masterfip)
				{
					/* reset masterFip */
					PROFIP_DEBUG(__USER__"reset MasterFip\n");
					pf_config.next_state = PROFIP_STATE_PROGRAMMING;
				}
				else if(cas_cmd_stop_macrocycle())
				{
					PROFIP_DEBUG(__USER__"stop macrocycle\n");
					pf_config.next_state = PROFIP_STATE_STOPPED;
					mf_handler_stop_macrocycle();
				}

				break;

			case PROFIP_STATE_ERROR:
				if(pf_config.flag_disconnected)
				{
					pf_config.next_state = PROFIP_STATE_CONNECTING;
					pf_config.flag_disconnected = 0;
				}
				else if(cmd_reset_masterfip)
				{
					/* reset masterFip */
					PROFIP_DEBUG(__USER__"reset MasterFip\n");
					pf_config.next_state = PROFIP_STATE_PROGRAMMING;
				}

				break;

			case PROFIP_STATE_RESET:

				/*wait for reset*/
				break;
			}

			/* flags not handled properly */
			if(pf_config.flag_config_done)
			{
				pf_err(PF_ERR_MODULE_PROFIP, PROFIP_ERR_WF_CONFIG_ALREADY_DONE_2, \
						pf_config.state, "'WorldFip configuration done' callback called when its already configured\n");
				pf_config.flag_config_done = 0;
			}

			if(pf_config.flag_disconnected)
			{
				pf_err(PF_ERR_MODULE_PROFIP, PROFIP_ERR_DISCONNECTED, \
						pf_config.state, "'Disconnected' signal received at an illegal time\n");
				tmp_flag_disconnected = pf_config.flag_disconnected;
			}
    	}

		/* actions on state change*/
    	if(pf_config.state != pf_config.next_state)
    	{
    		switch(pf_config.next_state) /* action on entering the state */
    		{
    		case PROFIP_STATE_PROGRAMMING:
				profip_led_on(PROFIP_LED_GREEN);
				profip_led_on(PROFIP_LED_YELLOW);
    			break;

    		case PROFIP_STATE_STOPPED:
				profip_led_on(PROFIP_LED_GREEN);
				profip_led_off(PROFIP_LED_YELLOW);
    			break;

    		case PROFIP_STATE_RUNNING:
				profip_led_blink(PROFIP_LED_GREEN, PROFIP_LED_BLINK_SLOW);
				profip_led_off(PROFIP_LED_YELLOW);
    			break;

    		case PROFIP_STATE_ERROR:
				profip_led_off(PROFIP_LED_GREEN);
				profip_led_off(PROFIP_LED_YELLOW);
				mf_handler_reset_masterfip();
				break;

    		case PROFIP_STATE_CONNECTING:
				profip_led_blink(PROFIP_LED_YELLOW, PROFIP_LED_BLINK_SLOW);
				profip_led_blink(PROFIP_LED_GREEN, PROFIP_LED_BLINK_FAST);

				/* clear masterFip configuration */
				memset(&pf_config.mf_cfg, 0, sizeof(pf_config.mf_cfg));
				break;

			case PROFIP_STATE_RESET:
				profip_led_off(PROFIP_LED_GREEN);
				profip_led_on(PROFIP_LED_RED);
				profip_led_off(PROFIP_LED_YELLOW);

				break;
    		default:
    			break;
    		}

    		PROFIP_DEBUG("[New state] %s -> %s\n", profip_state_names[pf_config.state], profip_state_names[pf_config.next_state]);
    		pf_config.state = pf_config.next_state;
    	}

		//cas_set_masterfip_status((unsigned char) pf_config.masterfip_state);
		cas_set_profip_status((unsigned char) pf_config.state);

		// trigger alarms every 5 sec
		static int alarm_trigger_cnt = 0;

		alarm_trigger_cnt = (alarm_trigger_cnt + 1) % (500);
		if (alarm_trigger_cnt == 0) {
			pf_alarm_trigger();
		}
    }
    return; //Unreachable code
}

/* -- public functions ---*/

PNIO_UINT32 profip_init(void)
{
		PNIO_UINT32 Status;

        /* Start profip thread */
	    Status = OsCreateThread (profip_main_task, (PNIO_UINT8*)"ProFIP_Main", TASK_PRIO_PROFIP_MAIN_LOW, &TskId_profip_main);
        if(PNIO_OK != Status)
        {
        	PROFIP_ERR("Cannot create profip thread\n");
            return Status;
        }

	    Status = OsCreateMsgQueue (TskId_profip_main);   // install the task message queue
        if(PNIO_OK != Status)
        {
        	PROFIP_ERR("Cannot create profip msg queue!\n");
            return Status;
        }

	    Status = OsStartThread (TskId_profip_main);		// start, after the task message queue has been installed
        if(PNIO_OK != Status)
        {
        	PROFIP_ERR("Cannot start profip thread!\n");
            return Status;
        }

        return PNIO_OK;
}

int profip_write_record(uint32_t slot, uint32_t index, uint8_t* buff, uint32_t size)
{
	if (index == RECORD_INDEX_NANOFIPNODE_PARAM)
		return profip_add_wf_node(slot, buff, size);

	if (index == RECORD_INDEX_RESET_VAR)
	{
		mf_handler_write_reset_var(slot);
		return 0;
	}

	pf_err(PF_ERR_MODULE_PROFIP, PROFIP_ERR_WRONG_PARAM_INDEX_FOR_NODE, index, "Wrong param index for nanofip node\n");
	return PROFIP_ERR_WRONG_PARAM_INDEX_FOR_NODE;
}

int profip_read_record(uint32_t slot, uint32_t index, uint8_t* buff, uint32_t size)
{
	memset(buff, 0, size);

	if (index != RECORD_INDEX_GET_IDENT_VAR)
		return PROFIP_ERR_WRONG_PARAM_INDEX_FOR_NODE;

	return mf_handler_get_ident_var(slot, buff, size);
}

static int profip_add_wf_node(uint32_t slot, uint8_t* buff, uint32_t size)
{
	if (size != 9) 
		return pf_err(PF_ERR_MODULE_PROFIP, PROFIP_ERR_WF_CONFIG_WRONG_SIZE, size, "Wrong parameter size\n");

	uint8_t slot_idx = SLOT_IDX(slot);
	uint8_t agt_id = buff[4];
	uint8_t cons_bsz = buff[5];
	uint8_t prod_bsz = buff[6];
	uint8_t enable_ident = buff[7];
	uint8_t enable_reset = buff[8];

	if(pf_config.state != PROFIP_STATE_CONNECTING)
		return pf_err(PF_ERR_MODULE_PROFIP, PROFIP_ERR_WF_CONFIG_ALREADY_DONE, pf_config.state, "WorldFIP configuration is already done\n");

	/* Check magic number */
	if(((uint32_t*)buff)[0] != PROFIP_NANOFIPNODE_PARAM_MAGIC_NUMBER)
		pf_err(PF_ERR_MODULE_PROFIP, PROFIP_ERR_WRONG_MAGIC_NUMBER, (((uint32_t*)buff)[0]), "Wrong magic number\n");

	if(slot_idx >= FIP_MAX_NBR_OF_NODES)
		return pf_err(PF_ERR_MODULE_PROFIP, PROFIP_ERR_WRONG_SLOT_NUMBER, slot, "Wrong slot number\n");

	pf_config.mf_cfg.agts[slot_idx].agt_idx = agt_id;
	pf_config.mf_cfg.agts[slot_idx].prod_bsz = prod_bsz;
	pf_config.mf_cfg.agts[slot_idx].cons_bsz = cons_bsz;
	pf_config.mf_cfg.agts[slot_idx].enable_ident = enable_ident;
	pf_config.mf_cfg.agts[slot_idx].enable_reset = enable_reset;

	pf_config.mf_cfg.agts[slot_idx].slot = slot;

	pf_config.mf_cfg.agts[slot_idx].is_initialized = 1U;

	return 0;
}

void profip_set_wf_config_done(void)
{
	tmp_flag_config_done = 1U;
}

// profinet 
uint8_t profip_copy_pf_to_pnStack(uint32_t slot, uint8_t* buff, uint32_t size)
{
	static enum nf_node_status prev_cons_status[FIP_MAX_NBR_OF_NODES];

	enum nf_node_status status = mf_handler_read_var(slot, buff, size);

	// Don't set alarms if ProFIP is not running
	if(pf_config.state != PROFIP_STATE_RUNNING || pf_config.next_state != PROFIP_STATE_RUNNING)
	{
		status = NF_NODE_DATA_OK;
	}

	if(prev_cons_status[SLOT_IDX(slot)] != status)
	{
		switch(prev_cons_status[SLOT_IDX(slot)])
		{
		case NF_NODE_DATA_OK:
			// do nth
			break;
		case NF_NODE_DATA_FRAME_ERROR:
			pf_alarm_reset(PF_ALARM_DATA_FRAME_ERROR, slot);
			break;
		case NF_NODE_DATA_PAYLOAD_ERROR:
			pf_alarm_reset(PF_ALARM_PAYLOAD_ERROR, slot);
			break;
		case NF_NODE_DATA_NOT_RECEIVED:
			pf_alarm_reset(PF_ALARM_DATA_NOT_RECEIVED, slot);
			break;
		case NF_NODE_DATA_EXPIRED:
			pf_alarm_reset(PF_ALARM_NO_CONS_DATA, slot);
			break;
		default:
			pf_alarm_reset(PF_ALARM_RUNTIME_SW_PROBLEM, slot);
			break;
		}

		switch(status)
		{
		case NF_NODE_DATA_OK:
			// do nth
			break;
		case NF_NODE_DATA_FRAME_ERROR:
			pf_alarm_set(PF_ALARM_DATA_FRAME_ERROR, slot);
			break;
		case NF_NODE_DATA_PAYLOAD_ERROR:
			pf_alarm_set(PF_ALARM_PAYLOAD_ERROR, slot);
			break;
		case NF_NODE_DATA_NOT_RECEIVED:
			pf_alarm_set(PF_ALARM_DATA_NOT_RECEIVED, slot);
			break;
		case NF_NODE_DATA_EXPIRED:
			pf_alarm_set(PF_ALARM_NO_CONS_DATA, slot);
			break;
		default:
			pf_alarm_set(PF_ALARM_RUNTIME_SW_PROBLEM, slot);
			break;
		}
	}

	prev_cons_status[SLOT_IDX(slot)] = status;

	return PNIO_S_GOOD;
}

// return IOCS
uint8_t profip_copy_pnStack_to_pf(uint32_t slot, uint8_t* buff, uint32_t size)
{
	static enum nf_node_status prev_prod_status[FIP_MAX_NBR_OF_NODES];

	enum nf_node_status status = mf_handler_write_var(slot, buff, size);

	// Don't set alarms if ProFIP is not running
	if(pf_config.state != PROFIP_STATE_RUNNING || pf_config.next_state != PROFIP_STATE_RUNNING)
	{
		status = NF_NODE_DATA_OK;
	}

	if(prev_prod_status[SLOT_IDX(slot)] != status)
	{
		switch(prev_prod_status[SLOT_IDX(slot)])
		{
		case NF_NODE_DATA_OK:
			// do nth
			break;
		case NF_NODE_DATA_EXPIRED:
			pf_alarm_reset(PF_ALARM_NO_PROD_DATA, slot);
			break;
		default:
			pf_alarm_reset(PF_ALARM_RUNTIME_SW_PROBLEM, slot);
			break;
		}

		switch(status)
		{
		case NF_NODE_DATA_OK:
			// do nth
			break;
		case NF_NODE_DATA_EXPIRED:
			pf_alarm_set(PF_ALARM_NO_PROD_DATA, slot);
			break;
		default:
			pf_alarm_set(PF_ALARM_RUNTIME_SW_PROBLEM, slot);
			break;
		}
	}

	prev_prod_status[SLOT_IDX(slot)] = status;

	return PNIO_S_GOOD;
}

void profip_pn_disconnected(void)
{
	tmp_flag_disconnected = 1U;
}

uint8_t profip_pn_var_init(void)
{
	return 0;
}

void profip_print_mcycle(void)
{
	mf_handler_print_macrocycle();
}

void profip_print_status(void)
{
	PROFIP_LOG("ProFIP state:       %s\n", profip_state_names[pf_config.state]);
	mf_handler_print_status();
	mf_handler_print_config();
}

void profip_print_alarms(void)
{
	pf_alarm_print();
}

void profip_print_presence(void)
{
	mf_handler_print_presence_list();
}

void profip_print_mfip_version(void)
{
	mf_handler_print_version();
}

void profip_print_nf_nodes(void)
{
	mf_handler_print_nodes();
}

void profip_print_slots(void)
{
	mf_handler_print_slots();
}

void profip_reset_masterFip(void)
{
	tmp_flag_reset_masterfip = 1U;
}

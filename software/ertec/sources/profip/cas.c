// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "cas.h"
#include "profip_log.h"
#include "pniobase.h"
#include "pf_err.h"
#include "os.h"
#include "profip_log.h"


/* internal error codes */
#define cas_ERR_COPY_TO_APPL -1
#define cas_ERR_COPY_FROM_APPL -2
#define cas_ERR_WRONG_INDEX_OR_SIZE -3
#define cas_ERR_WRONG_MAGIC_NUMBER -4

#define cas_PARAM_MAGIC_NUMBER 0xAABCDEFF

union pf_control_t
{
	uint8_t bytes[PF_CONTROL_SIZE];
	struct
	{
		uint8_t reset; /* reset whole profip (by WDG) */
		uint8_t reset_masterfip;
		uint8_t stop_macrocycle;
		uint8_t reserved_0;
		uint8_t clear_faults;
		uint8_t clear_nodes_faults;
		uint8_t tcp_reprogramming; ///< start 'flash programming over TCP'
	};
};

struct pf_init_param_t
{
	uint8_t init_done;
	uint16_t turnaround_time_us;
	uint32_t wait_wind_time_us;
	uint32_t aper_var_wind_time_us;
	uint8_t diag_enabled;
};

/*warn: write values in big-endianess*/
union pf_status_t
{
	uint8_t bytes[PF_CONTROL_SIZE];
	struct
	{
		uint8_t profip_status; 			// 0
		uint8_t masterfip_status; 		// 1
		uint8_t nbr_of_prod_variables;  // 2
		uint8_t nbr_of_cons_variables;	// 3

		uint16_t fip_cycle_cnt;			// 4 - 5
		uint16_t turnaround_time_us;	// 6 - 7

		uint32_t wait_wind_time_us;		// 8 - 11
		uint32_t aper_var_wind_time_us;	// 12 - 15

		uint8_t irq_thread_tick;		// 16
		uint8_t pf_thread_tick;			// 17
		uint8_t bus_speed;				// 18
		uint8_t tcp_reprogramming_enabled;// 19

		uint8_t reserved; //20
		uint8_t profip_version[3];		//21 - 23

		uint32_t git_version;           //24 - 27
		uint32_t mfip_git_vers;			//28 - 31
	};
};

static union pf_status_t pf_status;
static union pf_control_t pf_control = {.reset = 1};
static union pf_control_t pf_control_old = {.reset = 1};
static struct pf_init_param_t pf_init_param;
static struct pf_init_param_t pf_init_param_old;

/* semaphores */
static uint32_t cas_status_sem;

void cas_init(void)
{
    /* Initialize semaphores */
    OsAllocSemB(&cas_status_sem);
    OsGiveSemB(cas_status_sem);

	pf_status.profip_status = 0U;
	pf_status.masterfip_status = 0U;
	pf_status.nbr_of_cons_variables = 0U;
	pf_status.nbr_of_prod_variables = 0U;

	pf_status.profip_version[0] = PROFIP_VERSION_MAJOR;
	pf_status.profip_version[1] = PROFIP_VERSION_MINOR;
	pf_status.profip_version[2] = PROFIP_VERSION_PATCH;

	pf_status.git_version = CHANGE_ENDIANESS_32(GIT_VERSION);
}

uint8_t cas_copy_to_pnStack(uint8_t* buff, uint32_t size)
{
	if(PF_CONTROL_SIZE != size)
	{
		pf_err(PF_ERR_MODULE_CAS, cas_ERR_COPY_FROM_APPL, size, "Error during copy cas to pnStack. Wrong size\n");
	}
	else
	{
		OsTakeSemB(cas_status_sem);

		memcpy(buff, pf_status.bytes, PF_STATUS_SIZE);

		OsGiveSemB(cas_status_sem);
	}

	return PNIO_S_GOOD;
}

uint8_t cas_copy_from_pnStack(uint8_t* buff, uint32_t size)
{
	if(PF_STATUS_SIZE != size)
	{
		pf_err(PF_ERR_MODULE_CAS, cas_ERR_COPY_TO_APPL, size, "Error during copy cas from pnStack. Wrong size\n");
	}
	else
	{
		memcpy(pf_control.bytes, buff, PF_CONTROL_SIZE);
	}

	return PNIO_S_GOOD;
}

int cas_write_record(uint32_t index, uint8_t* buff, uint32_t size)
{
	if(index != 11 || size != 17)
		return pf_err(PF_ERR_MODULE_CAS, cas_ERR_WRONG_INDEX_OR_SIZE, size, "Wrong index or size in cas pf parameter\n");

	if(((uint32_t*)buff)[0] == cas_PARAM_MAGIC_NUMBER)
		return pf_err(PF_ERR_MODULE_CAS, cas_ERR_WRONG_MAGIC_NUMBER, (((uint32_t*)buff)[0]), "Wrong magic code in cas pf parameter\n");

	memcpy(&pf_init_param_old, &pf_init_param, sizeof(pf_init_param));

	pf_init_param.turnaround_time_us = CHANGE_ENDIANESS_16(*((uint16_t*)(buff+4)));
	pf_init_param.wait_wind_time_us = CHANGE_ENDIANESS_32(*((uint32_t*)(buff+8)));
	pf_init_param.aper_var_wind_time_us = CHANGE_ENDIANESS_32(*((uint32_t*)(buff+12)));
	pf_init_param.diag_enabled = *((uint8_t*)(buff+16));
	pf_init_param.init_done = 1U;

	PROFIP_LOG(__OK__"Initial CAS parameters received\n");

	return 0;
}

void cas_set_profip_status(uint8_t profip_status)
{
	OsTakeSemB(cas_status_sem);
	pf_status.profip_status = profip_status;
	OsGiveSemB(cas_status_sem);
}

void cas_set_masterfip_status(uint8_t masterfip_status)
{
	OsTakeSemB(cas_status_sem);
	pf_status.masterfip_status = masterfip_status;
	OsGiveSemB(cas_status_sem);
}

void cas_set_nbr_of_prod_variables(uint8_t nbr_of_prod_variables)
{
	OsTakeSemB(cas_status_sem);
	pf_status.nbr_of_prod_variables = nbr_of_prod_variables;
	OsGiveSemB(cas_status_sem);
}

void cas_set_nbr_of_cons_variables(uint8_t nbr_of_cons_variables)
{
	OsTakeSemB(cas_status_sem);
	pf_status.nbr_of_cons_variables = nbr_of_cons_variables;
	OsGiveSemB(cas_status_sem);
}

void cas_set_bus_speed(uint8_t bus_speed)
{
	OsTakeSemB(cas_status_sem);
	pf_status.bus_speed = bus_speed;
	OsGiveSemB(cas_status_sem);
}

void cas_set_turnaround_time_us(uint16_t turnaround_time_us)
{
	OsTakeSemB(cas_status_sem);
	pf_status.turnaround_time_us = CHANGE_ENDIANESS_16(turnaround_time_us);
	OsGiveSemB(cas_status_sem);
}

void cas_set_wait_wind_time_us(uint32_t wait_wind_time_us)
{
	OsTakeSemB(cas_status_sem);
	pf_status.wait_wind_time_us = CHANGE_ENDIANESS_32(wait_wind_time_us);
	OsGiveSemB(cas_status_sem);
}

void cas_set_aper_var_wind_time_us(uint32_t aper_var_wind_time_us)
{
	OsTakeSemB(cas_status_sem);
	pf_status.aper_var_wind_time_us = CHANGE_ENDIANESS_32(aper_var_wind_time_us);
	OsGiveSemB(cas_status_sem);
}

void cas_set_irq_thread_tick(uint8_t irq_thread_tick)
{
	OsTakeSemB(cas_status_sem);
	pf_status.irq_thread_tick = irq_thread_tick;
	OsGiveSemB(cas_status_sem);
}

void cas_set_pf_thread_tick(uint8_t pf_thread_tick)
{
	OsTakeSemB(cas_status_sem);
	pf_status.pf_thread_tick = pf_thread_tick;
	OsGiveSemB(cas_status_sem);
}

void cas_set_fip_cycle_cnt(uint16_t fip_cycle_cnt)
{
	OsTakeSemB(cas_status_sem);
	pf_status.fip_cycle_cnt = CHANGE_ENDIANESS_16(fip_cycle_cnt);
	OsGiveSemB(cas_status_sem);
}

void cas_set_tcp_reprogramming_enabled(uint8_t tcp_reprogramming_enabled)
{
	OsTakeSemB(cas_status_sem);
	pf_status.tcp_reprogramming_enabled = tcp_reprogramming_enabled;
	OsGiveSemB(cas_status_sem);
}

void cas_set_mfip_git_vers(uint32_t mfip_git_vers)
{
	OsTakeSemB(cas_status_sem);
	pf_status.mfip_git_vers = CHANGE_ENDIANESS_32(mfip_git_vers);
	OsGiveSemB(cas_status_sem);
}

uint8_t cas_get_init_done(void)
{
	return pf_init_param.init_done;
}

uint8_t cas_get_diag_enabled(void)
{
	return pf_init_param.diag_enabled;
}

uint16_t cas_get_turnaround_time_us(void)
{
	return pf_init_param.turnaround_time_us;
}

uint32_t cas_get_wait_wind_time_us(void)
{
	return pf_init_param.wait_wind_time_us;
}

uint32_t cas_get_aper_var_wind_time_us(void)
{
	return pf_init_param.aper_var_wind_time_us;
}

uint8_t cas_init_params_changed(void)
{
	if(memcmp(&pf_init_param, &pf_init_param_old, sizeof(pf_init_param)) == 0)
	{
		return 0;
	}

	return 1;
}


uint8_t cas_cmd_reset(void)
{
	uint8_t rc = 0U;

	if(pf_control.reset && !pf_control_old.reset)
	{
		rc = 1U;
	}

	pf_control_old.reset = pf_control.reset;

	return rc;
}

uint8_t cas_cmd_reset_masterfip(void)
{
	uint8_t rc = 0U;

	if(pf_control.reset_masterfip && !pf_control_old.reset_masterfip)
	{
		rc = 1U;
	}

	pf_control_old.reset_masterfip = pf_control.reset_masterfip;

	return rc;
}

uint8_t cas_cmd_stop_macrocycle(void)
{
	return pf_control.stop_macrocycle;
}

uint8_t cas_cmd_clear_faults(void)
{
	uint8_t rc = 0U;

	if(pf_control.clear_faults && !pf_control_old.clear_faults)
	{
		rc = 1U;
	}

	pf_control_old.clear_faults = pf_control.clear_faults;

	return rc;
}

uint8_t cas_cmd_clear_nodes_faults(void)
{
	uint8_t rc = 0U;

	if(pf_control.clear_nodes_faults && !pf_control_old.clear_nodes_faults)
	{
		rc = 1U;
	}

	pf_control_old.clear_nodes_faults = pf_control.clear_nodes_faults;

	return rc;
}

uint8_t cas_cmd_tcp_reprogramming(void)
{
	uint8_t rc = 0U;

	if(pf_control.tcp_reprogramming && !pf_control_old.tcp_reprogramming)
	{
		rc = 1U;
	}

	pf_control_old.tcp_reprogramming = pf_control.tcp_reprogramming;

	return rc;
}

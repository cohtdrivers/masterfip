// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "profip_led.h"
#include "profip_gpio.h"
#include "lsa_cfg.h"
#include "lsa_sys.h"
#include "os.h"

#include <stdint.h>

static uint16_t LedTimerID;

static uint8_t led_blink[3];

static void led_blinking_tim_cbk(void)
{
	static int cnt = 0;

	int i;

	for(i = 0; i<3; i++)
	{
		if(led_blink[i] == 0)
			continue;

		if(cnt % led_blink[i] == 0)
			profip_led_toggle(i);
	}

	cnt++;
}

/*
 * Gpio's positions are defined in profip GPIO module
 * gpio16 - led yellow
 * gpio17 - led green
 * gpio18 - led red
 * */
profip_gpio_T led_map[] =
{
		/*PROFIP_LED_YELLOW*/ PROFIP_GPIO_LED_YELLOW,
		/*PROFIP_LED_GREEN*/ PROFIP_GPIO_LED_RED,
		/*PROFIP_LED_RED*/ PROFIP_GPIO_LED_GREEN,
};

void profip_led_init(void)
{
	int i;

	for(i = 0; i<3; i++)
	{
		led_blink[i] = 0;
	}

	OsAllocTimer(&LedTimerID, LSA_TIMER_TYPE_CYCLIC ,LSA_TIME_BASE_10MS ,led_blinking_tim_cbk);

	uint16_t freq = 16; /*Hz*/
	OsStartTimer(LedTimerID, 0, 100 / freq);
}

void profip_led_blink(profip_led_T led, uint8_t mode)
{
	led_blink[led] = mode;
}

void profip_led_on(profip_led_T led)
{
	profip_gpio_write(led_map[led], 1U);
	led_blink[led] = 0;
}

void profip_led_off(profip_led_T led)
{
	profip_gpio_write(led_map[led], 0U);
	led_blink[led] = 0;
}

void profip_led_toggle(profip_led_T led)
{
	uint8_t val = profip_gpio_read(led_map[led]);
	profip_gpio_write(led_map[led], 1 - val);
}

uint8_t profip_led_get(profip_led_T led)
{
	return (led_blink[led] || profip_gpio_read(led_map[led]));
}

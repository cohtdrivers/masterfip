// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "../profip_gpio/profip_gpio.h"
#include <stdint.h>
#include "ertec200p_reg.h"

#define PROFIP_GPIO_LEN(tab) (sizeof(tab) / sizeof(tab[0]))

const profip_gpio_cfg_T profip_gpio_cfg[] =
{
		{0, 16, PROFIP_GPIO_DIR_OUT, PROFIP_GPIO_GPIO, 0U}, /*LED_YELLOW*/
		{0, 17, PROFIP_GPIO_DIR_OUT, PROFIP_GPIO_GPIO, 0U}, /*LED_RED*/
		{0, 18, PROFIP_GPIO_DIR_OUT, PROFIP_GPIO_GPIO, 0U}, /*LED_GREEN*/

		{2, 7, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U},  /* STATUS_C_0_RMQ_0 */
		{2, 8, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U},  /* STATUS_C_0_RMQ_1 */
		{2, 9, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U},  /* STATUS_C_0_RMQ_2 */
		{2, 10, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U}, /* STATUS_C_0_RMQ_3 */
		{2, 11, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U}, /* STATUS_C_0_RMQ_4 */
		{2, 12, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U}, /* STATUS_C_0_RMQ_5 */
		{2, 13, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U}, /* STATUS_C_1_RMQ_0 */

		{2, 6, PROFIP_GPIO_DIR_OUT, PROFIP_GPIO_GPIO, 1U} /* IP_RESET */
		,{2, 5, PROFIP_GPIO_DIR_OUT, PROFIP_GPIO_GPIO, 1U}  /* MT_CPU_RESET */
		,{2, 4, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_GPIO, 0U}  /* MT_CPU_RESET_LOOPBACK */

		,{1,22, PROFIP_GPIO_DIR_OUT, PROFIP_GPIO_ALT3, 0U}  /* DATA_UART_TX */
		,{1,23, PROFIP_GPIO_DIR_IN, PROFIP_GPIO_ALT3, 0U}  /* DATA_UART_RX */
};


void profip_gpio_init(void)
{
	uint32_t IOCTRL, PORT_MODE_H, PORT_MODE_L, PORT_MODE, PORT_OUT;
	uint32_t i, offset;

	for(i=0; i<PROFIP_GPIO_LEN(profip_gpio_cfg); i++)
	{
		offset = profip_gpio_cfg[i].port * 0x20;
		IOCTRL = U_GPIO__GPIO_IOCTRL_0 + offset;
		PORT_MODE_H = U_GPIO__GPIO_PORT_MODE_0_H + offset;
		PORT_MODE_L = U_GPIO__GPIO_PORT_MODE_0_L + offset;
		PORT_OUT = U_GPIO__GPIO_OUT_0 + offset;

		//set direction
		REG32(IOCTRL) &= ~( 1U << profip_gpio_cfg[i].pin );
		REG32(IOCTRL) |= ( (profip_gpio_cfg[i].dir) << profip_gpio_cfg[i].pin );

		//set mode
		PORT_MODE = (profip_gpio_cfg[i].pin >= 16) ? PORT_MODE_H : PORT_MODE_L;
		REG32(PORT_MODE) &= ~(3U << ((profip_gpio_cfg[i].pin & 0xF) * 2));
		REG32(PORT_MODE) |= (profip_gpio_cfg[i].alt << ((profip_gpio_cfg[i].pin & 0xF) * 2));

		//clear or set the outputs
		if(profip_gpio_cfg[i].dir == PROFIP_GPIO_DIR_OUT && profip_gpio_cfg[i].alt == PROFIP_GPIO_GPIO)
		{
			if(profip_gpio_cfg[i].init_val == 0)
			{
				REG32(PORT_OUT) &= ~(1U << profip_gpio_cfg[i].pin);
			}
			else
			{
				REG32(PORT_OUT) |= (1U << profip_gpio_cfg[i].pin);
			}
		}
	}
}

uint8_t profip_gpio_read(profip_gpio_T gpio)
{
	uint32_t offset = profip_gpio_cfg[gpio].port * 0x20;
	uint32_t PORT_IN = U_GPIO__GPIO_IN_0 + offset;
	return ((REG32(PORT_IN) & (1 << profip_gpio_cfg[gpio].pin)) != 0) ? 1U : 0U;
}

void profip_gpio_write(profip_gpio_T gpio, uint8_t val)
{
	uint32_t offset = profip_gpio_cfg[gpio].port * 0x20;
	uint32_t PORT_OUT = U_GPIO__GPIO_OUT_0 + offset;

	if (0U == val)
	{
		REG32(PORT_OUT) &= ~(1U << profip_gpio_cfg[gpio].pin);
	}
	else
	{
		REG32(PORT_OUT) |= (1U << profip_gpio_cfg[gpio].pin);
	}
}

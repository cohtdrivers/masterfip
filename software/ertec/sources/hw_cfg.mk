# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

#width (in words) of the RMQ entry
DEFD += -DRMQ_WIDTH=128

#maximum number of nodes connected to the worldFIP bus
DEFD += -DFIP_MAX_NBR_OF_NODES=30
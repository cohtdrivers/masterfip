#include "masterfip-mq.h"
#include <stdint.h>

#ifdef MFIP_FOR_PROFIP

void mstrfip_add_ctrlnbr(struct trtl_fw_msg *msg)
{
	uint32_t ctrlnbr = 0; 

	if (msg->header->len > 0) {
		ctrlnbr  = ((uint32_t*)msg->payload)[0] ^ 0xAAAAAAAA;
		ctrlnbr += ((uint32_t*)msg->payload)[msg->header->len - 1] ^ 0x55555555;
	}

	memcpy(((uint8_t*)msg->payload) + (msg->header->len * 4), &ctrlnbr, sizeof(ctrlnbr));
	msg->header->len++;
}

#endif

/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __MASTERFIP_SMEM_H
#define __MASTERFIP_SMEM_H

#include "masterfip-common-priv.h"

#define MSTRFIP_SMEM_BUF_WSZ 8192

extern SMEM uint32_t volatile smem_buf[];

extern void rt_smem_lock();
extern void rt_smem_unlock();

#endif /* __MASTERFIP_SMEM_H */

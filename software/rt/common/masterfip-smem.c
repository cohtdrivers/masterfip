/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "mockturtle-rt.h"
#include "masterfip-smem.h"

/*
 * allocate the full smem space
 * At run time, when the application defines the macro cycle, the smem buffer is
 * dynamically structured to fit the ba programmation.
 */
SMEM uint32_t volatile smem_buf[MSTRFIP_SMEM_BUF_WSZ];

/*
 * Simple mutex mechanism
 */
void rt_smem_lock(uint32_t *smem_mtx)
{
	/*
	 * wait until another CPU releases the mutex by writing 0 to the
	 * corresponding memory cell.
	 */
	while(smem_atomic_test_and_set((int *)smem_mtx))
		;
}

void rt_smem_unlock(uint32_t *smem_mtx)
{
	/*
	 * releases the mutex
	 */
	*smem_mtx = 0;
}

int rt_mtx_timedlock(uint32_t ticks)
{
	/*TODO : timed lock implementation ....*/
	return -1;
}

/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"


static inline int mstrfip_handle_tx_msg(int new_cycle,
			struct smem_aper_msg volatile *aper_msg, uint32_t *rptr)
{
	uint32_t dest, mcycle_time;

	/*
	 * Enough time to schedule a message transaction. It's include
	 * ID_MSG + RP_MSG + RP_FIN frames
	 */
	/* rptr[3]:destination is encoded in the three more significant byte*/
	dest = rptr[3] & 0xFFFF;
	mstrfip_write_question_tx_reg(MSTRFIP_ID_MSG, dest);

	if (new_cycle) {
		if (mstrfip_wait_macrocyc_counter() == MSTRFIP_MACROCYC_ABORT)
			return MSTRFIP_MACROCYC_ABORT;
	}
	else {
		mstrfip_wait_trtime();
	}

	mstrfip_start_tx(2);
	if (smem_data->record.cfg.flg) {
		mcycle_time = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
			      MASTERFIP_REG_MACROCYC_TIME_CNT);
		mstrfip_record(mcycle_time, new_cycle,
			       ba->report.cycle_state,
			       MSTRFIP_ID_MSG,
			       dest, 0, MSTRFIP_EVTREC_DIR_PROD);
	}
	/* critical section is kept to a minimum */
	rt_smem_lock(&smem_data->smem_mtx); /* lock SMEM */
	/* payload's message starts at rptr[3] */
	mstrfip_write_frame_tx_reg(&rptr[3], MSTRFIP_RP_MSG_NOACK, rptr[0]);
	--aper_msg->payload_count;
	aper_msg->r_idx = (aper_msg->r_idx + 1) % aper_msg->payload_buf_sz;
	rt_smem_unlock(&smem_data->smem_mtx); /* unlock SMEM */
	/* wait end of sending question frame */
	mstrfip_wait_end_of_tx(2);
	mstrfip_wait_trtime();
	mstrfip_start_tx(rptr[1]);
	if (smem_data->record.cfg.flg) {
		mcycle_time = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
			      MASTERFIP_REG_MACROCYC_TIME_CNT);
		mstrfip_record(mcycle_time, 0,
			       ba->report.cycle_state,
			       MSTRFIP_RP_MSG_NOACK,
			       dest, 0, MSTRFIP_EVTREC_DIR_PROD);
	}
	mstrfip_wait_end_of_tx(rptr[1]);

	mstrfip_write_msg_rpfin_tx_reg();
	mstrfip_wait_trtime();
	mstrfip_start_tx(0);
	if (smem_data->record.cfg.flg) {
		mcycle_time = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
			      MASTERFIP_REG_MACROCYC_TIME_CNT);
		mstrfip_record(mcycle_time, 0,
			       ba->report.cycle_state,
			       MSTRFIP_RP_FIN,
			       dest, 0, MSTRFIP_EVTREC_DIR_PROD);
	}
	mstrfip_wait_end_of_tx(0);
	return 0;
}

static inline int mstrfip_handle_rx_msg(int new_cycle, uint32_t global_irq)
{
	uint32_t var_id, mcycle_time, payload_wsz_org;
	int res_msg, res_rpfin, send_ack, expect_rpfin;
	struct rx_frame_ctx frame;

	/*
	 * Enough time to schedule a message transaction. It's include
	 * ID_MSG + RP_MSG + RP_FIN frames
	 */
	/* destination is encoded in the three more significant byte*/
	var_id = ba->msg.entries[ba->msg.r_idx];
	mstrfip_write_question_tx_reg(MSTRFIP_ID_MSG, var_id);

	if (new_cycle) {
		if (mstrfip_wait_macrocyc_counter() == MSTRFIP_MACROCYC_ABORT)
			return MSTRFIP_MACROCYC_ABORT;
	}
	else {
		mstrfip_wait_trtime();
	}

//	dp_writel(0x00, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED_DBG);
	mstrfip_start_tx(2);
	if (smem_data->record.cfg.flg) {
		mcycle_time = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
			      MASTERFIP_REG_MACROCYC_TIME_CNT);
		mstrfip_record(mcycle_time, new_cycle,
			       ba->report.cycle_state,
			       MSTRFIP_ID_MSG,
			       var_id, 0, MSTRFIP_EVTREC_DIR_PROD);
	}
	mstrfip_wait_end_of_tx(2);
	/*
	 * TODO: handling agent request for message we should know if the agent
	 * expects a NO_ACK or an ACK  message. to be completed....
	 */
	frame.payload_bsz = MSTRFIP_MSG_PAYLOAD_MAX_DATA_BSZ +
			    MSTRFIP_MSG_PAYLOAD_HDR_BSZ;
	frame.payload_wsz = MSTRFIP_MSG_PAYLOAD_MAX_WSZ;
	frame.var_id = 0;
	frame.key = var_id & 0xFF; /* keep agent addr as key */
	frame.pdu_type = MSTRFIP_PDU_NONE;
	frame.report_tmo = 1;
	payload_wsz_org = frame.payload_wsz;
	res_msg = mstrfip_handle_rx_frame(MSTRFIP_HMQ_O_APP_APER_MSG, &frame, 0);

	/* TODO: this PAIR/IMPAIR need to be understood. We expect to have a
	 * single control byte MSTRFIP_RP_MSG_ACK */
	send_ack = 0;
	expect_rpfin = 1;

	if( res_msg == MSTRFIP_FRAME_OK )
	 	payload_wsz_org = (frame.acq_payload_bsz / 4) + (!!(frame.acq_payload_bsz % 4));

	if ((frame.acq_ctrl_byte == MSTRFIP_RP_MSG_ACK_EVEN) ||
	    (frame.acq_ctrl_byte == MSTRFIP_RP_MSG_ACK_ODD))
		send_ack = 1; /* agent expects an ACK from master */
	if (send_ack && (res_msg != MSTRFIP_FRAME_OK)) {
		/* frame was wrong no ack from the master and as a consequence
		 * master will not wait for RP_FIN
		 */
		send_ack = 0;
		expect_rpfin = 0;
	}
	if (send_ack) { /* master has to send a proper RP_MSG_ACK */
		mstrfip_write_msg_rpack_tx_reg(frame.acq_ctrl_byte);
		mstrfip_wait_trtime();
		mstrfip_start_tx(0);
		if (smem_data->record.cfg.flg) {
			mcycle_time = ba->cycle.nticks -
				      dp_readl(MASTERFIP_CSR_BASE +
			      	      MASTERFIP_REG_MACROCYC_TIME_CNT);
			mstrfip_record(mcycle_time, 0,
				       ba->report.cycle_state,
				       frame.acq_ctrl_byte,
				       var_id, 0, MSTRFIP_EVTREC_DIR_PROD);
		}
		mstrfip_wait_end_of_tx(0);
	}
	/*
	 * RP_FIN to end transaction: payload ignored, just nbytes is checked
	 * Even if RX message frame was not OK, master waits for RP_FIN only for
	 * RP_MSG_NO_ACK. There is no point to wait for RP_FIN from an agent if
	 * the master didn't ack the message because it had errors.
	 * We Keep meessage original payload wsz in case something goes wrong
	 * and invalidation of the data entry is required
	 */

	if (expect_rpfin) { /* wait for RP_FIN from client */
		frame.payload_bsz = 3 + 2;
		frame.payload_wsz = 2;
		frame.pdu_type = MSTRFIP_PDU_NONE;
		frame.report_tmo = 1;
		/* Wait for RX RP_FIN*/
		res_rpfin = mstrfip_handle_rx_frame(MSTRFIP_HMQ_O_UNDEFINED,
						    &frame, 0);
		/* we check if we receive a true RP_FIN */
		if ((res_rpfin == MSTRFIP_FRAME_OK) &&
		    (frame.acq_ctrl_byte != MSTRFIP_RP_FIN))
			res_rpfin = MSTRFIP_FRAME_BAD_CTRL;
	}
	/*
	 * if res_msg is not MSTRFIP_FRAME_OK entry is alredy invalidated and
	 * error is already reported. If res_msg is OK but rp_fin failed the
	 * entry should be invalidated.
	 */
	if ((res_msg == MSTRFIP_FRAME_OK) && (res_rpfin != MSTRFIP_FRAME_OK)) {
		/*
		 * RP_FIN is not a valid frame or CTRL type is not RP_FIN:
		 * Even if the message frame has been received properly, the
		 * transaction is invalidated.
		 */
		mstrfip_hmq_invalidate_last_data_entry(
		     MSTRFIP_HMQ_O_APP_APER_MSG, payload_wsz_org, res_rpfin, 1);

#ifdef RTDEBUG_RX_FRAME_1
		pr_debug(__FILE__":%d %d", __LINE__, res_rpfin);
#endif
	}
	ba->msg.r_idx = (ba->msg.r_idx + 1) % ba->msg.entries_sz;
	--ba->msg.entries_count;
	/* rx aperiodic message is always immediately reported */
	if (!global_irq)
		mstrfip_hmq_post_irq(MSTRFIP_HMQ_O_APP_APER_MSG, frame.key,
						MSTRFIP_DATA_FLAGS_CONS, 1);
	return 0;
}

inline void mstrfip_do_ba_aper_msg_wind(int new_cycle,
	struct mstrfip_ba_aper_msg_wind_args *args)
{
	struct smem_aper_msg volatile *aper_msg = &smem_data->aper_msg;
	uint32_t now_ticks, fix_ticks, msg_ticks, bsz;
	uint32_t cons_msg_count, prod_msg_count;
	uint32_t *rptr, dest_addr, *payload_ptr, *payload_buf;
	/*
	 * macro cycle counter is counting down: end_ticks gives the
	 * counter value when the aper var window ends.
	 */
	uint32_t end_ticks = ba->cycle.nticks - args->ticks_end_time;

	ba->report.cycle_state = MSTRFIP_FSM_RUN_APER_MSG;
	/* fix time to send mesg. The rest depends of the payload size */
	fix_ticks = ba->hw.idfr_ticks + (3 * ba->hw.tr_ticks) +
						ba->hw.rpfin_ticks;
	cons_msg_count = 0;
	prod_msg_count = 0;
	for(;;) { /*pending messages */
		if (ba->msg.entries_count) { /* an agent wants to send a msg */
			/*
			 * msg duration in ticks and check if
			 * it remains enough time
			 */
			msg_ticks = (ba->hw.byte_ticks * 268) + fix_ticks;
			now_ticks = dp_readl( MASTERFIP_CSR_BASE +
						MASTERFIP_REG_MACROCYC_TIME_CNT);
			if (now_ticks < msg_ticks + end_ticks)
				/*
				 * definitevely not enough time to serve the
				 * pending request, we have to leave the loop
				 * and wait for the end of aperiodic window.
				 */
				break; /* not enough time */
			if (mstrfip_handle_rx_msg(new_cycle,
					          args->cons_msg_global_irq) ==
			    MSTRFIP_MACROCYC_ABORT)
			    	return;
			new_cycle = 0; /* not anymore beginning of new cycle */
			++cons_msg_count;
			continue;
		}
		if (aper_msg->payload_count) { /* produced msg */
			payload_buf = (uint32_t *)(aper_msg->payload_buf_addr);
			rptr = (uint32_t *)(payload_buf[aper_msg->r_idx]);
			bsz = rptr[1];
			/* msg duration in ticks and check if it remains enough time*/
			msg_ticks = (ba->hw.byte_ticks * bsz) + fix_ticks;
			now_ticks = dp_readl( MASTERFIP_CSR_BASE +
						MASTERFIP_REG_MACROCYC_TIME_CNT);
			if (now_ticks < msg_ticks + end_ticks) {
				/*
				 * definitevely not enough time to serve the
				 * pending request, we have to leave the loop
				 * and wait for the end of aperiodic window.
				 */
				break; /* not enough time */
			}
//			dp_writel(0x10, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED_DBG);
			if (mstrfip_handle_tx_msg(new_cycle, aper_msg, rptr) ==
			    MSTRFIP_MACROCYC_ABORT)
			    	return;
			new_cycle = 0; /* not anymore beginning of new cycle */
//			dp_writel(0x0, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED_DBG);
			++prod_msg_count;
			if (rptr[2] & MSTRFIP_DATA_FLAGS_IRQ ||
					args->prod_msg_global_irq) {
				dest_addr = (rptr[3] >> 8) & 0xFF;
				/*
				 * insert a data_entry corresponding to the
				 * produced msg: there is no payload, just the
				 * key is used (dest addr)to recognize the mesg
				 */
				mstrfip_hmq_append_data_entry(
						MSTRFIP_HMQ_O_APP_APER_MSG, 0,
						0, dest_addr, &payload_ptr, 0);
				//application is interested to be notified.
				//Register the id of produced msg.
				if (rptr[2] & MSTRFIP_DATA_FLAGS_IRQ) {
					// notify now.
					mstrfip_hmq_post_irq(
						MSTRFIP_HMQ_O_APP_APER_MSG,
						dest_addr,
						MSTRFIP_DATA_FLAGS_PROD, 0);
				}
			}
			continue;
		}
		/* nothing to do: check again if it remains enough time  */
		now_ticks = dp_readl( MASTERFIP_CSR_BASE +
					MASTERFIP_REG_MACROCYC_TIME_CNT);
		/*
		 * no more time: leave early 5us in case we have to
		 * post some irq and then finest wait on tr counter. Nothing
		 * wrong because 5us is not sufficient for a message transaction
		 */
		if ( now_ticks < (end_ticks + ba->hw.tr_ticks +
				  delays_in_cpu_ticks[IDX_5US_DELAY]) )
			break;
	}
	/* end of aper msg wind: check if we have to publish data */
	if (cons_msg_count && args->cons_msg_global_irq)
		mstrfip_hmq_post_irq(MSTRFIP_HMQ_O_APP_APER_MSG, 0, /* global report */
						MSTRFIP_DATA_FLAGS_CONS, 0);
	if (prod_msg_count && args->prod_msg_global_irq) {
		// fill data using r_idx and post irq
	}

	/*
	 * wait end of apermsg-window keeping a margin of tr time + 500 ns
	 * in order to prepare next transaction and rearm tr counter
	 */
	mstrfip_wait_and_rearm_tr(end_ticks + ba->hw.tr_ticks +
				  delays_in_cpu_ticks[IDX_500NS_DELAY]);
}

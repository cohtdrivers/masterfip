/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"

/*
 * read the payload of the given variable from the SMEM and write it into teh
 * TX register. Reset refresh bit in the MPS status.
 * The last step consist to write TX start bit to send the frame
 */
static inline void mstrfip_prepare_tx_rpdat(struct mstrfip_data_hdr *var_hdr, uint32_t payload_wsz)
{
	uint32_t idx = var_hdr->key; /* index of the header in smem */
	struct smem_per_var volatile *per_var = &smem_data->per_var;
	uint32_t smem_pos;
	uint32_t *payload_buf, *ptr;
	uint32_t val, mps_shift_bsz, mask;

	payload_buf = (uint32_t *)(per_var->payload_buf_addr);
	smem_pos = per_var->payload_wsz * idx;
	ptr = &payload_buf[smem_pos];

	rt_smem_lock(&smem_data->smem_mtx); /* lock SMEM */
	mstrfip_write_frame_tx_reg(ptr, MSTRFIP_RP_DAT, payload_wsz);
	/*
	 * locate MPS status and reset refresh bit to indicate that the payload
	 * has been consumed (sent to the agent)
	 */
	val = ptr[payload_wsz - 1];
	mps_shift_bsz = (var_hdr->max_bsz + MSTRFIP_VAR_PAYLOAD_HDR_BSZ) % 4;
	// check if payload is fresh (set by the application since it has been
	// consumed). If not fresh increment tx_mps_status_err.
	if (!(val & (MSTRFIP_MPS_REFRESH_BIT << (mps_shift_bsz * 8))))
		++ba->report.tx_mps_status_err;
	else { // payload is fresh, reset fresh bit.
		mask = 0xffffffff & (~(MSTRFIP_MPS_REFRESH_BIT << (mps_shift_bsz * 8)));
		val &= mask;
		ptr[payload_wsz - 1] = val;
	}
	rt_smem_unlock(&smem_data->smem_mtx); /* unlock SMEM */
}

/* start and stop are indexes in the global var fip array*/
inline void mstrfip_do_ba_per_var_wind(int new_cycle,
				       int start_idx, int stop_idx)
{
	int idx_var;
	uint32_t slot, payload_bsz, payload_wsz;
	struct rx_frame_ctx frame;
	uint32_t mcycle_time;

	ba->report.cycle_state = MSTRFIP_FSM_RUN_PER_VAR;
	for (idx_var = start_idx; idx_var < stop_idx; ++idx_var) {
		mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT,
					      ba->var.hdrs[idx_var].id);
		if (idx_var == start_idx && new_cycle) {
			/*
			 * if ba->hw.ext_trig and new_cycle wait_trtime synchronises
			 * with external pulse
			 */
			if (mstrfip_wait_macrocyc_counter() ==
			    MSTRFIP_MACROCYC_ABORT)
				return;
		}
		else {
			mstrfip_wait_trtime();
		}

		/*
		 * Start sending question frame
		 * bsz = 2 for an ID_DAT. Frame structure
		 * |identifier (2bytes)|
		 */
		mstrfip_start_tx(2);
		if (smem_data->record.cfg.flg) {
			mcycle_time = ba->cycle.nticks - dp_readl(MASTERFIP_CSR_BASE +
								  MASTERFIP_REG_MACROCYC_TIME_CNT);
			mstrfip_record(mcycle_time, new_cycle,
				       ba->report.cycle_state,
				       MSTRFIP_ID_DAT,
				       ba->var.hdrs[idx_var].id, 0,
				       MSTRFIP_EVTREC_DIR_PROD);
		}
		slot = ba->var.hdrs[idx_var].mq_slot;
		/*
		 * MPS_VAR frame format: |Ctrl(1byte)|Paylaod(nbyte)|CRC(2byte)
		 * Payload frame format: |PDUType(1byte)|Length(1Byte|data(nbyte)|MPSStatus(1byte)|
		 * frame_payload_bsz=data_length + PDU + length + MPS status byte
		 * data_bsz=data_length + MPS status byte
		 */
		payload_bsz = ba->var.hdrs[idx_var].max_bsz +
			MSTRFIP_VAR_PAYLOAD_HDR_BSZ +
			MSTRFIP_VAR_MPS_STATUS_BSZ;
		payload_wsz = ((payload_bsz / 4) + (!!(payload_bsz % 4)));
		if (ba->var.hdrs[idx_var].flags & MSTRFIP_DATA_FLAGS_PROD) { /* produced var */
			mstrfip_prepare_tx_rpdat(&ba->var.hdrs[idx_var], payload_wsz);
			mstrfip_wait_end_of_tx(2);
			mstrfip_wait_trtime();
			mstrfip_start_tx(payload_bsz);
			if (smem_data->record.cfg.flg) {
				mcycle_time = ba->cycle.nticks -
					dp_readl(MASTERFIP_CSR_BASE +
						 MASTERFIP_REG_MACROCYC_TIME_CNT);
				mstrfip_record(mcycle_time, 0, //new_cycle cannot start with RP_DAT
					       ba->report.cycle_state,
					       MSTRFIP_RP_DAT,
					       ba->var.hdrs[idx_var].id, 0,
					       MSTRFIP_EVTREC_DIR_PROD);
			}
			mstrfip_wait_end_of_tx(payload_bsz);
		}
		else { /* consumed var */
			mstrfip_wait_end_of_tx(2);
			/* MPS_VAR frame encoding */
			/* |Ctrl(1byte)|Paylaod(nbyte)|CRC(2byte)| */
			/*|PDUType(1byte)|Length(1Byte|data(nbyte)|Status(1byte)|*/
			/* status byte is treated as belonging to the payload*/
			frame.data_bsz = payload_bsz - MSTRFIP_VAR_PAYLOAD_HDR_BSZ;
			frame.payload_bsz = payload_bsz;
			frame.payload_wsz = payload_wsz;
			frame.var_id = ba->var.hdrs[idx_var].id;
			frame.key = ba->var.hdrs[idx_var].key;
			frame.pdu_type = MSTRFIP_PDU_MPS_VAR;
			frame.report_tmo = 1;
			mstrfip_handle_rx_frame(slot, &frame, 0);
		}
		/* check if the produced/consumed var should raise an IRQ */
		if (ba->var.hdrs[idx_var].flags & MSTRFIP_DATA_FLAGS_IRQ)
			mstrfip_hmq_post_irq(slot, ba->var.hdrs[idx_var].key,
					     ba->var.hdrs[idx_var].flags, 0);
		new_cycle = 0; // it's not anymore the begin of a new macro cycle
	}
}

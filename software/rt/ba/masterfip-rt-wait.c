/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle-rt.h>

#include "masterfip-smem.h"
#include "hw/masterfip_hw.h"
#include "masterfip-rt-ba.h"

static inline void do_fd_reset()
{
	dp_writel(0x02, MASTERFIP_CSR_BASE + MASTERFIP_REG_RST); // reset fieldrive
}

/*
 * get Field Drive register to check basically if there was a watchdog a an
 * error during the current macro cycle. If so, the problem is reported and
 * also the HW provides the time in the macro-cycle when such error happened.
 * Then the field drive is reseted
 */
static inline void do_get_fd_regs()
{
	/* TODO: to be implemented */
	uint32_t val;

	/*
	 * fieldrive tx error shoul be read first because this register is
	 * reseted upon a hw_reset in case of fieldrive watchdog.
	 */
	val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_FD_TXER_CNT);
	if (val != 0) {
		ba->report.fd_tx_err += val;
		val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_FD_TXER_TSTAMP);
		ba->report.fd_tx_err_hwtime = val;
		ba->report.fd_tx_err_cycle = ba->report.cycles; /* store the guilty cycle number */
	}
	val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_FD);
	if (val & MASTERFIP_FD_WDG) { /* watch dog is activated */
		val = dp_readl( MASTERFIP_CSR_BASE + MASTERFIP_REG_FD_WDG_TSTAMP); /* time stamp */
		ba->report.fd_tx_watchdog += 1;
		ba->report.fd_tx_watchdog_hwtime = val & 0x7FFFFFFF;
		ba->report.fd_tx_watchdog_cycle = ba->report.cycles; /* store the guilty cycle number */
		do_fd_reset();
	}
}

static inline void do_update_report()
{
	struct mstrfip_report volatile *rep = &smem_data->report;
	uint32_t volatile *ptr_dest = (uint32_t *)rep;
	uint32_t *ptr_src = (uint32_t *)&ba->report;
	uint32_t raw_temp, led_val = 0, nwords, i;

	++ba->report.cycles;
	do_get_fd_regs();
	raw_temp = dp_readl(MASTERFIP_CSR_BASE + MASTERFIP_REG_DS1820_TEMPER);
	ba->report.temp = (raw_temp & 0xFFFF) / 16;

	//update tx/rx leds
	if (ba->report.tx_ok != 0)
		led_val |= MASTERFIP_LED_TX_ACT;
	if (ba->report.tx_err > rep->tx_err)
		led_val |= MASTERFIP_LED_TX_ERR;
	if (ba->report.rx_ok)
		led_val |= MASTERFIP_LED_RX_ACT;
	if (ba->report.rx_err > rep->rx_err)
		led_val |= MASTERFIP_LED_RX_ERR;
	ba->led_reg &= 0x70; // reset the last 4 bits
	ba->led_reg |= led_val; // set rx/tx bits
	dp_writel(ba->led_reg, MASTERFIP_CSR_BASE + MASTERFIP_REG_LED);

	rt_smem_lock(&smem_data->smem_mtx); /* lock SMEM */
	/* memcpy takes two time more (2.2us)than coping field by field (1.3us)*/
//	memcpy((void *)rep, (void *)&mstrfip_report,
//					sizeof(struct mstrfip_report));
	nwords = sizeof(struct mstrfip_report) / 4;
	for (i = 0; i < nwords; ++i)
		ptr_dest[i] = ptr_src[i];
	rt_smem_unlock(&smem_data->smem_mtx); /* unlock SMEM */
	ba->report.tx_ok = 0; /* per macro cycle */
	ba->report.rx_ok = 0; /* per macro cycle */
}

inline void mstrfip_do_ba_wait_wind(int new_cycle, int last_instr,
				    uint32_t silent_flg, uint32_t time_ticks)
{
	/*
	 * macro cycle counter is counting down: end_cycle_ticks gives the
	 * counter value when the wait ends.
	 */
	int end_ticks = ba->cycle.nticks - time_ticks;
	int end_margin_ticks = ba->hw.idfr_ticks + ba->hw.s_ticks;
	int limit_ticks = end_ticks + end_margin_ticks;
	int now_ticks;
	uint32_t val, missed, mcycle_time;

	ba->report.cycle_state = MSTRFIP_FSM_RUN_WAIT;
	if (new_cycle)
		/* we are still in the last turn around time of the previous cycle */
		now_ticks = ba->cycle.nticks;
	else { /* get where we are in the current cycle */
		if (last_instr) { // last instruction before end of macro cycle
			// do some extra jobs: this takes around 3us
			do_update_report();
			if (ba->hw.ext_trig) {
				val = dp_readl( MASTERFIP_CSR_BASE +
						MASTERFIP_REG_EXT_SYNC_P_CNT);
				missed = val - 1 -
					ba->report.ext_sync_pulse_count;
				if (missed > 0)
					ba->report.ext_sync_pulse_missed_count += missed;
				ba->report.ext_sync_pulse_count = val;

				/* enable safe window */
//				val = dp_readl( MASTERFIP_CSR_BASE +
//						MASTERFIP_REG_EXT_SYNC_CTRL);
//				val |= 0x1000000;
//				dp_writel(val, MASTERFIP_CSR_BASE +
//						MASTERFIP_REG_EXT_SYNC_CTRL);

			}
		}
		now_ticks = dp_readl( MASTERFIP_CSR_BASE +
				      MASTERFIP_REG_MACROCYC_TIME_CNT);
	}
	/*
	 * check first if the ba-wait window is already expired that is to say
	 * the remaining time is less than the turn around time: this happen
	 * when a ba wait follows an aperiodic window with the same end time
	 */
	if ( now_ticks < (end_ticks + ba->hw.tr_ticks +
			  delays_in_cpu_ticks[IDX_500NS_DELAY]) )
		return;
	/*
	 * if ba->hw.ext_trig and new_cycle wait_trtime synchronises
	 * with external pulse
	 */
	if (now_ticks > limit_ticks) {
		if (!silent_flg) {
			mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT, MSTRFIP_PADDING_ID_DAT);
			mstrfip_wait_trtime();
		}
		for(;;) {
			if (!silent_flg) {
				mstrfip_start_tx(2);
				if (smem_data->record.cfg.flg) {
					mcycle_time = ba->cycle.nticks -
						dp_readl(MASTERFIP_CSR_BASE +
							 MASTERFIP_REG_MACROCYC_TIME_CNT);
					mstrfip_record(mcycle_time, new_cycle,
						       ba->report.cycle_state,
						       MSTRFIP_ID_DAT, MSTRFIP_PADDING_ID_DAT,
						       0, MSTRFIP_EVTREC_DIR_PROD);
				}
				mstrfip_wait_end_of_tx(2);
				mstrfip_write_question_tx_reg(MSTRFIP_ID_DAT,
							      MSTRFIP_PADDING_ID_DAT);
			}
			else
				mstrfip_start_scounter(0);/*arm manually silence counter*/
			now_ticks = dp_readl( MASTERFIP_CSR_BASE +
					      MASTERFIP_REG_MACROCYC_TIME_CNT);
			if (now_ticks <= limit_ticks)
				break;
			new_cycle = 0; /* not anymore a new cycle */
			mstrfip_wait_stime();
		}
	}
	/*
	 * wait end of wait-window keeping a margin of tr time + 50ticks
	 * in order to prepare next transaction and rearm tr counter
	 */
	mstrfip_wait_and_rearm_tr(end_ticks + ba->hw.tr_ticks +
				  delays_in_cpu_ticks[IDX_1US_DELAY]);
}

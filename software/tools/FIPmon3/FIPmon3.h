// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*============================================================================*/
/* Name : def_mod_glob.h :                                                    */
/*============================================================================*/
/* Description : global defines                                               */
/*                                                                            */
/*============================================================================*/
/* Auteur   : julien.palluel@cern.ch                                          */
/*============================================================================*/
#include "libmasterfip.h"
#include <time.h>

#define GLOBAL
#define NODEBUG

/* number max of agents */
#define NB_ST_MAX 1
#define VAR_PER_AGENT 2

/* BA definition usec*/
#define MACROCYCLE 40000

/*length in bytes of a variable */
#define VAR_LEN 2

#define REFRESH_DISP 100000

/* used for the display */
#define STATUS_STOP 0
#define STATUS_RUN 2

extern unsigned int dev_id;
extern int trigmode;
extern int inttrig;
extern unsigned long BAperiod;
extern int display;
extern enum mstrfip_bitrate fip_speed; /* FIP bus speed get from the HW */
extern time_t first_date;
extern struct mstrfip_data *fip_diag_var[VAR_PER_AGENT];

void start_fip();
void start_user_var();
void start_BA();
void stop_BA();
void display_monitoring();
void fip_error_handler();
void fip_exit();





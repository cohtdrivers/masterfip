# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

# If it exists includes Makefile.specific. In this Makefile, you should put
# specific Makefile code that you want to run before this. For example,
# build a particular environment.
-include Makefile.specific

# define REPO_PARENT to get build environment
TOP_DIR?=$(shell pwd)/../../..
REPO_PARENT?=$(TOP_DIR)/..
-include $(REPO_PARENT)/parent_common.mk

-include ../../version.mk

MFIP ?= $(TOP_DIR)/software
TRTL ?= $(TOP_DIR)/dependencies/mockturtle/software

CFLAGS += -Wall -ggdb $(DBG) -I. -I$(MFIP) -I$(MFIP)/include -I$(MFIP)/lib \
		-I../libs -I../extest -I$(TRTL)/include
CFLAGS += -D__GIT_VER__="\"$(MFIP_GIT_VERSION)\""
CFLAGS += -Werror
LDLIBS += -Wl,-Bstatic -L$(MFIP)/lib -L$(TRTL)/lib -L../libs -L../extest
LDLIBS += -lmasterfip -lmockturtle -lmfipmap-res -lextest
LDLIBS += -Wl,-Bdynamic -lpthread -lm -lrt -lreadline

PROGS := masterfip-diag masterfip-diag-shm

all: $(PROGS)

# make nothing for modules_install, but avoid errors
modules_install:

DESTDIR ?=
prefix ?= /usr/local
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin

install: $(PROGS)
	mkdir -m 0775 -p $(DESTDIR)$(bindir)
	install -D -t $(DESTDIR)$(bindir) -m 0555 $(PROGS)

.PHONY: all clean

clean:
	rm -f *.o *.so *.a $(PROGS)

CPPCHECK ?= cppcheck
cppcheck:
	$(CPPCHECK) -q -I. -I$(MFIP)/include -I$(MFIP)/lib --suppress=missingIncludeSystem --enable=all *.c *.h

FLAWFINDER ?= flawfinder
flawfinder:
	$(FLAWFINDER) -SQDC --error-level=6 *.c *.h

.PHONY: cppcheck flawfinder

/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stddef.h>
#include <glob.h>
#include <libgen.h>

#include "mfipmap-res.h"
#include "extest.h"
#define __KERNEL__
#include <mockturtle/hw/mockturtle_addresses.h>
#undef __KERNEL__
#include <libmasterfip.h>
#include <rt/common/hw/fmc_masterfip_csr.h>

struct mfipdiag_map_reg_desc {
	char *name;
	uint32_t offset;
	uint32_t depth;
};

struct mfipdiag_map_res_desc {
	char *name;
	uint32_t nentries;
	uint64_t size; /* in bytes */
	struct mfip_map_args args;
	struct mfip_map_hdl *hdl;
	struct mfipdiag_map_reg_desc *regs;
};

struct mfipdiag_dev_desc {
	unsigned int lun;
	uint32_t pci_devid;
	uint32_t trtl_devid;
	uint16_t vme_slot;
	int is_vme;
};

/*
 * we know that number of masterFIP device cannot exceed 2.
 * (so 5 is more than enough )
 */
struct mfipdiag_dev_desc *mfipdiag_devlist;
int mfipdiag_ndev;

#define MFIPDIAG_RES_COUNT 4
struct mfipdiag_res {
	int dev_idx; /* index of selected device in devlist */
	struct mfipdiag_map_res_desc mtrtl_shm;
	struct mfipdiag_map_res_desc mfip_core;
	struct mfipdiag_map_res_desc vic_core;
};

static struct mfipdiag_map_reg_desc mfip_regs_desc[] = {
	{
		.name = "MASTERFIP_REG_VER",
		.offset = MASTERFIP_REG_VER,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_RST",
		.offset = MASTERFIP_REG_RST,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_ID",
		.offset = MASTERFIP_REG_ID,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_LED",
		.offset = MASTERFIP_REG_LED,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_DS1820_TEMPER",
		.offset = MASTERFIP_REG_DS1820_TEMPER,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_DS1820_ID_LSB",
		.offset = MASTERFIP_REG_DS1820_ID_LSB,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_DS1820_ID_MSB",
		.offset = MASTERFIP_REG_DS1820_ID_MSB,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_EXT_SYNC_CTRL",
		.offset = MASTERFIP_REG_EXT_SYNC_CTRL,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_EXT_SYNC_P_CNT",
		.offset = MASTERFIP_REG_EXT_SYNC_P_CNT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_SPEED",
		.offset = MASTERFIP_REG_SPEED,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_MACROCYC",
		.offset = MASTERFIP_REG_MACROCYC,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_MACROCYC_TIME_CNT",
		.offset = MASTERFIP_REG_MACROCYC_TIME_CNT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_MACROCYC_NUM_CNT",
		.offset = MASTERFIP_REG_MACROCYC_NUM_CNT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_TURNAR",
		.offset = MASTERFIP_REG_TURNAR,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_TURNAR_TIME_CNT",
		.offset = MASTERFIP_REG_TURNAR_TIME_CNT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_SILEN",
		.offset = MASTERFIP_REG_SILEN,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_SILEN_TIME_CNT",
		.offset = MASTERFIP_REG_SILEN_TIME_CNT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_TX_CTRL",
		.offset = MASTERFIP_REG_TX_CTRL,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_TX_STAT",
		.offset = MASTERFIP_REG_TX_STAT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_FD",
		.offset = MASTERFIP_REG_FD,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_FD_WDG_TSTAMP",
		.offset = MASTERFIP_REG_FD_WDG_TSTAMP,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_FD_TXER_CNT",
		.offset = MASTERFIP_REG_FD_TXER_CNT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_FD_TXER_TSTAMP",
		.offset = MASTERFIP_REG_FD_TXER_TSTAMP,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_RX_CTRL",
		.offset = MASTERFIP_REG_RX_CTRL,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_RX_STAT",
		.offset = MASTERFIP_REG_RX_STAT,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_RX_STAT_CURR_WORD_INDX",
		.offset = MASTERFIP_REG_RX_STAT_CURR_WORD_INDX,
		.depth = 1
	},
	{
		.name = "MASTERFIP_REG_RX_STAT_CRC_ERR_CNT",
		.offset = MASTERFIP_REG_RX_STAT_CRC_ERR_CNT,
		.depth = 1
	},
};
#define mfip_map_nentries (sizeof(mfip_regs_desc)/sizeof(struct mfipdiag_map_reg_desc))

static struct mfipdiag_map_reg_desc vic_regs_desc[] = {
	{
		.name = "VIC_REG_CTL",
		.offset = 0x0,
		.depth = 1
	},
	{
		.name = "VIC_REG_RISR",
		.offset = 0x4,
		.depth = 1
	},
	{
		.name = "VIC_REG_IER",
		.offset = 0x8,
		.depth = 1
	},
	{
		.name = "VIC_REG_IDR",
		.offset = 0xc,
		.depth = 1
	},
	{
		.name = "VIC_REG_IMR",
		.offset = 0x10,
		.depth = 1
	},
	{
		.name = "VIC_REG_VAR",
		.offset = 0x14,
		.depth = 1
	},
	{
		.name = "VIC_REG_SWIR",
		.offset = 0x18,
		.depth = 1
	},
	{
		.name = "VIC_REG_EOIR",
		.offset = 0x1c,
		.depth = 1
	},
};
#define vic_map_nentries (sizeof(vic_regs_desc)/sizeof(struct mfipdiag_map_reg_desc))

static struct mfipdiag_map_reg_desc mtrtl_shm_desc[] = {
	[0] = {
		.name = "shm size in words(cannot exceed 16384 (0x4000) words)",
	 	.offset = offsetof(struct smem_data_cfg, smem_wsz),
		.depth = 1
	},
	[1] = {
		.name = "bss size in words (cannot exceed 20000 (0x4e20) words)",
	 	.offset = offsetof(struct smem_data_cfg, bss_wsz),
		.depth = 1
	},
	[2] = {
		.name = "smem mutex",
		.offset = offsetof(struct smem_data_cfg, smem_mtx),
		.depth = 1
	},
	[3] = {
		.name = "ba state cmd",
		.offset = offsetof(struct smem_data_cfg, ba_state_cmd),
		.depth = 1
	},
	[4] = {
		.name = "aper var: requested presence",
		.offset = offsetof(struct smem_data_cfg, aper_var) +
			  offsetof(struct smem_aper_var, presence_request),
		.depth = MSTRFIP_PRESENCE_REQUEST_WSIZE
	},
	[5] = {
		.name = "aper var: requested agents ident",
		.offset = offsetof(struct smem_data_cfg, aper_var) +
			  offsetof(struct smem_aper_var, ident_request),
		.depth = MSTRFIP_IDENT_REQUEST_WSIZE
	},
	[6] = {
		.name = "aper var: request for ident. traffic",
		.offset = offsetof(struct smem_data_cfg, aper_var) +
			  offsetof(struct smem_aper_var, ident_req),
		.depth = 1
	},
	[7] = {
		.name = "aper msg: max msg payload wsz",
		.offset = offsetof(struct smem_data_cfg, aper_msg) +
			  offsetof(struct smem_aper_msg, payload_wsz),
		.depth = 1
	},
	[8] = {
		.name = "aper msg: pointer to payload entries area",
		.offset = offsetof(struct smem_data_cfg, aper_msg) +
			  offsetof(struct smem_aper_msg, payload_buf_addr),
		.depth = 1
	},
	[9] = {
		.name = "aper_msg: max number of msg payloads",
		.offset = offsetof(struct smem_data_cfg, aper_msg) +
			  offsetof(struct smem_aper_msg, payload_buf_sz),
		.depth = 1
	},
	[10] = {
		.name = "aper msg: current pending msg payloads",
		.offset = offsetof(struct smem_data_cfg, aper_msg) +
			  offsetof(struct smem_aper_msg, payload_count),
		.depth = 1
	},
	[11] = {
		.name = "aper msg: payload entries area read index",
		.offset = offsetof(struct smem_data_cfg, aper_msg) +
			  offsetof(struct smem_aper_msg, r_idx),
		.depth = 1
	},
	[12] = {
		.name = "aper msg: payload entries area write index",
		.offset = offsetof(struct smem_data_cfg, aper_msg) +
			  offsetof(struct smem_aper_msg, w_idx),
		.depth = 1
	},
	[13] = {
		.name = "per var: max per var payload wsz",
		.offset = offsetof(struct smem_data_cfg, per_var) +
			  offsetof(struct smem_per_var, payload_wsz),
		.depth = 1
	},
	[14] = {
		.name = "per_var: produced var count",
		.offset = offsetof(struct smem_data_cfg, per_var) +
			  offsetof(struct smem_per_var, prod_nvar),
		.depth = 1
	},
	[15] = {
		.name = "per var: pointer to payload entries area",
		.offset = offsetof(struct smem_data_cfg, per_var) +
			  offsetof(struct smem_per_var, payload_buf_addr),
		.depth = 1
	},
	[16] = {
		.name = "per var: pointer to payload state area",
		.offset = offsetof(struct smem_data_cfg, per_var) +
			  offsetof(struct smem_per_var, payload_state_addr),
		.depth = 1
	},
//	[17] = {
//		.name = "stats: cycles count",
//		.offset = offsetof(struct smem_data_cfg, report) +
//			  offsetof(struct mstrfip_report, cycles),
//		.depth = 1
//	},
//	[18] = {
//		.name = "stats: ba state",
//		.offset = offsetof(struct smem_data_cfg, report) +
//			  offsetof(struct mstrfip_report, ba_state),
//		.depth = 1
//	},
//	[19] = {
//		.name = "stats: cycle state",
//		.offset = offsetof(struct smem_data_cfg, report) +
//			  offsetof(struct mstrfip_report, cycle_state),
//		.depth = 1
//	},
//	[20] = {
//		.name = "stats: HW temp",
//		.offset = offsetof(struct smem_data_cfg, report) +
//			  offsetof(struct mstrfip_report, temp),
//		.depth = 1
//	},
	[17] = {
		.name = "report",
		.offset = offsetof(struct smem_data_cfg, report),
		.depth = sizeof(struct mstrfip_report) / 4
	},
	[18] = {
		.name = "record-traffic",
		.offset = offsetof(struct smem_data_cfg, record),
		.depth = sizeof(struct smem_record_traffic) / 4
	},
};
#define mtrtl_shm_map_nentries (sizeof(mtrtl_shm_desc)/sizeof(struct mfipdiag_map_reg_desc))

#define TRTL_BASE_ADDR 0x40000
#define SPEC_VIC_BASE_ADDR 0x100
#define SPEC_MFIP_BASE_ADDR 0x60000
#define SVEC_VIC_BASE_ADDR 0x100
#define SVEC_MFIP_BASE_ADDR 0x30000

static struct mfipdiag_res * mfipres;

static struct mfipdiag_res svec_mfipres = {
	.mtrtl_shm = {
		.name = "MockTurtle SHM",
		.nentries = mtrtl_shm_map_nentries,
		.hdl = NULL,
		.args = {
			.bar = 1,
			.offset = TRTL_BASE_ADDR + TRTL_ADDR_OFFSET_SHM,
			.is_be = 1,
		},
		.regs = mtrtl_shm_desc,
	},
	.mfip_core = {
		.name = "MasterFIP core regs",
		.nentries = mfip_map_nentries,
		.hdl = NULL,
		.args = {
			.bar = 1,
			.offset = SVEC_MFIP_BASE_ADDR,
			.is_be = 1,
		},
		.regs = mfip_regs_desc,
	},
	.vic_core = {
		.name = "VIC core regs",
		.nentries = vic_map_nentries,
		.hdl = NULL,
		.args = {
			.bar = 1,
			.offset = SVEC_VIC_BASE_ADDR,
			.is_be = 1,
		},
		.regs = vic_regs_desc,
	}
};

static struct mfipdiag_res spec_mfipres = {
	.mtrtl_shm = {
		.name = "MockTurtle SHM",
		.nentries = mtrtl_shm_map_nentries,
		.hdl = NULL,
		.args = {
			.bar = 0,
			.offset = TRTL_BASE_ADDR + TRTL_ADDR_OFFSET_SHM,
			.is_be = 0,
		},
		.regs = mtrtl_shm_desc,
	},
	.mfip_core = {
		.name = "MasterFIP core regs",
		.nentries = mfip_map_nentries,
		.hdl = NULL,
		.args = {
			.bar = 0,
			.offset = SPEC_MFIP_BASE_ADDR,
			.is_be = 0,
		},
		.regs = mfip_regs_desc,
	},
	.vic_core = {
		.name = "VIC core regs",
		.nentries = vic_map_nentries,
		.hdl = NULL,
		.args = {
			.bar = 0,
			.offset = SPEC_VIC_BASE_ADDR,
			.is_be = 0,
		},
		.regs = vic_regs_desc,
	}
};

static struct mstrfip_dev *mstrfip_dev = NULL;
static struct mstrfip_diag_shm *mstrfip_shm = NULL;

//static void mfipdiag_open_mstrfipdev() {
//	int res;
//
//	res = mstrfip_init();
//	if (res) {
//		fprintf(stderr, "Cannot init fip library: %s\n",
//				mstrfip_strerror(errno));
//		exit(1);
//	}
//	mstrfip_dev = mstrfip_open_by_id(mfipdiag_devlist[mfipres->dev_idx].trtl_devid);
//	if(mstrfip_dev == NULL) {
//		fprintf(stderr, "Cannot open mstrfip dev: %s\n",
//				mstrfip_strerror(errno));
//		exit(1);
//	}
//}

static int mfipdiag_shm_attach()
{
	int shm_id;
	void *ptr_shm;

	if (mstrfip_shm != NULL) /* already atached */
		return 0;

	shm_id = shmget(MSTRFIP_DIAG_SHM_KEY + mfipdiag_devlist[mfipres->dev_idx].lun,
		sizeof(struct mstrfip_diag_shm), 0444);
	if(shm_id == -1 )
		return -1;

	ptr_shm = shmat(shm_id, (void *)0, 0);
	if (ptr_shm == (void *)-1 ) {
		fprintf(stderr, "attach to MasterFIP shm failed: %s\n",
			strerror(errno));
		return -1;
	}

	mstrfip_shm = (struct mstrfip_diag_shm *)ptr_shm;
	return 0;
}

static void mfipdiag_open_res(struct mfipdiag_map_res_desc *res)
{
	uint64_t size; /* page aligned size and offset */

	res->args.devid = mfipdiag_devlist[mfipres->dev_idx].pci_devid;
	res->args.is_vme = mfipdiag_devlist[mfipres->dev_idx].is_vme;
	res->args.vme_slot = mfipdiag_devlist[mfipres->dev_idx].vme_slot;
	size = res->regs[res->nentries - 1].offset +
	       ((res->regs[res->nentries - 1].depth - 1) * 4);
	res->hdl = mfip_map(&res->args, size);
}

static int mfipdiag_dump_regs_res(struct mfipdiag_map_res_desc *res,
				  int nentries)
{
	uint32_t val;
	int i, j;

	if (res->hdl == NULL) {
		mfipdiag_open_res(res);
		if (res->hdl == NULL) {
			fprintf(stderr, "Failed mapping %s "
				"offset:0x%08"PRIx64" size:0x%08"PRIx64"\n",
				res->name, res->args.offset, res->size);
			return -1;
		}
	}

	fprintf(stdout, "%s:\n", res->name);
	for (i = 0; i < nentries; ++i) {
		for (j = 0; j < res->regs[i].depth; ++j) {
			val = mfip_readl(res->hdl,
					 res->regs[i].offset + (j * 4));
//			val = ual_readl(res->hdl,
//				res->regs[i].offset + (j * 4));
			fprintf(stdout, "[0x%08"PRIx32"] = 0x%08"PRIx32" (%s)\n",
				res->regs[i].offset + (j * 4), val, res->regs[i].name);

		}
	}
	return 1;
}

static int get_mfip_core_regs(struct cmd_desc *cmdd, struct atom *atoms)
{
	return mfipdiag_dump_regs_res(&mfipres->mfip_core,
				      mfipres->mfip_core.nentries);
}

static int get_vic_core_regs(struct cmd_desc *cmdd, struct atom *atoms)
{
	return mfipdiag_dump_regs_res(&mfipres->vic_core,
				      mfipres->vic_core.nentries);
}

static int get_mtrtl_shm(struct cmd_desc *cmdd, struct atom *atoms)
{
	/* record entry is not dumped */
	return mfipdiag_dump_regs_res(&mfipres->mtrtl_shm,
				      mfipres->mtrtl_shm.nentries - 2);
}

#define VERSION_MAJ(_v) ((_v >> 16) & 0xFF)
#define VERSION_MIN(_v) ((_v >> 8) & 0xFF)
static int get_mfip_vers(struct cmd_desc *cmdd, struct atom *atoms)
{
	struct mstrfip_version *vers;
	char rt_vers[16], fpga_vers[16], lib_vers[16];
	char rt_id[16];
	uint8_t *ptr_uint8;
	int res;

	res = mfipdiag_shm_attach();
	if (res)
		return -1;

	vers = &mstrfip_shm->ext.vers;
	sprintf(rt_vers, "%u.%u.%u", VERSION_MAJ(vers->rt_version),
		VERSION_MIN(vers->rt_version), vers->rt_version & 0xFF);
	sprintf(fpga_vers, "%u.%u.%u", VERSION_MAJ(vers->fpga_version),
		VERSION_MIN(vers->fpga_version), vers->fpga_version & 0xFF);
	sprintf(lib_vers, "%u.%u.%u", VERSION_MAJ(vers->lib_version),
		VERSION_MIN(vers->lib_version), vers->lib_version & 0xFF);
	ptr_uint8 = (uint8_t *)&vers->rt_id;
	sprintf(rt_id, "%c%c%c%c",
		ptr_uint8[3], ptr_uint8[2], ptr_uint8[1], ptr_uint8[0]);
	fprintf(stdout, "Versions: \n"
		"FPGA id:0x%x\nRTapp id:%s(0x%x)\n"
		"FPGA vers:%s\nRTapp vers:%s\nlib vers:%s\n"
		"RTapp git vers:0x%x\nlib git vers:0x%x\n",
		vers->fpga_id, rt_id, vers->rt_id, fpga_vers,
		rt_vers, lib_vers, vers->rt_git_version,
		vers->lib_git_version);
//	}
	return 1;
}

static void mfipdiag_dump_report(struct mstrfip_report *report, int verbose_flg)
{
	static char *mstrfip_ba_fsm[] = {
		"INITIAL", "READY", "RUNNING",
	};
	static char *mstrfip_cycle_fsm[] = {
		"RUN_WAIT_WIND","RUN_APER_MSG_WIND","RUN_APER_VAR_WIND",
		"RUN_PER_VAR_WIND", "WAIT_TX_END", "WAIT_RX_PREAMB",
		"WAIT_RX_END", "WAIT_EXT_TRG","WAIT_RX_FIRST_CURR_WORD",
		"WAIT_MACROCYC_TICKS_COUNTER",
	};
	static char *mstrfip_report_topic[][2] = {
		[0] = {
			"FielDrive (line driver) errors:",
			"\t/* fd_tx_err: indicates bus underload/overload like,\n"
			"\t/*\tWorldFIP cable disconnected or Manchester encoding issue;\n"
			"\t/*\tThe timestamp indicates the moment of the last error.\n"
			"\t/*\tPlease note that this counter can increase quickly, for instance\n"
			"\t/*\tif FIP cable is disconnected it increments on every tx_frame.\n"
			"\t/* fd_tx_watchdog: indicates a transmission > 1024 bytes-long;\n"
			"\t/*\tThe timestamp indicates the moment of the last error.\n"
			"\t/* fd_cd: indicates RX and TX traffic at the same time.\n\n",
		      },
		[1] = {
			"RX/TX errors:",
			"\t/* rx_err: RX frame errors like CRC, length, preamb, pdu, etc.\n"
			"\t/* tx_err: TX frame errors indicating a serializer issue.\n\n",
		      },
		[2] = {
			"RX missing nodes:",
			"\t/* rx_tmo: RX timeout indicates nodes that are not responding to periodic frames.\n\n",
		      },
		[3] = {
			"MPS status errors:",
			"\t/* TX/RX MPS status error indicates not fresh payloads.\n\n",
		      },
		[4] = {
			"Macro cycles info:",
			"\t/* cycles_counter: Counter of macrocycles since the application startup.\n"
			"\t/* tx_ok: number of correctly transmitted frames per macrocycle.\n"
			"\t/* tx_ok: number of correctly received frames per macrocycle.\n\n",
		      },
		[5] = {
			"Trigger info:",
			"\t/* External or internal pulse counter and missed external pulses.\n\n",
		       },
		[6] = {
			"Bus arbiter and macrocycle state:",
			"\t/* BA state: INITIAL(not configured), READY(configured), RUNNING(macrocycle running.\n"
			"\t/* Macro cycle state: transient state, changing very fast;\n"
			"\t/*\tgives an indication on what action is taking place in the macrocycle\n\n",
		      },
		[7] = {
			"HW info:",
			"\n",
		      },
	};
	time_t now_secs;
	float mcycle_seclength;
	char tx_err_time_str[32], tx_watchdog_time_str[32];

	tx_err_time_str[0] = '\0';
	tx_watchdog_time_str[0] = '\0';
	if (report->fd_tx_err_cycle || report->fd_tx_watchdog_cycle) {
		int res = mfipdiag_shm_attach();
		if (res)
			return;
		now_secs = time(NULL);
		mcycle_seclength = (float)(mstrfip_shm->ext.ba_cfg.cycle_uslength) / 1000000.0;
	}
	if (report->fd_tx_err_cycle) {
		now_secs -= (int)(mcycle_seclength *
			     (report->cycles - report->fd_tx_err_cycle));
		strftime(tx_err_time_str, sizeof(tx_err_time_str), "%c",
			 localtime(&now_secs));
	}
	if (report->fd_tx_watchdog_cycle) {
		now_secs -= (int)(mcycle_seclength *
			    (report->cycles - report->fd_tx_watchdog_cycle));
		strftime(tx_watchdog_time_str, sizeof(tx_watchdog_time_str),
			 "%c", localtime(&now_secs));
	}

	fprintf(stdout, "report:\n");
	fprintf(stdout, "%s\n%s"
		"\tfd_tx_err=%u fd_tx_err_hwtime(ms)=%6.2f faulty_mcycle=%u (%s)\n"
		"\tfd_tx_watchdog=%u fd_tx_watchdog_hwtime=%6.2f faulty_mcycle=%u (%s)\n"
		"\tfd_cd=%u\n", mstrfip_report_topic[0][0],
		(verbose_flg) ? mstrfip_report_topic[0][1] : "",
		report->fd_tx_err, report->fd_tx_err_hwtime/100000.0,
		report->fd_tx_err_cycle, tx_err_time_str,
		report->fd_tx_watchdog, report->fd_tx_watchdog_hwtime/100000.0,
		report->fd_tx_watchdog_cycle, tx_watchdog_time_str, report->fd_cd);
	fprintf(stdout, "%s\n%s"
		"\trx_err=%u\n\ttx_err=%u\n", mstrfip_report_topic[1][0],
		(verbose_flg) ? mstrfip_report_topic[1][1] : "",
		report->rx_err, report->tx_err);
	fprintf(stdout, "%s\n%s"
		"\trx_tmo=%u\n", mstrfip_report_topic[2][0],
		(verbose_flg) ? mstrfip_report_topic[2][1] : "", report->rx_tmo);
	fprintf(stdout, "%s\n%s"
		"\ttx_mps_status_err=%u\n\trx_mps_status_err=%u\n",
		mstrfip_report_topic[3][0],
		(verbose_flg) ? mstrfip_report_topic[3][1] : "",
		report->tx_mps_status_err, report->rx_mps_status_err);
	fprintf(stdout, "%s\n%s"
		"\tcycles_counter=%u\n\ttx_ok(last cycle)=%u\n\trx_ok(last cycle)=%u\n",
		mstrfip_report_topic[4][0],
		(verbose_flg) ? mstrfip_report_topic[4][1] : "",
		report->cycles, report->tx_ok, report->rx_ok);
	fprintf(stdout, "%s\n%s"
		"\text_trig_count=%u missed=%u\n"
		"\tint_trig_count=%u\n", mstrfip_report_topic[5][0],
		(verbose_flg) ? mstrfip_report_topic[5][1] : "",
		report->ext_sync_pulse_count, report->ext_sync_pulse_missed_count,
		report->int_sync_pulse_count);
	fprintf(stdout, "%s\n%s"
		"\tba_state=%s\n"
		"\tcycle_state=%s\n", mstrfip_report_topic[6][0],
		(verbose_flg) ? mstrfip_report_topic[6][1] : "",
		mstrfip_ba_fsm[report->ba_state], mstrfip_cycle_fsm[report->cycle_state]);
	fprintf(stdout, "%s\n%s"
		"\ttemperature:%u\n",
		mstrfip_report_topic[7][0],
		(verbose_flg) ? mstrfip_report_topic[7][1] : "",
		report->temp);
}

static int get_mfip_report(struct cmd_desc *cmdd, struct atom *atoms)
{
	struct mfipdiag_map_res_desc *res = &mfipres->mtrtl_shm;
	uint32_t offset, report_wsz;
	int i, verbose_flg;
	struct mstrfip_report report;
	uint32_t *ptr_dest;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - [options]\n"
		       "By default display not verbose report.\n"
		       "options: -v for verbose report\n", cmdd->name);
		return 1;
	}

	++atoms;
	verbose_flg = 0;
	if (atoms->type != Terminator) { // set data valid
		while (atoms->type != Terminator) {
			if (!strncmp(atoms->text, "-v", sizeof("-v"))) {
				verbose_flg = 1;
				break;
			}
		}
	}

	if (res->hdl == NULL) {
		mfipdiag_open_res(res);
		if (res->hdl == NULL) {
			fprintf(stderr, "Failed mapping %s "
				"offset:0x%08"PRIx64" size:0x%08"PRIx64" : %s\n",
				res->name, res->args.offset, res->size, strerror(errno));
			return -1;
		}
	}

	offset = res->regs[17].offset;
	report_wsz = sizeof(struct mstrfip_report) / 4;
	ptr_dest = (uint32_t *)&report;
	for (i = 0; i < report_wsz; ++i, ++ptr_dest) {
		uint32_t val = mfip_readl(res->hdl, offset + (i * 4));
//		val = ual_readl(res->hdl, offset + (i * 4));
		*ptr_dest = val;
	}

	mfipdiag_dump_report(&report, verbose_flg);
	return 1;
}

static void display_per_varlist(int from, int to)
{
	int idx;
	struct mstrfip_per_var_desc *pvar = mstrfip_shm->ext.ba_cfg.per_varlist;

	fprintf(stdout, "\t\t\tvar_id\tdir\tbsz\tcb\n");
	for (idx = from; idx < to; ++idx) {
		fprintf(stdout, "\t\t\t%#06x\t%s\t%3u\t%s\n",
			pvar[idx].var_id,
			(pvar[idx].dir & MSTRFIP_DATA_FLAGS_PROD) ? "prod" : "cons",
			pvar[idx].sz, (pvar[idx].cb) ? "yes" : "no");
	}
}

static void display_macrocycle_window(struct mstrfip_ba_instr *instr, int wind_no)
{
	uint32_t cpu_hz = mstrfip_shm->ext.mtrtl_cpu_speed_hz;
	switch (instr->code) {
	case MSTRFIP_BA_PER_VAR_WIND:
		fprintf(stdout, "\t\tWindow %d: Periodic var window\n",
			wind_no);
		display_per_varlist(instr->per_var_wind.start_var_idx,
				    instr->per_var_wind.stop_var_idx);
		break;
	case MSTRFIP_BA_APER_VAR_WIND:
		fprintf(stdout, "\t\tWindow %d: Aperiodic var window ending at %u us\n",
			wind_no,
			ticks_to_us(instr->aper_var_wind.ticks_end_time, cpu_hz));
		break;
	case MSTRFIP_BA_APER_MSG_WIND:
		fprintf(stdout, "\t\tWindow %d: Aperiodic msg window ending at %u us\n"
			"\t\t\tconsumed msg FIFO size:%u\n"
			"\t\t\tproduced msg FIFO size:%u\n"
			"\t\t\tproduced msg payload max size in bytes:%u\n"
			"\t\t\tconsumed msg global notification:%s\n"
			"\t\t\tproduced msg global notification:%s\n",
			wind_no,
			ticks_to_us(instr->aper_msg_wind.ticks_end_time, cpu_hz),
			instr->aper_msg_wind.cons_msg_fifo_sz,
			instr->aper_msg_wind.prod_msg_fifo_sz,
			instr->aper_msg_wind.prod_msg_max_bsz,
			(instr->aper_msg_wind.cons_msg_global_irq) ? "yes" : "no",
			(instr->aper_msg_wind.prod_msg_global_irq) ? "yes" : "no");

		break;
	case MSTRFIP_BA_WAIT_WIND:
		fprintf(stdout, "\t\tWindow %d: Wait window: ending at %u us (stuffing %s)\n",
			wind_no,
			ticks_to_us(instr->wait_wind.ticks_end_time, cpu_hz),
			(instr->wait_wind.is_silent) ? "disabled" : "enabled");
		break;
	default:
		break;
	}
}

static char *bus_speed[3] = {
	"31.25 kb/s",
	"1 Mb/s",
	"2.5 Mb/s",
};

static int get_mfip_hw_cfg(struct cmd_desc *cmdd, struct atom *atoms)
{
	int res;

	res = mfipdiag_shm_attach();
	if (res)
		return -1;

	fprintf(stdout, "HW config:\n");
	fprintf(stdout, "\tspeed: %s\n\texternal trigger enabled: %s"
		"\n\t50 ohms termination enabled: %s"
		"\n\tinternal trigger enabled: %s"
		"\n\tMaster turn around time in us: %u\n",
		bus_speed[mstrfip_shm->ext.bitrate],
		(mstrfip_shm->ext.hw_cfg.enable_ext_trig) ? "Yes" : "No",
		(mstrfip_shm->ext.hw_cfg.enable_ext_trig_term) ? "Yes" : "No",
		(mstrfip_shm->ext.hw_cfg.enable_int_trig) ? "Yes" : "No",
		mstrfip_shm->ext.hw_cfg.turn_around_ustime);
	return 1;
}

static int get_mfip_ba_cfg(struct cmd_desc *cmdd, struct atom *atoms)
{
	int ninstr;
	int res;

	res = mfipdiag_shm_attach();
	if (res)
		return -1;

	fprintf(stdout, "Macrocyle configuration:\n\tduration %u us\n"
		"\tcomposition:\n", mstrfip_shm->ext.ba_cfg.cycle_uslength);
	for (ninstr = 0; ninstr < mstrfip_shm->ext.ba_cfg.ninstr; ++ninstr) {
		display_macrocycle_window(&mstrfip_shm->ext.ba_cfg.instrlist[ninstr],
					  ninstr);
	}
	return 1;
}

static int get_mfip_seg_resp_time(struct cmd_desc *cmdd, struct atom *atoms)
{
	int row, col, max_nrow = 16, max_ncol = 16;
	uint32_t resp_nstime;
	int res, i, j;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s -\n"
		       "\tGet segment configuration by providing response time in ns\n"
		       "\tof any agent from address 1-255, replying to identification variable\n",
		       cmdd->name);
		return 1;
	}

	res = mfipdiag_shm_attach();
	if (res)
		return -1;

	fprintf(stdout, "Response time in ns recorded at the startup of FIP application\n"
			"==============================================================\n");
	fprintf(stdout, "%5s", "");
	for (col = 0; col < max_ncol; ++col)
		fprintf(stdout, "%8d", col);
	fprintf(stdout, "\n");
	for (row = 0; row < max_nrow; ++row) {
		fprintf(stdout, "%5d", row * max_nrow);
		for (col = 0; col < max_ncol; ++col) {
			if (row == 0 && col == 0) { /* master */
				fprintf(stdout, "%8s", "");
				continue;
			}
			resp_nstime = mstrfip_shm->ext.seg.resp_nstime[col + row * max_ncol];
			if (resp_nstime == 0)
				fprintf(stdout, "%8s", "----");
			else
				fprintf(stdout, "%8u", resp_nstime);
		}
		fprintf(stdout, "\n");
	}

	fprintf(stdout, "Current presence list:\n"
  			"======================\n");
	for (i= 0; i < 32; ++i) {
		for (j = 0; j < 8; j++) {
			if (mstrfip_shm->present_list[i] & (1 << j))
				fprintf(stdout, "%d ", (8 * i) + j);
		}
	}
	fprintf(stdout, "\n");

	return 1;
}

static char* ctrlbyte_to_string(uint32_t  ctrlbyte)
{
	char *str = NULL;

	switch (ctrlbyte) {
	case MSTRFIP_ID_DAT:
		str = "ID_DAT";
		break;
	case MSTRFIP_RP_DAT:
		str = "RP_DAT";
		break;
	case MSTRFIP_ID_MSG:
		str = "ID_MSG";
		break;
	case MSTRFIP_RP_DAT_MSG:
		str = "RP_DAT_MSG";
		break;
	case MSTRFIP_RP_MSG_NOACK:
		str = "RP_MSG_NOACK";
		break;
	case MSTRFIP_RP_MSG_ACK_EVEN:
		str = "RP_MSG_ACK_EVEN";
		break;
	case MSTRFIP_RP_MSG_ACK_ODD:
		str = "RP_MSG_ACK_ODD";
		break;
	case MSTRFIP_RP_FIN:
		str = "RP_FIN";
		break;
	case MSTRFIP_RP_UNKNOWN:
		str = "---";
		break;
	}
	return str;
}

static char* wind_type_to_string(uint32_t  wind_type)
{
	char *str;

	switch(wind_type) {
	case MSTRFIP_FSM_RUN_WAIT:
		str = "Wait window";
		break;
	case MSTRFIP_FSM_RUN_APER_MSG:
		str = "Aperiodic vmessage window";
		break;
	case MSTRFIP_FSM_RUN_APER_VAR:
		str = "Aperiodic var window";
		break;
	case MSTRFIP_FSM_RUN_PER_VAR:
		str = "Periodic var window";
		break;
	default:
		str = "undefined window";
		break;
	}
	return str;
}

static char* status_to_string(uint32_t  evt_desc)
{
	uint32_t status;
	char *str;

	status = (evt_desc & MSTRFIP_EVTREC_STATUS_MASK) >> MSTRFIP_EVTREC_STATUS_SHIFT;
	switch(status) {
	case MSTRFIP_EVTREC_STATUS_OK:
		str = "OK";
		break;
	case MSTRFIP_EVTREC_STATUS_TMO:
		str = "TMO";
		break;
	case MSTRFIP_EVTREC_STATUS_ERR:
		str = "Error";
		break;
	default:
		str = "undefined";
	}
	return str;
}

static char* dir_to_string(uint32_t  evt_desc, uint32_t ctrlbyte)
{
	uint32_t dir;
	char *str;

	if (ctrlbyte == MSTRFIP_ID_DAT || ctrlbyte == MSTRFIP_ID_MSG)
		/*
		 * we know that it's a question send by the master
		 * no point to write prod which make the display less clear
		 */
		return "";

	dir = evt_desc & MSTRFIP_EVTREC_DIR_MASK;

	switch (dir) {
	case MSTRFIP_EVTREC_DIR_PROD:
		str = "prod";
		break;
	case MSTRFIP_EVTREC_DIR_CONS:
		str = "cons";
		break;
	}
	return str;
}

static void mfipdiag_dump_record(struct mfipdiag_map_res_desc *res, int nentries)
{
	int i, cycle_cnt;
	uint32_t offset, prev_fr_ticktime;
	int prev_wind_type = -1;
	uint32_t cpu_hz = mstrfip_shm->ext.mtrtl_cpu_speed_hz;

	offset = res->regs[18].offset + sizeof(struct mstrfip_record_cfg);
	prev_fr_ticktime = 0;

	cycle_cnt = 0;
	fprintf(stdout, "\t\t%5s%16s%16s%16s%2s%8s%20s%8s%8s\n", "", "abs_time(us)",
		"mcycle_time(us)", "delta_time(us)", "", "varid", "ctrlbyte",
		"status", "dir");
	uint32_t mcycle_ustime = 0;
	for (i = 0; i < nentries; ++i, offset += 8) {
		char *ctrlbyte_str, *wind_str, *status_str, *dir_str;
		uint32_t abs_fr_ustime, delta_fr_ustime;
		uint32_t curr_fr_ticktime, evt_desc;
		uint32_t varid, new_mcycle, ctrlbyte;
		int wind_type;

		// A record entry is made of two 32bits words: time + evt_desc

		// Second decode evt description: made of several bitfields
		evt_desc = mfip_readl(res->hdl, offset + 4);
		varid = (evt_desc & MSTRFIP_EVTREC_VARID_MASK) >> MSTRFIP_EVTREC_VARID_SHIFT;
		ctrlbyte = (evt_desc & MSTRFIP_EVTREC_CTRLBYTE_MASK) >>
				MSTRFIP_EVTREC_CTRLBYTE_SHIFT;
		ctrlbyte_str = ctrlbyte_to_string(ctrlbyte);
		wind_type = (evt_desc & MSTRFIP_EVTREC_WIND_MASK) >> MSTRFIP_EVTREC_WIND_SHIFT;
		wind_str = wind_type_to_string(wind_type);
		new_mcycle = evt_desc & MSTRFIP_EVTREC_MCYCLE_MASK;
		status_str = status_to_string(evt_desc);
		dir_str = dir_to_string(evt_desc, ctrlbyte);

		// display record
		if (new_mcycle == MSTRFIP_EVTREC_MCYCLE_MASK) {
			fprintf(stdout, "New macro cycle\n");
			prev_fr_ticktime = 0;
			++cycle_cnt;
			mcycle_ustime = cycle_cnt * mstrfip_shm->ext.ba_cfg.cycle_uslength;
		}
		if (new_mcycle || (wind_type != prev_wind_type)) // New macrocycle window is executed
			fprintf(stdout, "\t%s\n", wind_str);
		if (new_mcycle)
			fprintf(stdout, "\t\t%5s%16s%16s%16s%2s%8s%20s%8s%8s\n",
				"", "abs_time(us)", "mcycle_time(us)",
				"delta_time(us)","", "varid", "ctrlbyte", "status",
				"dir");
		prev_wind_type = wind_type;

		// First decode time: sec + cpu ticks (1 tick = 10ns)
		curr_fr_ticktime = mfip_readl(res->hdl, offset);
		if (prev_fr_ticktime == 0)
			prev_fr_ticktime = curr_fr_ticktime;
		delta_fr_ustime =
			ticks_to_us(curr_fr_ticktime - prev_fr_ticktime, cpu_hz);
		abs_fr_ustime =
			mcycle_ustime + ticks_to_us(curr_fr_ticktime, cpu_hz);

		fprintf(stdout, "\t\t%5d%16u%16u%16u%4s%#06x%20s%8s%8s\n",
			i, abs_fr_ustime, ticks_to_us(curr_fr_ticktime, cpu_hz),
			delta_fr_ustime,
			"", varid, ctrlbyte_str, status_str, dir_str);
		prev_fr_ticktime = curr_fr_ticktime;
	}
}

static int get_mfip_record_traffic(struct cmd_desc *cmdd, struct atom *atoms)
{
	struct mfipdiag_map_res_desc *res = &mfipres->mtrtl_shm;
	int nevt = 0, prev_nevt = 0, err;
	uint32_t ctrltype, varid, status, req_records, offset;
	int timeout;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - [options]\n"
		       "without option start recording immediately.\n"
		       "options: --ctrltype ctrl_type --varid var_id --status status\n"
		       "\t--ctrltype: recording starts when a frame matches given"
		       " ctrltype: ID_DAT:0x3 ID_MSG:0x5 RP_DAT:0x2 RP_DAT_MSG:0x6\n"
		       "\t--varid: recording starts when a frame matches given var_id\n "
		       "\t--status: recording starts when a frame matches given status:TMO:0x1 Error:0x2\n"
		       "\t--timeout(sec): timeout in second to leave waiting recording\n"
		       "\t--nrecords: number of FIP frames to record (max %d)\n",
		       cmdd->name, MSTRFIP_MAX_RECORD_COUNT);
		return 1;
	}

	ctrltype = varid = status = 0x0;
	timeout = -1; /* no time out */
	req_records = MSTRFIP_MAX_RECORD_COUNT;
	++atoms;
	if (atoms->type != Terminator) { // set data valid
		int found_err = 1;
		while (atoms->type != Terminator) {
			if (!strncmp(atoms->text, "--ctrltype", sizeof("--ctrltype"))) {
				++atoms; //next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				ctrltype = atoms->val;
				found_err = 0;
			}
			if (!strncmp(atoms->text, "--varid", sizeof("--varid"))) {
				++atoms; //next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				varid = atoms->val;
				found_err = 0;
			}
			if (!strncmp(atoms->text, "--status", sizeof("--status"))) {
				++atoms; //next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				status = atoms->val;
				found_err = 0;
			}
			if (!strncmp(atoms->text, "--timeout", sizeof("--timeout"))) {
				++atoms; //next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				timeout = atoms->val;
				found_err = 0;
			}
			if (!strncmp(atoms->text, "--nrecords", sizeof("--nrecords"))) {
				++atoms; //next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				if (atoms->val <= MSTRFIP_MAX_RECORD_COUNT)
					req_records = atoms->val;

				found_err = 0;
			}
			++atoms;
		}
		if (found_err != 0) {
			fprintf(stderr, "Wrong arguments. "
				"Please check %s usage by running: h %s\n",
				cmdd->name, cmdd->name);
			return -1;
		}
	}
	if (res->hdl == NULL) {
		mfipdiag_open_res(res);
		if (res->hdl == NULL) {
			fprintf(stderr, "Failed mapping %s "
				"offset:0x%08"PRIx64" size:0x%08"PRIx64" : %s\n",
				res->name, res->args.offset, res->size, strerror(errno));
			return -1;
		}
	}
	err = mfipdiag_shm_attach();
	if (err)
		return -1;
	/*
	 * Start recording : ual doesn't give access to the mmap pointer,
	 * so we are forced to used hardcoded offset.
	 * TODO: replace ual by my devmap library.
	 */
	offset = res->regs[18].offset;
	/* reset nentries */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, nentries), 0);
	/* reset stime */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, stime), 0);
	/* set requested records */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, req_nentries), req_records);
	/* set ctrltype trigger filter */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, ctrltype_trigger), ctrltype);
	/* set varid trigger filter */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, varid_trigger), varid);
	/* set status trigger filter */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, status_trigger), status);
	/* raise start recording */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, flg), 1); /* raise start recording */

	while(timeout) {
		nevt = mfip_readl(res->hdl, offset +
				  offsetof(struct mstrfip_record_cfg, nentries));
//		nevt = ual_readl(res->hdl, offset +
//				 offsetof(struct mstrfip_record_cfg, nentries));
		if ( (nevt == req_records) ||
		     (nevt != 0 && (nevt == prev_nevt)) )
			break;
		sleep(1);
		prev_nevt = nevt;
		fprintf(stdout, "recorded frames: %d...(record flcg:%d)\n", nevt,
			mfip_readl(res->hdl, offset +
				   offsetof(struct mstrfip_record_cfg, flg)));
//			ual_readl(res->hdl, offset + offsetof(struct mstrfip_record_cfg, flg)));
		--timeout;
	}
	/* lower start recording just in case */
	mfip_writel(res->hdl, offset +
		    offsetof(struct mstrfip_record_cfg, flg), 0); /* lower start recording */
	if (timeout == 0)
		fprintf(stdout, "recording timeout expired\n");
	if (nevt > 0) {
		fprintf(stdout, "%d frames have been recorded.\n", nevt);
		mfipdiag_dump_record(res, nevt);
	}
	return 1;
}

enum mfipdiag_cmd_id{
	MFIPDIAG_CMD_MFIPCORE = CMD_USR,
	MFIPDIAG_CMD_VICORE,
	MFIPDIAG_CMD_MTRTLSHM,
	MFIPDIAG_CMD_REPORT,
	MFIPDIAG_CMD_VERS,
	MFIPDIAG_CMD_HW_CFG,
	MFIPDIAG_CMD_BA_CFG,
	MFIPDIAG_CMD_SEG_RESP,
	MFIPDIAG_CMD_RECORD_TRAFFIC,
	MFIPDIAG_CMD_LAST,
};

#define MFIPDIAG_CMD_NB MFIPDIAG_CMD_LAST - CMD_USR
struct cmd_desc mfipdiag_cmd[MFIPDIAG_CMD_NB + 1] = {
	{ 1, MFIPDIAG_CMD_MFIPCORE, "mfip",
	  "Dump masterFIP core registers",
	  "", 0, get_mfip_core_regs},
	{ 2, MFIPDIAG_CMD_VICORE, "vic",
	  "Dump VIC core registers",
	  "", 0, get_vic_core_regs},
	{ 3, MFIPDIAG_CMD_MTRTLSHM, "mtrtlshm",
	  "Dump MockTurtle SHM content",
	  "", 0, get_mtrtl_shm},
	{ 4, MFIPDIAG_CMD_REPORT, "stats",
	  "Get masterfip statistics (report)",
	  "[-v]", 0, get_mfip_report},
	{ 5, MFIPDIAG_CMD_VERS, "vers",
	  "Get masterfip version",
	  "", 0, get_mfip_vers},
	{ 6, MFIPDIAG_CMD_HW_CFG, "hwcfg",
	  "Get HW config",
	  "", 0, get_mfip_hw_cfg},
	{ 7, MFIPDIAG_CMD_BA_CFG, "bacfg",
	  "Get macrocycle configuration",
	  "", 0, get_mfip_ba_cfg},
	{ 8, MFIPDIAG_CMD_SEG_RESP, "segcfg",
	  "Get segment configuration",
	  "", 0, get_mfip_seg_resp_time},
	{ 9, MFIPDIAG_CMD_RECORD_TRAFFIC, "rec",
	  "Start recording traffic and display it",
	  "[--ctrltype --varid --status --timeout --nrecords]",
	  0, get_mfip_record_traffic},
	{0, },
};

//static void print_version(void)
//{
//	fprintf(stderr, "Built in masterfip repo ver:%s, by %s on %s %s\n",
//		__GIT_VER__, __GIT_USR__, __TIME__, __DATE__);
//}

static void mfipdiag_help()
{
	fprintf(stderr,
		"masterfip-diag \n");
	fprintf(stderr, "No arg required.In case of several spec devices "
		"please select the rigth one\n");
	exit(1);
}

static void mfipdiag_free()
{
	if (mfipres->mfip_core.hdl != NULL)
		mfip_unmap(mfipres->mfip_core.hdl);
	if (mfipres->mtrtl_shm.hdl != NULL)
		mfip_unmap(mfipres->mtrtl_shm.hdl);
	if (mfipres->vic_core.hdl != NULL)
		mfip_unmap(mfipres->vic_core.hdl);
	if (mstrfip_dev != NULL) {
		mstrfip_exit();
		mstrfip_close(mstrfip_dev);
		mstrfip_dev = NULL;
	}
}

static void sig_hndl()
{
	// Signal occured: free resource and exit
	fprintf(stderr, "Handle signal: free resource and exit.\n");
	mfipdiag_free();
	exit(1);
}

static int mstrfip_id_to_vmeslot(struct mfipdiag_dev_desc * desc) {
	char trtl_path[32], trtl_path_full[256];
	char *svec_dev;
	unsigned int vme_slot;
	int ret;

	snprintf(trtl_path, sizeof(trtl_path), "/sys/class/mockturtle/trtl-%04x", desc->trtl_devid);
	ret = readlink(trtl_path, trtl_path_full, sizeof(trtl_path_full));
	if (ret < 0)
		return ret;

	svec_dev = strstr(trtl_path_full, "svec-vme.");

	if (!svec_dev) {
		errno = ENODEV;
		return -1;
	}

	ret = sscanf(svec_dev, "svec-vme.%d", &vme_slot);

	if (ret != 1 ) {
		errno = EINVAL;
		return -1;
	}

	desc->pci_devid = 0;
	desc->vme_slot = vme_slot;
	desc->is_vme = 1;

	return 0;
}

/**
 * Return the masterfip PCI ID from its ID
 *
 * @param[in] a PCI ID
 *
 * @return the PCI ID associated with the given masterfip ID on success, -1 on
 * error and errno is set appropriately.
 */
static int mstrfip_id_to_pciid(uint32_t devid, uint32_t *pciid)
{
	char trtl_path[32], trtl_path_full[256];
	char *spec_dev;
	unsigned int bus, slot, fun;
	int ret;

	snprintf(trtl_path, sizeof(trtl_path), "/sys/class/mockturtle/trtl-%04x", devid);
	ret = readlink(trtl_path, trtl_path_full, sizeof(trtl_path_full));
	if (ret < 0)
		return ret;

	spec_dev = strstr(trtl_path_full, "spec");
	if (!spec_dev) {
		errno = ENODEV;
		return -1;
	}
	ret = sscanf(spec_dev, "spec-0000:%x:%x.%x", &bus, &slot, &fun);
	if (ret != 3 ) {
		errno = EINVAL;
		return -1;
	}
	*pciid = (bus & 0xFF) << 16;
	*pciid |= (slot & 0xFF) << 8;
	*pciid |= fun & 0xFF;

	return 0;
}

static struct mfipdiag_dev_desc *mfipdiag_alloc_and_init(void)
{
	struct mfipdiag_dev_desc *list;
	unsigned int i;
	char **mstrfip_trtl_list;

	mfipdiag_ndev = mstrfip_count();
	if (!mfipdiag_ndev)
		return NULL;

	list = calloc(mfipdiag_ndev, sizeof(*list));
	if (!list)
		return NULL;

	mstrfip_trtl_list = mstrfip_list();
	for (i = 0; i < mfipdiag_ndev; ++i) {
		int ret;

		ret = sscanf(mstrfip_trtl_list[i], "trtl-%4x", &list[i].trtl_devid);
		if (ret != 1) {
			errno = EINVAL;
			goto err_sscanf;
		}
		ret = mstrfip_id_to_lun(list[i].trtl_devid,
					&list[i].lun);
		if (ret < 0)
			goto err_lun;

		ret = mstrfip_id_to_pciid(list[i].trtl_devid,
					  &list[i].pci_devid);

		/* if this MasterFip does not run on SPEC, check if it runs on SVEC */
		if (ret < 0)
			ret = mstrfip_id_to_vmeslot(&list[i]);

		if (ret < 0)
			goto err_pciid;
	}
	mstrfip_list_free(mstrfip_trtl_list);

	return list;

err_pciid:
err_lun:
err_sscanf:
	mstrfip_list_free(mstrfip_trtl_list);
	free(list);
	return NULL;
}

static void mfipdiag_free_list(struct mfipdiag_dev_desc *list)
{
	if (list)
		free(list);
}

int main(int argc, char *argv[])
{
	char c;
	int dev_idx, lun, res;

	mstrfip_dev = NULL;
	mfipdiag_ndev = 0;
	while ((c = getopt (argc, argv, "h?:")) != -1) {
		switch (c) {
		case 'h':
		case '?':
			mfipdiag_help();
			break;
		}
	}

	mfipdiag_devlist = mfipdiag_alloc_and_init();
	if (!mfipdiag_devlist) {
		fprintf(stderr, "Can't find MasterFIP devices. %s\n",
			strerror(errno));
		exit(1);
	}

	dev_idx = 0;
	if (mfipdiag_ndev > 1) {
		int i;

		fprintf(stdout, "MasterFIP devices found:\n");
		for (i = 0; i < mfipdiag_ndev; ++i)
			fprintf(stdout, "\tlun:%u Mockturtle device :0x%04x\n",
				mfipdiag_devlist[i].lun,
				mfipdiag_devlist[i].trtl_devid);
		fprintf(stdout, "\n");
		fprintf(stdout, "Please enter LUN: ");
		scanf("%d", &lun);
		for (i = 0; i < mfipdiag_ndev; ++i) {
			if (mfipdiag_devlist[i].lun == lun) {
				dev_idx = i;
				break;
			}
		}
		if (i == mfipdiag_ndev) {
			fprintf(stderr, "unknown lun:%d\n", lun);
			errno = ENODEV;
			res = -1;
			goto out;
		}
	}

	if (mfipdiag_devlist[dev_idx].is_vme)
		mfipres = &svec_mfipres;
	else
		mfipres = &spec_mfipres;

	mfipres->dev_idx = dev_idx;
	fprintf(stdout, "Selected LUN: %u\n",
		mfipdiag_devlist[mfipres->dev_idx].lun);

	res = extest_register_user_cmd(mfipdiag_cmd, MFIPDIAG_CMD_NB);
	if (res)
		goto out;

	/* execute command loop */
	res = extest_run("mfipdiag", sig_hndl);
out:
	mfipdiag_free();
	mfipdiag_free_list(mfipdiag_devlist);
	return (res) ? -1 : 0;
}

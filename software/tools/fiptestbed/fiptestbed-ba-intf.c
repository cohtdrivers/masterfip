// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>

#include <libmasterfip.h>

#include "fiptestbed.h"

const uint16_t ftb_var_id[2] = {
	0x05, /* produced var */
	0x06, /* consumed var */
};

static int ftb_create_per_var(struct ftb_ba *ba)
{
	struct mstrfip_data_cfg var_cfg;
	int agent_idx;

	/* build prod varlist */
	for (agent_idx = 0; agent_idx < FTB_AGENT_COUNT; agent_idx++) {
		var_cfg.max_bsz = ftb->var_length[agent_idx];
		var_cfg.id = (0xFF00 & (ftb_var_id[0] << 8)) | (agent_idx + 1);
		var_cfg.flags = MSTRFIP_DATA_FLAGS_PROD;
		var_cfg.mstrfip_data_handler = (agent_idx == FTB_AGENT_COUNT - 1) ?
			ftb_mstrfip_prod_var_handler : NULL;
		/* Set callback on the last produced periodic var */
		ba->prod_varlist[agent_idx] =
				mstrfip_var_create(ba->mc, &var_cfg);
		if (ba->prod_varlist[agent_idx] == NULL)
			return -1;
	}
	/* build consumed varlist */
	for (agent_idx = 0; agent_idx < FTB_AGENT_COUNT; agent_idx++) {
		var_cfg.max_bsz = ftb->var_length[agent_idx];
		var_cfg.id = (0xFF00 & (ftb_var_id[1] << 8)) | (agent_idx + 1);
		var_cfg.flags = MSTRFIP_DATA_FLAGS_CONS;
		var_cfg.mstrfip_data_handler = (agent_idx == FTB_AGENT_COUNT - 1) ?
			ftb_mstrfip_var_handler : NULL;
		ba->cons_varlist[agent_idx] =
				mstrfip_var_create(ba->mc, &var_cfg);
		if (ba->cons_varlist[agent_idx] == NULL)
			return -1;
	}
	return 0;
}

static int ftb_create_ident_var(struct ftb_ba *ba)
{
	int agent_idx, agent_addr;

	/* build identification varlist */
	for (agent_idx = 0; agent_idx < FTB_AGENT_COUNT; agent_idx++) {
		agent_addr = agent_idx + 1; /* addr is in the range of [1..25] */
		ba->ident_varlist[agent_idx] =
			mstrfip_ident_var_create(ba->mc, agent_addr);
		if (ba->ident_varlist[agent_idx] == NULL)
			return -1;
	}
	return 0;
}

static int ftb_set_ba_instr(struct ftb_ba *ba)
{
	struct mstrfip_per_var_wind_cfg pwind_cfg = {0};
	struct mstrfip_aper_var_wind_cfg apwind_cfg = {0};

	/* append a periodic window for produced variables */
	pwind_cfg.varlist = ba->prod_varlist; /* periodic var list */
	pwind_cfg.var_count = FTB_AGENT_COUNT; /* periodic var count */
	if (mstrfip_per_var_wind_append(ba->mc, &pwind_cfg) < 0)
		return -1;


	/* append a periodic window for consumed varaiables */
	pwind_cfg.varlist = ba->cons_varlist; /*periodic var list */
	pwind_cfg.var_count = FTB_AGENT_COUNT; /* periodic var count */
	if (mstrfip_per_var_wind_append(ba->mc, &pwind_cfg) < 0)
		return -1;


	if (ftb->opt.enable_aper_wind) {
		/* append an aperiodic window */
		apwind_cfg.end_ustime = ftb->opt.cycle_uslength - 200; /* end at 10% of cycle */
		apwind_cfg.enable_diag = 1; /* enable diagnostic */
		/* identification variable list that can be requested at run time */
		apwind_cfg.ident_varlist = ba->ident_varlist;
		apwind_cfg.ident_var_count = FTB_AGENT_COUNT;
		/* user handler calls back when an ident request has been fully served */
		apwind_cfg.mstrfip_ident_var_handler = ftb_mstrfip_ident_var_handler;
		if (mstrfip_aper_var_wind_append(ba->mc, &apwind_cfg) < 0)
			return -1;
	}

	/* append a wait window without padding (silent) */
	if (mstrfip_wait_wind_append(ba->mc, 1, ftb->opt.cycle_uslength) < 0)
		return -1;

	return 0;
}

int ftb_ba_init(struct ftb_dev *ftb)
{
	struct mstrfip_hw_cfg hw_cfg = {0};
	struct mstrfip_sw_cfg sw_cfg = {0};
	int i;

	/* set HW config */
	hw_cfg.enable_ext_trig = ftb->opt.ext_trig;
	hw_cfg.enable_int_trig = ftb->opt.int_trig;
	hw_cfg.enable_ext_trig_term = ftb->opt.ext_trig_term;
	if (ftb->opt.tr_ustime != 0)
		hw_cfg.turn_around_ustime = ftb->opt.tr_ustime;
	if (mstrfip_hw_cfg_set(ftb->dev, &hw_cfg) < 0) {
		fprintf(stderr, "Cannot configure HW: %s\n",
			mstrfip_strerror(errno));
		return -1;
	}

	/* set SW config */
	sw_cfg.irq_thread_prio = 50;
	sw_cfg.diag_thread_prio = 20;
	sw_cfg.mstrfip_error_handler = ftb_mstrfip_error_handler;
	if (mstrfip_sw_cfg_set(ftb->dev, &sw_cfg) < 0) {
		fprintf(stderr, "Cannot configure SW: %s\n",
			mstrfip_strerror(errno));
		return -1;
	}

	/* create macro cycle */
	for (i = 0; i < 2; ++i) {
		ftb->ba[i].mc = mstrfip_macrocycle_create(ftb->dev);
		if (ftb->ba[i].mc == NULL) {
			fprintf(stderr, "Create macrocyle failed: %s\n",
				mstrfip_strerror(errno));
			return -1;
		}

		/* create periodic variable */
		if (ftb_create_per_var(&ftb->ba[i]) < 0 ) {
			fprintf(stderr, "Create periodic variable failed: %s\n",
				mstrfip_strerror(errno));
			return -1;
		}

		/* create ident variable */
		if (ftb->opt.enable_aper_wind) {
			if (ftb_create_ident_var(&ftb->ba[i]) < 0 ) {
				fprintf(stderr, "Create identification variable failed: %s\n",
					mstrfip_strerror(errno));
				return -1;
			}
		}

		if (ftb_set_ba_instr(&ftb->ba[i]) < 0) {
			fprintf(stderr, "Programming BA failed: %s\n",
				mstrfip_strerror(errno));
			return -1;
		}
		if (mstrfip_macrocycle_isvalid(ftb->dev, ftb->ba[i].mc) == 0) {
			fprintf(stderr, "Macrocycle unvalid: %s\n",
				mstrfip_strerror(errno));
			return -1;
		}
	}

	/* init some fields */
	ftb->hwcycle_nstime = ftb->opt.cycle_uslength * 1000;
	ftb->swcycle_ustime = ftb->opt.cycle_uslength;
	return 0;
}

int ftb_ba_start(struct ftb_dev *ftb)
{
	struct timeval tv;
	struct tm *tm;
	int agt_idx, i, res;
	int agt_addr[FTB_AGENT_COUNT];

	/* initial control value for each agent */
	for (agt_idx = 0; agt_idx < FTB_AGENT_COUNT; ++agt_idx) {
		ftb->pctrl_byte[agt_idx][0] = 2*agt_idx;
		ftb->pctrl_byte[agt_idx][1] = 2*agt_idx + 1;
		ftb->nctrl_byte[agt_idx][0] = 2*agt_idx;
		ftb->nctrl_byte[agt_idx][1] = 2*agt_idx + 1;
	}
	gettimeofday(&tv, NULL);
	tm=localtime(&tv.tv_sec);
	snprintf(ftb->start_date, 127, "%02d/%02d/%04d %d:%02d:%02d",
		tm->tm_mday, tm->tm_mon+1, 1900 + tm->tm_year, tm->tm_hour,
		tm->tm_min, tm->tm_sec);
	ftb->ba_idx = (ftb->ba_idx == 0) ? 1 : 0;
	if (mstrfip_ba_reset(ftb->dev) < 0) {
		fprintf(stderr, "Reset BA failed: %s\n",
			mstrfip_strerror(errno));
		return -1;
	}
	if (mstrfip_ba_load(ftb->dev, ftb->ba[ftb->ba_idx].mc) < 0) {
		fprintf(stderr, "Loading macrocycle failed: %s\n",
			mstrfip_strerror(errno));
		return -1;
	}

	/* get turn around time of each agents */
	for (i = 0; i < FTB_AGENT_COUNT; ++i)
		agt_addr[i] = i + 1;
	res = mstrfip_hw_response_time_get(ftb->dev, FTB_AGENT_COUNT, agt_addr,
						ftb->tr_nstime);
	if (res) {
		fprintf(stderr, "Read turn around time failed: %s\n",
			mstrfip_strerror(errno));
	}

	if (mstrfip_ba_start(ftb->dev) < 0) {
		fprintf(stderr, "Start BA failed: %s\n",
			mstrfip_strerror(errno));
		return -1;
	}
	return 0;
}

int ftb_ba_stop(struct ftb_dev *ftb)
{
	if (mstrfip_ba_stop(ftb->dev) < 0) {
		fprintf(stderr, "Stop BA failed: %s\n",
			mstrfip_strerror(errno));
		return -1;
	}
	return 0;
}

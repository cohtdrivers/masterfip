// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <libmasterfip.h>

#include "fiptestbed.h"

static char* fsm_ba_state_string(uint32_t fsm_state)
{
	switch (fsm_state) {
	case MSTRFIP_FSM_RUNNING:
		return "FSM_RUNNING";
		break;
	case MSTRFIP_FSM_READY:
		return "MSTRFIP_FSM_READY";
		break;
	case MSTRFIP_FSM_INITIAL:
		return "MSTRFIP_FSM_INITIAL";
		break;
	default:
		return "";
		break;
	}
	return "";
}

static char* fsm_cycle_state_string(uint32_t state)
{
	switch (state) {
	case MSTRFIP_FSM_WAIT_TX_END:
		return "FSM_WAIT_TX_END";
		break;
	case MSTRFIP_FSM_WAIT_RX_PREAMB:
		return "FSM_WAIT_RX_FRAME_PREAMB";
		break;
	case MSTRFIP_FSM_WAIT_RX_END:
		return "FSM_WAIT_RX_END";
		break;
	case MSTRFIP_FSM_WAIT_EXT_TRG:
		return "FSM_WAIT_EXT_TRIGGER";
		break;
	case MSTRFIP_FSM_WAIT_RX_CTRL_BYTE:
		return "FSM_WAIT_RX_CTRL_BYTE";
		break;
	case MSTRFIP_FSM_RUN_WAIT:
		return "MSTRFIP_FSM_RUN_WAIT";
		break;
	case MSTRFIP_FSM_RUN_APER_MSG:
		return "MSTRFIP_FSM_RUN_APER_MSG";
		break;
	case MSTRFIP_FSM_RUN_APER_VAR:
		return "MSTRFIP_FSM_RUN_APER_VAR";
		break;
	case MSTRFIP_FSM_RUN_PER_VAR:
		return "MSTRFIP_FSM_RUN_PER_VAR";
		break;
	default:
		return "";
		break;
	}
	return "";
}

static void write_stream(struct mstrfip_irq *irq, struct mstrfip_report *report,
				int error, FILE *stream)
{
	int agt_idx, i;
	struct mstrfip_data *pvar;
	char time_str[32], *fsm_ba_state_str, *fsm_cycle_state_str;
	time_t epoch_time;
	uint8_t *ptr8;



	fprintf(stream, "%s bus-speed:%s temp:%d Cycle:%dus \n",
			ftb->dev_id_str, ftb->bus_speed_str, report->temp,
			ftb->opt.cycle_uslength);
	fprintf(stream, "Start at : %s\n",ftb->start_date);

	fprintf(stream, "Cycle: %9d var_cb_time: %dus(max: %dus)\n",
		ftb->cycle_counter, ftb->cb_ustime, ftb->max_cb_ustime);
	fprintf(stream, "hw_meas_cycle: %ldns Jitter: %ldns "
		"(max: %ldns)\n",
		ftb->hwcycle_nstime, ftb->hwirq_nsjitter,
		ftb->max_hwirq_nsjitter);
	fprintf(stream, "sw_meas_cycle: %dus Jitter: %ldus "
		"(max: %ldus)\n",
		ftb->swcycle_ustime, ftb->swirq_usjitter,
		ftb->max_swirq_usjitter);

	fprintf(stream, "Var_irq: : irq_count:%d var_count:%d irq_lost:%d\n",
		irq->irq_count, irq->acq_count, ftb->irq_lost);

	fsm_ba_state_str = fsm_ba_state_string(report->ba_state);
	fsm_cycle_state_str = fsm_cycle_state_string(report->cycle_state);
	fprintf(stream, "Report: TX_ok:%d TX_err:%d RX_ok:%d RX_err:%d "
		"fsm_ba_state:%s fsm_cycle_state:%s\n",
		report->tx_ok, report->tx_err, report->rx_ok, report->rx_err,
		fsm_ba_state_str, fsm_cycle_state_str);
	time_str[0] = '\0';
	if (report->fd_tx_watchdog) {
		epoch_time = report->fd_tx_watchdog_hwtime;
		strftime(time_str, sizeof(time_str), "%c", localtime(&epoch_time));
	}
	fprintf(stream, "fieldrive TX watchdog: %d (time: %s)\n",
		report->fd_tx_watchdog, time_str);
	time_str[0] = '\0';
	if (report->fd_tx_err) {
		epoch_time = report->fd_tx_err_hwtime;
		strftime(time_str, sizeof(time_str), "%c", localtime(&epoch_time));
	}
	fprintf(stream, "fieldrive TX err: %d (time: %s)\n",
		report->fd_tx_err, time_str);
	fprintf(stream, "fieldrive CD err: %d \n", report->fd_cd);

	fprintf(stream, "cb_err_count:%d ext_trig_count:%d "
		"ext_trig_missed_count:%d int_sync_pulse_count:%d "
		"cb_error_msg:%s(%d)\n", ftb->cb_error_counter,
		report->ext_sync_pulse_count,
		report->ext_sync_pulse_missed_count,
		report->int_sync_pulse_count,
		(error) ? mstrfip_strerror(error) : "", error);

	fprintf(stream, "Agt sz    ns_tr rd0 rd1 wr0 wr1     !write     !=data  !received    "
		"--payload_status--     fr_tmo     fr_err   fr_nbyte "
		" fr_badpdu fr_badctrl\n");
	fprintf(stream, "                                                	           "
		"!refresh  !signific\n");

	for (agt_idx = 0; agt_idx < ftb->opt.agent_count; ++agt_idx) {
		pvar = ftb->ba[ftb->ba_idx].cons_varlist[agt_idx];
		fprintf(stream, "%03d %03d  %6d  %02x  %02x  %02x  %02x %10d %10d %10d "
			"%10d %10d %10d %10d %10d %10d %10d\n",
			agt_idx + 1, pvar->bsz, ftb->tr_nstime[agt_idx],
			pvar->buffer[0],
			pvar->buffer[1], ftb->pctrl_byte[agt_idx][0],
			ftb->pctrl_byte[agt_idx][1],
			ftb->var_err[agt_idx].write_failed,
			ftb->var_err[agt_idx].bad_data,
			ftb->var_err[agt_idx].missed,
			ftb->var_err[agt_idx].not_freshed,
			ftb->var_err[agt_idx].not_significant,
			ftb->var_err[agt_idx].tmo,
			ftb->var_err[agt_idx].hw_err,
			ftb->var_err[agt_idx].hw_bsz,
			ftb->var_err[agt_idx].hw_pdu,
			ftb->var_err[agt_idx].hw_ctrl);
	}
	if (ftb->opt.enable_aper_wind) {
		fprintf(stream, "ident var. update_count: %d\n",
					ftb->ident_refresh_counter);
		for (agt_idx = 0; agt_idx < ftb->opt.agent_count; ++agt_idx) {
			pvar = ftb->ba[ftb->ba_idx].ident_varlist[agt_idx];
			ptr8 = (uint8_t *)pvar->buffer;
			fprintf(stream, "%03d ", agt_idx + 1);
			for (i = 0; i < 8; ++i)
				fprintf(stream, "0x%x ", ptr8[i]);
			fprintf(stream, "\n");
		}
	}
}

static void update_display(struct mstrfip_irq *irq, struct mstrfip_report *report,
				int error)
{
	FILE *stream = stdout;
	printf("%c[;H%c[2J",27,27);
	write_stream(irq, report, error, stream);
}

static int ftb_read_acq()
{
	int agt_idx, i, j, count, bsz;
	struct mstrfip_data *pvar;
	int found_error, res;
	uint8_t *ptr_ref;

	found_error = 0;
	for (agt_idx = 0; agt_idx < ftb->opt.agent_count; ++agt_idx) {
		pvar = ftb->ba[ftb->ba_idx].cons_varlist[agt_idx];
		mstrfip_var_update(ftb->dev, pvar);
		switch (pvar->status) {
		case MSTRFIP_DATA_OK:
			// how many pair of two bytes
			count = (pvar->bsz / 2);
			// Agent in stand alone mode only the first byte is
			// sending back (so only one byte to compare)
			// If count is more than one (means nanofip dev agent)
			// which send back all the bytes (compared 2 by 2)
			bsz = (count > 1) ? 2 : 1;
			// facts: nanofip device is faster than microfip
			// and produced variable are scheduled before
			// consumed variables (to be able to measure mockturtle
			// jitter). For nanofip (count > 1 ) the comparison is
			// done with next ctrl while for microfip is done with
			// previous ctrl
			if (ftb->fbus_speed != MSTRFIP_BITRATE_31) {
				ptr_ref = (count > 1) ? &(ftb->nctrl_byte[agt_idx][0]) :
						&(ftb->pctrl_byte[agt_idx][0]);
			}
			else {
				// bus speed MSTRFIP_BITRATE_31 so slow
				// that next ctrl is used to compare with acq
				ptr_ref = &(ftb->nctrl_byte[agt_idx][0]);
			}
			for (i = 0, j = 0; i < count; ++i, j+=2) {
				res = memcmp(&(pvar->buffer[j]), ptr_ref,
								bsz);
				if (res) {
					++(ftb->var_err[agt_idx].bad_data);
					found_error = 1;
					continue; /* leave, one error detected */
				}
			}
			break;

		case MSTRFIP_DATA_PAYLOAD_ERROR:
			found_error = 1;
			if (pvar->payload_error &
					MSTRFIP_FRAME_PAYLOAD_NOT_REFRESH)
				++(ftb->var_err[agt_idx].not_freshed);
			if (pvar->payload_error &
					MSTRFIP_FRAME_PAYLOAD_NOT_SIGNIFICANT)
				++(ftb->var_err[agt_idx].not_significant);
			break;

		case MSTRFIP_DATA_NOT_RECEIVED:
			found_error = 1;
			++(ftb->var_err[agt_idx].missed);
			break;

		case MSTRFIP_DATA_FRAME_ERROR:
			found_error = 1;
			if (pvar->frame_error & MSTRFIP_FRAME_TMO)
				++(ftb->var_err[agt_idx].tmo);
			if (pvar->frame_error & MSTRFIP_FRAME_ERR)
				++(ftb->var_err[agt_idx].hw_err);
			if (pvar->frame_error & MSTRFIP_FRAME_BAD_CTRL)
				++(ftb->var_err[agt_idx].hw_ctrl);
			if (pvar->frame_error & MSTRFIP_FRAME_BAD_PDU)
				++(ftb->var_err[agt_idx].hw_pdu);
			if (pvar->frame_error & MSTRFIP_FRAME_BAD_BSZ)
				++(ftb->var_err[agt_idx].hw_bsz);
		}
	}
	return found_error;
}

static int ftb_send_ctrl()
{
	int agt_idx, res;
	struct mstrfip_data *pvar;
	int count, j, i;

	/* increment setting */
	/* send new setting */
	res = 0;
	for (agt_idx = 0; agt_idx < ftb->opt.agent_count; ++agt_idx) {
		pvar = ftb->ba[ftb->ba_idx].prod_varlist[agt_idx];
		ftb->pctrl_byte[agt_idx][0] = ftb->nctrl_byte[agt_idx][0];
		ftb->pctrl_byte[agt_idx][1] = ftb->nctrl_byte[agt_idx][1];
		if (ftb->nctrl_byte[agt_idx][1] == 127) {
			ftb->nctrl_byte[agt_idx][0] = 0;
			ftb->nctrl_byte[agt_idx][1] = 1;
		}
		else {
			++(ftb->nctrl_byte[agt_idx][0]);
			++(ftb->nctrl_byte[agt_idx][1]);
		}
		/* Sent new ctrl value: two bytes filled with nctrl_byte value */
		pvar->bsz = ftb->var_length[agt_idx];
		count = (pvar->bsz / 2);
		for (i = 0, j = 0; i < count; ++i, j+=2)
			memcpy((void *)&pvar->buffer[j], (void *)&(ftb->nctrl_byte[agt_idx][0]),
					FTB_VAR_LENGTH);
		res |= mstrfip_var_write(ftb->dev, pvar);
		if (res) {
			++ftb->var_err[agt_idx].write_failed;
			fprintf(stderr, "mstrfip_var_write error:%d(%s)\n",
				res, strerror(res));
		}
	}
	return res;
}

static int ftb_send_ident()
{
	int res;

	res = mstrfip_ident_request(ftb->dev, ftb->ba[ftb->ba_idx].ident_varlist,
					ftb->opt.agent_count);
	if (res)
		fprintf(stdout, "mstrfip_ident_request failed\n");
	return res;
}

void ftb_mstrfip_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *pvar,
				struct mstrfip_irq *irq)
{
	uint64_t sw_ustime;
//	struct timeval tval;
	uint64_t jitter, val1, val2;
	struct mstrfip_report report;
	int err, count;
	struct timespec tval;

	clock_gettime(CLOCK_REALTIME, &tval);
	sw_ustime = (tval.tv_sec * 1000000) + (tval.tv_nsec / 1000);

//	gettimeofday(&tval, NULL);
//	sw_ustime = tval.tv_usec + (tval.tv_sec * 1000000);

	++ftb->cycle_counter;
	err = ftb_read_acq();
	err |= ftb_send_ctrl();
	ftb->error_detected = (err != 0) ? err : ftb->error_detected;
	if (ftb->opt.enable_aper_wind && (!(ftb->cycle_counter % 5)))
		ftb_send_ident();
	/* compute max jitter and handler duration */
	if (ftb->cycle_counter <= 2) { /* skip first 2 cycles */
		ftb->sw_ustime = sw_ustime;
		ftb->max_swirq_usjitter = 0;
		ftb->max_cb_ustime = 0;
	}
	else {
		val1 = sw_ustime - ftb->sw_ustime;
		val2 = ftb->opt.cycle_uslength;
		jitter = labs(val1 - val2);
//		if (jitter > 300) {
//			/* just to debug why rarely jitter is so big */
//			mstrfip_ba_stop(ftb->dev);
//			mstrfip_ba_reset(ftb->dev);
//			fprintf(stdout, "jitter:%ld clock_gettime(us) current:%ld "
//				"previous:%ld\n", jitter, sw_ustime, ftb->sw_ustime);
//			fprintf(stderr, "wrong jitter\n");
//		}
		ftb->swcycle_ustime = val1;
		ftb->sw_ustime = sw_ustime;
		ftb->swirq_usjitter = jitter;
		if (jitter > ftb->max_swirq_usjitter)
			ftb->max_swirq_usjitter = ftb->swirq_usjitter;
	}
	mstrfip_report_get(dev, &report);

	clock_gettime(CLOCK_REALTIME, &tval);
	ftb->cb_ustime = ((tval.tv_sec * 1000000) + (tval.tv_nsec / 1000)) - ftb->sw_ustime;
//	gettimeofday(&tval, NULL);
//	ftb->cb_ustime = tval.tv_usec + (tval.tv_sec * 1000000) - ftb->sw_ustime;
	if (ftb->cb_ustime > ftb->max_cb_ustime)
		ftb->max_cb_ustime = ftb->cb_ustime;

	count = irq->irq_count - ftb->irq_count;
	if (count > 1)
		ftb->irq_lost += count;
	ftb->irq_count = irq->irq_count;

	if (!(ftb->cycle_counter % ftb->opt.update_display_rate))
		update_display(irq, &report, 0);
}

void ftb_mstrfip_prod_var_handler(struct mstrfip_dev *dev, struct mstrfip_data *pvar,
				struct mstrfip_irq *irq)
{
	uint64_t hw_nstime;
	uint64_t jitter;
	uint64_t hw_sec;
	uint64_t cycle_uslength;
	int count;

	/* compute max jitter and handler duration */
	hw_sec = irq->hw_sec;
	hw_nstime = hw_sec * 1000000000 + irq->hw_ns;
	if (ftb->cycle_counter < 2) { /* skip first 2 cycles */
		ftb->hw_nstime = hw_nstime;
		ftb->max_hwirq_nsjitter = 0;
		ftb->hwcycle_nstime = 0;
	}
	else {
		ftb->hwcycle_nstime = hw_nstime - ftb->hw_nstime;
		cycle_uslength = ftb->opt.cycle_uslength;
		jitter = labs(ftb->hwcycle_nstime - (cycle_uslength * 1000));
		ftb->hwirq_nsjitter = jitter;
//		if (jitter > 4000) { //admissible jitter in case of recording enabled
//			/* just to debug why rarely jitter is so big */
//			mstrfip_ba_stop(ftb->dev);
//			mstrfip_ba_reset(ftb->dev);
//			fprintf(stdout, "jitter:%ld hw time(ns) current:%ld "
//				"previous:%ld\n", jitter, hw_nstime, ftb->hw_nstime);
//			fprintf(stderr, "wrong jitter\n");
//		}
		ftb->hw_nstime = hw_nstime;
		if (jitter > ftb->max_hwirq_nsjitter)
			ftb->max_hwirq_nsjitter = jitter;
	}
//	fprintf(stdout, "hw_nstime:%ld, hw_sec:%d, hw_ns:%d "
//		"ftb->hwcycle_nstime:%ld ftb->hw_nstime:%ld "
//		"ftb->hwirq_nsjitter:%ld jitter:%ld ftb->max_hwirq_nsjitter:%ld\n",
//		hw_nstime, irq->hw_sec, irq->hw_ns,
//		ftb->hwcycle_nstime, ftb->hw_nstime, ftb->hwirq_nsjitter,
//		jitter, ftb->max_hwirq_nsjitter);
	count = irq->irq_count - ftb->irq_count;
	if (count > 1)
		ftb->irq_lost += count;
	ftb->irq_count = irq->irq_count;
}

void ftb_mstrfip_ident_var_handler(struct mstrfip_dev *dev,
					struct mstrfip_irq *irq)
{
	int count;

	mstrfip_identlist_update(ftb->dev, ftb->ba[ftb->ba_idx].ident_varlist,
						ftb->opt.agent_count);
	++ftb->ident_refresh_counter;
	count = irq->irq_count - ftb->irq_count;
	if (count > 1)
		ftb->irq_lost += count;
	ftb->irq_count = irq->irq_count;
}

void ftb_mstrfip_error_handler(struct mstrfip_dev *dev, enum mstrfip_error_list error)
{
	struct mstrfip_report report;
	struct mstrfip_irq irq;

	memset(&irq, '\0', sizeof(irq));
	mstrfip_report_get(dev, &report);
	++ftb->cb_error_counter;
	update_display(&irq, &report, error);
}

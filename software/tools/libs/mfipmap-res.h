// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef __LIBMFIPMAP_H__
#define __LIBMFIPMAP_H__

#include <stdint.h>
#include <arpa/inet.h>

#define mfipmemr32(is_be, val) ((is_be) ? ntohl(val) : val)
#define mfipmemw32(is_be, val) ((is_be) ? htonl(val) : val)

struct mfip_map_args {
	uint64_t devid; /* pci devid */
	uint16_t vme_slot; /* vme slot (used only for SVEC)*/
	int      bar; /* bar number */
	uint64_t offset; /* resource offset in the bar */
	int      is_be; /* tells the device endianess */
	int		 is_vme; /* tells the device bus */
};

/* device resource mapped into memory */
struct mfip_map_hdl {
	int fd;
	void *mmap;
	int map_pa_length; /* mapped length is page aligned */
	volatile void *base; /* address of the concerned memory region */
	int is_be; /* tells the device endianess */
};

/**
 * @defgroup device resource mapping
 *@{
 */
extern struct mfip_map_hdl *mfip_map(struct mfip_map_args *map_args,
				     uint64_t map_length);
extern void mfip_unmap(struct mfip_map_hdl *map_hdl);

static inline uint32_t mfip_readl(struct mfip_map_hdl *hdl, uint32_t offset)
{
	volatile uint32_t *ptr = hdl->base + offset;
	return (hdl->is_be) ? (ntohl(*ptr)) : *ptr;
}

static inline void mfip_writel(struct mfip_map_hdl *hdl, uint32_t offset, uint32_t val)
{
	volatile uint32_t *ptr = hdl->base + offset;
	*ptr = (hdl->is_be) ? (htonl(val)) : val;
}

/** @}*/

#endif //__LIBDEVMAP_H__

/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef __mft_GEN_LIB_H
#define __mft_GEN_LIB_H


#ifdef __cplusplus
extern "C" {
#endif

#include <libmasterfip.h>

struct mft_mcycle {
	struct mstrfip_macrocycle *mc;

	/*
	 * list of all periodic vars sorted in the order in which
	 * they are scheduled in the complete macro cycle
	 */
	struct mstrfip_data **sorted_pvars;  /* array of pointer to per var */
	int 		    npvar;           /* number of elements in the array */

	/*
	 * for convenience, mainly in callback, produced and consumed periodic
	 * variables can be retrieved from 2 different lists instead of
	 * iterating over the complete one above to find cons pvar for instance
	 */
	struct mstrfip_data **cons_pvars;
	int	            ncons_pvar;
	struct mstrfip_data **prod_pvars;
	int	            nprod_pvar;

	/*
	 * list of all aperiodic variables instantiated in the macro cycle
	 */
	struct mstrfip_data **apvars;
	int	            napvar;

	/*
	 * list of all aperiodic messages
	 */
	struct mstrfip_data **cons_apmsgs;
	int	            ncons_apmsg;
	struct mstrfip_data **prod_apmsgs;
	int	            nprod_apmsg;
};



typedef void (* mft_data_cb)(struct mstrfip_dev *dev,
			      struct mstrfip_data *data,
			      struct mstrfip_irq *irq);

struct mft_data_desc {
	int addr;        /* 2bytes ex: 0x060A: [06:var number][0a: agt number] */
	int bsz;         /* payload size in bytes */
	int dir;         /* mft direction: Produced(OUT) or Consumed(IN) */
	mft_data_cb cb; /* User callback called when the variable is scheduled */
};

enum mft_wind_type {
	MFT_WIND_PER_VAR,
	MFT_WIND_APER_VAR,
	MFT_WIND_APER_MSG,
	MFT_WIND_WAIT,
};

struct mft_wind_pvar_desc {
	int pvar_count;	       /* number of per var */
	struct mft_data_desc *pvars_desc[];  /* list of per var */
};

typedef void (* mft_ident_var_cb)(struct mstrfip_dev *dev,
			          struct mstrfip_irq *irq);

struct mft_wind_apvar_desc {
	int end_ustime;                    /* macrocycle time in us when the aper
					      wind ends */
	int enable_diag; 	           /* enable diagnostic */
	int ident_var_count;               /* number of identification var */
	struct mft_data_desc **ident_vars; /* list of identification var */
	mft_ident_var_cb ident_var_cb;     /* User callback called when all ident
					      vars from the list have been scheduled*/
};

struct mft_wind_apmsg_desc {
	int dummy;
};

struct mft_wind_wait_desc {
	int end_ustime; /* macrocycle time in us when the wait wind ends */
	int silent;     /* 1: disable stuffing 0: enable stuffing */
};


struct mft_wind_desc {
	enum mft_wind_type type;
	union {
		struct mft_wind_pvar_desc *wind_pvar_desc;
		struct mft_wind_apvar_desc *wind_apvar_desc;
		struct mft_wind_apmsg_desc *wind_apmsg_desc;
		struct mft_wind_wait_desc *wind_wait_desc;
	};
};

struct mft_mcycle_desc {
	int wind_count;
	struct mft_wind_desc winds_desc[];
};

struct mft_options {
	int ext_trig;
	uint32_t tr_ustime;
	int irq_thread_prio;
};

typedef void (* mft_error_handler_cb)(struct mstrfip_dev *dev,
				      enum mstrfip_error_list error);
struct mft_config_data {
	struct mft_options *opts;
	struct mft_mcycle_desc *mcycle_desc;

	/* callback in case of runtime error */
	mft_error_handler_cb mfip_error_handler;
};

struct mft_dev {
	struct mft_config_data *cfg_data;
	int devid;
	int mcycle_count;
	enum mstrfip_bitrate speed;
	struct mstrfip_dev *dev;
	struct mft_mcycle mcycle;
};

extern struct mft_dev *mft_init(int devid);
extern int mft_load_mcycle(struct mft_dev *mft, struct mft_config_data *cfg_data);
extern int mft_init_hw_sw_cfg(struct mft_dev* mft);
extern int mft_create_mcycle(struct mft_dev* mft);
extern void mft_delete_mcycle(struct mft_dev* mft);
extern void mft_exit(struct mft_dev *mft_dev);
extern int mft_start_mcycle(struct mft_dev *mft);
extern int mft_stop_mcycle(struct mft_dev *mft);
extern int mft_wait(struct timespec *ts);
extern void mft_print_report(struct mstrfip_report *report);
extern char* mft_speed2str(int speed);
#ifdef __cplusplus
};
#endif

#endif //__LIB_MASTERFIP_H

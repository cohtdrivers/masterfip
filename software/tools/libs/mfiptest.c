// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <libmasterfip.h>

#include "mfiptest.h"

static void mft_signal_dis() {
	sigset_t newmask;
	/*
	 * block signals: set properly the signal mask in the main thread
	 * because other threads will inherit the thread sigmask
	 */
	sigemptyset(&newmask);
	sigaddset(&newmask, SIGINT);
	sigaddset(&newmask, SIGTERM);
	sigaddset(&newmask, SIGABRT);
	sigaddset(&newmask, SIGFPE);
	sigaddset(&newmask, SIGILL);
	sigaddset(&newmask, SIGUSR1);
	pthread_sigmask(SIG_BLOCK, &newmask, NULL);
}

int mft_wait(struct timespec *ts)
{
	static int init = 0;
	static sigset_t newmask;
	siginfo_t siginfo;
	int signo;

	if (!init) {
		/*
	 	 * All signals are received by main thread
	 	 */
		sigemptyset(&newmask); /* unblock any signal */
		sigaddset(&newmask, SIGINT);
		sigaddset(&newmask, SIGTERM);
		sigaddset(&newmask, SIGABRT);
		sigaddset(&newmask, SIGFPE);
		sigaddset(&newmask, SIGILL);
		sigaddset(&newmask, SIGUSR1);
		init = 1;
	}
	signo = sigtimedwait(&newmask, &siginfo, ts);
	return (signo > 0) ? 1 : 0;
}

int mft_init_hw_sw_cfg(struct mft_dev* mft)
{
	struct mstrfip_hw_cfg hw_cfg = {0};
	struct mstrfip_sw_cfg sw_cfg = {0};
	int int_trig, prio;

	/* set HW config */
	int_trig = (mft->cfg_data->opts->ext_trig == 0) ? 1 : 0;
	hw_cfg.enable_ext_trig = mft->cfg_data->opts->ext_trig;
	hw_cfg.enable_int_trig = int_trig;
//	hw_cfg.enable_ext_trig_term = mft->opt.ext_trig_term;
	if (mft->cfg_data->opts->tr_ustime != 0)
		hw_cfg.turn_around_ustime = mft->cfg_data->opts->tr_ustime;
	if (mstrfip_hw_cfg_set(mft->dev, &hw_cfg) < 0) {
		return -1;
	}

	/* set SW config */
	prio = (mft->cfg_data->opts->irq_thread_prio == 0) ?
				50 : mft->cfg_data->opts->irq_thread_prio;
	sw_cfg.irq_thread_prio = prio;
	sw_cfg.diag_thread_prio = 20;
	sw_cfg.mstrfip_error_handler = mft->cfg_data->mfip_error_handler;
	if (mstrfip_sw_cfg_set(mft->dev, &sw_cfg) < 0) {
		return -1;
	}
	return 0;
}

char* mft_speed2str(int speed)
{
	switch (speed) {
	case MSTRFIP_BITRATE_31:
		return "31.25 kb/s";
	case MSTRFIP_BITRATE_1000:
		return "1 Mb/s";
	case MSTRFIP_BITRATE_2500:
		return "2.5 Mb/s";
	default:
	case MSTRFIP_BITRATE_UNDEFINED:
		break;
	}
	return "Undefined speed";
}

void mft_delete_mcycle(struct mft_dev* mft)
{
	struct mft_mcycle *mcycle = &mft->mcycle;

	if (mcycle->cons_pvars) {
		free(mcycle->cons_pvars);
		mcycle->cons_pvars = NULL;
		mcycle->ncons_pvar = 0;
	}
	if (mcycle->prod_pvars) {
		free(mcycle->prod_pvars);
		mcycle->prod_pvars = NULL;
		mcycle->nprod_pvar = 0;
	}
	if (mcycle->sorted_pvars) {
		free(mcycle->sorted_pvars);
		mcycle->sorted_pvars = NULL;
		mcycle->npvar = 0;
	}
	if (mcycle->mc) {
		mstrfip_macrocycle_delete(mft->dev, mcycle->mc);
		mcycle->mc = NULL;
	}
}

static int mft_mcycle_alloc(struct mft_dev* mft)
{
	int ncons_pvar = 0, nprod_pvar = 0;
	int idx;
	struct mft_mcycle_desc *desc = mft->cfg_data->mcycle_desc;

	for (idx = 0; idx < desc->wind_count; ++idx) {
		if (desc->winds_desc[idx].type == MFT_WIND_PER_VAR) {
			int i;
			struct mft_wind_pvar_desc *pvar_wind = desc->winds_desc[idx].wind_pvar_desc;

			for (i = 0; i < pvar_wind->pvar_count; ++i) {
				if (pvar_wind->pvars_desc[i]->dir == MSTRFIP_DATA_FLAGS_CONS)
					++ncons_pvar;
				else
					++nprod_pvar;
			}
		}
	}

	/* allocate all periodic var array */
	mft->mcycle.sorted_pvars = calloc(ncons_pvar + nprod_pvar,
				  	  sizeof(struct mstrfip_var *));
	if (mft->mcycle.sorted_pvars == NULL)
		goto err;

	/* allocate consumed periodic var array */
	mft->mcycle.cons_pvars = calloc(ncons_pvar, sizeof(struct mstrfip_var *));
	if (mft->mcycle.cons_pvars == NULL)
		goto err;

	/* allocate consumed periodic var array */
	mft->mcycle.prod_pvars = calloc(nprod_pvar, sizeof(struct mstrfip_var *));
	if (mft->mcycle.prod_pvars == NULL)
		goto err;

	/* aperiodic variables */
	/* not implemented */
	/* aperiodic messages */
	/* not implemented */

	return 0;

err:
	mft_delete_mcycle(mft);
	return -1;
}

static int mft_create_pvar(struct mft_mcycle *mcycle,
			   struct mft_data_desc *pvar_desc)
{
	struct mstrfip_data_cfg var_cfg;
	struct mstrfip_data *pvar;

	var_cfg.max_bsz = pvar_desc->bsz;
	var_cfg.id = pvar_desc->addr;
	var_cfg.flags = pvar_desc->dir;
	var_cfg.mstrfip_data_handler = pvar_desc->cb;
	pvar = mstrfip_var_create(mcycle->mc, &var_cfg);
	if (pvar == NULL)
		return -1;

	mcycle->sorted_pvars[mcycle->npvar] = pvar;
	++mcycle->npvar;

	if (pvar_desc->dir == MSTRFIP_DATA_FLAGS_CONS) {
		mcycle->cons_pvars[mcycle->ncons_pvar] = pvar;
		++mcycle->ncons_pvar;
	}
	else {
		mcycle->prod_pvars[mcycle->nprod_pvar] = pvar;
		++mcycle->nprod_pvar;
	}
	return 0;
}

int mft_create_mcycle(struct mft_dev* mft)
{
	struct mft_mcycle_desc *desc = mft->cfg_data->mcycle_desc;
	int idx;

	mft->mcycle.mc = mstrfip_macrocycle_create(mft->dev);
	if (mft->mcycle.mc == NULL) {
		return -1;
	}

	if (mft_mcycle_alloc(mft) < 0) {
		mft_delete_mcycle(mft);
		return -1;
	}

	for (idx = 0; idx < desc->wind_count; ++idx) {
		if (desc->winds_desc[idx].type == MFT_WIND_PER_VAR) {
			int i, start_idx, pvar_count;
			struct mft_wind_pvar_desc *pvar_wind = desc->winds_desc[idx].wind_pvar_desc;
			struct mstrfip_per_var_wind_cfg pwind_cfg = {0};

			start_idx = mft->mcycle.npvar;
			for (i = 0; i < pvar_wind->pvar_count; ++i) {
				mft_create_pvar(&mft->mcycle, pvar_wind->pvars_desc[i]);
			}
			pvar_count = mft->mcycle.npvar - start_idx;
			/* periodic var list attached to this window */
			pwind_cfg.varlist = &mft->mcycle.sorted_pvars[start_idx];
			pwind_cfg.var_count = pvar_count;
			if (mstrfip_per_var_wind_append(mft->mcycle.mc,
							&pwind_cfg) < 0)
				return -1;
		}
		else if (desc->winds_desc[idx].type == MFT_WIND_APER_VAR) {
			struct mft_wind_apvar_desc *apvar_wind = desc->winds_desc[idx].wind_apvar_desc;
			struct mstrfip_aper_var_wind_cfg apwind_cfg = {0};

			apwind_cfg.enable_diag = apvar_wind->enable_diag;
			apwind_cfg.end_ustime = apvar_wind->end_ustime;
			apwind_cfg.ident_varlist = NULL;
			apwind_cfg.ident_var_count = 0;
			apwind_cfg.mstrfip_ident_var_handler = apvar_wind->ident_var_cb;
			if (mstrfip_aper_var_wind_append(mft->mcycle.mc,
							 &apwind_cfg) < 0)
				return -1;
		}
		else if (desc->winds_desc[idx].type == MFT_WIND_WAIT) {
			struct mft_wind_wait_desc *wait_wind = desc->winds_desc[idx].wind_wait_desc;

			if (mstrfip_wait_wind_append(mft->mcycle.mc,
						     wait_wind->silent,
						     wait_wind->end_ustime) < 0)
				return -1;
		}
	}

	return 0;
}

static struct mstrfip_dev *mft_open_dev(int devid)
{
	int res;
	struct mstrfip_dev *dev;

	res = mstrfip_init();
	if (res) {
		fprintf(stdout, "Cannot init fip library: %s\n",
			mstrfip_strerror(errno));
		return NULL;
	}
	/* device can be specified by lun or by pci dev id */
	dev = mstrfip_open_by_id(devid);
	if(dev == NULL) {
		fprintf(stdout, "Cannot open mstrfip dev: %s\n",
			mstrfip_strerror(errno));
		return NULL;
	}

	/* reset RT app */
	if (mstrfip_rtapp_reset(dev) < 0) {
		fprintf(stdout, "Cannot reset rt app: %s\n",
			mstrfip_strerror(errno));
		mstrfip_close(dev);
		return NULL;
	}
	return dev;
}

void mft_exit(struct mft_dev *mft_dev)
{
}

struct mft_dev *mft_init(int devid)
{
	struct mft_dev* mft;
	int res;

	mft = calloc(1, sizeof(struct mft_dev));
	if (mft == NULL) {
		perror("Can't instantiate mft device\n");
		exit(1);
	}

	mft_signal_dis(); /* block all signals */

	mft->devid = devid;
	mft->dev = mft_open_dev(mft->devid);
	if (mft->dev == NULL)
		return NULL;

	/* Get FIP bus speed */
	res = mstrfip_hw_speed_get(mft->dev, &mft->speed);
	if (res)
		fprintf(stderr, "Can't get FIP speed: %s. Exit\n",
			mstrfip_strerror(errno));
	fprintf(stdout, "Bus speed: %s\n", mft_speed2str(mft->speed));

	return mft;
}

int mft_load_mcycle(struct mft_dev *mft, struct mft_config_data *cfg_data)
{
	int res;

	mft->cfg_data = cfg_data;

	res = mft_init_hw_sw_cfg(mft);
	if (res) {
		mft_exit(mft);
		return res;
	}

	res = mft_create_mcycle(mft);
	if (res) {
		mft_exit(mft);
		return res;
	}

	if ((res = mstrfip_ba_reset(mft->dev)) < 0) {
		return res;
	}

	if ((res = mstrfip_ba_load(mft->dev, mft->mcycle.mc)) < 0) {
		return res;
	}

	return 0;
}

void mft_print_report(struct mstrfip_report *report)
{
	fprintf(stdout, "Current masterFip report:\n");
	fprintf(stdout, "tx_ok: %d\n", report->tx_ok);
	fprintf(stdout, "tx_err: %d\n", report->tx_err);
	fprintf(stdout, "fd_tx_err: %d\n", report->fd_tx_err);
	fprintf(stdout, "fd_tx_err_hwtime: %d\n", report->fd_tx_err_hwtime);
	fprintf(stdout, "fd_tx_err_cycle: %d\n", report->fd_tx_err_cycle);
	fprintf(stdout, "fd_cd: %d\n", report->fd_cd);
	fprintf(stdout, "fd_tx_watchdog: %d\n", report->fd_tx_watchdog);
	fprintf(stdout, "fd_tx_watchdog_hwtime: %d\n", report->fd_tx_watchdog_hwtime);
	fprintf(stdout, "fd_tx_watchdog_cycle: %d\n", report->fd_tx_watchdog_cycle);
	fprintf(stdout, "rx_ok: %d\n", report->rx_ok);
	fprintf(stdout, "rx_err: %d\n", report->rx_err);
	fprintf(stdout, "rx_tmo: %d\n", report->rx_tmo);
	fprintf(stdout, "rx_mps_status_err: %d\n", report->rx_mps_status_err);
	fprintf(stdout, "tx_mps_status_err: %d\n", report->tx_mps_status_err);
	fprintf(stdout, "cycles: %d\n", report->cycles);
	fprintf(stdout, "int_sync_pulse_count: %d\n", report->int_sync_pulse_count);
	fprintf(stdout, "ba_state: %d\n", report->ba_state);
}

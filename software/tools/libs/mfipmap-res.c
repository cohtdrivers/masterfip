// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <inttypes.h>

#include "mfipmap-res.h"

static const char * const libdevmap_version_s  __attribute__((unused)) = "libdevmap version: " __GIT_VER__ ", " __TIME__ " " __DATE__;

/**
 * It maps into memory the given device resource
 * @parma[in] resource_path path to the resource file which can be mmapped
 * @param[in] offset virtual address of the device memory region of interest
 *            within the resource
 *
 * @todo make the open more user friendly
 *
 * @return NULL on error and errno is approproately set.
 *         On success a mapping descriptor.
 */
static char pci_res_path[1024]; /* big enough to contain pci sysfs path */
struct mfip_map_hdl *mfip_map(struct mfip_map_args *map_args,
				   uint64_t map_length)
{
	struct mfip_map_hdl *map_hdl;
	off_t pa_offset /*page aligned offset */;

	map_hdl = malloc(sizeof(struct mfip_map_hdl));
	if (!map_hdl)
		goto out_alloc;

	if (map_args->is_vme) {
		snprintf(pci_res_path, sizeof(pci_res_path), "/sys/bus/vme/devices"
		 "/vme.%u/resource%d",
		 map_args->vme_slot,
		 map_args->bar);
	} else {
		snprintf(pci_res_path, sizeof(pci_res_path), "/sys/bus/pci/devices/"
		 "/0000:%02"PRIx64":%02"PRIx64".%"PRIx64"/resource%d",
		 (map_args->devid >> 16) & 0xFF,
		 (map_args->devid >> 8) & 0xFF,
		 (map_args->devid) & 0xFF,
		 map_args->bar);
	}

	map_hdl->fd = open(pci_res_path, O_RDWR | O_SYNC);
	if (map_hdl->fd <= 0)
		goto out_open;

	/* offset is page aligned */
	pa_offset = map_args->offset & ~(getpagesize() - 1);
	map_hdl->map_pa_length = map_length + map_args->offset - pa_offset;
	map_hdl->mmap = mmap(NULL, map_hdl->map_pa_length,
			   PROT_READ | PROT_WRITE,
			   MAP_SHARED, map_hdl->fd, pa_offset);
	if ((long)map_hdl->mmap == -1)
		goto out_mmap;

	map_hdl->base = map_hdl->mmap + map_args->offset - pa_offset;
 	/*
	 * @todo for future VME devices handled via a resource file,
	 * exposed in standard place (/sys/bus/vme/device/xxx/resource-file)
	 * the bus type (pci or vme) should be checked to set properly
	 * the endianess (could be get from the path's resource)
	 */
	map_hdl->is_be = map_args->is_be; /* default set to little endian */
	return map_hdl;

out_mmap:
	close(map_hdl->fd);
out_open:
	free(map_hdl);
out_alloc:
	return NULL;
}

/**
 * It releases the resources allocated by dev_map(). The mapping will
 * not be available anymore and the descriptor will be invalidated
 * @param[in,out] descriptor token from dev_map()
 */
void mfip_unmap(struct mfip_map_hdl *map_hdl)
{
	munmap(map_hdl->mmap, map_hdl->map_pa_length);
	close(map_hdl->fd);
	free(map_hdl);
}

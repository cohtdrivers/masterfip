#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: CC-BY-SA-4.0+

"""
SPDX-License-Identifier: CC-BY-SA-4.0+
SPDX-FileCopyrightText: 2020 CERN
"""

from distutils.core import setup

setup(name='PyMFip',
      version='1.0',
      description='Python Module to handle Master FIP',
      author='Michel Arruat',
      author_email='michel.arruat@cern.ch',
      maintainer="Michel Arruat",
      maintainer_email="michel.arruat@cern.ch",
      url='',
      packages=['PyMFip'],
      license='LGPL-3.0-or-later')

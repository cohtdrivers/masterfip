/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <mockturtle/libmockturtle.h>

#include "libmasterfip-priv.h"
#include "libmasterfip.h"

static char *mstrfip_static_errors[] = {
	"Received an invalid answer from mock-turtle CPU",
	"Hardware doesn't support requested bitrate",
	"Invalid BA instruction count",
	"BA is not instantiated",
	"Max number of periodic variables exceeded",
	"Invalid periodic var count.Should be in the range [1..n]",
	"Invalid periodic window config: variable callback is provided"
		" while none of the periodic var has the irq flag raised",
	"Invalid instruction's parameter",
	"Unable to complete request: BA is not in STOP state",
	"Current set of macro cycle instructions can't accomodate"
		" aperiodic traffic",
	"Wait or Aperiodic window time doesn't fit with the theoretical length"
		" of previous instructions",
	"MSTRFIP_BA_INCONSISTENT_INSTR_SET .... ",
	"BA instructions set should end with a BA_WAIT instruction.",
};

char *mstrfip_errors[__MSTRFIP_MAX_ERROR_CODE - MSTRFIP_ERROR_CODE_OFFSET];

/*
 * Initialize mstrfip_errors array containig static and dynamic error messages.
 * masterFIP library returns some errors requiring a dynamic error message to
 * provide the application with a clear message
 */
void mstrfip_error_init()
{
	static int mstrfip_error_init_flg = 0;

	int i;
	int static_err_count = sizeof(mstrfip_static_errors) / sizeof(char *);

	if (mstrfip_error_init_flg)
		return;

	for (i = 0; i < static_err_count; ++i) {
		mstrfip_errors[i] = mstrfip_static_errors[i];
	}
	for (; i < __MSTRFIP_MAX_ERROR_CODE - MSTRFIP_ERROR_CODE_OFFSET; ++i) {
		mstrfip_errors[i] = calloc(MSTRFIP_DYNAMIC_ERR_MSG_SZ, (size_t)1);
		mstrfip_errors[i][0] = '\0'; /* useless with calloc !!! */
	}
	mstrfip_error_init_flg = 1;
}

void mstrfip_error_free()
{
	int i = 0;
	int static_err_count = sizeof(mstrfip_static_errors) / sizeof(char *);

	for (i = static_err_count;
	     i < __MSTRFIP_MAX_ERROR_CODE - MSTRFIP_ERROR_CODE_OFFSET; ++i) {
		if (mstrfip_errors[i] != NULL)
			free(mstrfip_errors[i]);
	}
}

/**
 * It returns a string messages corresponding to a given error code. If
 * it is not a libmasterfip error code, it will run trtl_strerror().
 * Error message is volatile and is significant till the next FIP's function
 * call.
 * @param[in] err error code
 * @return a message error
 */
const char *mstrfip_strerror(int err)
{
	if (err < MSTRFIP_ERROR_CODE_OFFSET || err >= __MSTRFIP_MAX_ERROR_CODE)
		return trtl_strerror(err);

	return mstrfip_errors[err - MSTRFIP_ERROR_CODE_OFFSET];
}

/*
 * Copyright (C) 2016-2017 CERN (www.cern.ch)
 * Author: Michel Arruat <michel.arruat@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <poll.h>

#include <mockturtle/libmockturtle.h>

#include "libmasterfip-priv.h"
#include "masterfip-common-priv.h"
#include "libmasterfip.h"

/*
 * The idea is not to allocate mockturtle message on every update.
 * So this function evaluates the max number of periodic variables between 2
 * irq and this number is used to allocate the max number of trtl messages.
 * It's a bit overdimensioned because the RT side packs variable as much as
 * possible into a single mockturtle message.
 */
static int mstrfip_per_var_alloc_trtl_msg(struct mstrfip_ba_per_var_set *per_vars)
{
	int idx, var_msg_n, var_msg_nmax;

	var_msg_nmax = 0;
	var_msg_n = 1;
	for (idx = 0; idx < per_vars->sorted_var_count; ++idx) {
		struct mstrfip_data_priv *pvar_priv = (struct mstrfip_data_priv *)(per_vars->sorted_varlist[idx]->priv);

		if (pvar_priv->hdr.flags & MSTRFIP_DATA_FLAGS_IRQ) {
			if (var_msg_n > var_msg_nmax)
				var_msg_nmax = var_msg_n;
			var_msg_n = 1;
		}
		else {
			if (pvar_priv->hdr.flags & MSTRFIP_DATA_FLAGS_CONS)
				/*
				 * only consumed var are considered,
				 * (produced var doesn't send message)
				 */
				++var_msg_n;
		}
	}
	per_vars->trtl.msglist_size = var_msg_nmax;
	/* Allocate internal lists: produced, consumed  var lists */
	if (per_vars->trtl.msglist != NULL)
		free(per_vars->trtl.msglist);
	per_vars->trtl.msglist = calloc(per_vars->trtl.msglist_size,
					sizeof(struct trtl_msg));
	return (!per_vars->trtl.msglist) ? -1 : 0;
}

/*
 * Same goal than the function above, but for aperiodic messages
 */
static int mstrfip_aper_msg_alloc_trtl_msg(struct mstrfip_ba_aper_msg_set *aper_msgs)
{
	/* Allocate internal lists: as many as consumed msg*/
	aper_msgs->trtl.msglist_size = aper_msgs->cons.count;
	if (aper_msgs->trtl.msglist != NULL)
		free(aper_msgs->trtl.msglist);
	aper_msgs->trtl.msglist = calloc(aper_msgs->trtl.msglist_size,
					sizeof(struct trtl_msg));
	return (!aper_msgs->trtl.msglist) ? -1 : 0;
}

/*
 *  macrocycle loading: this function builds internal list
 *  for produced and consumed periodic var.
 */
static int mstrfip_prepare_per_varlist(struct mstrfip_ba_macrocycle *mc)
{
	struct mstrfip_ba_per_var_set *per_vars = &mc->per_vars;
	struct mstrfip_data_priv *pvar_priv;
	int idx_instr, idx_cons_var, idx_prod_var, idx, start_idx, stop_idx;
	int res;

	/* Allocate internal lists: produced, consumed  var lists */
	if (per_vars->prod_varlist != NULL)
		free(per_vars->prod_varlist);
	per_vars->prod_varlist = calloc(per_vars->prod_var_count,
						sizeof(struct mstrfip_data*));
	if (!per_vars->prod_varlist)
		return -1;

	if (per_vars->cons_varlist != NULL)
		free(per_vars->cons_varlist);
	per_vars->cons_varlist = calloc(per_vars->cons_var_count,
						sizeof(struct mstrfip_data*));
	if (!per_vars->cons_varlist)
		return -1;

	/*
	 * Go through the Ba instr list and fill the prod and cons varlist
	 * which will be used run time between rt and lib
	 */
	idx_cons_var = idx_prod_var = 0;
	for (idx_instr = 0; idx_instr < mc->instr_count; ++idx_instr) {
		switch (mc->instrlist[idx_instr].code) {
		case MSTRFIP_BA_PER_VAR_WIND:
			/*
			 * periodic var window instruction : go through the
			 * sorted var list, fill consumed  and produced var list,
			 * and set the key field in the data header. For fast lookup
			 * reasons, the key value is basically the index in the
			 * list.
			 */
			start_idx = mc->instrlist[idx_instr].per_var_wind.start_var_idx;
			stop_idx = mc->instrlist[idx_instr].per_var_wind.stop_var_idx;;
			/* idx : index used to iterate the varlist */
			for(idx = start_idx; idx < stop_idx; ++idx) {
				pvar_priv = (struct mstrfip_data_priv *)
					per_vars->sorted_varlist[idx]->priv;
				if (pvar_priv->hdr.flags & MSTRFIP_DATA_FLAGS_CONS) {
					/* consumed var */
					per_vars->cons_varlist[idx_cons_var] =
						per_vars->sorted_varlist[idx];
					pvar_priv->hdr.key = idx_cons_var;
                    ++idx_cons_var;
				}
				else { /* produced var */
					per_vars->prod_varlist[idx_prod_var] =
						per_vars->sorted_varlist[idx];
					pvar_priv->hdr.key = idx_prod_var;
					++idx_prod_var;
				}
			}
			break;
		default:
			break;
		}
	}
	/*
	 * allocate mockturtle messages as much as necessary for periodic vars
	 * and aperiodic messages
	 */
	res = mstrfip_per_var_alloc_trtl_msg(per_vars);
	if (res)
		return res;
	res = mstrfip_aper_msg_alloc_trtl_msg(&mc->aper_msgs);
	if (res)
		return res;
	return 0;
}

/*
 * macrocycle loading: this function send the set of instruction which define
 * the structure of teh macrocycle to mockturtle
 * NB: it assumes that the complete set of instruction doesn't exceed the size
 * of a mockcturtle message which is reasonnable
 */
static int ba_instr_send(struct mstrfip_desc *mstrfip)
{
	struct mstrfip_ba_macrocycle *mc = mstrfip->ba.mcycle;
	struct trtl_msg msg;
	struct mstrfip_ba_instr_hdr_trtlmsg* instr_trtlmsg =
				(struct mstrfip_ba_instr_hdr_trtlmsg *)msg.data;
	uint32_t cpu_hz = trtl_config_get(mstrfip->trtl)->clock_freq;
	int err;

	if ((sizeof(struct mstrfip_ba_instr) * mc->instr_count) >
				sizeof(msg.data))
		return -1;

	instr_trtlmsg->trtl_hdr.id = MSTRFIP_CMD_SET_BA_CYCLE;
	instr_trtlmsg->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	/* macro cyle length in CPU ticks */
	instr_trtlmsg->cycle_ticks_length = us_to_ticks(mc->cycle_ustime, cpu_hz);
	instr_trtlmsg->instr_count = mc->instr_count;
	msg.hdr.len = MSTRFIP_BA_INSTR_HDR_TRTLMSG_WSZ;
	/* fill the message payload by copying the set of instructions */
	memcpy(&msg.data[msg.hdr.len], mc->instrlist,
			sizeof(struct mstrfip_ba_instr) * mc->instr_count);
	/* set datalen in 32bits word count */
	msg.hdr.len += (sizeof(struct mstrfip_ba_instr) / 4) * mc->instr_count;
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_BA;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_SET_BA_CYCLE;

#ifdef DUMP_TRTLMSG
	dump_trtlmsg("mstrfip_instr-send  (dir:input slot:0)", &msg);
#endif
	/* Send the message and get answer */
	err = trtl_msg_sync(mstrfip->trtl, MSTRFIP_CPU0, MSTRFIP_HMQ_IO_CPU0_CMD, &msg, &msg, MSTRFIP_DEFAULT_TIMEOUT);

	if (err < 0)
		return err;

	/* check the answer */
	err = mstrfip_validate_acknowledge(&msg);
	if (err) {
		errno = err; /* set properly errno */
		return -1;
	}
	return 0;
}

/*
 * macrocycle loading: this function packs all the header of periodic var into
 * a mockturtle message and send it. The number of periodic var might required
 * more than a single message.
 */
#define MSTRFIP_DATA_HDR_WSIZE (sizeof(struct mstrfip_data_hdr)/sizeof(uint32_t))
static int ba_varlist_send(struct mstrfip_desc *mstrfip)
{
	struct mstrfip_ba_macrocycle *mc = mstrfip->ba.mcycle;
	struct trtl_msg msg;
	struct mstrfip_data_hdr_trtlmsg *var_hdrs;
	struct mstrfip_data_priv *pvar_priv;
	int idx_var, err, nvar, word_pos;


	memset(&msg, 0, sizeof(struct trtl_msg));
	var_hdrs = (struct mstrfip_data_hdr_trtlmsg *)msg.data;
	idx_var = 0;
	while (idx_var != mc->per_vars.sorted_var_count) {
		var_hdrs->id = MSTRFIP_CMD_SET_VAR_LIST;
		var_hdrs->seq = 0; /* sequence number: always 0 for cmd */
		word_pos = MSTRFIP_DATA_HDR_TRTLMSG_WSZ;
		for (nvar = 0;
		     (idx_var < mc->per_vars.sorted_var_count) &&
		     ( (word_pos + MSTRFIP_DATA_HDR_WSIZE) <
		     	MSTRFIP_TRTLMSG_MAX_PAYLOAD_WSZ_HMQ );
		     ++idx_var, ++nvar) {
		     	pvar_priv = (struct mstrfip_data_priv *)
				mc->per_vars.sorted_varlist[idx_var]->priv;
			memcpy(&msg.data[word_pos], &pvar_priv->hdr,
			       sizeof(struct mstrfip_data_hdr));
			word_pos += MSTRFIP_DATA_HDR_WSIZE;
		}
		/* Send the message and get answer */
		var_hdrs->ndata = nvar;
		msg.hdr.len = word_pos;
		msg.hdr.rt_app_id = MSTRFIP_APP_ID_BA;
		msg.hdr.flags = 0;
		msg.hdr.msg_id = MSTRFIP_CMD_SET_VAR_LIST;

#ifdef DUMP_TRTLMSG
	dump_trtlmsg("mstrfip_varlist_send  (dir:input slot:0)", &msg);
#endif
		err = trtl_msg_sync(mstrfip->trtl, MSTRFIP_CPU0, MSTRFIP_HMQ_IO_CPU0_CMD, &msg, &msg, MSTRFIP_DEFAULT_TIMEOUT);
		if (err < 0)
			return err;
#ifdef DBG_CHK_MSG
		// send back received data
		printf("ba_varlist_send() feed-back\n");
#else
		/* check the answer */
		err = mstrfip_validate_acknowledge(&msg);
		if (err) {
			errno = err; /* set errno accordingly */
			return 1;
		}
#endif
	}
	return 0;
}

/*
 * The aim of this function is to check the segment in order to get all the
 * agent's response time. It loops over all possible agent address [0..255] and
 * send by batch of 85 adresses to fir the answer into a single mockturtle
 * message.
 * This information should be used to validate the macrocycle against real
 * response time instaed of using theoretical time. This way the validation
 * should be very precise.
 */
#define MSTRFIP_RESP_BATCH 85
static void mstrfip_segment_chk(struct mstrfip_desc *mstrfip)
{
	struct trtl_msg msg;
	struct mstrfip_response_time_trtlmsg *req_msg, *ans_msg;
	int i, agt_idx, ans_idx, nloop;
	uint32_t nstime, iddat_time, mcycle_nticks;
	uint32_t cpu_hz = trtl_config_get(mstrfip->trtl)->clock_freq;

	memset(&msg, 0, sizeof(struct trtl_msg));
	/*
	 * request and answer points to the same trtl message because it
	 * carries request and answer
	 */
	req_msg = (struct mstrfip_response_time_trtlmsg *)msg.data;
	ans_msg = (struct mstrfip_response_time_trtlmsg *)msg.data;

	iddat_time = (2 + 6) * 8 * mstrfip_bit_nstime[mstrfip->ba.bitrate];
	mcycle_nticks = ns_to_ticks((iddat_time +
				     mstrfip_ts_nstime[mstrfip->ba.bitrate]) *
				     MSTRFIP_RESP_BATCH, cpu_hz);
	nloop = MSTRFIP_MAX_AGENT_COUNT / MSTRFIP_RESP_BATCH;
	for (i = 0; i < nloop; ++i) {
		int err;

		// build request msg
		req_msg->trtl_hdr.id = MSTRFIP_CMD_GET_RESPTIME;
		req_msg->trtl_hdr.seq = 0;
		req_msg->cycle_nticks = mcycle_nticks;
		req_msg->from_agt_addr = 1 + (MSTRFIP_RESP_BATCH * i);
		req_msg->to_agt_addr = req_msg->from_agt_addr + (MSTRFIP_RESP_BATCH - 1);
		// datalen is set to max size because same msg is used to
		// receive the answer which is bigger than the input.
		msg.hdr.len = 100;
		msg.hdr.rt_app_id = MSTRFIP_APP_ID_BA;
		msg.hdr.flags = 0;
		msg.hdr.msg_id = MSTRFIP_CMD_GET_RESPTIME;

		// send message and get answer
		err = trtl_msg_sync(mstrfip->trtl, MSTRFIP_CPU0, MSTRFIP_HMQ_IO_CPU0_CMD, &msg, &msg, MSTRFIP_DEFAULT_TIMEOUT);

		if (!(err < 0) && msg.hdr.len != 0) { // no error
			for (agt_idx = ans_msg->from_agt_addr, ans_idx = 0;
					agt_idx < ans_msg->to_agt_addr;
					++agt_idx, ++ans_idx) {
				nstime = ((uint32_t *)ans_msg)
					[MSTRFIP_RESPONSE_TIME_TRTLMSG_WSZ + ans_idx];
				if (nstime != 0)
					++mstrfip->ba.seg.present_nb;
				mstrfip->ba.seg.resp_nstime[agt_idx] = nstime;
			}
		} else {
			return;
			/* @TODO report error */
		}
	}
}

static void mstrfip_diag_cfg_update(struct mstrfip_desc *mstrfip)
{
	struct mstrfip_ba_cfg *diag_ba_cfg =
		(struct mstrfip_ba_cfg *)&mstrfip->ba.shm->ext.ba_cfg;
	struct mstrfip_ba_macrocycle *mc = mstrfip->ba.mcycle;
	struct mstrfip_data_hdr *pvar_hdr;
	struct mstrfip_data_priv *pvar_priv;
	uint32_t nvar;
	struct mstrfip_version vers;
	int res;

	/* update macrocycle composition */
	diag_ba_cfg->ninstr = mc->instr_count;
	memcpy(diag_ba_cfg->instrlist, mc->instrlist,
			sizeof(struct mstrfip_ba_instr) * mc->instr_count);
	/* update list of periodic var */
	diag_ba_cfg->n_per_var = mc->per_vars.sorted_var_count;
	for (nvar = 0; nvar < mc->per_vars.sorted_var_count; ++nvar) {
		struct mstrfip_per_var_desc *pvar_desc;

		pvar_priv = (struct mstrfip_data_priv *)
			mc->per_vars.sorted_varlist[nvar]->priv;
		pvar_hdr = (struct mstrfip_data_hdr *)&pvar_priv->hdr;
		pvar_desc = &diag_ba_cfg->per_varlist[nvar];
		pvar_desc->var_id = pvar_hdr->id;
		pvar_desc->dir = (pvar_hdr->flags & MSTRFIP_DATA_FLAGS_PROD) ?
			MSTRFIP_DATA_FLAGS_PROD: MSTRFIP_DATA_FLAGS_CONS;
		pvar_desc->sz = pvar_hdr->max_bsz;
		pvar_desc->cb = (pvar_hdr->flags & MSTRFIP_DATA_FLAGS_IRQ) ? 1 : 0;
	}
	/* update macrocyle length */
	diag_ba_cfg->cycle_uslength = mc->cycle_ustime;
	/* update version */
	res = mstrfip_version_get((struct mstrfip_dev *)mstrfip, &vers);
	if (!res) {
		mstrfip->ba.shm->ext.vers = vers;
	}
	/* update segment agent's response time */
	mstrfip->ba.shm->ext.seg = mstrfip->ba.seg;
	/* update HW config and speed */
	mstrfip->ba.shm->ext.hw_cfg = mstrfip->ba.hw_cfg;
	mstrfip->ba.shm->ext.bitrate = mstrfip->ba.bitrate;
	mstrfip->ba.shm->ext.mtrtl_cpu_speed_hz =
				trtl_config_get(mstrfip->trtl)->clock_freq;

	return;
}

int mstrfip_mcycle_check_and_prepare(struct mstrfip_desc *mstrfip,
				     struct mstrfip_ba_macrocycle *mc)
{
	struct mstrfip_ba *ba = &mstrfip->ba;
	uint32_t cpu_hz = trtl_config_get(mstrfip->trtl)->clock_freq;
	int res;

	/*
	 * ba instruction set should always end by a BA_WAIT instruction
	 * The last ba_wait instruction defines the macro cycle length
	 */
	if (mc->instrlist[mc->instr_count -1].code != MSTRFIP_BA_WAIT_WIND) {
		errno = MSTRFIP_BA_WAIT_WIND_INSTR_MISSING;
		return -1;
	}
	/*
	 * macro cycle length (in us): retrieved from the end-time of
	 * the last wait wind.
	 */
	mc->cycle_ustime = ticks_to_us(mc->instrlist[mc->instr_count -1].wait_wind.ticks_end_time,
				       cpu_hz);
//	// TODO  macro cycle consistency: should be added in ba-rules.c
//	// for instance requesting diagnostic service requires to schedule an
//	// aperiodic window in the macro cycle.
//	if (!mstrfip_ba_check_consistency(mstrfip)) {
//		errno = MSTRFIP_BA_INCONSISTENCY;
//		return -1;
//	}
	/*
	 * compute theoretical cycle length (in us) and compare to
	 * the expected one (in us). During the cycle length computation
	 * inconsistencies can be detected and errno is set accordingly.
	 */
	if (ba->seg.present_nb == 0)
		mstrfip_segment_chk(mstrfip);

	res = mstrfip_ba_cyclelength_chk(ba, mc);
	if (res)
		return res;
	// assign macro cycle to ba
	ba->mcycle = mc;
	/* application has completed the BA definition */
	/* time to prepare internal lists ordered according to the macro cycle */
	res = mstrfip_prepare_per_varlist(mc);

	return res;
}

/**
 * Macrocycle load
 * @param[in] dev device token
 * @param[in] pointer to the macrocycle object to be loaded
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_ba_load(struct mstrfip_dev *dev,
				struct mstrfip_macrocycle *macrocycle)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct mstrfip_ba *ba = &mstrfip->ba;
	struct mstrfip_ba_macrocycle *mc =
			(struct mstrfip_ba_macrocycle *)macrocycle;
	int res;

	if (mstrfip->current_error) {
		errno = mstrfip->current_error;
		return -1;
	}

	if (ba->state != MSTRFIP_FSM_INITIAL) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}
	if (mc->instr_count == 0) {
		errno = MSTRFIP_BA_NOT_STOP_STATE;
		return -1; /* not allowed */
	}

	/*
	 * Create diag shm if it is not already created
	 */
	if (ba->shm == NULL) {
		res = mstrfip_diag_shm_create(dev);
		if (res)
			return -1; // errno is set
	}
	/* Reset ba_cfg space in the diag_shm: new macrocycle is loaded */
	memset(&ba->shm->ext.ba_cfg, 0, sizeof(struct mstrfip_ba_cfg));

	res = mstrfip_mcycle_check_and_prepare(mstrfip, mc);
	if (res)
		return -1; // errno is set
	/*
	 * In case a reset has been executed, sending back the HW config
	 * before loading macro-cycle.
	 */
	res = mstrfip_hw_cfg_set(dev, &mstrfip->ba.hw_cfg);

	/*
	 * From now on we start to send macrocycle configuration to mturtle.
	 * If one action fails, we reset the mockturtle to keep it in a proper
	 * state and we report the error
	 */
	/* first, send list of vars to the HW */
	res |= ba_varlist_send(mstrfip);
	 /* Second, send macro cycle sequence (list of instructions) to the HW.
	 * The macro cycle definition must be sent the latest because it acts as
	 * a trigger to move the RT FSM into a state BA-READY waiting for start
	 * or reset.
	 */
	res |= ba_instr_send(mstrfip);
	if (res) {
		// reset partial config: reset requires FSM_READY state
		mstrfip->ba.state = MSTRFIP_FSM_READY;
		mstrfip_ba_reset(dev);
		// set ba state
		mstrfip->ba.state = MSTRFIP_FSM_INITIAL;
	}
	else {
		mstrfip->ba.state = MSTRFIP_FSM_READY;
		mstrfip_diag_cfg_update(mstrfip);
	}
	return res;
}

/**
 * Starts running macro cycle
 * @param[in] dev device token
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_ba_start(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct trtl_msg msg;
	struct mstrfip_ba_state_trtlmsg *ba_state_trtlmsg =
				(struct mstrfip_ba_state_trtlmsg *)&msg.data;
	int res;

//	// TODO check if macro cycle is loaded
//	if (mstrfip->ba. != MACRO_CYCLE_LOADED)
//		return -1;
	if (mstrfip->current_error) {
		errno = mstrfip->current_error;
		return -1;
	}

	/* Start FIP irq thread only if cb has been registered */
	if (mstrfip->ba.mcycle->has_registered_cb) {
		res = mstrfip_irq_start(mstrfip);
		if (res < 0)
			return res;
	}

	/* Start diag thread if diag service is requested */
	if (mstrfip->ba.mcycle->diag.varlist[0] != NULL) {
		res = mstrfip_diag_start(dev);
		if (res < 0)
			return res;
	}
	/* START BA: send the start command. No payload is required */
	ba_state_trtlmsg->trtl_hdr.id = MSTRFIP_CMD_SET_BA_STATE;
	ba_state_trtlmsg->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	ba_state_trtlmsg->state = MSTRFIP_BA_STATE_CMD_START;
	msg.hdr.len = sizeof(struct mstrfip_ba_state_trtlmsg) / 4;
#ifdef DUMP_TRTLMSG
	dump_trtlmsg("mstrfip_ba_start  (dir:input slot:0)", &msg);
#endif
	/* Send the message and get answer */
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_SET_BA_STATE;

	res = mstrfip_trtl_msg_async_send(mstrfip, &msg);
	if (res < 0)
		return res;
	mstrfip->ba.state = MSTRFIP_FSM_RUNNING;
	return res;
}

/**
 * Stops the macro cycle
 * @param[in] dev device token
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_ba_stop(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct trtl_msg msg;
	struct mstrfip_ba_state_trtlmsg *ba_state_trtlmsg =
				(struct mstrfip_ba_state_trtlmsg *)&msg.data;
	int res;

	if (mstrfip->current_error) {
		errno = mstrfip->current_error;
		return -1;
	}

	if (mstrfip->ba.state != MSTRFIP_FSM_RUNNING)
		/* Already stopped */
		return 0;

	/* STOP BA */
	ba_state_trtlmsg->trtl_hdr.id = MSTRFIP_CMD_SET_BA_STATE;
	ba_state_trtlmsg->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	ba_state_trtlmsg->state = MSTRFIP_BA_STATE_CMD_STOP;
	msg.hdr.len = sizeof(struct mstrfip_ba_state_trtlmsg) / 4;
#ifdef DUMP_TRTLMSG
	dump_trtlmsg("mstrfip_ba_stop (dir:input slot:0)", &msg);
#endif
	/* Send the message and get answer */
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_SET_BA_STATE;

	res = mstrfip_trtl_msg_async_send(mstrfip, &msg);
	if (res < 0)
		return res;
	/* even if the HW access failed, state is moved into BA_STOP */
	mstrfip->ba.state = MSTRFIP_FSM_READY;
	return res;
}

/**
 * Reset the macro cycle and destroy BA config in the mockturtle.
 * This action requires to load again a macrocycle.
 * @param[in] dev device token
 * @return 0 on success, -1 on error and errno is set appropriately
 */
int mstrfip_ba_reset(struct mstrfip_dev *dev)
{
	struct mstrfip_desc *mstrfip = (struct mstrfip_desc *)dev;
	struct trtl_msg msg;
	struct mstrfip_ba_state_trtlmsg *ba_state_trtlmsg =
				(struct mstrfip_ba_state_trtlmsg *)&msg.data;
	int res;

	if (mstrfip->current_error) {
		errno = mstrfip->current_error;
		return -1;
	}

	if (mstrfip->ba.state == MSTRFIP_FSM_INITIAL)
		/* Alreay reseted */
		return 0;

	/* RESET BA */
	ba_state_trtlmsg->trtl_hdr.id = MSTRFIP_CMD_SET_BA_STATE;
	ba_state_trtlmsg->trtl_hdr.seq = 0; /* sequence number: always 0 for cmd */
	ba_state_trtlmsg->state = MSTRFIP_BA_STATE_CMD_RESET;
	msg.hdr.len = sizeof(struct mstrfip_ba_state_trtlmsg) / 4;
#ifdef DUMP_TRTLMSG
	dump_trtlmsg("mstrfip_ba_reset (dir:input slot:0)", &msg);
#endif
	/* Send the message and get answer */
	msg.hdr.rt_app_id = MSTRFIP_APP_ID_CMD;
	msg.hdr.flags = 0;
	msg.hdr.msg_id = MSTRFIP_CMD_SET_BA_STATE;

	res = mstrfip_trtl_msg_async_send(mstrfip, &msg);
	if (res < 0)
		return res;

	/*
	 * Reset cmd has been send, state is moved into INITIAL state
	 * and irq_thread is stopped
	 */
	mstrfip->ba.state = MSTRFIP_FSM_INITIAL;
	mstrfip_irq_stop(mstrfip);

	return res;
}

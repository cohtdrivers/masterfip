--_________________________________________________________________________________________________
--                                                                                                |
--                                        |SPEC masterFIP|                                        |
--                                                                                                |
--                                         CERN, BE/CO-HT                                         |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                        spec_masterfip_mt_urv                                   |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         spec_masterfip_mt_urv.vhd                                                         |
--                                                                                                |
-- Description  Top level of the masterFIP design with Mock Turtle on a SPEC carrier.             |
--                                                                                                |
--              Figure 1 shows the architecture and main components of the design.                |
--                ______________________________________________________________________          |
--               |                                                                      |         |
--               |                         _________________________________________    |         |
--               |                        |                             MOCK TURTLE |   |         |
--         _     |                        |                                _____    |   |         |
--        | |    |                        |                         ___   |     |   |   |         |
--        |F|    |                     . .| . . . . . . . . . . . >|   |  |     |   |   |         |
--        |I|    |   _____             .  |                        |   |  |     |   |   |         |
--        |E|    |  |     |            .  |        . . . . . . . .>|   |  |     |   |   |         |
--        |L| <--|  |     |            .  |        .         HMQs  |   |  |     |   |   |         |
--        |D|    |  |  F  |            .  |        .               |   |  |     |   |   |         |
--        |R|    |  |  M  |            .  |     ______             |   |  |     |   |   |         |
--        |I| -->|  |  C  |            .  | DP |      |            |   |  |     |   |   |         |
--        |V|    |  |     |            . .|. .>| CPU0 |   _____    | X |  |  G  |   |   |         |
--        |E|    |  |  M  |     ____   .  |    |______|  |     |   | b |  |  N  |   |   | <-PCIe->|
--        |_|    |  |  A  |    |    |  .  |              | SH. |   | a |  |  4  |   |   |   host  |
--               |  |  S  |. . |Xbar|. .  |     ______   | MEM |   | r |  |  1  |   |   |         |
-- ext pulse --> |  |  T  |    |____|  .  |    |      |  |_____|   |   |  |  2  |   |   |         |
--               |  |  E  |            .  | DP | CPU1 |            |   |  |  4  |   |   |         |
--               |  |  R  |            . .|. .>|______|            |   |  |     |   |   |         |
-- FMC 1wire <-->|  |  F  |               |       .                |   |  |     |   |   |         |
--               |  |  I  |               |       .           HMQs |   |  |     |   |   |         |
--               |  |  P  |               |       . . . . . . . . >|___|  |     |   |   |         |
--   FMC LEDs <--|  |     |               |                               |_____|   |   |         |
--               |  |_____|               |                                 _^_     |   |         |
--               |                        |                                |   |    |   |         |
--               |                        |                                |VIC|    |   |         |
--               |                        |                                |___|    |   |         |
--               |                        |_________________________________________|   |         |
--               |______________________________________________________________________|         |
--                              Figure 1: spec_masterfip_mt_urv architecture                      |
--                                                                                                |
--                                                                                                |
--              FMC MASTERFIP CORE:                                                               |
--              On one side the FMC MASTERFIP CORE is the interface to the FMC hardware (i.e.     |
--              FielDrive chip, external pulse LEMO, 1-wire DS18B20 chip, LEDs) on the other side |
--              it provides a wbgen2 WISHBONE where a set of control and status registers have    |
--              been defined to interface with the MOCK TURTLE.                                   |
--              The core ignores the notion of the WorldFIP frame type (ID_DAT/RT_DAT/..etc),     |
--              or the macrocycle sequence and macrocycle timing; the sw running on the Mock      |
--              Turtle CPUs is responsible for managing these aspects and for providing to this   |
--              core all the payload bytes (coming from the host) that have to be serializedand,  |
--              together with a serialization startup trigger, or for enabling the deserializer   |
--              and then providing to the host the deserialized bytes.                            |
--              Figure 2 shows the structure of a WorldFIP frame. The core is internally          |
--              generating (in the case of serialization) or validating (in the case of           |
--              deserialization) only the FSS, CRC and FES bytes; the rest of the bytes are       |
--              retrieved from or provided to the MOCK TURTLE. The core also encodes/decodes all  |
--              the bytes to/from the Manchester2 code (as specified by the WorldFIP protocol) and|
--              controls/monitors all the FielDrive signals.                                      |
--               _____________________________________________________________________________    |
--              |_____FSS_____|__Ctrl__|_____________Payload_____________|_____CRC____|__FES__|   |
--                                                                                                |
--                                     Figure 2: WorldFIP frame structure                         |
--                                                                                                |
--              MOCK TURTLE:                                                                      |
--              Instead of having a big FSM in HDL that would be executing the WorldFIP           |
--              macrocycle, we have software running on an embedded CPU, in order to add          |
--              flexibility and ease the implementation of the design. Mock Turtle is the         |
--              generic core that offers multi-CPU processing and all the infrastructure around.  |
--              The interface between the CPUs and the PCIe host is though HostMessageQueues(HMQ).|
--              The interface between the CPUs with the FMC MASTERFIP CORE is a set of wbgen2-    |
--              generated registers.                                                              |
--              In this design MT is configured with 2 CPUs:                                      |
--              - CPU0 is the heart of the design; it is "playing" the WorldFIP macrocycle.       |
--                For example,it initiates the delivery of a WorldFIP question frame, by providing|
--                the frame bytes to the FMC MASTERFIP CORE, and then awaits for the reception of |
--                the response frame.It retrieves these consumed data from the FMC MASTERFIP CORE,|
--                packs them in the corresponding HMQ (according to the frame type) and can notify|
--                the host through an IRQ.                                                        |
--              - CPU1 is mainly polling the host to retrieve new payload bytes for production.   |
--                When new data is received from the host through a dedicated HMQ, CPU1 puts them |
--                into the Shared Memory for CPU0 to retrieve them and provide them to the        |
--                FMC MASTERFIP CORE for serialization.                                           |
--                CPU1 does not need access to the FMC MASTERFIP CORE; however access is possible |
--                for debugging purposes.                                                         |
--                                                                                                |
--              XBAR:                                                                             |
--              The crossbar between the FMC MASTERFIP CORE and MOCK TURTLE is used so that       |
--              CPU0, CPU1 and to the PCIe host can access directly the wbgen2-defined regs       |
--              in the FMC MASTERFIP CORE.                                                        |
--              Note that to give access to the FMC MASTERFIP CORE to both CPU0 and CPU1, we      |
--              could have used the Shared Port of MT, instead of using the Dedicated Ports (DP)  |
--              and this crossbar; this though would have also affected (potentially slowed down) |
--              the accesses to the MT Shared Memory.                                             |
--              Note also that as mentioned above CPU1 is only accessing the FMC MASTERFIP CORE   |
--              for debugging purposes; the same goes also for the PCIe host.                     |
--                                                                                                |
--              CLK, RST:                                                                         |
--              There is only one clock domain of 62.5 MHz, in the whole design. The clock is     |
--              generated inside the MOCK TURTLE, from the 125 MHz SPEC PLL IC6 output clock      |
--              (clk_125m_pllref_p_i,clk_125m_pllref_n_i) and it is used by both MOCK TURTLE CPUs,|
--              by the FMC MASTERFIP CORE and the XBAR. A PCIe reset signal, synchronous to       |
--              the 62.5 MHz clock is also provided by MOCK TURTLE.                               |
--                                                                                                |
--              MEMORY MAP AS SEEN FROM PCIe:                                                     |
--              0x00000000 (size: 4 bytes)      : SDB signature                                   |
--              0x00002000 (size: 64 bytes)     : VIC                                             |
--              0x00010000 (size: 644 bytes)    : Host access to the FMC MASTERFIP CORE           |
--              0x00020000 (size: 128 kB)       : MOCK TURTLE                                     |
--                |-- 0x00020000                : HMQ Global Control Registers                    |
--                |-- 0x00024000                : HMQ incoming slots (Host->CPUs)                 |
--                |-- 0x00028000                : HMQ outgoing slots (CPUs->Host)                 |
--                |-- 0x0002c000                : CPU Control/Status Registers                    |
--                |-- 0x00030000                : Shared Memory (64 KB)                           |
--                                                                                                |
-- Authors      Evangelia Gousiou (Evangelia.Gousiou@cern.ch)                                     |
--              Eva Calvo Giraldo (Eva.Calvo.Giraldo@cern.ch)                                     |
--              Tomasz Wlostowski (Tomasz.Wlostowski@cern.ch)                                     |
--                                                                                                |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;       -- std_logic definitions
use IEEE.numeric_std.all;          -- conversion functions

library work;
use work.wishbone_pkg.all;         -- for the wb_crossbar
use work.mt_mqueue_pkg.all;        -- for the HMQ
use work.mock_turtle_pkg.all;      -- for the Mockturtle
use work.masterFIP_pkg.all;        -- for the fmc_masterfip_core definition
use work.masterfip_wbgen2_pkg.all; -- for the masterfip_wbgen2_csr records
use work.streamers_pkg.all;
use work.wr_board_pkg.all;
use work.buildinfo_pkg.all;

entity spec_masterfip_mt_urv is
  generic 
  ( -- Reduces some timeouts to speed up simulations 
    g_simulation        : boolean := false;
    -- bypasses the gennum core and routes WB master to
    -- sim_master_(i/o) to speed up simulations where full PCIe
    -- access is not required
    g_sim_bypass_gennum : boolean := false
  );
  port
  ( -- Carrier signals
    --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
    -- Clock
     clk_125m_pllref_p_i : in   std_logic;                     -- 125 MHz PLL reference,
     clk_125m_pllref_n_i : in   std_logic;                     -- used in MT to generate 100 MHz

    -- GENNUM interface                   
     gn_rst_n_i          : in std_logic;                       -- reset from GENNUM (RSTOUT18_N)
     gn_gpio_b           : inout std_logic_vector(1 downto 0); -- general purpose interface
     -- -- PCIe to Local [Inbound Data] - RX
     gn_p2l_rdy_o        : out   std_logic;                    -- rx buffer full flag
     gn_p2l_clk_n_i      : in    std_logic;                    -- receiver source synch clock-
     gn_p2l_clk_p_i      : in    std_logic;                    -- receiver source synch clock+
     gn_p2l_data_i       : in    std_logic_vector(15 downto 0);-- parallel receive data
     gn_p2l_dframe_i     : in    std_logic;                    -- receive frame
     gn_p2l_valid_i      : in    std_logic;                    -- receive data valid
     -- -- Inbound Buffer Request/Status
     gn_p_wr_req_i       : in    std_logic_vector(1 downto 0); -- PCIe write request
     gn_p_wr_rdy_o       : out   std_logic_vector(1 downto 0); -- PCIe write ready
     gn_rx_error_o       : out   std_logic;                    -- receive error
     -- -- Local to Parallel [Outbound Data] - TX
     gn_l2p_data_o       : out   std_logic_vector(15 downto 0);-- parallel transmit data
     gn_l2p_dframe_o     : out   std_logic;                    -- transmit data frame
     gn_l2p_valid_o      : out   std_logic;                    -- transmit data valid
     gn_l2p_clk_n_o      : out   std_logic;                    -- transmitter source synch clock-
     gn_l2p_clk_p_o      : out   std_logic;                    -- transmitter source synch clock+
     gn_l2p_edb_o        : out   std_logic;                    -- packet termination and discard
     -- -- Outbound Buffer Status
     gn_l2p_rdy_i        : in    std_logic;                    -- tx buffer full flag
     gn_l_wr_rdy_i       : in    std_logic_vector(1 downto 0); -- Local-to-PCIe Write
     gn_p_rd_d_rdy_i     : in    std_logic_vector(1 downto 0); -- PCIe-to-Local read resp data ready
     gn_tx_error_i       : in    std_logic;                    -- transmit error
     gn_vc_rdy_i         : in    std_logic_vector(1 downto 0); -- channel ready

    -- SPEC LEDs
     led_green_o         : out   std_logic;                    -- blinking with clk_62m5_sys
     led_red_o           : out   std_logic;                    -- active during a PCIe rst, gn_rst_n_i

    -- FMC signals
    --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
    -- FMC presence
     fmc_prsnt_m2c_n_i   : in    std_logic;                    -- FMC presence (used by MT)

    -- FMC 1-wire
     fmc_onewire_b       : inout std_logic;                    -- temper and unique id

    -- WorldFIP bus speed                                      -- 31K25bps: speed_b1=0, speed_b0=0
     speed_b0_i          : in  std_logic;                      -- 1Mbps   : speed_b1=0, speed_b0=1
     speed_b1_i          : in  std_logic;                      -- 2M5bps  : speed_b1=1, speed_b0=0
                                                               -- 5Mbps   : speed_b1=1, speed_b0=1
    -- WorldFIP FielDrive
     fd_rstn_o           : out   std_logic;                    -- reset
     fd_rxcdn_i          : in    std_logic;                    -- rx carrier detect
     fd_rxd_i            : in    std_logic;                    -- rx data
     fd_txer_i           : in    std_logic;                    -- tx error
     fd_wdgn_i           : in    std_logic;                    -- tx watchdog
     fd_txck_o           : out   std_logic;                    -- tx clk
     fd_txd_o            : out   std_logic;                    -- tx data
     fd_txena_o          : out   std_logic;                    -- tx enable

    -- External synchronisation pulse (input signal and transceiver control)
     ext_sync_term_en_o  : out std_logic;                      -- enable 50 Ohm termin of the pulse
     ext_sync_dir_o      : out std_logic := '0';               -- direction fixed B -> A
     ext_sync_oe_n_o     : out std_logic;                      -- transceiver output enable
     ext_sync_i          : in  std_logic;                      -- input sync pulse 

    -- FMC Front panel LEDs: controlled by the MT firmware, updated every macrocycle
     led_rx_act_n_o      : out   std_logic;
     led_rx_err_n_o      : out   std_logic;
     led_tx_act_n_o      : out   std_logic;
     led_tx_err_n_o      : out   std_logic;
     led_sync_act_n_o    : out   std_logic;                    -- stays OFF when ext_sync is not used
     led_sync_err_n_o    : out   std_logic;                    -- stays OFF when ext_sync is not used

    -- Test points
     tp1_o               : out   std_logic;                    -- connected to fd_rxd
     tp2_o               : out   std_logic;                    -- connected to fd_txd
     tp3_o               : out   std_logic;                    -- connected to MT led&dbg reg bit 8
     tp4_o               : out   std_logic;                    -- connected to MT led&dbg reg bit 9

    -- To be removed on hw V3
     adc_1v8_shdn_n_o    : out   std_logic;
     adc_m5v_shdn_n_o    : out   std_logic;
     adc_5v_en_n_o       : out   std_logic;

     -- Signals added to remove synthesis errors due to 
     -- the usage of SPEC convention. They are not used 
     -- in this design
     button1_n_i         : in std_logic;
     -- PCB version
     pcbrev_i            : in std_logic_vector(3 downto 0);
     fmc0_prsnt_m2c_n_i  : in std_logic;
     fmc0_scl_b          : inout std_logic;
     fmc0_sda_b          : inout std_logic

    -- synthesis translate_off 
    ;
     sim_wb_i      : in  t_wishbone_slave_in := cc_dummy_slave_in;
     sim_wb_o      : out t_wishbone_slave_out
    -- synthesis translate_on
);
end spec_masterfip_mt_urv;


--=================================================================================================
--                                    architecture declaration
--=================================================================================================
architecture rtl of spec_masterfip_mt_urv is

---------------------------------------------------------------------------------------------------
--                                     MOCK TURTLE CONSTANTS                                     --
---------------------------------------------------------------------------------------------------
--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
-- HMQ: It total 7 HMQs have been defined. Each HMQ has 4 entries of 128 x 32 bits.

--   6 bi-directional HMQs for core 0
--     - 0: HMQ from CPU0 with the WorldFIP payloads from periodic consumed variables
--     - 1: HMQ from CPU0 with the WorldFIP payloads from aperiodic consumed variables
--         (only for the case of identif variable, scheduled as periodic variable, by radMon app)
--     - 2: HMQ from CPU0 with the WorldFIP payloads from aperiodic consumed messages
--     - 3: HMQ from CPU0 with the WorldFIP payloads from periodic consumed diagnostic variables
--         (only for the case of the FIPdiag variable 0x067F)
--     - 4: HMQ from CPU0 with the WorldFIP payloads from aperiodic consumed diagnostic variables
--         (aperiodic presence and identification)
--     - 5: HMQ towards CPU0 with commands for the bus config, used only at startup (e.g.: HW_RESET,
--          PROGRAM_BA, BA_START, BA_RUNNING) and for the responses of CPU0 to the commands of the host

--   1 bi-directional HMQ for core 1
--     - 0: HMQ towards CPU1 with the payloads for produced WorldFIP frames (variables and messages;
--          CPU1 then puts this data into the Shared Memory for CPU0 to access and put them in the
--          bus) as well as requests for report data, requests for the scheduling of aperiodic traffic
--          (presence/identification) etc (CPU1 again passes these requests into the Shared Memory) 
--          and for the responses of CPU1 to the commands of the host

  -- Device ID or application (masterFIP) ID needed in MT
  constant c_MSTRFIP_APP_ID : std_logic_vector(31 downto 0) := x"53504D41";

  constant C_NODE_CONFIG : t_mt_config :=
    (app_id          => c_MSTRFIP_APP_ID,
     cpu_count       => 2,
     cpu_config      => (0 => (memsize    => 24576,  -- in words(0x1800); the size should be enough for the storage of the RT sw running on CPU0 and for the macrocycle configuration
                               hmq_config => (6, (others => (2, 7, 2, x"0000_0000",true))), -- 4 entries, 128 wide, 2 header bits 
                               rmq_config => (0, (others => (c_DUMMY_MT_MQUEUE_SLOT)))),
                        1 => (memsize    => 2048, -- in words(0x800)
                               hmq_config => (1, (others => (2, 7, 2, x"0000_0000",true))),
                               rmq_config => (0, (others => (c_DUMMY_MT_MQUEUE_SLOT)))),
                         others => (0, c_MT_DEFAULT_MQUEUE_CONFIG, c_MT_DEFAULT_MQUEUE_CONFIG)),
     shared_mem_size =>8192); -- 32768 in bytes = 8192 words (divided by 4, as the new MT config requires)

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
-- masterFIP crossbar constants
  constant C_SLAVE_ADDR : t_wishbone_address_array(0 downto 0):= (0 => x"00000000");
  constant C_SLAVE_MASK : t_wishbone_address_array(0 downto 0):= (0 => x"00000000");

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
-- mockturtle interconnect crossbar constants
  constant c_NUM_WB_MASTERS : integer := 3; 
  constant c_NUM_WB_SLAVES  : integer := 1; --gn4124 port

  constant c_MASTER_GENNUM  : integer := 0;

  constant c_SLAVE_METADATA : integer := 0;
  constant c_SLAVE_MT       : integer := 1; 
  constant c_SLAVE_FMC0     : integer := 2; 

  constant  c_xwb_metadata_sdb : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"00",
    wbd_endian    => c_sdb_endian_little,  -- 0 for big, 1 for little
    wbd_width     => x"7",                 -- 8/16/32-bit port granularity
    sdb_component => (
      addr_first  => x"000000000002ffff",
      addr_last   => x"000000000003ffff",
      product     => (
      vendor_id   => x"000000000000CE42",  -- CERN
      device_id   => c_MSTRFIP_APP_ID,           
      version     => x"00000001",
      date        => x"20220717",
      name        => "xWB_METADATA_SDB_01")));
      
  -- Convention metadata base address
  constant c_METADATA_ADDR : t_wishbone_address := x"0002_0000";
  constant c_MT_ADDR       : t_wishbone_address := x"0004_0000";
  constant c_FMC0_ADDR     : t_wishbone_address := x"0006_0000";


  constant c_INTERCONNECT_LAYOUT : t_sdb_record_array(c_NUM_WB_MASTERS - 1 downto 0) :=
    (
      c_SLAVE_METADATA=> f_sdb_embed_device(c_xwb_metadata_sdb, c_METADATA_ADDR),
      c_SLAVE_MT      => f_sdb_embed_device(c_MOCK_TURTLE_SDB, c_MT_ADDR),
      c_SLAVE_FMC0    => f_sdb_embed_device(cc_dummy_sdb_device, c_FMC0_ADDR)
    );

  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

  signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
  signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_METADATA : integer := 0;

---------------------------------------------------------------------------------------------------
--                                            Signals                                            --
---------------------------------------------------------------------------------------------------
  -- clk, reset
  signal clk_62m5_sys           : std_logic;
  signal local_reset_n          : std_logic;
  signal rst_n_sys              : std_logic;
  signal console_irq            : std_logic;
  signal hmq_in_irq             : std_logic;
  signal hmq_out_irq            : std_logic;
  signal notify_irq             : std_logic;
  -- Mock Turtle
  signal fmc_core_wb_out        : t_wishbone_master_out_array(0 to 1);
  signal fmc_core_wb_in         : t_wishbone_master_in_array(0 to 1);
  signal fmc_wb_muxed_out       : t_wishbone_master_out;
  signal fmc_wb_muxed_in        : t_wishbone_master_in;
  -- SPEC LEDs
  signal led_divider            : unsigned(22 downto 0);
  signal leds                   : std_logic_vector(31 downto 0);
  signal spec_led               : std_logic_vector(7 downto 0);
  signal fd_txd                 : std_logic;
  -- Simulation Wishbone signals
  signal sim_wb_in              : t_wishbone_slave_in := cc_dummy_slave_in;
  signal sim_wb_out             : t_wishbone_slave_out;

--=================================================================================================
--                                       architecture begin
--=================================================================================================
begin

---------------------------------------------------------------------------------------------------
--                                        FIXED SIGNALS                                          --
---------------------------------------------------------------------------------------------------
  ext_sync_dir_o    <= '0'; -- Direction fixed to: B -> A

  -- To be removed on hw V3
  -- Note: For the hw v1 signals ext_sync_tst_n_o, adc_prim_conn_n_o and adc_sec_conn_n_o, in order
  -- to disable them (that is set them to 'Z'), the ISE setting "Unused IOB pins" is set to 
  -- "floating", so there is no need to declare them.
  adc_1v8_shdn_n_o  <= '0'; -- OFF
  adc_m5v_shdn_n_o  <= '0'; -- OFF
  adc_5v_en_n_o     <= '1'; -- OFF

  -- KB: the local reset generated by spec's pll
  --     and then transferred to the other cores
  rst_n_sys <= local_reset_n;
---------------------------------------------------------------------------------------------------
--                                      MOCK TURTLE CORE                                         --
---------------------------------------------------------------------------------------------------
  cmp_mt_intercon : entity work.xwb_sdb_crossbar
  generic map (
    g_num_masters => c_NUM_WB_SLAVES,
    g_num_slaves  => c_NUM_WB_MASTERS,
    g_registered  => true,
    g_wraparound  => true,
    g_layout      => c_INTERCONNECT_LAYOUT,
    g_sdb_addr    => c_SDB_ADDRESS)
  port map (
    clk_sys_i => clk_62m5_sys,
    rst_n_i   => local_reset_n,
    slave_i   => cnx_slave_in,
    slave_o   => cnx_slave_out,
    master_i  => cnx_master_in,
    master_o  => cnx_master_out);


  cmp_mock_turtle_urv: mock_turtle_core
  generic map(
    g_CONFIG            => C_NODE_CONFIG,
    g_SYSTEM_CLOCK_FREQ => 62500000, -- both CPUs at 62,5 MHz
    g_WITH_WHITE_RABBIT => FALSE)    -- no WR support
  port map(
    clk_i               => clk_62m5_sys,     -- 62,5 MHz; one clk domain in the whole design
    rst_n_i             => local_reset_n,    -- PCIe rst, synced with clk_sys
    -- shared peripheral port: not used
    
    -- dedicated (per-cpu) peripheral port
    -- WISHBONE connection of the fmc_masterFIP_core to the MT CPUs
    dp_master_o         => fmc_core_wb_out, -- access from MT CPU0 and CPU1 at base address 0x100000
    dp_master_i         => fmc_core_wb_in,

    host_slave_i        => cnx_master_out(c_SLAVE_MT),
    host_slave_o        => cnx_master_in(c_SLAVE_MT),

    hmq_in_irq_o        => hmq_in_irq,
    hmq_out_irq_o       => hmq_out_irq,
    notify_irq_o        => notify_irq,
    console_irq_o       => console_irq);


  -------------------------------------------------------------------------------------------------
  --                                     Simualation only                                        --
  -------------------------------------------------------------------------------------------------
  -- synthesis translate_off
  gen_without_gennum : if g_sim_bypass_gennum generate
    cnx_slave_in(c_MASTER_GENNUM) <= sim_wb_i;
    sim_wb_o                      <= cnx_slave_out(c_MASTER_GENNUM);
    sim_wb_out                    <= cnx_slave_out(c_MASTER_GENNUM);
    sim_wb_in                     <= sim_wb_i;
  end generate gen_without_gennum;
  -- synthesis translate_on


---------------------------------------------------------------------------------------------------
--                                             XBAR                                              --
---------------------------------------------------------------------------------------------------
-- Crossbar to give access to the fmc_masterFIP_core to CPU0, CPU1 and directly to the PCIe host.
-- Note that to give access to the fmc_masterFIP_core to both CPU0 and CPU1, the SP of MT could
-- have been used instead of the DP and this crossbar; this though would have also affected
-- (potentially slowed down) the accesses to the MT Shared Memory.
-- Note that in the MT firmware the CPU1 is only accessing the masterfip_leds register for debugging
-- purposes. The PCIe host is accessing the core directly only for testing purposes.
  cmp_wb_crossbar : entity work.xwb_crossbar
  generic map
    (g_num_masters => 3,
     g_num_slaves  => 1,
     g_registered  => true,
     g_address     => C_SLAVE_ADDR,
     g_mask        => C_SLAVE_MASK)
  port map
    (clk_sys_i     => clk_62m5_sys,
     rst_n_i       => rst_n_sys,
     slave_i(0)    => fmc_core_wb_out(0),
     slave_i(1)    => fmc_core_wb_out(1),
     slave_i(2)    => cnx_master_out(c_SLAVE_FMC0),
     slave_o(0)    => fmc_core_wb_in(0),
     slave_o(1)    => fmc_core_wb_in(1),
     slave_o(2)    => cnx_master_in(c_SLAVE_FMC0),
     master_o(0)   => fmc_wb_muxed_out,
     master_i(0)   => fmc_wb_muxed_in);

---------------------------------------------------------------------------------------------------
--                                            METADATA                                           --
---------------------------------------------------------------------------------------------------
  cmp_xwb_metadata : entity work.xwb_metadata
  generic map
    (g_VENDOR_ID    => x"0000_10DC",      -- taken from wiki
     g_DEVICE_ID    => c_MSTRFIP_APP_ID,  -- ASCII is SPMA (SPEC+MasterFIP)
     g_VERSION      => x"0100_0000",      -- version 1.0.0 of masterfip with MT (urv)
     g_CAPABILITIES => x"0000_0000",
     g_COMMIT_ID    => (others => '0'))
  port map
    (clk_i   => clk_62m5_sys,
     rst_n_i => local_reset_n,
     wb_i    => cnx_master_out(c_SLAVE_METADATA),
     wb_o    => cnx_master_in(c_SLAVE_METADATA));


---------------------------------------------------------------------------------------------------
--                                           SPEC BASE                                           --
---------------------------------------------------------------------------------------------------
  inst_spec_base : entity work.spec_base_wr
  generic map
    ( -- If true, instantiate a VIC/ONEWIRE/SPI/WR/DDRAM+DMA.
     g_WITH_VIC           => true,  -- use of xwb_vic
     g_WITH_ONEWIRE       => false, -- use of onewire (here we have the fmc_masterfip-core)
     g_WITH_SPI           => false, -- no use of SPI
     g_WITH_WR            => false, -- no use of WR
     g_WITH_DDR           => false, -- no use of DDR RAM
     --  Address of the application meta-data.  0 if none.
     g_APP_OFFSET         => c_METADATA_ADDR, --0 to 1fff is reserved from SPEC. So metadata/application exist in this address
     --  Number of user interrupts
     g_NUM_USER_IRQ       => 4,     -- 4: hmq in/out, notify and console_irq  
     -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
     -- changed to non-zero in the instantiation of the top level DUT in the testbench.
     -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
     g_SIMULATION         => g_simulation,
     -- Increase information messages during simulation
     g_VERBOSE            => g_simulation, 
     g_SIM_BYPASS_GENNUM  => g_sim_bypass_gennum)
  port map
    (
     -- 125 MHz PLL reference
     clk_125m_pllref_p_i  => clk_125m_pllref_p_i,
     clk_125m_pllref_n_i  => clk_125m_pllref_n_i,
     -- 20MHz VCXO clock (for WR)
     clk_20m_vcxo_i       => '0',
     -- 125 MHz GTP reference
     clk_125m_gtp_n_i     => '0',
     clk_125m_gtp_p_i     => '0',
     -- Aux clocks, which can be disciplined by the WR Core
     clk_aux_i            => (others => '0'),
     ---------------------------------------------------------------------------
     -- GN4124 PCIe bridge signals
     ---------------------------------------------------------------------------
     -- From GN4124 Local bus
     gn_rst_n_i      => gn_rst_n_i,        -- Reset from GN4124 (RSTOUT18_N)
     -- PCIe to Local [Inbound Data] - RX
     gn_p2l_clk_n_i  => gn_p2l_clk_n_i,    -- Receiver Source Synchronous Clock-
     gn_p2l_clk_p_i  => gn_p2l_clk_p_i,    -- Receiver Source Synchronous Clock+
     gn_p2l_rdy_o    => gn_p2l_rdy_o,     -- Rx Buffer Full Flag
     gn_p2l_dframe_i => gn_p2l_dframe_i,  -- Receive Frame
     gn_p2l_valid_i  => gn_p2l_valid_i,   -- Receive Data Valid
     gn_p2l_data_i   => gn_p2l_data_i,    -- Parallel receive data
     -- Inbound Buffer Request/Status
     gn_p_wr_req_i   => gn_p_wr_req_i,    -- PCIe Write Request
     gn_p_wr_rdy_o   => gn_p_wr_rdy_o,    -- PCIe Write Ready
     gn_rx_error_o   => gn_rx_error_o,    -- Receive Error
     -- Local to Parallel [Outbound Data] - TX
     gn_l2p_clk_n_o  => gn_l2p_clk_n_o,   -- Transmitter Source Synchronous Clock-
     gn_l2p_clk_p_o  => gn_l2p_clk_p_o,   -- Transmitter Source Synchronous Clock+
     gn_l2p_dframe_o => gn_l2p_dframe_o,  -- Transmit Data Frame
     gn_l2p_valid_o  => gn_l2p_valid_o,   -- Transmit Data Valid
     gn_l2p_edb_o    => gn_l2p_edb_o,     -- Packet termination and discard
     gn_l2p_data_o   => gn_l2p_data_o,    -- Parallel transmit data
     -- Outbound Buffer Status
     gn_l2p_rdy_i    => gn_l2p_rdy_i,     -- Tx Buffer Full Flag
     gn_l_wr_rdy_i   => gn_l_wr_rdy_i,    -- Local-to-PCIe Write
     gn_p_rd_d_rdy_i => gn_p_rd_d_rdy_i,  -- PCIe-to-Local Read Response Data Ready
     gn_tx_error_i   => gn_tx_error_i,    -- Transmit Error
     gn_vc_rdy_i     => gn_vc_rdy_i,         -- Channel ready
     -- General Purpose Interface
     gn_gpio_b       => gn_gpio_b,        -- gn_gpio[0] -> GN4124 GPIO8
                                          -- gn_gpio[1] -> GN4124 GPIO9
     ---------------------------------------------------------------------------
     -- FMC interface
     ---------------------------------------------------------------------------
     -- Presence  (there is a pull-up)
     fmc0_prsnt_m2c_n_i => '0',
     ---------------------------------------------------------------------------
     -- Flash memory SPI interface
     ---------------------------------------------------------------------------
     spi_miso_i      => '0',
     onewire_b       => open,
     ---------------------------------------------------------------------------
     -- Miscellanous SPEC pins
     ---------------------------------------------------------------------------
     -- PCB version
     pcbrev_i          => (others=>'0'), -- 4-bit
     fmc0_scl_b        => fmc0_scl_b,
     fmc0_sda_b        => fmc0_sda_b,
     --  Clocks and reset.
     clk_dmtd_125m_o   => open,
     clk_62m5_sys_o    => clk_62m5_sys,  -- we only need the 62.5MHz clock that spec_base provides
     rst_62m5_sys_n_o  => local_reset_n,
     clk_125m_ref_o    => open,
     rst_125m_ref_n_o  => open,
     --  Interrupts (go directly to VIC, if it is activated)
     irq_user_i(6)     => hmq_in_irq,
     irq_user_i(7)     => hmq_out_irq,
     irq_user_i(8)     => notify_irq,
     irq_user_i(9)     => console_irq,
     --  The wishbone bus from the gennum/host to the application
     --  Addresses 0-0x1fff are not available (used by the carrier).
     --  This is a pipelined wishbone with byte granularity.
     app_wb_o   => cnx_slave_in(c_MASTER_GENNUM),
     app_wb_i   => cnx_slave_out(c_MASTER_GENNUM),
     -- for simulation, we need to declare also the 
     sim_wb_o => sim_wb_out,
     sim_wb_i => sim_wb_in
  );


---------------------------------------------------------------------------------------------------
--                                      FMC MASTERFIP CORE                                       --
---------------------------------------------------------------------------------------------------
  cmp_masterFIP_core : entity work.fmc_masterFIP_core
  generic map
    (g_span             => 32,
     g_width            => 32,
     g_simulation       => g_simulation)
  port map
    (clk_i              => clk_62m5_sys,
     rst_n_i            => rst_n_sys,
     -- FMC one-wire
     onewire_b          => fmc_onewire_b,
     -- WorldFIP speed
     speed_b0_i         => speed_b0_i,
     speed_b1_i         => speed_b1_i,
     -- FIELDRIVE
     fd_rxcdn_a_i       => fd_rxcdn_i,
     fd_rxd_a_i         => fd_rxd_i,
     fd_txer_a_i        => fd_txer_i,
     fd_wdgn_a_i        => fd_wdgn_i,
     fd_rstn_o          => fd_rstn_o,
     fd_txck_o          => fd_txck_o,
     fd_txd_o           => fd_txd,
     fd_txena_o         => fd_txena_o,
     -- External Synch
     ext_sync_term_en_o => ext_sync_term_en_o,
     ext_sync_a_i       => ext_sync_i,
     ext_sync_dir_o     => open, -- hard-wired to '0'
     ext_sync_oe_n_o    => ext_sync_oe_n_o,
     -- LEDs
     leds_o             => leds,
     -- WISHBONE interface with MT CPU0 and CPU1
     wb_adr_i           => fmc_wb_muxed_out.adr,
     wb_dat_i           => fmc_wb_muxed_out.dat,
     wb_stb_i           => fmc_wb_muxed_out.stb,
     wb_we_i            => fmc_wb_muxed_out.we,
     wb_cyc_i           => fmc_wb_muxed_out.cyc,
     wb_sel_i           => fmc_wb_muxed_out.sel,
     wb_dat_o           => fmc_wb_muxed_in.dat,
     wb_ack_o           => fmc_wb_muxed_in.ack,
     wb_stall_o         => fmc_wb_muxed_in.stall);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- unused WISHBONE signals
  fmc_wb_muxed_in.err   <= '0';
  fmc_wb_muxed_in.rty   <= '0';
--  fmc_wb_muxed_in.int   <= '0';

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  led_rx_act_n_o        <= leds(0); -- probe on R4
  led_rx_err_n_o        <= leds(1); -- probe on R8
  led_tx_act_n_o        <= leds(2); -- probe on R4
  led_tx_err_n_o        <= leds(3); -- probe on R7
  led_sync_act_n_o      <= leds(4); -- probe on R1
  led_sync_err_n_o      <= leds(5); -- probe on R6

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  fd_txd_o              <= fd_txd;
  tp1_o                 <= fd_rxd_i;
  tp2_o                 <= fd_txd;
  tp3_o                 <= leds(8);
  tp4_o                 <= leds(9);


---------------------------------------------------------------------------------------------------
--                                     SPEC front panel LEDs                                     --
---------------------------------------------------------------------------------------------------
  p_drive_spec_led_clk_sys: process (clk_62m5_sys)
  begin
    if rising_edge(clk_62m5_sys) then
      if(rst_n_sys = '0') then
        spec_led     <= "01111111";
        led_divider <= (others => '0');
      else
        led_divider <= led_divider+ 1;
        if(led_divider = 0) then
          spec_led   <= spec_led(6 downto 0) & spec_led(7);
        end if;
      end if;
    end if;
  end process;

  led_green_o <= spec_led(7);
  led_red_o   <= not rst_n_sys;


end rtl;
--=================================================================================================
--                                        architecture end
--=================================================================================================
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------

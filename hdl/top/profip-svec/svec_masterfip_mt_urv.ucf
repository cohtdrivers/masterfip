#===============================================================================
# IO Location Constraints
#===============================================================================

#----------------------------------------
# UART
#----------------------------------------
NET "uart_txd_o" LOC = U27;
NET "uart_rxd_i" LOC = U25;

NET "uart_txd_o" IOSTANDARD = "LVCMOS33";
NET "uart_rxd_i" IOSTANDARD = "LVCMOS33";

#----------------------------------------
# SPI
#----------------------------------------
NET "ertec_spi_clk_i" LOC = "AC15";
NET "ertec_spi_clk_i" TIG;
NET "ertec_spi_clk_i" IOSTANDARD = "LVCMOS25";

NET "ertec_spi_cs_n_i" LOC = "AB19";
NET "ertec_spi_cs_n_i" IOSTANDARD = "LVCMOS25";
NET "ertec_spi_cs_n_i" PULLUP;

NET "ertec_spi_mosi_i" LOC = "AD22";
NET "ertec_spi_mosi_i" IOSTANDARD = "LVCMOS25";

NET "ertec_spi_miso_o" LOC = "AD15";
NET "ertec_spi_miso_o" IOSTANDARD = "LVCMOS25";

#----------------------------------------
# ERTEC MISCELLANEOUS
#----------------------------------------

NET "fmc_profinet_rst_n_o" LOC = "AF24";
NET "fmc_profinet_rst_n_o" IOSTANDARD = "LVCMOS25";

NET "ertec_rst_n_i" LOC = "AJ15";
NET "ertec_rst_n_i" IOSTANDARD = "LVCMOS25";
NET "ertec_rst_n_i" PULLUP;

NET "ertec_rst_cpu_n_i" LOC = "AK15";
NET "ertec_rst_cpu_n_i" IOSTANDARD = "LVCMOS25";
NET "ertec_rst_cpu_n_i" PULLUP;

NET "ertec_rmq_status_o[6]" LOC = "AE13";
NET "ertec_rmq_status_o[6]" IOSTANDARD = "LVCMOS25";

NET "ertec_rmq_status_o[5]" LOC = "AF15";
NET "ertec_rmq_status_o[5]" IOSTANDARD = "LVCMOS25";

NET "ertec_rmq_status_o[4]" LOC = "AD12";
NET "ertec_rmq_status_o[4]" IOSTANDARD = "LVCMOS25";

NET "ertec_rmq_status_o[3]" LOC = "AF13";
NET "ertec_rmq_status_o[3]" IOSTANDARD = "LVCMOS25";

NET "ertec_rmq_status_o[2]" LOC = "AE11";
NET "ertec_rmq_status_o[2]" IOSTANDARD = "LVCMOS25";

NET "ertec_rmq_status_o[1]" LOC = "AF11";
NET "ertec_rmq_status_o[1]" IOSTANDARD = "LVCMOS25";

NET "ertec_rmq_status_o[0]" LOC = "AC12";
NET "ertec_rmq_status_o[0]" IOSTANDARD = "LVCMOS25";

#----------------------------------------
# BANK 4 P2V5: SPEC LEDs
#----------------------------------------

NET "LED_RED_O" LOC = "D5";
NET "LED_RED_O" IOSTANDARD = "LVCMOS25";

NET "LED_GREEN_O" LOC = "E5";
NET "LED_GREEN_O" IOSTANDARD = "LVCMOS25";



# Bank 0 FMC2
NET "led_profinet_y_o" LOC = "Y19";
NET "led_profinet_y_o" IOSTANDARD = "LVCMOS25";

NET "led_profinet_g_o" LOC = "AA22";
NET "led_profinet_g_o" IOSTANDARD = "LVCMOS25";

NET "led_profinet_r_o" LOC = "AC22";
NET "led_profinet_r_o" IOSTANDARD = "LVCMOS25";

NET "ertec_gpio_16_i" LOC = "AA15";
NET "ertec_gpio_16_i" IOSTANDARD = "LVCMOS25";

NET "ertec_gpio_17_i" LOC = "AF21";
NET "ertec_gpio_17_i" IOSTANDARD = "LVCMOS25";

NET "ertec_gpio_18_i" LOC = "AB20";
NET "ertec_gpio_18_i" IOSTANDARD = "LVCMOS25";
#----------------------------------------
# P2V5: FMC
#----------------------------------------

# BANK 0
NET "fmc_onewire_b" LOC = "K11";
NET "fmc_onewire_b" IOSTANDARD = "LVCMOS25";

NET "led_tx_err_n_o" LOC = "J12";
NET "led_tx_err_n_o" IOSTANDARD = "LVCMOS25";

NET "led_tx_act_n_o" LOC = "H12";
NET "led_tx_act_n_o" IOSTANDARD = "LVCMOS25";

NET "led_rx_err_n_o" LOC = "H11";
NET "led_rx_err_n_o" IOSTANDARD = "LVCMOS25";

NET "led_rx_act_n_o" LOC = "G11";
NET "led_rx_act_n_o" IOSTANDARD = "LVCMOS25";

NET "speed_b0_i" LOC = "B25";
NET "speed_b0_i" IOSTANDARD = "LVCMOS25";

NET "speed_b1_i" LOC = "A25";
NET "speed_b1_i" IOSTANDARD = "LVCMOS25";

NET "fd_rstn_o" LOC = "E9";
NET "fd_rstn_o" IOSTANDARD = "LVCMOS25";

NET "fd_txd_o" LOC = "E13";
NET "fd_txd_o" IOSTANDARD = "LVCMOS25";

NET "fd_txck_o" LOC = "F9";
NET "fd_txck_o" IOSTANDARD = "LVCMOS25";

NET "fd_txer_i" LOC = "E15";
NET "fd_txer_i" IOSTANDARD = "LVCMOS25";

NET "fd_rxcdn_i" LOC = "F11";
NET "fd_rxcdn_i" IOSTANDARD = "LVCMOS25";

NET "fd_rxd_i" LOC = "E11";
NET "fd_rxd_i" IOSTANDARD = "LVCMOS25";

NET "fd_wdgn_i" LOC = "F15";
NET "fd_wdgn_i" IOSTANDARD = "LVCMOS25";

NET "fd_txena_o" LOC = "F13";
NET "fd_txena_o" IOSTANDARD = "LVCMOS25";

NET "ext_sync_dir_o" LOC = "H22";
NET "ext_sync_dir_o" IOSTANDARD = "LVCMOS25";

NET "led_sync_err_n_o" LOC = "F20";
NET "led_sync_err_n_o" IOSTANDARD = "LVCMOS25";

NET "ext_sync_i" LOC = "D24";
NET "ext_sync_i" IOSTANDARD = "LVCMOS25";

NET "ext_sync_oe_n_o" LOC = "J22";
NET "ext_sync_oe_n_o" IOSTANDARD = "LVCMOS25";

NET "tp1_o" LOC = "M15";
NET "tp1_o" IOSTANDARD = "LVCMOS25";

NET "tp2_o" LOC = "K15";
NET "tp2_o" IOSTANDARD = "LVCMOS25";
NET "tp3_o" LOC = "L14";
NET "tp3_o" IOSTANDARD = "LVCMOS25";

NET "tp4_o" LOC = "K14";
NET "tp4_o" IOSTANDARD = "LVCMOS25";

NET "ext_sync_term_en_o" LOC = "C24";
NET "ext_sync_term_en_o" IOSTANDARD = "LVCMOS25";

NET "led_sync_act_n_o" LOC = "G20";
NET "led_sync_act_n_o" IOSTANDARD = "LVCMOS25";

NET "ertec_uart_tx_1_i" LOC = "AE23";
NET "ertec_uart_tx_1_i" IOSTANDARD = "LVCMOS25";

NET "ertec_uart_rx_1_o" LOC = "AF23";
NET "ertec_uart_rx_1_o" IOSTANDARD = "LVCMOS25";

NET "ertec_uart_tx_2_o" LOC = "AE15";
NET "ertec_uart_tx_2_o" IOSTANDARD = "LVCMOS25";

NET "ertec_uart_rx_2_i" LOC = "AD10";
NET "ertec_uart_rx_2_i" IOSTANDARD = "LVCMOS25";

#PIN "cmp_mt_profip_translator/cmp_mosi_async_fifo/empty_int_BUFG.O" CLOCK_DEDICATED_ROUTE = FALSE;
TIMESPEC "TS_exception_cdc"= FROM "cmp_mt_profip_translator/clk_i" TO "cmp_mt_profip_translator/spi_sample_clk_i" TIG;
TIMESPEC "TS_exception_cdc_2"= FROM "cmp_mt_profip_translator/spi_sample_clk_i" TO "cmp_mt_profip_translator/clk_i" TIG;
#TIMESPEC "TS_exception666_3"= FROM "cmp_spi_rmq_bridge/spi_sample_clk_i/s_tx_reg_31" TO "cmp_spi_rmq_bridge/spi_miso_o" 15 ns DATAPATHONLY;

# Reset false path
NET "*/s_rst_n" TIG;


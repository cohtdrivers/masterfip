# In order not to have a conflict with the wf_package that is being used
# for both Nanofip and Masterfip projects, include here the exact cores
# that masterfip uses from Nanofip. In this way, we ensure that the masterfip's
# wf_package will be used. Both of the packages, share the same name and some 
# same constants, but with different values. In case of a change in these files 
# in Nanofip, the change will be reflected here. If a new core will be added to
# Nanofip and this core will be used also in Masterfip, then it needs to be added
# in this Manifest.py file.

files = [
    "svec_masterfip_mt_urv.ucf",
    "svec_masterfip_mt_urv.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_crc.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_decr_counter.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_incr_counter.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_rx_deglitcher.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_rx_deserializer.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_rx_osc.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_tx_osc.vhd",
    "../../../dependencies/nanofip-gateware/src/wf_tx_serializer.vhd"
]

fetchto = "../../ip_cores"

modules = {
    "local" : [
        "../../rtl",
        "../../../dependencies/svec",
        "../../../dependencies/vme64x-core",
        "../../../dependencies/general-cores",
        "../../../dependencies/urv-core",
        "../../../dependencies/mockturtle",
        "../../../dependencies/wr-cores",
        "../../../dependencies/ddr3-sp6-core"
    ],
}



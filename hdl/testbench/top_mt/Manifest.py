sim_tool  = "modelsim"
sim_top   = "main"
top_module= "main"
action    = "simulation"
board     = "svec"
target    = "xilinx"
fetchto   = "../../ip_cores"
vcom_opt  = "-2008 -mixedsvvh"
vlog_opt  = "+incdir+../include +incdir+."

syn_device="xc6slx45t"

if locals().get('fetchto', None) is None:
    fetchto = "../../ip_cores"
    fetchto = "../../rtl"

# Ideally this should be done by hdlmake itself, to allow downstream Manifests to be able to use the
# fetchto variable independent of where those Manifests reside in the filesystem.
import os
fetchto = os.path.abspath(fetchto)


include_dirs = [
    "../include",
    fetchto + "/general-cores/sim",
    fetchto + "/mockturtle/hdl/testbench/include",
    fetchto + "wr-cores/board",
    fetchto + "/vme64x-core/hdl/sim/vme64x_bfm/"
]

files = [ "main.sv",
          "buildinfo_pkg.vhd"]

modules = {
    "local" :  [
        "../../top/svec",
        "../../ip_cores/wr-cores/board"
    ],
    "git" : [
        "https://ohwr.org/project/vme64x-core.git",
    ]
}

svec_base_ucf = []

ctrls = ["bank4_64b_32b", "bank5_64b_32b"]

# Do not fail during hdlmake fetch
try:
   exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
   pass

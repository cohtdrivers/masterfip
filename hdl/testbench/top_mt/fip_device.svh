/*
 * This program source code file is part of MasterFip project.
 *
 * Copyright (C) 2013-2017 CERN
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/* 
   fip_device.svh - implementation of the FipDevice class, representing
   an abstract model of a device speaking WorldFIP protocol
*/

`ifndef __FIP_DEVICE_MODEL_INCLUDED
`define __FIP_DEVICE_MODEL_INCLUDED
  
`include "fip_phy_model.svh"
`include "logger.svh"

typedef class FipFrame;
   
class FipDevice extends LoggerClient;
   // PHY interface the device is connected to
   virtual FipPHY m_phy;
   
   task automatic  attach (virtual FipPHY phy );
      m_phy = phy;
      m_phy.setHandler(this);
   endtask // attachInterface

   virtual task automatic send ( FipFrame frame );
      m_phy.send(frame);
   endtask // send
   
   virtual task automatic  onReceive( FipFrame frame );
   endtask // onReceive

   virtual task automatic onPreamble();
   endtask // onReceive
   
   function automatic bit isIdle();
      return m_phy.isIdle();
   endfunction // isIdle

   task automatic setSpeed( int speed );
      m_phy.setSpeed( speed );
   endtask // setSpeed
   
   function automatic longint getTurnaroundTime();
      return m_phy.getTurnaroundTime();
   endfunction // getTurnaroundTime
   
   function automatic longint getSilenceTime();
      return m_phy.getSilenceTime();
   endfunction // getTurnaroundTime
endclass // FipDevice

`endif //  `ifndef __FIP_DEVICE_MODEL_INCLUDED


/*
 * This program source code file is part of MasterFip project.
 *
 * Copyright (C) 2013-2017 CERN
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/* fip_agent_device.svh - implementation of a virtual WorldFIP Agent */

`ifndef __FIP_AGENT_DEVICE_SVH
 `define __FIP_AGENT_DEVICE_SVH

typedef enum 
  {
   ST_IDLE = 0,
   ST_CONSUME_PERIODIC = 1,
   ST_CONSUME_MESSAGE = 2,
   ST_WAIT_RP_FIN = 3,
   ST_WAIT_RP_ACK = 4
   } AgentState;

typedef enum 
  {
   AFM_NONE = 0,
   AFM_DUPLICATE_RP_DAT = 1,
   AFM_OVERRIDE_RP_FIN = 2,
   AFM_DROP_RP_FIN = 3,
   AFM_OVERRIDE_RP_ACK = 4
} AgentFailureMode;

/*
 * Class FipAgentTransaction
 * 
 * Represents a single transaction executed by a virtual FIP agent. Used by
 * FFipAgentDevice to store consumed/produced variables and messages.
 */
class FipAgentTransaction;
   // if set and doProduceVar == 1, the agent will produce variables with requested payload
   FipFrame producedVar;
   // if doConsumeVar == 1, the agent will save contents of the consumed variable to consumedVar
   FipFrame consumedVar;
   // same as above, but for messages
   FipFrame producedMsg;
   FipFrame consumedMsg;

   // custom RP_FIN frame in case of AFM_OVERRIDE_RP_FIN failure mode
   FipFrame rpFinFrame;
   // custom RP_ACK frame in case of AFM_OVERRIDE_RP_ACK failure mode
   FipFrame rpAckFrame;
   
   
   bit 	     doProduceVar;
   bit 	     doProduceMsg;
   bit 	     consumedMsgValid;
   bit 	     consumedVarValid;

   // failure mode: allows to reproduce pathological situations normal FIP agent
   // never does
   AgentFailureMode failureMode;
   
   
   function new;
      failureMode =  AFM_NONE;
      doProduceVar = 0;
      doProduceMsg = 0;
      consumedMsgValid = 0;
      consumedVarValid = 0;
   endfunction // new
   
endclass // AgentTransaction

/*
 * Class FipAgentDevice
 * 
 * A minimum functional implementation of a FIP agent capable of speaking CERN's WorldFIP subset:
 * - producing/consuming periodic variables
 * - producing/consuming aperiodic messages (with RP_DAT_MSG requests). Odd-ACKed and non-ACKed messages
 *   are supported.
 * The agent here has no intelligence, it just sends pre-programmed messages and variables and it will
 * store all incoming traffic. Basic run-time validation of FIP protocol is also provided.
 */
class FipAgentDevice extends FipDevice;
// address of the agent
   protected int m_address;
// state of the agent's FSM
   protected AgentState m_state;
// last received ID_DAT/ID_MSG grame
   protected FipFrame m_idFrame;
// list of variables/IDs supported by this agent 
   FipAgentTransaction m_transactions[256];

   // returns a transaction object for a given variable/message ID
   function automatic FipAgentTransaction getTransaction( int addr );
      return m_transactions[addr];
   endfunction // getTransaction

   function automatic FipFrame getProducedVar( int addr );
      return m_transactions[addr].producedVar;
   endfunction // getTransaction

   function automatic FipFrame getProducedMsg( int addr );
      return m_transactions[addr].producedMsg;
   endfunction // getTransaction

   function automatic FipFrame getConsumedVar( int addr );
      return m_transactions[addr].consumedVar;
   endfunction // getTransaction

   function automatic FipFrame getConsumedMsg( int addr );
      return m_transactions[addr].consumedMsg;
   endfunction // getTransaction

   // sets the payload of the produced variable of address addr to f
   task automatic setProducedVar ( int index, FipFrame frame );
      m_transactions[index].producedVar = frame;
      m_transactions[index].doProduceVar = 1;
   endtask
   
   function new(int address_);
      m_address = address_;
      m_state = ST_IDLE;

      for(int i = 0; i < 256; i++)
	   m_transactions[i] = new;

      // create default variable 0x14 (presence response)
      m_transactions[ 'h14 ].doProduceVar = 1;
      m_transactions[ 'h14 ].producedVar = FipFrame::makePresence();
   endfunction // onReceive


   // invoked by onReceive if the master has requested this agent to produce
   // a message
   virtual  task automatic onProduceMessage( FipFrame frame, int variableId );
      automatic FipAgentTransaction trans = m_transactions[ variableId ];
      automatic FipFrame response = trans.producedMsg;

      // are we able to respond to this message request at all?
      if(response == null || !trans.doProduceMsg)	
	return;
      
      msg(0, $sformatf("Agent 0x%02x produces aperiodic message 0x%02x.", m_address, variableId ) );
      msg(0, $sformatf("Agent 0x%02x sending RP_MSG.", m_address ) );

      // send the pre-defined RP_MSG
      send( response );

      // in case of an ACK-ed message, set the FSM to wait for the ACK packet, otherwise go handle another transaction
      if ( response.getControl() == FT_RP_MSG_ACK_ODD ||
	    response.getControl() == FT_RP_MSG_ACK_EVEN )
	begin
	   m_state = ST_WAIT_RP_ACK;
	   msg(0, $sformatf("Agent 0x%02x waiting for RP_ACK.", m_address ) );
	end else begin
	   m_state = ST_IDLE;

	   msg(0, $sformatf("Agent 0x%02x sending RP_FIN.", m_address ) );

	   if ( trans.failureMode == AFM_DROP_RP_FIN )
	     return;
	   
	   send( trans.failureMode == AFM_OVERRIDE_RP_FIN ? trans.rpFinFrame : FipFrame::makeRP_FIN() );
	end
      
   endtask // onProduceMessage

   // invoked by onReceive if the master has sent a message to this agent.
   virtual task automatic onConsumeMessage( FipFrame frame, int variableId );
      msg(0, $sformatf("Agent 0x%02x consumes aperiodic message 0x%02x.", m_address, variableId ) );
      m_transactions[variableId].consumedMsg = frame;
      m_transactions[variableId].consumedMsgValid = 1;
   endtask // onProduceMessage
   
   // invoked by onReceive if the master has requested this agent to produce
   // a periodic variable
   virtual task automatic onProducePeriodicVariable( FipFrame frame, int variableId );
      FipAgentTransaction trans = m_transactions[ variableId ];
      FipFrame response = trans.producedVar;

      // are we able to respond to this variable request at all?
      if(response == null || !trans.doProduceVar)	
	return;
      
      msg(0, $sformatf("Agent 0x%02x produces periodic variable 0x%02x (%d bytes).", m_address, variableId, response.size() ) );

      send( response );

      if ( trans.failureMode == AFM_DUPLICATE_RP_DAT )
	begin
	   msg(0, $sformatf("Agent 0x%02x produces DUPLICATE RP_DAT frame for variable 0x%02x (%d bytes).", m_address, variableId, response.size() ) );

	   send( response );
	end
      
	

   endtask

   // invoked by onReceive if the master has sent a periodic variable to this agent.
   virtual task automatic onConsumePeriodicVariable( FipFrame frame, int variableId );
      msg(0, $sformatf("Agent 0x%02x consumes periodic variable 0x%02x (PDU_TYPE 0x%x, data length %d bytes, MPS status 0x%02x).", m_address, variableId,
			 frame.getPDUType(),
			 frame.getPayloadSize(),
			 frame.getMPSStatus()
			 ) );

      m_transactions[variableId].consumedVar = frame;
      m_transactions[variableId].consumedVarValid = 1;
   endtask

   // Callback invoked by the PHY whenever a new frame has been received.
   // Runs the main Agent state maching.
   virtual task automatic onReceive ( FipFrame frame );
      int ctl = frame.getControl();
      int v = frame.getVariable();


      // handle incoming ID_DAT/ID_MSG frames.
      if(ctl == FT_ID_DAT || ctl == FT_ID_MSG )
	begin
	   if ( frame.getAddress() != m_address )
	     return; // not for us

	   m_idFrame = frame;

	   if( ctl == FT_ID_DAT && !m_transactions[v].doProduceVar )
	     m_state = ST_CONSUME_PERIODIC;
	   else if ( ctl == FT_ID_MSG && !m_transactions[v].doProduceMsg )
	     m_state = ST_CONSUME_MESSAGE;
	   else	if( ctl == FT_ID_DAT ) 
	     onProducePeriodicVariable( frame, v );
	   else if ( ctl == FT_ID_MSG )
	     onProduceMessage( frame, v );

	   return;
	end // if (ctl == FT_ID_DAT || ctl == FT_ID_MSG )

      case ( m_state )
	// waiting for an RP_DAT from the master following ID_DAT
	ST_CONSUME_PERIODIC:
	  begin
	     if ( ctl != FT_RP_DAT )
	       begin
		  fail($sformatf("Agent 0x%x expected a consumable RP_DAT frame following ID_DAT, but got a %s.",
				   m_address, frame.str()));
	       end else begin
		  onConsumePeriodicVariable( frame, m_idFrame.getVariable() );
	       end

	     m_state = ST_IDLE;
	     
	  end

	ST_CONSUME_MESSAGE:
	// waiting for an RP_MSG from the master following ID_DAT
	  begin
	     if ( ctl != FT_RP_DAT_MSG && ctl != FT_RP_MSG_NOACK )
	       begin
		  fail($sformatf("Agent 0x%x expected a consumable RP_MSG_NOACK frame following ID_MSG, but got a %s.",
				   m_address, frame.str()));
		  m_state = ST_IDLE;

	       end else begin
		  onConsumeMessage( frame, m_idFrame.getVariable() );
		  m_state = ST_WAIT_RP_FIN;
		  
	       end
	  end

	// waiting for an RP_FIN from the master following message exchange
	ST_WAIT_RP_FIN:
	  begin
	     if (frame.getControl() != FT_RP_FIN)
	       begin
		  fail($sformatf("Agent 0x%x expected an RP_FIN frame following RP_MSG, but got a %s.",
				   m_address, frame.str() ) );

	       end
	     
	     m_state = ST_IDLE;
	  end

	// waiting for an RP_ACK from the master following RP_MSG_ACK
	ST_WAIT_RP_ACK:
	  begin
	     if ( (frame.getControl() != FT_RP_ACK_ODD) && (frame.getControl() != FT_RP_ACK_EVEN) )
	       begin
		  fail($sformatf("Agent 0x%x expected an RP_ACK frame following RP_MSG, but got a %s.",
				   m_address, frame.str() ) );

	       end else begin
		  msg(0, $sformatf("Agent 0x%02x sending RP_FIN.", m_address ) );
		  send( FipFrame::makeRP_FIN() );

	       end
	     m_state = ST_IDLE;
	  end
      endcase // case ( m_state )
   endtask // onReceive
   
endclass // FipAgentDevice

/*
 * Class MasterfipTestAgentDevice
 * 
 * A specialization of a FipAgentDevice according
 * to the MasterFIP test specification.
 */

class MasterfipTestAgentDevice extends FipAgentDevice;

   // variant defines the macrocycle variant (0 = no ACKed messages, 1 = with ACKed messages)
   function new(int address_, int variant);
      super.new(address_);

      // we always produce variable 0x2.
      m_transactions['h02].doProduceVar = 1;

      case (address_)
	'h01:
	  begin
	     // agent 0x1 produces 64-byte RP_DAT_MSG
	     m_transactions['h02].producedVar = FipFrame::makeRP_DAT( FT_RP_DAT_MSG, makeRange(0, 63) );
	     // and a 127-byte RP_MSG_NOACK
	     m_transactions['h02].producedMsg = FipFrame::makeRP_MSG( FT_RP_MSG_NOACK, 0, 'h400, makeRange(0, 124) );
	     m_transactions['h02].doProduceMsg = 1;
	     m_transactions['h00].doProduceMsg=  0;
	  end
	'h5f:
	  begin
	     // agent 0x5f produces 2-byte RP_DAT/RP_DAT_MSG
	     m_transactions['h02].producedVar = FipFrame::makeRP_DAT( variant == 0 ? FT_RP_DAT : FT_RP_DAT_MSG, makeRange(0, 1) );
	     // and a 256-byte RP_MSG_ACK(o) 
	     m_transactions['h02].producedMsg = FipFrame::makeRP_MSG( FT_RP_MSG_ACK_ODD, 0, 'h400, makeRange(0, 255) );
	     m_transactions['h02].doProduceMsg = 1;
	     
	  end
	
	'hb4:
	  // agent 0xb4 produces a long (127-byte) RP_DAT
	  m_transactions['h02].producedVar = FipFrame::makeRP_DAT( FT_RP_DAT, makeRange(0, 'h7b) );
      endcase // case (address_)


      
      endfunction // new

endclass // MasterfipAgentDevice

`endif //  `ifndef __FIP_AGENT_DEVICE_SVH


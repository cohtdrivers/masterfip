/*
 * This program source code file is part of MasterFip project.
 *
 * Copyright (C) 2013-2017 CERN
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

/* 
   masterfip_driver.svh - Virtual MasterFIP device driver, on top of the Mock
   Turtle library. Provides minimum API functionality necessary for verification
   of the RT code.
*/

`ifndef __MASTERFIP_DRIVER_INCLUDED
 `define __MASTERFIP_DRIVER_INCLUDED

 `include "logger.svh"
 `include "mock_turtle_driver.svh"
// `include "serializable.svh"
 `include "masterfip_common.svh"
 `include "fip_frame.svh"


// copies a number of bytes from a word-aligned buffer (reported to HMQ by the MasteFIP)
// to an array, swapping the endianess.
function automatic void copyLEBytes( ByteBuffer data, ref uint8_t dst[$], input int count );
   int i;
   
   for(i=0;i<(count+3)/4;i++)
     begin
	int remaining = (count - i * 4) >= 4 ? 4 : (count - i * 4);
	uint32_t w = data.getWord();

	for(int j = 0; j < remaining;j++)
	  begin
	     uint8_t b = (w >> (8*j)) & 'hff;
	     dst.push_back(b);
	  end
     end
endfunction // copyLEBytes

class MasterfipVariablePayload extends Serializable;
   uint32_t error;
   uint8_t payload[$];

   virtual function automatic void deserialize( ByteBuffer data );
      automatic uint32_t key = data.getWord();
      automatic uint32_t payload_bsz = data.getWord();

      error = data.getWord();
      copyLEBytes( data, payload, payload_bsz);
   endfunction // deserialize

   virtual function automatic FipFrame asFipFrame( FipFrameControl control );
      automatic FipFrame f = new;
      automatic uint8_t raw[$] = '{ control };
      for(int i = 0; i < payload.size(); i++)
	raw.push_back(payload[i]);
      f.deserializeBytes(raw);
      return f;
   endfunction // asFipFrame
   
   
endclass // MasterfipVariablePayload

class MasterfipHostTLVMessage #( type FrameType = MasterfipVariablePayload ) extends Serializable;

   bit 	   m_useMessageHack;

   function new;
      m_useMessageHack = 0;
   endfunction // deserialize
   
   
   FrameType m_payloads[$];
   
   virtual function automatic void  deserialize( ByteBuffer data );
      automatic uint32_t tlv_type, tlv_len;
      automatic uint32_t tlv_count;
      
/* -----\/----- EXCLUDED -----\/-----
      for (int i = 0; i < (data.size()+3)/4;i++)
	$display("%x: %x", i, data.getWord());
 -----/\----- EXCLUDED -----/\----- */
  
      data.reset();

      // skip header
      data.getWord();
      tlv_count = data.getWord();
      data.getWord();
      

      for(int i = 0; i < tlv_count; i++)
	begin
	   automatic int startPos, endPos;
	   startPos = data.getPos() & 'hffc;
	   
	   tlv_type = data.getWord();
	   tlv_len = data.getWord();

/* -----\/----- EXCLUDED -----\/-----
	   $display("parse tlv %d %d\n", tlv_type, tlv_len );
 -----/\----- EXCLUDED -----/\----- */
	   
	   
	   if(tlv_len == 0)
	     return;
		    
	   case(tlv_type)
	     0: begin // DATA TLV
		
		automatic FrameType p = new;
//		$display("DATA");
		p.deserialize(data);
		m_payloads.push_back(p);

/* -----\/----- EXCLUDED -----\/-----
		if(m_useMessageHack && !p.error)
		  begin
		     $display("hack in action!");
		     tlv_len += 5;
		  end
 -----/\----- EXCLUDED -----/\----- */

		
	     end // case: 0
	     1: begin // IRQ TLV : skip for the moment
		automatic int i;
//		$display("IRQ");
		
		for(i=0;i<6;i++) data.getWord();
		
	     end
	     default:
	       $error("WRONG TLV %d %d", tlv_type, tlv_len);
	     
	   endcase // case (tlv_type)

	   endPos = data.getPos() & 'hffc;

//	   $display("start_pos %d tlv_len %d", startPos, tlv_len );
	   

	   
	   
	   data.setPos( startPos + tlv_len * 4 );
	   
	   
//	   $display("d %d tlvLen %d type %d", (endPos - startPos) / 4, tlv_len, tlv_type);
	end // while (data.getPos() < data.size() )

//      $display("\n\n\n\nFrames: %d irqs: %d\n\n\n\n", frames, irqs);
      
   endfunction // deserialize

   
   
endclass // mstrfip_trtlmsg_hdr



class MasterfipDriver;

   MockTurtleDriver m_mt;

   protected const time c_hmq_timeout = 2000;
   
   
   int 		    m_bitrate;
   
   static const uint32_t mstrfip_bit_nstime[] = '{30769, 1000, 400};
   static const uint32_t mstrfip_tr_nstime[] = '{532000, 10000, 6000};
   static const uint32_t mstrfip_ts_nstime[] = '{4312000, 178000, 116000};
   static const int MSTRFIP_trtl_CPU_TICKS_NS = 10;
   
   function new(MockTurtleDriver mt);
      m_mt = mt;
   endfunction // repeat

   function MockTurtleDriver MockTurtle();
      return m_mt;
   endfunction // MockTurtle  
   
   task automatic init();
      automatic Logger l = Logger::get();
      const uint32_t MSTRFIP_APP_ID= 'hf1dc03e;
      automatic uint32_t id, core_count, mem0_size, mem1_size;

      l.startTest("Initialize MT drivers");
      
      m_mt.init();
      m_mt.cfg_rom_display();

      id = m_mt.rom.getAppID();

      l.msg(0, $sformatf("Read App ID: 0x%x", id));
      if ( id != MSTRFIP_APP_ID )
	   begin
	      l.fail($sformatf("  Incorrect App ID: 0x%x, expected %x", id, MSTRFIP_APP_ID));
	      return;
	   end

      core_count = m_mt.rom.getCoreCount();

      l.msg(0, $sformatf("Read CPU core count: %d", core_count));
      if ( core_count != 2 )
	   begin
	      l.fail($sformatf("Incorrect CPU cores number: %d, expected 2", core_count));
	      return;
	   end

      l.pass();
   endtask // init

   task sendAndReceiveSync ( MQueueMsg msg_in, ref MQueueMsg msg_out, input real timeout_us = 1000000, ref int status );
      time t0;

      $display(msg_in.tostring());
      $display(msg_out.tostring());

      m_mt.hmq_send_message(msg_in);      

      t0 = $time;
      $display("msg_in = %p",msg_in);

      while ( real'($time - t0) / real'(1us) < timeout_us )
      begin

         if( m_mt.hmq_pending_messages(msg_out.core, msg_out.slot) ) begin
            m_mt.hmq_receive_message(msg_out);
      	    $display("msg_out = ",msg_out.tostring());

            status = 0;      
            return;        
         end      
      
         #100ns;
      end

      $error("sendAndReceiveSync: timeout [%d us] expired!", int'(timeout_us));
      status = -1;
      
   endtask // sendAndReceiveSync
   

   
  
endclass // MasterfipDriver


`endif


// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//==============================================================================

//! @file main.sv

//==============================================================================

//------------------------------------------------------------------------------
//! CERN (BE-CEM-EDL)
//
//! @brief SystemVerilog top level testbench for Profip. The purpose of this is 
//!        to verify the basic functionality of Profip. Sending through SPI
//!        messages using RMQ protocol and waiting for the response from the
//!        CPUs of MockTurtle. It also contains the SW binaries which are
//!        necessary for the Testbench and this is our way to interact with the
//!        CPUs. The latest are:  https://gitlab.cern.ch/cohtdrivers/masterfip/-/jobs/
//
//! @author Konstantinos Blantos
//
//! @date 26/09/2023
//
//------------------------------------------------------------------------------


`timescale 1ns/1ps

`include "vme64x_bfm.svh"
`include "svec_vme_buffers.svh"

`include "vhd_wishbone_master.svh"
`include "logger.svh"
`include "mock_turtle_driver.svh"
`include "masterfip_driver.svh"
`include "fip_hardware_model.svh"
`include "fip_phy_model.svh"
`include "wf_message_pkg.svh"
`include "spi_if.svh"

import wf_message_pkg::*;

`define VME_OFFSET      'h8000_0000
`define MT_BASE         `VME_OFFSET + 'h0004_0000

module main;

    parameter g_fast_load_cpu_firmware = 0;

    reg rst_n = 0;
    reg clk_sys = 0;
    reg clk_test = 0;
    reg rst;
    reg ertec_rst = 0;
    reg ertec_rst_cpu = 0;

    logic fmc_rst = 'b0;
    logic [31:0] mosi_data;
    logic [31:0] miso_data;
    logic [6:0]  rmq_status_o;
    logic [31:0] miso_spi_data;

    logic [4:0]  posedge_cnt = 'h0;

    logic tmp_spi_clk = 1'b0;

    logic [31:0] q_mosi_data[$];
    logic [31:0] q_miso_data[$];

    // Array containing the worldfip messages used in the Testbench
    logic [31:0] send_msg[];

    always #8ns clk_sys <= ~clk_sys;
    always #100ns tmp_spi_clk <= ~tmp_spi_clk; // 500ns for 1MHz, 100ns for 5MHz, 50ns for 10MHz, 25ns for 20MHz

    initial begin
        repeat(20) @(posedge clk_sys);
        rst_n = 1;
        ertec_rst = 1;
        ertec_rst_cpu = 1;
    end

    IVME64X VME(rst_n);

    `DECLARE_VME_BUFFERS(VME.slave);

    bit [4:0] slot_id = 8;

    // Board and FIP bus instantiation
    pulldown(fip_bus.p);
    pulldown(fip_bus.n);

    FIPBus fip_bus();
    FieldriveFPGA fieldrive();

    FipPHY U_Agent1PHY(fip_bus.p, fip_bus.n);
    FipPHY U_Agent2PHY(fip_bus.p, fip_bus.n);
    FipPHY U_Agent3PHY(fip_bus.p, fip_bus.n);
    FipPHY U_MonitorPHY(fip_bus.p, fip_bus.n);
    FipPHY U_VirtualMasterPHY(fip_bus.p, fip_bus.n);

    MasterfipMezzanine U_Mezzanine(fieldrive, fip_bus);

    // Instantiate the Interface for SPI signals
    spi_if if_spi();

    // Creating a virtual interface to use the interface's tasks
    virtual spi_if vif_spi;

    // Assign the interface to the virtual interface
    initial vif_spi = if_spi;

    initial miso_data = vif_spi.miso_spi_data;

    // Design Under Test
    svec_masterfip_mt_urv #(
        .g_simulation(1)
    ) DUT (
        // Clock, Reset
        .rst_n_i             (rst_n),
        .clk_125m_pllref_p_i (clk_sys),
        .clk_125m_pllref_n_i (~clk_sys),
        // External reset to ERTEC
        .fmc_profinet_rst_n_o(fmc_rst),
        // Defining the SPEED
        .speed_b0_i          (1'b0), // "10" -> 2.5MHz
        .speed_b1_i          (1'b1),
        // ERTEC SPI Interface
        .ertec_spi_clk_i     (if_spi.spi_clk),
        .ertec_spi_mosi_i    (if_spi.spi_mosi),
        .ertec_spi_cs_n_i    (if_spi.spi_cs_n),
        .ertec_spi_miso_o    (if_spi.spi_miso),
        .ertec_rst_n_i       (ertec_rst),
        .ertec_rmq_status_o  (rmq_status_o),
        .ertec_rst_cpu_n_i   (ertec_rst_cpu),
        // Used to connect NanoFIP
        .fd_rxcdn_i          (fieldrive.fd_rxcdn),
        .fd_rxd_i            (fieldrive.fd_rxd),
        .fd_txer_i           (fieldrive.fd_txer),
        .fd_wdgn_i           (fieldrive.fd_wdgn),
        .fd_rstn_o           (fieldrive.fd_rstn),
        .fd_txck_o           (fieldrive.fd_txck),
        .fd_txd_o            (fieldrive.fd_txd),
        .fd_txena_o          (fieldrive.fd_txena),
        // VME Interface
        .vme_sysreset_n_i    (VME_RST_n),
        .vme_as_n_i          (VME_AS_n),
        .vme_write_n_i       (VME_WRITE_n),
        .vme_am_i            (VME_AM),
        .vme_ds_n_i          (VME_DS_n),
        .vme_gap_i           (^slot_id),
        .vme_ga_i            (~slot_id),
        .vme_berr_o          (VME_BERR),
        .vme_dtack_n_o       (VME_DTACK_n),
        .vme_retry_n_o       (VME_RETRY_n),
        .vme_retry_oe_o      (VME_RETRY_OE),
        .vme_lword_n_b       (VME_LWORD_n),
        .vme_addr_b          (VME_ADDR),
        .vme_data_b          (VME_DATA),
        .vme_irq_o           (VME_IRQ_n),
        .vme_iack_n_i        (VME_IACK_n),
        .vme_iackin_n_i      (VME_IACKIN_n),
        .vme_iackout_n_o     (VME_IACKOUT_n),
        .vme_dtack_oe_o      (VME_DTACK_OE),
        .vme_data_dir_o      (VME_DATA_DIR),
        .vme_data_oe_n_o     (VME_DATA_OE_N),
        .vme_addr_dir_o      (VME_ADDR_DIR),
        .vme_addr_oe_n_o     (VME_ADDR_OE_N)
    );

    IMockTurtleIRQ IrqMonitor (`MT_ATTACH_IRQ(DUT.cmp_mock_turtle_urv));


    IVHDWishboneMaster U_WBMaster (
        .clk_i(DUT.clk_62m5_sys),
        .rst_n_i(DUT.s_rst_n)
    );

    task automatic checkSDBSignature( CBusAccessor acc );
        automatic uint64_t sig;
        automatic Logger l = Logger::get();

        const uint32_t SDB_ID = 'h5344422d;
        acc.read(0, sig);
        l.startTest("Check Wishbone communication");
        l.msg(0, $sformatf("SDB Signature: 0x%x", sig));
        if(sig != SDB_ID)
            l.fail($sformatf("Incorrect SDB signature (got 0x%x, expected 0x%x)", sig, SDB_ID));
        else
            l.pass();
    endtask // check_sdb_signature

    task automatic checkCPUCommunication(MasterfipDriver drv);
        automatic Logger l = Logger::get();
        automatic MQueueMsg request = new (0, 5, '{}, '{});
        automatic int status;

        automatic mstrfip_hw_speed_trtlmsg response = new ();
        automatic MQueueMsg tmp = response;
        response.core = 0;
        response.slot = 5;
        request.header.flags  = `TRTL_HMQ_HEADER_FLAG_RPC | `TRTL_HMQ_HEADER_FLAG_SYNC;
        request.header.msg_id = MSTRFIP_CMD_GET_BITRATE;
        request.header.len = 2;
        request.data[0] = MSTRFIP_CMD_GET_BITRATE;

        $display("request [1]",request.tostring());
        $display("response [1]",response.tostring());

        drv.sendAndReceiveSync(request, tmp, 100, status );

        $display("tmp ",tmp.tostring());

        // TODO: The right one, for bitrate is the response.header.msg_id, because
        // this information is in the header part of the message and not in
        // payload. This is to be fixed.
        if(status < 0)
        begin
        	l.fail("No response from CPU 0 within 100us");
        	return;
        end else begin

        if ( tmp.data[0] != MSTRFIP_REP_BITRATE )
        begin
            l.fail($sformatf("  Incorrect response from the RT, expected 0x10, got 0x%x", tmp.data[0] ) );
            return;
        end
//      response.data_unpack();
//      l.msg(0, $sformatf("Got bitrate from CPU0: %d", response.data[0]));
        end
        l.pass();
    endtask

    // Storing in a queue the MISO data
    always @(posedge vif_spi.spi_clk) begin
        miso_data <= {miso_data[30:0], vif_spi.spi_miso};
    end

    task automatic loadCPUFirmwares(MasterfipDriver drv);
        automatic Logger l = Logger::get();
        const string cpu0_fw = "masterfip-rt-ba.bin";
        const string cpu1_fw = "masterfip-rt-cmd.bin";
        l.startTest("Load CPU Firmwares & check CPU communication");
        drv.m_mt.rom.pretty_print();

        if (!g_fast_load_cpu_firmware)
        begin
        	  l.msg(0, $sformatf("Loading '%s' to CPU0", cpu0_fw) );
        	  drv.m_mt.load_firmware(0, cpu0_fw);
        	  l.msg(0, $sformatf("Loading '%s' to CPU1", cpu1_fw) );
            drv.m_mt.load_firmware(1, cpu1_fw);
        end
        l.msg(0, "Resetting CPUs");
        drv.m_mt.reset_core(0, 0);
        drv.m_mt.reset_core(1, 0);
        #100us; // give some time for the firmware to boot up
        checkCPUCommunication(drv);
    endtask // loadCPUFirmwares

    CBusAccessor_VME64x acc;

    // Counter that counts the posedge of spi_clk
    always @(posedge vif_spi.spi_clk)
    begin
        if (vif_spi.spi_cs_n)
            posedge_cnt <= 0;
        else
            posedge_cnt <= posedge_cnt + 1;
    end

    /*
    ** Tasks for SPI & RMQ protocol
    */
    logic [7:0]  operation;
    logic [7:0]  data_len;
    logic [7:0]  rmq_id;
    logic [31:0] basic_info;

    task read_from_specific_rmq(input logic [7:0] rmq_id);

        automatic int len = 0;

        vif_spi.spi_cs_n = 'b0;
        operation = 'b1;
        /* READ Request */
        basic_info = {8'h01, rmq_id, 8'h00, 8'h00};
        vif_spi.write_spi(basic_info, 32);

        /* Header size is 1 when read */

        vif_spi.read_spi(32);
        #100ns;
        vif_spi.spi_clk = 1'b1;
        #100ns;
        vif_spi.spi_clk = 1'b0;
        len = miso_data[7:0];
        $display("len = %d", len);

        for (int d = 0; d < len; d++) begin
            vif_spi.read_spi(32);
            assert (miso_spi_data != 32'hDEADCAFE) else $error("Fifo Empty -- %d\n",rmq_id);
        end

        #100ns;
        vif_spi.spi_clk = 1'b1;
        #100ns;
        vif_spi.spi_clk = 1'b0;
        operation = 'b0;
        vif_spi.spi_cs_n = 'b1;
        #1us;

    endtask

    int mosi_cnt = 0; // index to the mosi data queue
    int miso_cnt = 0; // index to the miso data queue

    logic [15:0] shreg_msb, shreg_lsb;
    logic [31:0] shreg = 32'h0;

    // Collects the data that is written in RMQ. From each data, the last 16-bits are valid
    always @(posedge clk_sys)
    begin
        if (DUT.rmq_src_out.valid & !DUT.rmq_src_out.error & DUT.rmq_src_in.ready) begin
            shreg_msb <= DUT.rmq_src_out.data[15:0];
            shreg_lsb <= shreg_msb;
            shreg     <= {shreg_lsb, shreg_msb};
        end
    end

    // Push back in the miso queue the miso data
    always @(posedge vif_spi.spi_clk)
    begin
        if (operation == 1)
            if (posedge_cnt == 0)
                q_miso_data.push_back(miso_data);
    end


    // Push back in the mosi queue the MOSI data
    always @(posedge DUT.cmp_mt_profip_translator.s_wr_data_valid)
    begin
        q_mosi_data.push_back(DUT.cmp_mt_profip_translator.s_data_reg_o);
    end

    // Initialize SPI signals
    initial begin
        vif_spi.spi_clk  = 1'b0;
        vif_spi.spi_cs_n = 1'b1;
        vif_spi.spi_miso = 1'b0;
        vif_spi.spi_mosi = 1'b0;        
    end

    // Main testbench
    initial begin
        automatic MockTurtleDriver drv;
        automatic MasterfipDriver fip;
        automatic Logger l = Logger::get();
        automatic logic[7:0] get_rmq_response = 0;

        acc = new(VME.tb);
        /* map func0 to 0x80000000, A32 */
        acc.write('h7ff63, 'h80, A32|CR_CSR|D08Byte3);
        acc.write('h7ff67, 0, CR_CSR|A32|D08Byte3);
        acc.write('h7ff6b, 0, CR_CSR|A32|D08Byte3);
        acc.write('h7ff6f, 36, CR_CSR|A32|D08Byte3);
        acc.write('h7ff33, 1, CR_CSR|A32|D08Byte3);
        acc.write('h7fffb, 'h10, CR_CSR|A32|D08Byte3); /* enable module (BIT_SET = 0x10) */

        acc.set_default_modifiers(A32 | D32 | SINGLE);
        acc.set_default_xfer_size(A32 | D32 | SINGLE);

        drv = new (acc, `MT_BASE, IrqMonitor);
        fip = new (drv);

        drv.init();
        drv.enable_hmqi_irq ( 0, 0, 1 );
        drv.enable_console_irq ( 0, 1 );
        drv.enable_console_irq ( 1, 1 );
        $display("Before loading the firmware!\n");

        loadCPUFirmwares(fip);
        $display("End of loading firmware\n");

        /* START */
        for (int it=0; it < 20; it++) begin

            case (it)
                0:  assign send_msg = wf_message_pkg::send_rst;
                1:  assign send_msg = wf_message_pkg::send_worldfip_speed;
                2:  assign send_msg = wf_message_pkg::send_hw_config_set;
                3:  assign send_msg = wf_message_pkg::send_msg_1;
                4:  assign send_msg = wf_message_pkg::send_msg_2;
                5:  assign send_msg = wf_message_pkg::send_msg_3;
                6:  assign send_msg = wf_message_pkg::send_msg_4;
                7:  assign send_msg = wf_message_pkg::send_msg_5;
                8:  assign send_msg = wf_message_pkg::send_msg_6;
                9:  assign send_msg = wf_message_pkg::send_msg_7;
                10: assign send_msg = wf_message_pkg::send_msg_8;
                11: assign send_msg = wf_message_pkg::send_msg_9;
                12: assign send_msg = wf_message_pkg::send_msg_10;
                13: assign send_msg = wf_message_pkg::send_msg_11;
                14: assign send_msg = wf_message_pkg::send_msg_12;
                15: assign send_msg = wf_message_pkg::send_msg_13;
                16: assign send_msg = wf_message_pkg::send_msg_14;
                17: assign send_msg = wf_message_pkg::send_msg_15;
                18: assign send_msg = wf_message_pkg::send_msg_16;
                19: assign send_msg = wf_message_pkg::send_msg_17;
                default:
                    begin
                        $error(" **** ERROR with IT: %d **** \n",it);
                    end
            endcase

            $display(" **** Sending message %d **** \n", it+1);

            foreach (send_msg[i])
            begin
                vif_spi.write_spi(send_msg[i], 32);
            end
            #100ns;
            vif_spi.spi_clk = ~vif_spi.spi_clk;

            vif_spi.spi_cs_n = 'b1;
            #1us;

            $displayh("FRAME WRITTEN TO RMQ : %p\n", q_mosi_data);

            wait (rmq_status_o != 'h7F);
            for (int id = 6; id >= 0; id--) begin
                if (rmq_status_o[id] == 1'b0) begin

                    $display("**** Reading the message %d from RMQ %d ****\n", it+1, id);
                    read_from_specific_rmq(id);
                    $display("READ Size %d \n", q_miso_data.size());
                    q_miso_data.pop_front();
                    q_miso_data.pop_front();
                    $displayh("FRAME READ FROM RMQ : %p\n", q_miso_data);

                end
                else begin
                    $display("**** Nothing to be read in RMQ %d ****\n", id);
                end
            end

            q_mosi_data.delete();
            q_miso_data.delete();

        end // FOR LOOP

        $display("************* END FOR LOOP *************\n");
        $display(" Waiting for possible messages in FIFOs \n");

//        while(1) begin
        if (rmq_status_o != 'h7F) begin

//            wait (rmq_status_o != 'h7F);

            for (int id = 6; id < 0; id --) begin
                if (rmq_status_o[id] == 1'b0) begin

                    $display("**** Reading the message from RMQ %d ****\n", id);
                    read_from_specific_rmq(id);
                    $display("READ Size %d \n", q_miso_data.size());
                    q_miso_data.pop_front();
                    q_miso_data.pop_front();
                    $displayh("FRAME READ FROM RMQ : %p\n", q_miso_data);

                end
                else begin
                    $display("**** Nothing to be read in RMQ %d ****\n", id);
                end
            end

            q_mosi_data.delete();
            q_miso_data.delete();

        end
        $finish;

    end

endmodule // main

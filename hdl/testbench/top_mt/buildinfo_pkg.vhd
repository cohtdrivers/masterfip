-- Buildinfo for project main
--
-- This file was automatically generated; do not edit

package buildinfo_pkg is
  constant buildinfo : string :=
       "buildinfo:1" & LF
     & "module:main" & LF
     & "commit:5e1b6eda9a12ddd6e0b0699f7a2d479165be73dc-dirty" & LF
     & "tag:v2.0.0-64-g5e1b6ed-dirty" & LF
     & "syntool:modelsim" & LF
     & "syndate:2023-03-09, 13:38 CET" & LF
     & "synauth:kblantos" & LF;
end buildinfo_pkg;

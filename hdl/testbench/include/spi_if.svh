// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//==============================================================================

//! @file spi_if.svh

//==============================================================================

//------------------------------------------------------------------------------
//! CERN (BE-CEM-EDL)
//
//! @brief SystemVerilog interface for SPI to be used in profip testbench
//
//! @author Konstantinos Blantos
//
//! @date 26/09/2023
//
//------------------------------------------------------------------------------

`ifndef __SPI_IF_SVH
 `define __SPI_IF_SVH

interface spi_if;

    logic spi_clk  = 1'b0;
    logic spi_mosi = 1'b0;
    logic spi_miso = 1'b0;
    logic spi_cs_n = 1'b1;

    logic [31:0] miso_spi_data;

    // Write SPI MOSI data task. Used to generate MOSI data
    task automatic write_spi(input logic [31:0] mosi_spi_data, input int data_length);
        spi_cs_n = 1'b0;
        for (int i=data_length;i>0;i--)
        begin
            #100ns;
            spi_clk = 1'b1;
            spi_mosi = mosi_spi_data[i-1];
            #100ns;
            spi_clk = 1'b0;
        end
    endtask // write_spi

    // Read SPI miso data. Used to generate MISO data
    task read_spi(input int data_length);
        for (int i=data_length;i>0;i--)
        begin
            #100ns;
            spi_clk = 1'b1;
            miso_spi_data <= {miso_spi_data[30:0], spi_miso};
            #100ns;
            spi_clk = 1'b0;
        end
    endtask // read_spi

endinterface

`endif
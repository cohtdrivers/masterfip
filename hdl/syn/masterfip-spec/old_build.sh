#!/usr/bin/make
# ##############################################################################
# Libraries
LIB_DIR?=../../ip_cores

ETHERNET_CORE_TAG=master
GENERAL_CORES_TAG=masterFIP_v1.1.0
GN4124_CORE_TAG=proposed_master
MOCKTURTLE_TAG=v3.1.0
NANOFIP_TAG=master
WR_CORE_TAG=tom-wr-node

ETHERNET_CORE_DIR=${LIB_DIR}/etherbone-core
GENERAL_CORES_DIR=${LIB_DIR}/general-cores
GN4124_CORE_DIR=${LIB_DIR}/gn4124-core
MOCKTURTLE_DIR=${LIB_DIR}/mockturtle
NANOFIP_DIR=${LIB_DIR}/nanofip
WR_CORE_DIR=${LIB_DIR}/wr-cores

GN4124_CORE_COMMIT=e3a0bf97e125020c83bff6e40199a717e7fda738
SETUP_CMD=/bin/bash -c

# ##############################################################################
# CI commands
ifdef GITLAB_CI
	ETHERNET_CORE_URL=https://gitlab-reader:${CI_JOB_TOKEN}@ohwr.org/hdl-core-lib/etherbone-core.git
	GENERAL_CORES_URL=https://gitlab-reader:${CI_JOB_TOKEN}@ohwr.org/hdl-core-lib/general-cores.git
	GN4124_CORE_URL=https://gitlab-reader:${CI_JOB_TOKEN}@ohwr.org/hdl-core-lib/gn4124-core.git
	MOCKTURTLE_URL=https://gitlab-reader:${CI_JOB_TOKEN}@gitlab.cern.ch/coht/mockturtle.git
	NANOFIP_URL=https://gitlab-reader:${CI_JOB_TOKEN}@ohwr.org/cern-fip/nanofip/nanofip-gateware.git
	WR_CORE_URL=https://gitlab-reader:${CI_JOB_TOKEN}@ohwr.org/project/wr-cores.git
	ETHERNET_CORE_CMD='git clone --depth=1 -b ${ETHERNET_CORE_TAG} ${ETHERNET_CORE_URL} ${ETHERNET_CORE_DIR}'
	GENERAL_CORES_CMD='git clone --depth=1 -b ${GENERAL_CORES_TAG} ${GENERAL_CORES_URL} ${GENERAL_CORES_DIR}'
	GN4124_CORE_CMD='git clone -b ${GN4124_CORE_TAG} ${GN4124_CORE_URL} ${GN4124_CORE_DIR}'
	MOCKTURTLE_CMD='git clone --depth=1 -b ${MOCKTURTLE_TAG} ${MOCKTURTLE_URL} ${MOCKTURTLE_DIR}'
	NANOFIP_CMD='git clone --depth=1 -b ${NANOFIP_TAG} ${NANOFIP_URL} ${NANOFIP_DIR}'
	WR_CORE_CMD='git clone --depth=1 -b ${WR_CORE_TAG} ${WR_CORE_URL} ${WR_CORE_DIR}'
else
	ETHERNET_CORE_URL=https://ohwr.org/hdl-core-lib/etherbone-core.git
	GENERAL_CORES_URL=https://ohwr.org/hdl-core-lib/general-cores.git
	GN4124_CORE_URL=https://ohwr.org/hdl-core-lib/gn4124-core.git
	MOCKTURTLE_URL=https://gitlab.cern.ch/coht/mockturtle.git
	NANOFIP_URL=https://ohwr.org/cern-fip/nanofip/nanofip-gateware.git
	WR_CORE_URL=https://ohwr.org/project/wr-cores.git
	ETHERNET_CORE_CMD='git clone -b ${ETHERNET_CORE_TAG} ${ETHERNET_CORE_URL} ${ETHERNET_CORE_DIR}'
	GENERAL_CORES_CMD='git clone -b ${GENERAL_CORES_TAG} ${GENERAL_CORES_URL} ${GENERAL_CORES_DIR}'
	GN4124_CORE_CMD='git clone -b ${GN4124_CORE_TAG} ${GN4124_CORE_URL} ${GN4124_CORE_DIR}'
	MOCKTURTLE_CMD='git clone -b ${MOCKTURTLE_TAG} ${MOCKTURTLE_URL} ${MOCKTURTLE_DIR}'
	NANOFIP_CMD='git clone -b ${NANOFIP_TAG} ${NANOFIP_URL} ${NANOFIP_DIR}'
	WR_CORE_CMD='git clone -b ${WR_CORE_TAG} ${WR_CORE_URL} ${WR_CORE_DIR}'
endif

# ##############################################################################
setup_lib:
	${SETUP_CMD} 'echo "Cloninig submodules in ${LIB_DIR}"'
	${SETUP_CMD} ${ETHERNET_CORE_CMD}
	${SETUP_CMD} ${GENERAL_CORES_CMD}
	${SETUP_CMD} ${GN4124_CORE_CMD}
	${SETUP_CMD} ${MOCKTURTLE_CMD}
	${SETUP_CMD} ${NANOFIP_CMD}
	${SETUP_CMD} ${WR_CORE_CMD}
	${SETUP_CMD} 'echo "Checkout commit ${GN4124_CORE_COMMIT} in ${GN4124_CORE_DIR}"'
	${SETUP_CMD} 'cd ${GN4124_CORE_DIR} && git checkout ${GN4124_CORE_COMMIT}'

build: setup_lib
	${SETUP_CMD} 'echo "Building project"'
	xtclsh build.tcl

check_build:
	${SETUP_CMD} 'echo "Checking build"'
	./check_build.sh 1>&2

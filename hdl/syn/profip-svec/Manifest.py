action      = "synthesis"
target      = "xilinx"
board       = "svec"
syn_device  = "xc6slx150t"
syn_grade   = "-3"
syn_package = "fgg900"
syn_top     = "svec_masterfip_mt_urv"
syn_project = "svec_masterfip_mt_urv.xise"
syn_tool    = "ise"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../rtl"
    fetchto = "../../../dependencies"

# Ideally this should be done by hdlmake itself, to allow downstream Manifests to be able to use t    he
# fetchto variable independent of where those Manifests reside in the filesystem.
import os
fetchto = os.path.abspath(fetchto)

files = ["buildinfo_pkg.vhd"]

modules = {
    "local" : [
        "../common",
        "../../top/profip-svec",
    ],
}

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass

syn_post_project_cmd = "$(TCL_INTERPRETER) syn_extra_steps.tcl $(PROJECT_FILE)"

# GPIO pins are used for debugging('gpio'), otherwise leave it blanc
svec_base_ucf = ['gpio']

ctrls = ["bank4_64b_32b", "bank5_64b_32b"]

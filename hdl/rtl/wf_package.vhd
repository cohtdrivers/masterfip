--_________________________________________________________________________________________________
--                                                                                                |
--                                       |masterFIP core|                                         |
--                                                                                                |
--                                         CERN,BE/CO-HT                                          |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                           wf_package                                           |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         wf_package.vhd                                                                    |
--                                                                                                |
-- Description  Definitions of constants, types, entities, functions related to WorldFIP          |
--              serialization and deserialization; the package is essential for all the modules   |
--              coming from the nanoFIP design.                                                   |
--              As in the masterFIP design the clk is 100 MHz and in the nanoFIP 40 MHz it was    |
--              necessary to have a new wf_package. In principle the nanoFIP design could have    |
--              been modified to accept generics rather than constants, however as it is a stable |
--              design it was decided to keep it as it is and use for synthesis this package      |
--              rather than the one coming with the nanoFIP submodule.                            |
--              A different package, the masterfip_pkg is used in the masterfip design for all    |
--              other topics, not related to the WorldFIP serialization/ deserialization.         |
--                                                                                                |
-- Authors      Evangelia Gousiou     (Evangelia.Gousiou@cern.ch)                                 |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------



--=================================================================================================
--                                      Libraries & Packages
--=================================================================================================

-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all; -- std_logic definitions
use IEEE.NUMERIC_STD.all;    -- conversion functions



--=================================================================================================
--                              Package declaration for wf_package
--=================================================================================================
package wf_package is

---------------------------------------------------------------------------------------------------
--                             Constants regarding the system clock                              --
---------------------------------------------------------------------------------------------------
-- 16ns clock for the masterFIP_core; same for the CPUs

  constant c_QUARTZ_PERIOD_NS    : real    := 16.0;
  constant c_QUARTZ_FREQ_MHZ     : real    := 62.5;
  constant c_QUARTZ_FREQ_MHZ_INT : integer := 62;

  constant c_1SEC_CNT_LGTH  : natural  := 27; -- lgth of counter that counts 1 sec using the sys clk
  constant c_1SEC_CLK_TICKS : unsigned := to_unsigned((1000000000 / integer(c_QUARTZ_PERIOD_NS)),
                                                                                  c_1SEC_CNT_LGTH);


---------------------------------------------------------------------------------------------------
--                       Constants regarding the session timeout counters                        --
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- To add a robust layer of protection to the FSMs of the design (WorldFIP serializer and
-- deserializer), counters that depend only on the system clock have being implemented; when they
-- are filled up, they can bring the FSMs back to the IDLE state.

-- For the wf_rx_deserializer/wf_tx_serializer at the slowest bit rate, 31.25 kbps, the
-- reception/transmission of the longest frame takes: 268 bytes RP_DAT = 68608 us
-- This demands for a 23 bits counter.

-- This means that if after 83 ms the reception/transmission of a frame has not been completed,
-- the respective FSMs will be reset.

  constant c_SESSION_TIMEOUT_C_LGTH : natural := 23; -- 83 ms


---------------------------------------------------------------------------------------------------
--                             Constant regarding the deglitch filter                            --
---------------------------------------------------------------------------------------------------
  constant c_DEGLITCH_THRESHOLD : natural := 100 / integer(c_QUARTZ_PERIOD_NS);


---------------------------------------------------------------------------------------------------
--                            Constants regarding the CRC calculation                            --
---------------------------------------------------------------------------------------------------
  constant c_CRC_POLY_LGTH  : natural                                         := 16;
  constant c_CRC_GENER_POLY : std_logic_vector (c_CRC_POLY_LGTH - 1 downto 0) := "0001110111001111";
  constant c_CRC_VERIF_POLY : std_logic_vector (c_CRC_POLY_LGTH - 1 downto 0) := "0001110001101011";


---------------------------------------------------------------------------------------------------
--                     Constants regarding the the ID_DAT and RP_DAT frame structure             --
---------------------------------------------------------------------------------------------------
  constant c_VP   : std_logic_vector (1 downto 0) := "11";
  constant c_VN   : std_logic_vector (1 downto 0) := "00";
  constant c_ONE  : std_logic_vector (1 downto 0) := "10";
  constant c_ZERO : std_logic_vector (1 downto 0) := "01";

  constant c_PRE  : std_logic_vector (15 downto 0) := c_ONE & c_ZERO & c_ONE & c_ZERO & c_ONE & c_ZERO & c_ONE & c_ZERO;
  constant c_FSD  : std_logic_vector (15 downto 0) := c_ONE & c_VP & c_VN & c_ONE & c_ZERO & c_VN & c_VP & c_ZERO;
  constant c_FES  : std_logic_vector (15 downto 0) := c_ONE & c_VP & c_VN & c_VP & c_VN & c_ONE & c_ZERO & c_ONE;
  constant c_FSS  : std_logic_vector (31 downto 0) := c_PRE & c_FSD;


---------------------------------------------------------------------------------------------------
--                               Constant regarding the Transmitter                              --
---------------------------------------------------------------------------------------------------
  constant c_TX_SCHED_BUFF_LGTH : natural := 4; -- length of the buffer of pulses used for
                                                -- the transmission synchronization

---------------------------------------------------------------------------------------------------
--                 Constants regarding the position of bytes in the frame structure              --
---------------------------------------------------------------------------------------------------
  constant c_CTRL_BYTE_INDEX     : std_logic_vector (7 downto 0) := "00000000"; -- 0
  constant c_PDU_BYTE_INDEX      : std_logic_vector (7 downto 0) := "00000001"; -- 1
  constant c_LGTH_BYTE_INDEX     : std_logic_vector (7 downto 0) := "00000010"; -- 2
  constant c_1st_DATA_BYTE_INDEX : std_logic_vector (7 downto 0) := "00000011"; -- 3
  constant c_2nd_DATA_BYTE_INDEX : std_logic_vector (7 downto 0) := "00000100"; -- 4


---------------------------------------------------------------------------------------------------
--                           Constants & Types regarding the bit rate                            --
---------------------------------------------------------------------------------------------------
-- Calculation of the number of clk ticks equivalent to the reception/ transmission period

  constant c_PERIODS_COUNTER_LGTH : natural := 12; -- in the slowest bit rate (31.25kbps), the
                                                   -- period is 32000 ns and can be measured after
                                                   -- 3200 uclk ticks. Therefore a counter of 12
                                                   -- bits is the max needed for counting
                                                   -- transmission/ reception periods.

  constant c_BIT_RATE_UCLK_TICKS_31_25Kbit: unsigned :=
                          to_unsigned((32000 / integer(c_QUARTZ_PERIOD_NS)),c_PERIODS_COUNTER_LGTH);
  constant c_BIT_RATE_UCLK_TICKS_1_Mbit: unsigned    :=
                          to_unsigned((1000 / integer(c_QUARTZ_PERIOD_NS)),c_PERIODS_COUNTER_LGTH);
  constant c_BIT_RATE_UCLK_TICKS_2_5_Mbit: unsigned  :=
                          to_unsigned((400 /integer(c_QUARTZ_PERIOD_NS)),c_PERIODS_COUNTER_LGTH);
  constant c_BIT_RATE_UCLK_TICKS_5_Mbit: unsigned  :=
                          to_unsigned((200 /integer(c_QUARTZ_PERIOD_NS)),c_PERIODS_COUNTER_LGTH);

  -- Creation of a table with the c_BIT_RATE_UCLK_TICKS info per bit rate
  type t_uclk_ticks is array (Natural range <>) of unsigned (c_PERIODS_COUNTER_LGTH-1 downto 0);

  constant c_BIT_RATE_UCLK_TICKS : t_uclk_ticks (3 downto 0):=
                          (0 => (c_BIT_RATE_UCLK_TICKS_31_25Kbit),
                           1 => (c_BIT_RATE_UCLK_TICKS_1_Mbit),
                           2 => (c_BIT_RATE_UCLK_TICKS_2_5_Mbit),
                           3 => (c_BIT_RATE_UCLK_TICKS_5_Mbit));


---------------------------------------------------------------------------------------------------
--                                      Components Declarations:                                 --
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
  component wf_rx_deserializer
  port (
    uclk_i               : in std_logic;
    nfip_rst_i           : in std_logic;
    rx_rst_i             : in std_logic;
    signif_edge_window_i : in std_logic;
    adjac_bits_window_i  : in std_logic;
    fd_rxd_r_edge_p_i    : in std_logic;
    fd_rxd_f_edge_p_i    : in std_logic;
    fd_rxd_i             : in std_logic;
    sample_manch_bit_p_i : in std_logic;
    sample_bit_p_i       : in std_logic;
  -----------------------------------------------------------------
    byte_ready_p_o       : out std_logic;
    byte_o               : out std_logic_vector (7 downto 0);
    crc_wrong_p_o        : out std_logic;
    fss_crc_fes_ok_p_o   : out std_logic;
    fss_received_p_o     : out std_logic;
    rx_osc_rst_o         : out std_logic);
  -----------------------------------------------------------------
  end component wf_rx_deserializer;




---------------------------------------------------------------------------------------------------
  component wf_tx_serializer
  port (
    uclk_i                  : in std_logic;
    nfip_rst_i              : in std_logic;
    tx_start_p_i            : in std_logic;
    byte_request_accept_p_i : in std_logic;
    last_byte_p_i           : in std_logic;
    byte_i                  : in std_logic_vector (7 downto 0);
    tx_sched_p_buff_i       : in std_logic_vector (c_TX_SCHED_BUFF_LGTH -1 downto 0);
  -----------------------------------------------------------------
    tx_byte_request_p_o     : out std_logic;
    tx_completed_p_o        : out std_logic;
    tx_osc_rst_p_o          : out std_logic;
    tx_data_o               : out std_logic;
    tx_enable_o             : out std_logic);
  -----------------------------------------------------------------
  end component wf_tx_serializer;


---------------------------------------------------------------------------------------------------
  component wf_fd_receiver is
  port (
    uclk_i                : in std_logic;
    rate_i                : in std_logic_vector (1 downto 0);
    fd_rxd_a_i            : in std_logic;
    nfip_rst_i            : in std_logic;
    rx_rst_i              : in std_logic;
  -----------------------------------------------------------------
    rx_byte_o             : out std_logic_vector (7 downto 0);
    rx_byte_ready_p_o     : out std_logic;
    rx_fss_crc_fes_ok_p_o : out std_logic;
    rx_fss_received_p_o   : out std_logic;
    rx_crc_wrong_p_o      : out std_logic );
  -----------------------------------------------------------------
  end component wf_fd_receiver;



---------------------------------------------------------------------------------------------------
  component wf_rx_osc is
  port (
    uclk_i                  : in std_logic;
    rate_i                  : in  std_logic_vector (1 downto 0);
    nfip_rst_i              : in std_logic;
    fd_rxd_edge_p_i         : in std_logic;
    rx_osc_rst_i            : in std_logic;
  -----------------------------------------------------------------
    rx_manch_clk_p_o        : out std_logic;
    rx_bit_clk_p_o          : out std_logic;
    rx_signif_edge_window_o : out std_logic;
    rx_adjac_bits_window_o  : out std_logic );
  -----------------------------------------------------------------
end component wf_rx_osc;


---------------------------------------------------------------------------------------------------
  component wf_fd_transmitter is
  port (
    uclk_i                     : in std_logic;
    rate_i                     : in std_logic_vector (1 downto 0);
    nfip_rst_i                 : in std_logic;
    tx_byte_i                  : in std_logic_vector (7 downto 0);
    tx_byte_request_accept_p_i : in std_logic;
    tx_last_data_byte_p_i      : in std_logic;
    tx_start_p_i               : in std_logic;
  -----------------------------------------------------------------
    tx_byte_request_p_o        : out std_logic;
    tx_completed_p_o           : out std_logic;
    tx_data_o                  : out std_logic;
    tx_enable_o                : out std_logic;
    tx_clk_o                   : out std_logic);
  -----------------------------------------------------------------
  end component wf_fd_transmitter;



---------------------------------------------------------------------------------------------------
  component wf_tx_osc is
  port (
    uclk_i            : in std_logic;
    rate_i            : in  std_logic_vector (1 downto 0);
    nfip_rst_i        : in std_logic;
    tx_osc_rst_p_i    : in std_logic;
  -----------------------------------------------------------------
    tx_clk_o          : out std_logic;
    tx_sched_p_buff_o : out std_logic_vector (c_TX_SCHED_BUFF_LGTH -1 downto 0));
  -----------------------------------------------------------------
  end component wf_tx_osc;



---------------------------------------------------------------------------------------------------
  component  wf_crc
  port (
    uclk_i             : in std_logic;
    nfip_rst_i         : in std_logic;
    start_crc_p_i      : in std_logic;
    data_bit_i         : in std_logic;
    data_bit_ready_p_i : in std_logic;
  -----------------------------------------------------------------
    crc_ok_p_o         : out std_logic;
    crc_o              : out std_logic_vector (c_CRC_POLY_LGTH - 1 downto 0));
  -----------------------------------------------------------------
  end component wf_crc;



---------------------------------------------------------------------------------------------------
  component wf_manch_encoder is
  generic (g_word_lgth :  natural);
  port (
    word_i       : in std_logic_vector (g_word_lgth-1 downto 0);
  -----------------------------------------------------------------
    word_manch_o : out std_logic_vector ((2*g_word_lgth)-1 downto 0));
  -----------------------------------------------------------------
  end component wf_manch_encoder;



---------------------------------------------------------------------------------------------------
  component wf_rx_deglitcher
  port (
    uclk_i                 : in std_logic;
    nfip_rst_i             : in std_logic;
    fd_rxd_a_i             : in std_logic;
  -----------------------------------------------------------------
    fd_rxd_filt_o          : out std_logic;
    fd_rxd_filt_edge_p_o   : out std_logic;
    fd_rxd_filt_f_edge_p_o : out std_logic);
  -----------------------------------------------------------------
  end component wf_rx_deglitcher;


---------------------------------------------------------------------------------------------------
  component wf_bits_to_txd
  port (
    uclk_i              : in std_logic;
    nfip_rst_i          : in std_logic;
    txd_bit_index_i     : in unsigned (4 downto 0);
    data_byte_manch_i   : in std_logic_vector (15 downto 0);
    crc_byte_manch_i    : in std_logic_vector (31 downto 0);
    sending_fss_i       : in std_logic;
    sending_data_i      : in std_logic;
    sending_crc_i       : in std_logic;
    sending_fes_i       : in std_logic;
    stop_transmission_i : in std_logic;
    tx_clk_p_i          : in std_logic;
  -----------------------------------------------------------------
    txd_o               : out std_logic;
    tx_enable_o         : out std_logic);
  -----------------------------------------------------------------
  end component wf_bits_to_txd;


---------------------------------------------------------------------------------------------------
  component wf_decr_counter is
  generic (g_counter_lgth :  natural := 5);
  port (
    uclk_i            : in std_logic;
    counter_rst_i     : in std_logic;
    counter_top_i     : in unsigned (g_counter_lgth-1 downto 0);
    counter_load_i    : in std_logic;
    counter_decr_i    : in std_logic;
  -----------------------------------------------------------------
    counter_o         : out unsigned (g_counter_lgth-1 downto 0);
    counter_is_zero_o : out std_logic);
  -----------------------------------------------------------------
  end component wf_decr_counter;



---------------------------------------------------------------------------------------------------
  component wf_incr_counter is
  generic (g_counter_lgth :  natural := 8);
  port (
    uclk_i            : in std_logic;
    counter_reinit_i  : in std_logic;
    counter_incr_i    : in std_logic;
  -----------------------------------------------------------------
    counter_o         : out unsigned (g_counter_lgth-1 downto 0);
    counter_is_full_o : out std_logic);
  -----------------------------------------------------------------
  end component wf_incr_counter;


---------------------------------------------------------------------------------------------------
  function f_manch_encoder (word_i :std_logic_vector) return std_logic_vector;



end wf_package;

--=================================================================================================
--                                        package body
--=================================================================================================
package body wf_package is

---------------------------------------------------------------------------------------------------
-- Function for the encoding of a word to its Manchester 2 (manch.) equivalent.
-- Each bit "1" is replaced by "10" and each bit "0" by "01".
-- The manch. encoding ensures that there is one transition for each bit.
--   o bit            :    "0"           "1"
--   o manch. encoded :   "0 1"         "1 0"
--   o scheme         :    _|-           -|_

  function f_manch_encoder (word_i : std_logic_vector) return std_logic_vector is

  variable word_manch_o : std_logic_vector ((2*word_i'length) -1 downto 0);

  begin
    for I in word_i'range loop
      word_manch_o (I*2)   := not word_i(I);
      word_manch_o (I*2+1) := word_i(I);
    end loop;
  -----------------------------------------------------------------
    return word_manch_o;
  -----------------------------------------------------------------
  end function;



end wf_package;
--=================================================================================================
--                                         package end
--=================================================================================================
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------

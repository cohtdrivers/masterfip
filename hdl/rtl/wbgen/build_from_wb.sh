#!/bin/bash

wbgen2 -V masterfip_wbgen2_csr.vhd -H record -p masterfip_wbgen2_pkg.vhd -s defines -C masterfip_wbgen2_csr.h -D masterfip_wbgen2_csr.html masterfip_csr.wb

echo ""
echo "Moving wbgen2 generated files to the following locations..."
echo ""

mv -v ./master_wbgen2_csr.vhd ../.
mv -v ./master_wbgen2_pkg.vhd ../.

--_________________________________________________________________________________________________
--                                                                                                |
--                                       |masterFIP core|                                         |
--                                                                                                |
--                                         CERN,BE/CO-HT                                          |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                         masterfip_rx                                           |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         masterfip_rx.vhd                                                                  |
--                                                                                                |
-- Description  The unit groups the main actions that regard FIELDRIVE data reception.            |
--              Figure 1 shows the main units/processes; the units RX DESERIALIZER, RX OSC and    |
--              RX DEGLITCHER come unmodified from the nanoFIP project.                           |
--              Figure 2 shows the WorldFIP frame structure; note that the fmc_masterfip_core     |
--              ignores completely the notion of PDU_TYPE, LGTH, MPS, etc fields inside the       |
--              PAYLOAD part of the frame and is not checking them at reception.                  |
--              It is the processor (MT) that is handling individually the bytes inside the       |
--              PAYLOAD frame field. On the other hand the FSS, CRC and FES fields are checked    |
--              and validated by the masterfip_rx.                                                |
--              As long as the rx_rst_i is not activated the deserializer probes the bus          |
--              looking for a FSS; after the FSS detection, the deserialized bytes are packed     |
--              one-by-one in 32-bit words to be provided to the MT. Upon the detection of a FES  |
--              either the rx_fss_crc_fes_ok_p_o or the rx_crc_wrong_p_o is activated to signal to|
--              MT for the end of a correct or erroneous frame.                                   |
--                                                                                                |
--              RX OSC              for the clock recovery                                        |
--                                                                                                |
--              RX                  for the filtering of the input FD_RXD                         |
--                                                                                                |
--              RX DESERIALIZER     for the bytes retrieval; also detects FSS/FES & checks CRC    |
--                                                                                                |
--              BYTES_C             for the counting of the retrieved bytes                       |
--                                                                                                |
--              CREATE 32bit WORDS  for the formation of 32-bit words to be provided to the       |
--                                  processor (Mock Turtle for example)                           |
--                                                                                                |
--                                           Mock Turtle                                          |
--                     ___________________________________________________________                |
--                    |                                                           |               |
--                    |   _________       _______________________                 |               |
--                    |  |         |     |                       |                |               |
--                    |  | BYTES_C |     |   CREATE 32bit WORDS  |                |               |
--                    |  |_________|     |_______________________|                |               |
--                    |      /\                     /\                 _________  |               |
--                    |   _______________________________________     |         | |               |
--                    |  |                                       |    |         | |               |
--                    |  |             RX DESERIALIZER           |    |  RX OSC | |               |
--                    |  |                                       |  < |         | |               |
--                    |  |_______________________________________|    |         | |               |
--                    |                     /\                        |_________| |               |
--                    |   _______________________________________                 |               |
--                    |  |                                       |                |               |
--                    |  |              RX DEGLITCHER            |                |               |
--                    |  |_______________________________________|                |               |
--                    |                                                           |               |
--                    |___________________________________________________________|               |
--                                                 /\                                             |
--              ___________________________________________________________________               |
--              0_____________________________FIELDBUS______________________________0             |
--                                                                                                |
--           ___________ ______  ________________________________________  ___________ _______    |
--          |____FSS____|_CTRL_||_____________..DATA/PAYLOAD..___________||____CRC____|__FES__|   |
--                                                                                                |
--                                  Figure 2: WorldFIP Frame structure                            |
--                                                                                                |
-- Authors      Evangelia Gousiou     (Evangelia.Gousiou@cern.ch)                                 |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------



--=================================================================================================
--                                       Libraries & Packages
--=================================================================================================

-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all; -- std_logic definitions
use IEEE.NUMERIC_STD.all;    -- conversion functions
use IEEE.math_real.all;
-- Specific library
library work;
use work.masterFIP_pkg.all;  -- definitions of types, constants, entities
use work.wf_package.all;

--=================================================================================================
--                              Entity declaration for masterfip_rx
--=================================================================================================
entity masterfip_rx is port(
  -- INPUTS
    clk_i                 : in std_logic;   -- only one clk domain

    rst_i                 : in std_logic;   -- core rst, synched with clk_i

    rx_rst_i              : in std_logic;   -- dedicated rx reset during transmission OR
                                            -- reset pulse from the processor when for eg. a
                                            -- frame is rejected during reception
                                            -- (ex: RP_DAT > 133 bytes, wrong CTRL byte)

    speed_i               : in std_logic_vector(1 downto 0); -- WorldFIP bit rate

    rx_d_a_i              : in std_logic;   -- FielDrive receiver data


  -- OUTPUTS
    rx_byte_ready_p_o     : out std_logic;  -- pulse indicating a new retrieved byte

    rx_byte_o             : out std_logic_vector(C_BYTE_WIDTH-1 downto 0);
                                            -- currently retrieved byte

    rx_byte_index_o       : out std_logic_vector(C_FRAME_BYTES_CNT_LGTH-1 downto 0);
                                            -- index of currently retrieved byte
                                            -- counting starts after FSS; it includes
                                            -- the CTRL, DATA, CRC and FES fields;
                                            -- counting starts from 0 and normally
                                            -- the value should not exceed 265

    rx_word_index_o       : out std_logic_vector(C_FRAME_WORDS_CNT_LGTH-1 downto 0);
                                            -- index of currently retrieved word
                                            -- counting starts from 0 and normally
                                            -- the value should not exceed 66

    rx_ctrl_byte_o        : out std_logic_vector(C_BYTE_WIDTH-1 downto 0); -- frame CTRL byte

    rx_ctrl_byte_ok_o     : out std_logic;  -- active after the reception of the CTRL byte (first
                                            -- byte after FSS) and until a rst_i OR rst_rx_i

    rx_frame_o            : out rx_frame_t; -- frame DATA bytes
                                            -- structure with 67 words of 32-bit each = 268 bytes
                                            -- able to house a frame of max length.
                                            -- Upon the rx_fss_crc_fes_ok_p_o the processor needs
                                            -- to read the rx_ctrl_byte and rx_byte_index_o - 4
                                            -- bytes from the rx_frame_o. The content of rx_frame_o
                                            -- changes upon the end of reception of a new frame
                                            -- (i.e. a new rx_fss_crc_fes_ok_p_o).

    rx_fss_crc_fes_ok_p_o : out std_logic;  -- indication of a frame with correct FSS, FES & CRC;
                                            -- pulse upon FES detection

    rx_crc_wrong_p_o      : out std_logic;  -- indication of a frame with wrong CRC; pulse upon FES

    rx_fss_received_p_o   : out std_logic;  -- pulse upon FSS detection (ID/ RP_DAT)

    rx_bytes_num_err_o    : out std_logic); -- active after the reception of > C_MFP_MAX_FRAME_BYTES bytes
                                            -- and until a rst_i OR rst_rx_i

end entity masterfip_rx;


--=================================================================================================
--                                    architecture declaration
--=================================================================================================
architecture struc of masterfip_rx is

  -- wf_rx_osc
  signal rx_osc_rst, adjac_bits_window   : std_logic;
  signal signif_edge_window              : std_logic;
  signal sample_bit_p                    : std_logic;
  signal sample_manch_bit_p              : std_logic;
  -- wf_rx_deglitcher
  signal fd_rxd_filt, rxd_filt_edge_p    : std_logic;
  signal fd_rxd_filt_f_edge_p            : std_logic;
  signal fd_rxd_filt_r_edge_p            : std_logic;
  -- wf_rx_deserializer
  signal rx_byte_ready_p                 : std_logic;
  signal rx_fss_crc_fes_ok_p             : std_logic;
  signal rx_byte                         : std_logic_vector  (C_BYTE_WIDTH-1 downto 0);
  -- retrieved bytes into 32-bit regs
  signal byte0, byte1, byte2, byte3      : std_logic_vector(C_BYTE_WIDTH-1 downto 0);
  signal zero                            : std_logic_vector(C_BYTE_WIDTH-1 downto 0) := (others => '0');
  signal word32_num                      : integer range 0 to C_MAX_FRAME_WORDS-1;
  -- bytes counter
  signal rx_byte_index, rx_byte_index_d1 : std_logic_vector(C_FRAME_BYTES_CNT_LGTH-1 downto 0);
  signal bytes_c_rst                     : std_logic;


--=================================================================================================
--                                       architecture begin
--=================================================================================================
begin

---------------------------------------------------------------------------------------------------
--                            Deserializer as in the nanoFIP project                             --
---------------------------------------------------------------------------------------------------

  cmp_rx_deglitcher: entity work.wf_rx_deglitcher
  port map(
    uclk_i                 => clk_i,
    nfip_rst_i             => rx_rst_i,
    fd_rxd_a_i             => rx_d_a_i,
  -----------------------------------------------------------------
    fd_rxd_filt_o          => fd_rxd_filt,
    fd_rxd_filt_edge_p_o   => rxd_filt_edge_p,
    fd_rxd_filt_f_edge_p_o => fd_rxd_filt_f_edge_p);
  -----------------------------------------------------------------
    fd_rxd_filt_r_edge_p <= rxd_filt_edge_p and (not fd_rxd_filt_f_edge_p);

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  cmp_rx_deserializer: entity work.wf_rx_deserializer
  port map(
    uclk_i                  => clk_i,
    nfip_rst_i              => rst_i,
    rx_rst_i                => rx_rst_i,
    fd_rxd_f_edge_p_i       => fd_rxd_filt_f_edge_p,
    fd_rxd_r_edge_p_i       => fd_rxd_filt_r_edge_p,
    fd_rxd_i                => fd_rxd_filt,
    sample_bit_p_i          => sample_bit_p,
    sample_manch_bit_p_i    => sample_manch_bit_p,
    signif_edge_window_i    => signif_edge_window,
    adjac_bits_window_i     => adjac_bits_window,
  -----------------------------------------------------------------
    byte_o                  => rx_byte,
    byte_ready_p_o          => rx_byte_ready_p,
    fss_crc_fes_ok_p_o      => rx_fss_crc_fes_ok_p,
    rx_osc_rst_o            => rx_osc_rst,
    fss_received_p_o        => rx_fss_received_p_o,
    crc_wrong_p_o           => rx_crc_wrong_p_o);
  -----------------------------------------------------------------
  rx_byte_ready_p_o         <= rx_byte_ready_p;
  rx_byte_o                 <= rx_byte;
  rx_fss_crc_fes_ok_p_o     <= rx_fss_crc_fes_ok_p;

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  cmp_rx_osc: entity work.wf_rx_osc
  port map(
    uclk_i                  => clk_i,
    rate_i                  => speed_i,
    nfip_rst_i              => rst_i,
    fd_rxd_edge_p_i         => rxd_filt_edge_p,
    rx_osc_rst_i            => rx_osc_rst,
  -----------------------------------------------------------------
    rx_manch_clk_p_o        => sample_manch_bit_p,
    rx_bit_clk_p_o          => sample_bit_p,
    rx_signif_edge_window_o => signif_edge_window,
    rx_adjac_bits_window_o  => adjac_bits_window);
  -----------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--                                         bytes counter                                         --
---------------------------------------------------------------------------------------------------
  cmp_rx_bytes_cnt: entity work.incr_counter
  generic map(g_counter_lgth => C_FRAME_BYTES_CNT_LGTH)
  port map(
    clk_i             => clk_i,
    counter_reinit_i  => bytes_c_rst,
    counter_incr_i    => rx_byte_ready_p,
    counter_is_full_o => open,
    -------------------------------------------------------
    counter_o         => rx_byte_index);
    -------------------------------------------------------
  bytes_c_rst         <= rst_i or rx_rst_i;
  rx_byte_index_o     <= rx_byte_index;


---------------------------------------------------------------------------------------------------
--                                rx bytes exceeded C_MFP_MAX_FRAME_BYTES                            --
---------------------------------------------------------------------------------------------------
  -- indication that the rx counter exceeded the max expected number of bytes
  p_rx_bytes_num_err: process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_i = '1' or rx_rst_i = '1' then
        rx_bytes_num_err_o   <= '0';
      else
        if unsigned(rx_byte_index) > C_MFP_MAX_FRAME_BYTES then 
          rx_bytes_num_err_o   <= '1';
		  end if;
      end if;
    end if;
   end process;


---------------------------------------------------------------------------------------------------
--                      combination of four retrieved bytes to a 32-bit word                     --
---------------------------------------------------------------------------------------------------
  -- note: the values of the CTRL byte and all payload regs are kept till a rx_rst_i or a rst_i
  p_create_32bit_words: process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_i = '1' or rx_rst_i = '1' then
        byte0                 <= (others => '0');
        byte1                 <= (others => '0');
        byte2                 <= (others => '0');
        byte3                 <= (others => '0');
        rx_ctrl_byte_o        <= (others => '0');
        rx_ctrl_byte_ok_o     <= '0';
      else

        if rx_byte_ready_p = '1' then
          if unsigned(rx_byte_index) = resize(unsigned(c_CTRL_BYTE_INDEX),C_FRAME_BYTES_CNT_LGTH) then
            rx_ctrl_byte_o    <= rx_byte; -- CTRL byte stored in separate word from the rest of the frame
            rx_ctrl_byte_ok_o <= '1';     -- value kept till a rst
          else
            byte0             <= rx_byte;
            byte1             <= byte0;
            byte2             <= byte1;
            byte3             <= byte2;
          end if;
        end if;
      end if;
    end if;
   end process;


---------------------------------------------------------------------------------------------------
--                        Storage of consumed bytes to the cons_frame regs                       --
---------------------------------------------------------------------------------------------------
-- transfer 32bit words to the cons_frame registers

  p_delay: process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_i = '1' or rx_rst_i = '1' then
        rx_byte_index_d1   <= (others =>'0'); -- needed for synching
      else
        if rx_byte_ready_p = '1' then
          rx_byte_index_d1 <= rx_byte_index;
        end if;
      end if;
    end if;
  end process;

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  p_data_transfer_to_regs: process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_i = '1' or rx_rst_i = '1' then
        word32_num                 <= 0;
        rx_frame_o                 <= (others =>(others =>'0'));

      else
        -- upon rx_fss_crc_fes_ok_p, the last 32bit word will contain for sure CRC0 and CRC1
        -- and it may also contain one or two bytes of data.
        -- the last word will always include as last bytes, byte1 and byte0: the two CRC bytes.
        -- it could also include one or two useful data bytes
        if rx_fss_crc_fes_ok_p = '1' then
          if word32_num = 0 then                -- only in the case of RP_FIN, where there are not enough
                                                -- bytes to create a word; needed for keeping the MT sw generic
            word32_num             <= word32_num + 1;
            rx_frame_o(word32_num) <= byte0 & byte1 & byte2 & byte3;

          elsif (unsigned(rx_byte_index)-2) mod 4 = 3 then -- [CRC|CRC|BYTE|BYTE]
            word32_num             <= word32_num + 1;
            rx_frame_o(word32_num) <= byte0 & byte1 & byte2 & byte3; -- byte 3 and byte 2 are useful

          elsif (unsigned(rx_byte_index)-2) mod 4 = 2 then -- [0|CRC|CRC|BYTE]
            word32_num             <= word32_num + 1;
            rx_frame_o(word32_num) <= zero & byte0 & byte1 & byte2; -- one useful data byte: byte2 last byte
                                                 
          end if;                                          -- Note: for [CRC|BYTE|BYTE|BYTE], upon
                                                           -- rx_fss_crc_fes_ok_p a new word has been created

        -- for the rest of the bytes, i.e. everything before the rx_fss_crc_fes_ok_p, a new word is
        -- created after the reception of 4 bytes.
        -- note that rx_byte_index is checked to be within the limits [1..262], as:
        -- rx_byte_index = 0 refers to the CTRL byte that is written in a separate word, and
        -- when rx_byte_index = 262: the one-but-last word (rx_frame_o(66)) of a max-length-frame
        -- is written; the last word (rx_frame_o(67)) of a max-length-frame will be written
        -- upon rx_fss_crc_fes_ok_p.
        elsif (rx_byte_ready_p = '1' and unsigned(rx_byte_index_d1) > 0 and unsigned(rx_byte_index_d1) < 263  and unsigned(rx_byte_index_d1) mod 4 = 0) then
          word32_num             <= word32_num + 1;
          rx_frame_o(word32_num) <= byte0 & byte1 & byte2 & byte3;
        end if;
      end if;
    end if;
  end process;

--  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  rx_word_index_o <= std_logic_vector(to_unsigned(word32_num,C_FRAME_WORDS_CNT_LGTH));


end architecture struc;
--=================================================================================================
--                                        architecture end
--=================================================================================================
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------

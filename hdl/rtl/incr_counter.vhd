--_________________________________________________________________________________________________
--                                                                                                |
--                                       |masterFIP core|                                         |
--                                                                                                |
--                                         CERN,BE/CO-HT                                          |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                         incr_counter                                           |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         incr_counter.vhd                                                                  |
-- Description  Increasing counter with synchronous reinitialise and increase enable              |
-- Authors      Evangelia Gousiou     (Evangelia.Gousiou@cern.ch)                                 |
--                                                                                                |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------



--=================================================================================================
--                                      Libraries & Packages
--=================================================================================================

-- Standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all; -- std_logic definitions
use IEEE.NUMERIC_STD.all;    -- conversion functions

--=================================================================================================
--                           Entity declaration for incr_counter
--=================================================================================================

entity incr_counter is
  generic(g_counter_lgth : natural := 32); -- default length
  port(
  -- INPUTS
   clk_i             : in std_logic;       -- 40 MHz clock
   counter_incr_i    : in std_logic;       -- increment enable
   counter_reinit_i  : in std_logic;       -- reinitializes counter to 0

  -- OUTPUT
   counter_o         : out std_logic_vector (g_counter_lgth-1 downto 0); -- counter
   counter_is_full_o : out std_logic);     -- counter full indication, when all bits are '1'
end entity incr_counter;


--=================================================================================================
--                                    architecture declaration
--=================================================================================================
architecture rtl of incr_counter is

constant c_COUNTER_FULL : unsigned (g_counter_lgth-1 downto 0) := (others => '1');
signal   s_counter      : unsigned (g_counter_lgth-1 downto 0);


--=================================================================================================
--                                       architecture begin
--=================================================================================================
begin


---------------------------------------------------------------------------------------------------
  -- Synchronous process Incr_Counter
  Incr_Counter: process (clk_i)
  begin
    if rising_edge (clk_i) then
      if counter_reinit_i = '1' then
        s_counter   <= (others => '0');

      elsif counter_incr_i = '1' then
        s_counter   <= s_counter + 1;

      end if;
    end if;
  end process;

 --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  counter_o         <= std_logic_vector(s_counter);
  counter_is_full_o <= '1' when s_counter = c_COUNTER_FULL else '0';


end architecture rtl;
--=================================================================================================
--                                        architecture end
--=================================================================================================
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------
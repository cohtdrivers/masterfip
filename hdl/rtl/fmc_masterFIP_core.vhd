--_________________________________________________________________________________________________
--                                                                                                |
--                                       |masterFIP core|                                         |
--                                                                                                |
--                                         CERN,BE/CO-HT                                          |
--________________________________________________________________________________________________|

---------------------------------------------------------------------------------------------------
--                                                                                                |
--                                      fmc_masterFIP_core                                        |
--                                                                                                |
---------------------------------------------------------------------------------------------------
-- File         fmc_masterFIP_core.vhd                                                            |
--                                                                                                |
-- Description  The masterFIP_core instantiates all the modules needed to establish WorldFIP      |
--              communication. Figure 1 shows the modules of the core.                            |
--              There is only one clock domain in the core.                                       |
--                                                                                                |
--         _      ________________________________________________________________                |
--        |F|    |                                             fmc_masterFIP_core |               |
--        |I|    |   _________________    ____________    ____________    ______  |               |
--        |E|    |  |                 |  | MACROCYCLE |  |  SILENCE   |  |      | |               |
--        |L| <--|  |       TX        |  | TIME CNT   |  |  TIME CNT  |  |      | |               |
--        |D|    |   _________________|  |____________|  |____________|  |      | |               |
--        |R|    |   _________________    ____________    ____________   |      | |               |
--        |I| -->|  |                 |  | TURNAROUND |  |            |  |      | |               |
--        |V|    |  |       RX        |  |  TIME CNT  |  |   RESETS   |  |      | |               |
--        |E|    |  |_________________|  |____________|  |____________|  |WBGEN2| | <-processor-> |
--        |_|    |                                                       | CSR  | |               |
--               |   _________________                                   |      | |               |
-- sync pulse -->|  |  EXT SYNC PULSE |                                  |      | |               |
--               |  |_________________|                                  |      | |               |
--               |   ___________                                         |      | |               |
--   DS18B20 <-->|  |  ONEWIRE  |                                        |      | |               |
--               |  |___________|                                        |      | |               |
--               |   ______                                              |      | |               |
--     LEDs   <--|  | LEDs |                                             |      | |               |
--               |  |______|                                             |______| |               |
--               |________________________________________________________________|               |
--                             Figure 1: fmc_masterFIP_core architecture                          |
--                                                                                                |
--              MASTERFIP WBGEN2 CSR:                                                             |
--              The masterfip_wbgen2_csr module has been generated through the wbgen2 application.|
--              It establishes the interface with the processor, usually a Mock Turtle core.      |
--              This interface contains a set of control and status registers for each one of the |
--              units of Figure 1; it also contains the WorldFIP frame PAYLOAD data for the TX    |
--              and RX. Regarding the PAYLOAD data, it was decided not to use a FIFO for passing  |
--              the WorldFIP PAYLOAD data from the processor to this core for serialization or for|
--              passing the WorldFIP PAYLOAD data that have been deserialized from this core to   |
--              the processor. Instead a set of 67 32-bit-registers (= 268 bytes, which is the max|
--              WorldFIP frame size) is used for each of the TX and RX; like this the time for    |
--              which the data need to remain static to be read is minimized, leading to a simpler|
--              design.                                                                           |
--                                                                                                |
--              MASTERFIP TX:                                                                     |
--              The masterfip_tx places a complete WorldFIP frame on the WorldFIP bus.            |
--              The masterfip_tx ignores the frame type (ID_DAT/RT_DAT/RP_MSG etc..), or the      |
--              macrocycle sequence and macrocycle timing; the processor (MT) is responsible for  |
--              managing all these aspects and for providing to the masterfip_tx the bytes to     |
--              serialise, along with a start pulse.                                              |
--              The communication between the processor and the masterfip_tx is handled through a |
--              set of control (from the MT) and status (from the masterfip_tx) signals/registers |
--              defined in the masterfip_wbgen2_csr module.                                       |
--              Upon a rising edge on the tx_ctrl_start pulse, the masterfip_tx:                  |
--               - copies all the payload registers (tx_payld_ctrl, tx_payld_reg1..tx_payld_reg67)|
--                 and the register that indicates the number of payload bytes to                 |
--                 serialize (tx_ctrl_bytes_num)                                                  |
--               - starts serializing a WorldFIP frame (see following Figure). Note that the FSS, |
--                 CRC and FES fields are generated internally in the masterfip_tx unit.          |
--               - after the FES, rises the tx_stat_stop status bit to signal to the MT the end   |
--                 of a successful frame transmission.                                            |
--               ______________________________________________________________________________   |
--              |_____FSS_____|__Ctrl__|_____________tx_payld_____________|_____CRC____|__FES__|  |
--                                                                                                |
--              <---2 bytes--><-1byte-><------  tx_ctrl_bytes_num  -------><--2 bytes--><-1byte-> |
--                                                                                                |
--                                  Figure 2: WorldFIP tx frame structure                         |
--                                                                                                |
--              MASTERFIP RX:                                                                     |
--              The masterfip_rx retrieves a WorldFIP frame from the WorldFIP bus.                |
--              Similar to the masterfip_tx, the masterfip_rx has no intelligence regarding the   |
--              macrocycle sequence; it is controlled and monitored by the processor (MT) through |
--              the masterfip_wbgen2_csr, where a set of control and status registers are defined.|
--              As long as it is not under reset, the masterfip_rx is probing the WorldFIP bus    |
--              trying to identify the FSS sequence of a WorldFIP frame. It signals the FSS       |
--              detection to the processor through the status bit rx_stat_pream_ok and continues  |
--              deserializing the rest of the frame. It stores the first byte after the FSS to the|
--              rx_payld_ctrl register and the rest of the bytes to the registers                 |
--              rx_payld_reg1..rx_payld_reg67. Upon the detection of a FES the masterfip_rx       |
--              checks the CRC of the frame and enables the status bit rx_stat_frame_ok or        |
--              rx_stat_crc_err accordingly. Upon the rx_stat_frame_ok, the status register       |
--              rx_stat_bytes_num indicates the number of bytes in the frame (this indicates      |
--              the number of rx_payld_regs and the number of bytes inside the last rx_payld_reg  |
--              to be retrieved by the processor).                                                |
--              The processor should copy the rx_payld_regs upon a rx_stat_frame_ok; the regs     |
--              keep their values until a rx_rst or until the detection of a new rx_stat_frame_ok.|
--              This time, in the worst case (bit rate 2.5 Mbps) can be calculated as:            |
--              (Min Turnaround time of a node = 4 us) + (RP_FIN duration = 19.2 us) = 23.2 us    |
--               ______________________________________________________________________________   |
--              |_____FSS_____|__Ctrl__|_____________rx_payld_____________|_____CRC____|__FES__|  |
--                                                                                                |
--              <---2 bytes--><-1byte-><------  rx_ctrl_bytes_num  -------><--2 bytes--><-1byte-> |
--                                                                                                |
--                                    Figure 3: WorldFIP rx frame structure                       |
--                                                                                                |
--              EXT SYNC PULSE:                                                                   |
--              The modules related to the ext_sync_pulse, synchronise, deglitch and count the    |
--              number of rising-edge pulses that arrive to the ext_sync input of the board;      |
--              they provide the result to a dedicated masterfip_wbgen2_csr register.             |
--                                                                                                |
--              MACROCYCLE:                                                                       |
--              The modules related to the macrocycle, count the time of a macrocycle using the   |
--              10 ns input clock as well as the number of macrocycles since startup/a reset.     |
--              Dedicated registers in the masterfip_wbgen2_csr provide the counters values       |
--              to the processor (MT). Note that the duration/length of the macrocycle comes      |
--              from the processor through another dedicated register in the masterfip_wbgen2_csr |
--              that should be set once in the application startup.                               |
--                                                                                                |
--              TURNAROUND, SILENCE TIMES:                                                        |
--              The modules related to the turnaround and silence time, count the respective time |
--              using the 10 ns clock. Dedicated regs in the masterfip_wbgen2_csr provide the     |
--              counters values to the processor (MT). As in the case of the macrocycle length,   |
--              the turnaround and silence time length is provided through other dedicated regs in|
--              the masterfip_wbgen2_csr that should be set once in the application startup.      |
--                                                                                                |
--              ONEWIRE:                                                                          |
--              The DS18B20 module is for the 1-wire reading of the unique ID and temperature on  |
--              the mezzanine. Differently than in other designs that implement sw-bit-banging,   |
--              here the communication is hard-coded in vhdl, so as to simplify the drivers.      |
--                                                                                                |
-- Authors      Evangelia Gousiou (Evangelia.Gousiou@cern.ch)                                     |
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                      SOLDERPAD LICENSE                                         |
--                                   Copyright CERN 2014-2018                                     |
--                              ------------------------------------                              |
-- Copyright and related rights are licensed under the Solderpad Hardware License, Version 2.0    |
-- (the "License"); you may not use this file except in compliance with the License.              |
-- You may obtain a copy of the License at http://solderpad.org/licenses/SHL-2.0.                 |
-- Unless required by applicable law or agreed to in writing, software, hardware and materials    |
-- distributed under this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR       |
-- CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language   |
-- governing permissions and limitations under the License.                                       |
---------------------------------------------------------------------------------------------------

--=================================================================================================
--                                       Libraries & Packages
--=================================================================================================
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.masterfip_wbgen2_pkg.all;
use work.masterfip_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.genram_pkg.all;
use work.wf_package.all;

--=================================================================================================
--                                Entity declaration for fmc_masterFIP_core
--=================================================================================================
entity fmc_masterFIP_core is
  generic
    (g_span             : integer := 32;     -- address span in bus interfaces
     g_width            : integer := 32;     -- data width in bus interfaces
     g_simulation       : boolean := FALSE); -- set to TRUE when instantiated in a test-bench
  port
    (-- Clock and reset
     clk_i              : in  std_logic;     -- only one clk domain
     rst_n_i            : in  std_logic;     -- PCIe reset, synched with the clk_i

     -- Bus Speed                            -- 31.25 Kbps: speed_b1 = 0, speed_b0 = 0
     speed_b0_i         : in  std_logic;     -- 1 Mbps    : speed_b1 = 0, speed_b0 = 1
     speed_b1_i         : in  std_logic;     -- 2.5 Mbps  : speed_b1 = 1, speed_b0 = 0
                                             -- 5 Mbps    : speed_b1 = 1, speed_b0 = 1
     -- One wire DS18B20U+ on the mezzanine
     onewire_b          : inout std_logic;   -- temper and unique id

     -- External synchronisation pulse transceiver
     ext_sync_term_en_o : out std_logic;     -- enables the 50 Ohms termination of the pulse
     ext_sync_dir_o     : out std_logic;     -- transceiver direction
     ext_sync_oe_n_o    : out std_logic;     -- transceiver output enable negative
     ext_sync_a_i       : in  std_logic;     -- sync pulse

     -- FielDrive interface
     fd_rstn_o          : out std_logic;     -- reset
     fd_rxcdn_a_i       : in  std_logic;     -- rx carrier detect
     fd_rxd_a_i         : in  std_logic;     -- rx data
     fd_wdgn_a_i        : in  std_logic;     -- tx watchdog
     fd_txer_a_i        : in  std_logic;     -- tx error
     fd_txck_o          : out std_logic;     -- tx clk
     fd_txd_o           : out std_logic;     -- tx data
     fd_txena_o         : out std_logic;     -- tx enable

     -- WISHBONE classic bus interface with the processor (MT)
     wb_adr_i           : in  std_logic_vector(g_span-1 downto 0);
     wb_dat_i           : in  std_logic_vector(g_width-1 downto 0);
     wb_stb_i           : in  std_logic;
     wb_sel_i           : in  std_logic_vector(3 downto 0);
     wb_we_i            : in  std_logic;
     wb_cyc_i           : in  std_logic;
     wb_stall_o         : out std_logic;
     wb_ack_o           : out std_logic;
     wb_dat_o           : out std_logic_vector(g_width-1 downto 0);

     -- LEDs and debugging signals to pass to the higher levels
     leds_o             : out std_logic_vector(g_width-1 downto 0));
  end fmc_masterFIP_core;


--=================================================================================================
--                                    architecture declaration
--=================================================================================================
architecture rtl of fmc_masterFIP_core is

  -- wbgen2 regs from/to mock turtle
  signal reg_to_mt                            : t_masterfip_in_registers;
  signal reg_from_mt                          : t_masterfip_out_registers;
  -- resets
  signal core_rst, core_rst_n          : std_logic;
  signal fd_host_rst                          : std_logic;
  signal extend                               : std_logic_vector(c_PERIODS_COUNTER_LGTH-1 downto 0);
  -- config
  signal speed                                : std_logic_vector(1 downto 0);
  -- ext pulse
  signal ext_sync, ext_sync_p    : std_logic;
  signal ext_sync_p_cnt_rst                   : std_logic;
  signal ext_sync_filt       : std_logic;
  -- counters
  signal macrocyc_load_p, turnar_load_p       : std_logic;
  signal num_of_macrocyc_cnt_full             : std_logic;
  signal num_of_macrocyc_cnt_reinit           : std_logic;
  signal macrocyc_cnt                         : std_logic_vector(g_width-2 downto 0);
  signal macrocyc_cnt_zero_p, silen_load_p    : std_logic;
  -- tx
  signal tx_completed_p         : std_logic;
  signal tx_rst, fd_txd, fd_txck              : std_logic;
  signal tx_frame                             : tx_frame_t;
  -- rx
  signal rx_rst                               : std_logic;
  signal rx_fss_received_p   : std_logic;
  signal rx_frame_ok_p           : std_logic;
  signal rx_byte_ready_p        : std_logic;
  signal rx_crc_wrong_p, fd_txena             : std_logic;
  signal rx_frame                             : rx_frame_t;
  signal rx_byte                              : std_logic_vector(C_BYTE_WIDTH-1 downto 0);
  signal rx_byte_index                        : std_logic_vector(C_FRAME_BYTES_CNT_LGTH-1 downto 0);
  -- fd_wdgn, fd_txer, fd_rxcdn
  signal fd_wdgn, fd_wdg, fd_wdg_p, fd_rxcd   : std_logic;
  signal fd_rxcdn, fd_txer_cnt_reinit         : std_logic;
  signal fd_txer, fd_txer_filt, fd_txer_p     : std_logic;
  -- one wire
  signal tmp_temper                           : std_logic_vector(15 downto 0);
  signal tmp_id                               : std_logic_vector(63 downto 0);
  signal onewire_read_p, pps_is_zero          : std_logic;
  signal pps_load_p                           : std_logic;

  -- chipscope
   -- component chipscope_ila
     -- port (
       -- CONTROL : inout std_logic_vector(35 downto 0);
       -- CLK     : in    std_logic;
       -- TRIG0   : in    std_logic_vector(31 downto 0);
       -- TRIG1   : in    std_logic_vector(31 downto 0);
       -- TRIG2   : in    std_logic_vector(31 downto 0);
       -- TRIG3   : in    std_logic_vector(31 downto 0));
   -- end component;
   -- component chipscope_icon
     -- port (CONTROL0 : inout std_logic_vector(35 downto 0));
   -- end component;
   -- signal CONTROL : std_logic_vector(35 downto 0);
   -- signal CLK     : std_logic;
   -- signal TRIG0   : std_logic_vector(31 downto 0);
   -- signal TRIG1   : std_logic_vector(31 downto 0);
   -- signal TRIG2   : std_logic_vector(31 downto 0);
   -- signal TRIG3   : std_logic_vector(31 downto 0);


--=================================================================================================
--                                       architecture begin
--=================================================================================================
begin


---------------------------------------------------------------------------------------------------
--                                             speed                                             --
---------------------------------------------------------------------------------------------------
  speed              <= speed_b1_i & speed_b0_i;
  reg_to_mt.speed_i  <= speed;


---------------------------------------------------------------------------------------------------
--                                             RESETS                                            --
---------------------------------------------------------------------------------------------------
  core_rst   <= reg_from_mt.rst_core_o or (not rst_n_i);   -- reset from MT OR PCIe reset
  core_rst_n <= not core_rst;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  -- FIELDRIVE rst: generation of a pulse 1 x WorldFIP-clk-cycles long
  cmp_fd_rst_generate: entity work.gc_dyn_extend_pulse
  generic map(g_len_width => c_PERIODS_COUNTER_LGTH)
  port map
    (clk_i      => clk_i,
     rst_n_i    => core_rst_n,
     pulse_i    => reg_from_mt.rst_fd_o, -- monostable: 1-clk-tick-long pulse
     len_i      => extend,
     extended_o => fd_host_rst);

  extend    <= std_logic_vector(c_BIT_RATE_UCLK_TICKS(to_integer(unsigned(speed))));
  fd_rstn_o <= not fd_host_rst;


---------------------------------------------------------------------------------------------------
--                                WBGEN2 REGS FROM/TO MOCK TURTLE                                --
---------------------------------------------------------------------------------------------------
  cmp_masterfip_csr: entity work.masterfip_wbgen2_csr
  port map
    (rst_n_i    => rst_n_i,
     clk_sys_i  => clk_i,
     wb_adr_i   => wb_adr_i(9 downto 2),
     wb_dat_i   => wb_dat_i,
     wb_dat_o   => wb_dat_o,
     wb_cyc_i   => wb_cyc_i,
     wb_sel_i   => wb_sel_i,
     wb_stb_i   => wb_stb_i,
     wb_we_i    => wb_we_i,
     wb_ack_o   => wb_ack_o,
     wb_stall_o => wb_stall_o,
     regs_i     => reg_to_mt,
     regs_o     => reg_from_mt);


---------------------------------------------------------------------------------------------------
--                                         EXT SYNC PULSE                                        --
---------------------------------------------------------------------------------------------------
  ext_sync_dir_o     <= reg_from_mt.ext_sync_ctrl_dir_o;
  ext_sync_oe_n_o    <= reg_from_mt.ext_sync_ctrl_oe_n_o;
  ext_sync_term_en_o <= reg_from_mt.ext_sync_ctrl_term_en_o;

  -- input synchronizer of the ext_sync_a_i signal
  cmp_ext_sync_sync: entity work.gc_sync_ffs
  port map
    (clk_i     => clk_i,
     rst_n_i   => core_rst_n,
     data_i    => ext_sync_a_i,
     synced_o  => ext_sync);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- deglitch filter
  cmp_ext_sync_deglitch: entity work.gc_glitch_filt
  generic map(g_len => c_DEGLITCH_THRESHOLD)
                  -- glitches up to c_DEGLITCH_THRESHOLD x c_QUARTZ_PERIOD_NS = 100 ns are ignored;
  port map        -- Note that the filter adds a 100 ns delay to the ext_sync signal
    (clk_i   => clk_i,
     rst_n_i => core_rst_n,
     dat_i   => ext_sync,
     dat_o   => ext_sync_filt);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- rising edge detection on the deglitched signal
  cmp_ext_sync_deglitch_p_detect: entity work.gc_sync_ffs
  port map
    (clk_i    => clk_i,
     rst_n_i  => core_rst_n,
     data_i   => ext_sync_filt,
     ppulse_o => ext_sync_p);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- counter of the number of rising edges
  cmp_ext_sync_p_cnt : entity work.incr_counter
  generic map(g_counter_lgth => 32) -- for the fastest macrocycle of 20ms, the counter
                                    -- can keep counting for 2.7 years
  port map
    (clk_i            => clk_i,
     counter_incr_i   => ext_sync_p,
     counter_reinit_i => ext_sync_p_cnt_rst,
     counter_o        => reg_to_mt.ext_sync_p_cnt_i);
  --  --  --  --  --  --  --  --  --  --  --
  ext_sync_p_cnt_rst <= reg_from_mt.ext_sync_ctrl_p_cnt_rst_o or core_rst;


---------------------------------------------------------------------------------------------------
--                                      MACROCYCLE COUNTER                                       --
---------------------------------------------------------------------------------------------------
-- Regarding synchronisation, the masterFIP application can work in three modes:
-- 1) using an internal counter that counts each macrocycle based on the SPEC local oscillator;
-- this mode is active based on the state of the transceiver signal ext_sync_ctrl_oe_n_o.
-- 2) using the ext_sync pulse to signal the beginning of each macrocycle
-- 3) using the ext_sync pulse to signal the beginning of each macrocycle together with an input
-- from the processor indicating that all the periodic traffic has been completed and the processor
-- is ready to start a new macrocycle.  

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- counter counting the macrocycle time;
  -- the macrocycle length (counter top) should be set once upon the application startup
  cmp_macrocycle_time_cnt : entity work.decr_counter
  generic map(width   => g_width-1)
  port map
    (clk_i             => clk_i,
     rst_i             => core_rst,
     counter_load_i    => macrocyc_load_p,
     counter_top_i     => reg_from_mt.macrocyc_lgth_o,
     counter_o         => macrocyc_cnt,
     counter_is_zero_o => macrocyc_cnt_zero_p);
  --  --  --  --  --  --  --  --  --  --  --
  reg_to_mt.macrocyc_time_cnt_i <= macrocyc_cnt;

  macrocyc_load_p <= macrocyc_cnt_zero_p or reg_from_mt.macrocyc_start_o when reg_from_mt.ext_sync_ctrl_oe_n_o = '1'           -- internal counting
                     else ext_sync_p     when reg_from_mt.ext_sync_ctrl_oe_n_o = '0' and reg_from_mt.ext_sync_ctrl_opt_o = '0' -- pure external control
                     else ext_sync_p and reg_from_mt.ext_sync_ctrl_safe_wind_o;                                                -- macrocycle restart, based on macrocycle execution
  -- note: macrocyc_start_o is a monostable, 1-clk-tick-long pulse

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- counter counting the number of macrocycles;
  -- being a 32-bit counter, for the fastest application of 20 ms macrocycle, the counter can
  -- keep counting for 2.7 years; when it fills up it would restart from 0.
  cmp_macrocycles_cnt: entity work.incr_counter
  generic map(g_counter_lgth => g_width)
  port map
    (clk_i             => clk_i,
     counter_incr_i    => macrocyc_cnt_zero_p,
     counter_reinit_i  => num_of_macrocyc_cnt_reinit,
     counter_is_full_o => num_of_macrocyc_cnt_full,
     counter_o         => reg_to_mt.macrocyc_num_cnt_i);
  --  --  --  --  --  --  --  --  --  --  --
  num_of_macrocyc_cnt_reinit <= core_rst or num_of_macrocyc_cnt_full;


---------------------------------------------------------------------------------------------------
--                                       TURNAROUND COUNTER                                      --
---------------------------------------------------------------------------------------------------
-- counter counting the turnaround time i.e. the time after the end of transmission of a frame
-- (tx_completed_p), or after the end of reception of a frame (rx_frame_ok_p/ rx_crc_wrong_p)
-- and before the transmission of a new frame.

  -- turnaround counter
  cmp_turnaround_cnt: entity work.decr_counter
  generic map(width   => 31)
  port map
    (clk_i             => clk_i,
     rst_i             => core_rst,
     counter_load_i    => turnar_load_p,
     counter_top_i     => reg_from_mt.turnar_lgth_o,
     counter_o         => reg_to_mt.turnar_time_cnt_i,
     counter_is_zero_o => open); -- too fast to be used by MT
  --  --  --  --  --  --  --  --  --  --  --
  turnar_load_p <= tx_completed_p or rx_frame_ok_p or rx_crc_wrong_p or reg_from_mt.turnar_start_o;
  -- note: turnar_start_o is a monostable, 1-clk-tick-long pulse


---------------------------------------------------------------------------------------------------
--                                        SILENCE COUNTER                                        --
---------------------------------------------------------------------------------------------------
-- counter counting the silence time i.e. the maximum amount of time that the masterFIP waits for
-- a response frame; the counting starts after the transmission of a frame sent by the master
-- (tx_completed_p) or after the reception of a frame (rx_frame_ok_p/ rx_crc_wrong_p) for the
-- case of a RP_DAT_MSG that would be followed by RP_FIN.

  cmp_silence_cnt: entity work.decr_counter
  generic map(width   => 31)
  port map
    (clk_i             => clk_i,
     rst_i             => core_rst,
     counter_load_i    => silen_load_p,
     counter_top_i     => reg_from_mt.silen_lgth_o,
     counter_o         => reg_to_mt.silen_time_cnt_i,
     counter_is_zero_o => open); -- too fast to be used by MT
  --  --  --  --  --  --  --  --  --  --  --
  silen_load_p <= tx_completed_p or rx_frame_ok_p or rx_crc_wrong_p or reg_from_mt.silen_start_o;
  -- note: turnar_start_o is a monostable, 1-clk-tick-long pulse


---------------------------------------------------------------------------------------------------
--                                          MASTERFIP RX                                         --
---------------------------------------------------------------------------------------------------
-- Note that the deglitching of the fd_rxd_a_i takes place inside the masterfip_rx unit.
  cmp_masterfip_rx: entity work.masterfip_rx
  port map
    (clk_i                 => clk_i,
     rst_i                 => core_rst,
     rx_rst_i              => rx_rst,          -- reset from the MT or reset while transmitting
     speed_i               => speed,
     rx_d_a_i              => fd_rxd_a_i,
     rx_byte_index_o       => rx_byte_index,   -- current byte index
     rx_word_index_o       => reg_to_mt.rx_stat_curr_word_indx_i(C_FRAME_WORDS_CNT_LGTH-1 downto 0),
     rx_ctrl_byte_o        => reg_to_mt.rx_payld_ctrl_i,
     rx_ctrl_byte_ok_o     => reg_to_mt.rx_stat_ctrl_byte_ok_i,
     rx_frame_o            => rx_frame,
     rx_fss_crc_fes_ok_p_o => rx_frame_ok_p,
     rx_fss_received_p_o   => rx_fss_received_p,
     rx_crc_wrong_p_o      => rx_crc_wrong_p,
     rx_bytes_num_err_o    => reg_to_mt.rx_stat_bytes_num_err_i,
     rx_byte_o             => rx_byte,         -- for debugging
     rx_byte_ready_p_o     => rx_byte_ready_p);-- for debugging
 

---------------------------------------------------------------------------------------------------
--                                       Signals for the RX                                      --
---------------------------------------------------------------------------------------------------
-- The receiver RX is disabled when a frame is being transmitted (fd_txena active).
-- Note that the reg_from_mt.rx_ctrl_rst_o is a monostable, 1-clk-tick-long pulse
  rx_rst <= reg_from_mt.rx_ctrl_rst_o or fd_txena;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- registering the number of received bytes upon rx_frame_ok_p; the final number does not include
  -- FSS, CTRL, CRC and FES bytes.
  p_rx_bytes_num : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (core_rst = '1' or rx_rst = '1') then
        reg_to_mt.rx_stat_bytes_num_i  <= (others => '0');
        reg_to_mt.rx_stat_frame_ok_i   <= '0';
      else
        if rx_frame_ok_p = '1' then
          reg_to_mt.rx_stat_frame_ok_i   <= rx_frame_ok_p;
          reg_to_mt.rx_stat_bytes_num_i(C_FRAME_BYTES_CNT_LGTH-1 downto 0)  <= rx_byte_index - 3;
          -- data payload, without FSS, CTRL, CRC, FES
        end if;
      end if;
    end if;
  end process;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- extending the rx_fss_received_p until a core_rst or rx_rst is received
  p_rx_fss_received_extend : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if(core_rst = '1' or rx_rst = '1') then
        reg_to_mt.rx_stat_pream_ok_i   <= '0';
      else
        if rx_fss_received_p = '1' then
          reg_to_mt.rx_stat_pream_ok_i <= '1';
        end if;
      end if;
    end if;
  end process;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- extending the rx_crc_wrong_p  until a core_rst or rx_rst is received
  p_rx_crc_wrong_extend : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if(core_rst = '1' or rx_rst = '1') then
        reg_to_mt.rx_stat_frame_crc_err_i   <= '0';
      else
        if rx_crc_wrong_p = '1' then
          reg_to_mt.rx_stat_frame_crc_err_i <= '1';
        end if;
      end if;
    end if;
  end process;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- counter of frames with crc errors since the startup or a core reset (not on every macrocycle)
  cmp_rx_crc_err_cnt: entity work.incr_counter
  generic map(g_counter_lgth => 32)
  port map
    (clk_i            => clk_i,
     counter_incr_i   => rx_crc_wrong_p,
     counter_reinit_i => core_rst,
     counter_o        => reg_to_mt.rx_stat_crc_err_cnt_i);


---------------------------------------------------------------------------------------------------
--                                          MASTERFIP TX                                         --
---------------------------------------------------------------------------------------------------

  cmp_masterfip_tx: entity work.masterfip_tx
  port map
    (clk_i           => clk_i,
     rst_i           => tx_rst,
     speed_i         => speed,
     tx_bytes_num_i  => reg_from_mt.tx_ctrl_bytes_num_o(C_FRAME_BYTES_CNT_LGTH-1 downto 0),
                                                       -- num of bytes to serialize; sampled upon tx_start_p
     tx_start_p_i    => reg_from_mt.tx_ctrl_start_o,   -- monostable, 1-clk-tick-long pulse
     tx_frame_i      => tx_frame,
     tx_ctrl_byte_i  => reg_from_mt.tx_payld_ctrl_o,
     tx_byte_index_o => reg_to_mt.tx_stat_curr_byte_indx_i(C_FRAME_BYTES_CNT_LGTH-1 downto 0),
                                                       -- indx of current byte being serialized,
                                                       -- counting starts from 0 (indx 0 is
                                                       -- the Control byte) up to 262 bytes
     tx_end_p_o      => tx_completed_p,
     tx_d_o          => fd_txd,
     tx_ena_o        => fd_txena,
     tx_clk_o        => fd_txck);

  tx_rst     <= core_rst or reg_from_mt.tx_ctrl_rst_o; -- reg_from_mt.tx_ctrl_rst_o is a monostable
  fd_txena_o <= fd_txena;
  fd_txd_o   <= fd_txd;
  fd_txck_o  <= fd_txck;
  reg_to_mt.tx_stat_ena_i <= fd_txena;

---------------------------------------------------------------------------------------------------
--                                      Signals for the TX                                       --
---------------------------------------------------------------------------------------------------
-- extending the tx_completed_p until a tx_rst or
-- a new request for  serialization (reg_from_mt.tx_ctrl_start_o) is received

  p_tx_completed_extend : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if(tx_rst = '1' or reg_from_mt.tx_ctrl_start_o = '1') then
        reg_to_mt.tx_stat_stop_i   <= '0';
      else
        if tx_completed_p = '1' then
          reg_to_mt.tx_stat_stop_i <= '1'; -- stays active until a tx_rst or tx_start_p
        end if;
      end if;
    end if;
  end process;


---------------------------------------------------------------------------------------------------
--                                    FielDrive TXER, WDGN, CDN                                  --
---------------------------------------------------------------------------------------------------
-- The following modules provide to the MT information about the status signals coming from the
-- FielDrive: WDGN, CDN, TXER.

---------------------------------------------------------------------------------------------------
-- WDGN: is activated when the FielDrive detects activity > 1024 bytes long;
-- it is kept active until FielDrive's reinitialization with a fd_rstn_o.
-- Note that it is the logic running on the MT that is responsible for activating a fd_rst_o upon
-- the activation of a fd_wdgn.
-- The following processes provide to the MT the filtered fd_wdgn and the macrocycle number when it
-- was activated.

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- input synchronizer of the fd_wdgn_a_i signal
  cmp_fd_wdgn_sync: entity work.gc_sync_ffs
  port map
    (clk_i     => clk_i,
     rst_n_i   => core_rst_n,
     data_i    => fd_wdgn_a_i,
     synced_o  => fd_wdgn);

     fd_wdg    <= not fd_wdgn;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- deglitch filter
  cmp_fd_wdg_deglitch: entity work.gc_glitch_filt
  generic map(g_len => c_DEGLITCH_THRESHOLD)
                  -- glitches up to c_DEGLITCH_THRESHOLD x c_QUARTZ_PERIOD_NS = 100 ns are ignored;
                  -- Note that the filter adds a 100 ns delay to the ext_sync signal
  port map
    (clk_i   => clk_i,
     rst_n_i => core_rst_n,
     dat_i   => fd_wdg,
     dat_o   => reg_to_mt.fd_wdg_i);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- edge detection
  cmp_fd_wdgn_deglitch_p_detect: entity work.gc_sync_ffs
  port map
    (clk_i     => clk_i,
     rst_n_i   => core_rst_n,
     data_i    => reg_to_mt.fd_wdg_i,
     ppulse_o  => fd_wdg_p);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- process that registers the moment within the macrocycle (macrocycle_cnt) when the
  -- fd_wdgn_p appeared
  p_fd_wdgn_capture : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if(core_rst = '1' or fd_host_rst = '1') then -- resets upon core reset or FielDrive reset
        reg_to_mt.fd_wdg_tstamp_i   <= (others => '0');
      else
        if fd_wdg_p = '1' then
          reg_to_mt.fd_wdg_tstamp_i <= '0' & macrocyc_cnt;
        end if;
      end if;
    end if;
  end process;


---------------------------------------------------------------------------------------------------
-- CDN: synch and filtering of the incoming fd_rxcdn_a_i signal.
-- On the processor (MT) side there should be the verification that before a tx_ctrl_start the CDN
-- is inactive.

  -- input synchronizer of the fd_rxcdn_a_i signal
  cmp_fd_rxcdn_sync: entity work.gc_sync_ffs
  port map
    (clk_i    => clk_i,
     rst_n_i  => core_rst_n,
     data_i   => fd_rxcdn_a_i,
     synced_o => fd_rxcdn);

    fd_rxcd   <= not fd_rxcdn;

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --
  -- deglitch filter
  cmp_fd_rxcd_deglitch: entity work.gc_glitch_filt
  generic map(g_len => c_DEGLITCH_THRESHOLD)
                  -- glitches up to c_DEGLITCH_THRESHOLD x c_QUARTZ_PERIOD_NS = 100 ns are ignored;
                  -- Note that the filter adds a 100 ns delay to the ext_sync signal
  port map
    (clk_i   => clk_i,
     rst_n_i => core_rst_n,
     dat_i   => fd_rxcd,
     dat_o   => reg_to_mt.fd_cd_i);


---------------------------------------------------------------------------------------------------
-- TXER: is activated upon a bus overload/ underload detected by the FielDrive driver outputs;
-- like for example when the WorldFIP cable has been disconnected.
-- It is also activated when during transmission there has been no Manchester-edge detected after
-- the duration of 4 bits.
-- Note that the signal does not need a FielDrive reset to go back to inactive.

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- input synchronizer of the fd_txer_a_i signal
  cmp_fd_txer_sync: entity work.gc_sync_ffs
  port map
    (clk_i     => clk_i,
     rst_n_i   => core_rst_n,
     data_i    => fd_txer_a_i,
     synced_o  => fd_txer);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- deglitch filter
  cmp_fd_txer_deglitch: entity work.gc_glitch_filt
  generic map(g_len => c_DEGLITCH_THRESHOLD)
                  -- glitches up to c_DEGLITCH_THRESHOLD x c_QUARTZ_PERIOD_NS = 100 ns are ignored;
                  -- Note that the filter adds a 100 ns delay to the ext_sync signal
  port map
    (clk_i   => clk_i,
     rst_n_i => core_rst_n,
     dat_i   => fd_txer,
     dat_o   => fd_txer_filt);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- edge detection on the fd_txer_filt signal
  cmp_fd_txer_deglitch_p_detect: entity work.gc_sync_ffs
  port map
    (clk_i    => clk_i,
     rst_n_i  => core_rst_n,
     data_i   => fd_txer_filt,
     ppulse_o => fd_txer_p);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- counter counting the number of fd_txer_p
  cmp_fd_txer_cnt : entity work.incr_counter
  generic map(g_counter_lgth => 32)
  port map
    (clk_i             => clk_i,
     counter_incr_i    => fd_txer_p,
     counter_reinit_i  => fd_txer_cnt_reinit,
     counter_is_full_o => open,
     counter_o         => reg_to_mt.fd_txer_cnt_i);
  --  --  --  --  --  --  --  --  --  --  --
  fd_txer_cnt_reinit <= '1' when core_rst_n = '0' or macrocyc_load_p = '1' or fd_host_rst = '1'
                   else '0';

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- process that registers the moment within the !current! macrocycle (macrocycle_cnt value) when 
  -- the last fd_txer_p appeared
  p_fd_txer_capture : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if(core_rst = '1' or fd_host_rst = '1' or macrocyc_load_p = '1') then
        reg_to_mt.fd_txer_tstamp_i   <= (others => '0');
      else
        if fd_txer_p = '1' then
          reg_to_mt.fd_txer_tstamp_i <= '0' & macrocyc_cnt;
        end if;
      end if;
    end if;
  end process;


---------------------------------------------------------------------------------------------------
--                                       DS18B20U ONE WIRE                                       --
---------------------------------------------------------------------------------------------------
-- Communication with the 1-wire DS18B20U+ for the unique ID and temperature reading;
-- different than in other designs that implement sw-bit-banging, here the communication is
-- implemented in vhdl, so as to simplify the drivers.
-- Note that a temperature reading is provided every second, with the first one a couple of sec
-- after the board power-up/ reset.

  cmp_onewire: entity work.gc_ds182x_interface
  generic map (freq => c_QUARTZ_FREQ_MHZ_INT)
  port map
    (clk_i     => clk_i,
     rst_n_i   => core_rst_n,
     onewire_b => onewire_b,
     id_o      => tmp_id,
     temper_o  => tmp_temper,
     id_read_o => onewire_read_p,
     pps_p_i   => pps_is_zero);

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- pps generator based on the 100 MHz clk
  cmp_pps_gen: entity work.wf_decr_counter
  generic map(g_counter_lgth => c_1SEC_CNT_LGTH)
  port map
    (uclk_i            => clk_i,
     counter_rst_i     => core_rst,
     counter_decr_i    => '1',
     counter_load_i    => pps_load_p,
     counter_top_i     => c_1SEC_CLK_TICKS,
     counter_is_zero_o => pps_is_zero);
  --  --  --  --  --  --  --  --  --  --  --
  pps_load_p <= pps_is_zero; -- looping

  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  --  -
  -- registering of the read values upon the activation of the id_read_o
  p_reg_reading: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if core_rst = '1' then
        reg_to_mt.ds1820_temper_i   <= (others => '0');
        reg_to_mt.ds1820_id_lsb_i   <= (others => '0');
        reg_to_mt.ds1820_id_msb_i   <= (others => '0');
      else
        if(onewire_read_p = '1') then
          reg_to_mt.ds1820_temper_i <= tmp_temper;
          reg_to_mt.ds1820_id_lsb_i <= tmp_id (31 downto 0);
          reg_to_mt.ds1820_id_msb_i <= tmp_id (63 downto 32);
        end if;
      end if;
    end if;
  end process;


---------------------------------------------------------------------------------------------------
--                                          LEDs & AUX                                           --
---------------------------------------------------------------------------------------------------
-- active low LEDs
  leds_o(0) <= not reg_from_mt.led_rx_act_o;
  leds_o(1) <= not reg_from_mt.led_rx_err_o;
  leds_o(2) <= not reg_from_mt.led_tx_act_o;
  leds_o(3) <= not reg_from_mt.led_tx_err_o;
  leds_o(4) <= not reg_from_mt.led_ext_sync_act_o when reg_from_mt.ext_sync_ctrl_oe_n_o = '0' else '0';
  leds_o(5) <= not reg_from_mt.led_ext_sync_err_o when reg_from_mt.ext_sync_ctrl_oe_n_o = '0' else '0';
  leds_o(7 downto 6)  <= "00"; -- not used
  leds_o(31 downto 8) <= reg_from_mt.led_dbg_o;


---------------------------------------------------------------------------------------------------
--                                           Assignments                                         --
---------------------------------------------------------------------------------------------------
-- To simplify the interface between the MT and the fmc_masterfip_core, the serialized/deserialized
-- payload bytes are stored in registers, not in FIFO.
-- The fmc_masterfip_core is copying locally the tx registers upon a tx_stat_start.
-- The processor should copy all the rx data upon a rx_stat_frame_ok; the data stays stable in the
-- rx_frame regs until a rx_rst or until the detection of another rx_stat_frame_ok.
-- This time, in the worst case (bit rate 2.5 Mbps) can be calculated as:
-- (Min Turnaround time of a node = 4 us) + (RP_FIN duration = 19.2 us) = 23.2 us

  -- tx regs
  tx_frame(0)  <= reg_from_mt.tx_payld_reg1_o;
  tx_frame(1)  <= reg_from_mt.tx_payld_reg2_o;
  tx_frame(2)  <= reg_from_mt.tx_payld_reg3_o;
  tx_frame(3)  <= reg_from_mt.tx_payld_reg4_o;
  tx_frame(4)  <= reg_from_mt.tx_payld_reg5_o;
  tx_frame(5)  <= reg_from_mt.tx_payld_reg6_o;
  tx_frame(6)  <= reg_from_mt.tx_payld_reg7_o;
  tx_frame(7)  <= reg_from_mt.tx_payld_reg8_o;
  tx_frame(8)  <= reg_from_mt.tx_payld_reg9_o;
  tx_frame(9)  <= reg_from_mt.tx_payld_reg10_o;
  tx_frame(10) <= reg_from_mt.tx_payld_reg11_o;
  tx_frame(11) <= reg_from_mt.tx_payld_reg12_o;
  tx_frame(12) <= reg_from_mt.tx_payld_reg13_o;
  tx_frame(13) <= reg_from_mt.tx_payld_reg14_o;
  tx_frame(14) <= reg_from_mt.tx_payld_reg15_o;
  tx_frame(15) <= reg_from_mt.tx_payld_reg16_o;
  tx_frame(16) <= reg_from_mt.tx_payld_reg17_o;
  tx_frame(17) <= reg_from_mt.tx_payld_reg18_o;
  tx_frame(18) <= reg_from_mt.tx_payld_reg19_o;
  tx_frame(19) <= reg_from_mt.tx_payld_reg20_o;
  tx_frame(20) <= reg_from_mt.tx_payld_reg21_o;
  tx_frame(21) <= reg_from_mt.tx_payld_reg22_o;
  tx_frame(22) <= reg_from_mt.tx_payld_reg23_o;
  tx_frame(23) <= reg_from_mt.tx_payld_reg24_o;
  tx_frame(24) <= reg_from_mt.tx_payld_reg25_o;
  tx_frame(25) <= reg_from_mt.tx_payld_reg26_o;
  tx_frame(26) <= reg_from_mt.tx_payld_reg27_o;
  tx_frame(27) <= reg_from_mt.tx_payld_reg28_o;
  tx_frame(28) <= reg_from_mt.tx_payld_reg29_o;
  tx_frame(29) <= reg_from_mt.tx_payld_reg30_o;
  tx_frame(30) <= reg_from_mt.tx_payld_reg31_o;
  tx_frame(31) <= reg_from_mt.tx_payld_reg32_o;
  tx_frame(32) <= reg_from_mt.tx_payld_reg33_o;
  tx_frame(33) <= reg_from_mt.tx_payld_reg34_o;
  tx_frame(34) <= reg_from_mt.tx_payld_reg35_o;
  tx_frame(35) <= reg_from_mt.tx_payld_reg36_o;
  tx_frame(36) <= reg_from_mt.tx_payld_reg37_o;
  tx_frame(37) <= reg_from_mt.tx_payld_reg38_o;
  tx_frame(38) <= reg_from_mt.tx_payld_reg39_o;
  tx_frame(39) <= reg_from_mt.tx_payld_reg40_o;
  tx_frame(40) <= reg_from_mt.tx_payld_reg41_o;
  tx_frame(41) <= reg_from_mt.tx_payld_reg42_o;
  tx_frame(42) <= reg_from_mt.tx_payld_reg43_o;
  tx_frame(43) <= reg_from_mt.tx_payld_reg44_o;
  tx_frame(44) <= reg_from_mt.tx_payld_reg45_o;
  tx_frame(45) <= reg_from_mt.tx_payld_reg46_o;
  tx_frame(46) <= reg_from_mt.tx_payld_reg47_o;
  tx_frame(47) <= reg_from_mt.tx_payld_reg48_o;
  tx_frame(48) <= reg_from_mt.tx_payld_reg49_o;
  tx_frame(49) <= reg_from_mt.tx_payld_reg50_o;
  tx_frame(50) <= reg_from_mt.tx_payld_reg51_o;
  tx_frame(51) <= reg_from_mt.tx_payld_reg52_o;
  tx_frame(52) <= reg_from_mt.tx_payld_reg53_o;
  tx_frame(53) <= reg_from_mt.tx_payld_reg54_o;
  tx_frame(54) <= reg_from_mt.tx_payld_reg55_o;
  tx_frame(55) <= reg_from_mt.tx_payld_reg56_o;
  tx_frame(56) <= reg_from_mt.tx_payld_reg57_o;
  tx_frame(57) <= reg_from_mt.tx_payld_reg58_o;
  tx_frame(58) <= reg_from_mt.tx_payld_reg59_o;
  tx_frame(59) <= reg_from_mt.tx_payld_reg60_o;
  tx_frame(60) <= reg_from_mt.tx_payld_reg61_o;
  tx_frame(61) <= reg_from_mt.tx_payld_reg62_o;
  tx_frame(62) <= reg_from_mt.tx_payld_reg63_o;
  tx_frame(63) <= reg_from_mt.tx_payld_reg64_o;
  tx_frame(64) <= reg_from_mt.tx_payld_reg65_o;
  tx_frame(65) <= reg_from_mt.tx_payld_reg66_o;
  tx_frame(66) <= reg_from_mt.tx_payld_reg67_o;

  -- rx regs
  reg_to_mt.rx_payld_reg1_i  <= rx_frame(0);
  reg_to_mt.rx_payld_reg2_i  <= rx_frame(1);
  reg_to_mt.rx_payld_reg3_i  <= rx_frame(2);
  reg_to_mt.rx_payld_reg4_i  <= rx_frame(3);
  reg_to_mt.rx_payld_reg5_i  <= rx_frame(4);
  reg_to_mt.rx_payld_reg6_i  <= rx_frame(5);
  reg_to_mt.rx_payld_reg7_i  <= rx_frame(6);
  reg_to_mt.rx_payld_reg8_i  <= rx_frame(7);
  reg_to_mt.rx_payld_reg9_i  <= rx_frame(8);
  reg_to_mt.rx_payld_reg10_i <= rx_frame(9);
  reg_to_mt.rx_payld_reg11_i <= rx_frame(10);
  reg_to_mt.rx_payld_reg12_i <= rx_frame(11);
  reg_to_mt.rx_payld_reg13_i <= rx_frame(12);
  reg_to_mt.rx_payld_reg14_i <= rx_frame(13);
  reg_to_mt.rx_payld_reg15_i <= rx_frame(14);
  reg_to_mt.rx_payld_reg16_i <= rx_frame(15);
  reg_to_mt.rx_payld_reg17_i <= rx_frame(16);
  reg_to_mt.rx_payld_reg18_i <= rx_frame(17);
  reg_to_mt.rx_payld_reg19_i <= rx_frame(18);
  reg_to_mt.rx_payld_reg20_i <= rx_frame(19);
  reg_to_mt.rx_payld_reg21_i <= rx_frame(20);
  reg_to_mt.rx_payld_reg22_i <= rx_frame(21);
  reg_to_mt.rx_payld_reg23_i <= rx_frame(22);
  reg_to_mt.rx_payld_reg24_i <= rx_frame(23);
  reg_to_mt.rx_payld_reg25_i <= rx_frame(24);
  reg_to_mt.rx_payld_reg26_i <= rx_frame(25);
  reg_to_mt.rx_payld_reg27_i <= rx_frame(26);
  reg_to_mt.rx_payld_reg28_i <= rx_frame(27);
  reg_to_mt.rx_payld_reg29_i <= rx_frame(28);
  reg_to_mt.rx_payld_reg30_i <= rx_frame(29);
  reg_to_mt.rx_payld_reg31_i <= rx_frame(30);
  reg_to_mt.rx_payld_reg32_i <= rx_frame(31);
  reg_to_mt.rx_payld_reg33_i <= rx_frame(32);
  reg_to_mt.rx_payld_reg34_i <= rx_frame(33);
  reg_to_mt.rx_payld_reg35_i <= rx_frame(34);
  reg_to_mt.rx_payld_reg36_i <= rx_frame(35);
  reg_to_mt.rx_payld_reg37_i <= rx_frame(36);
  reg_to_mt.rx_payld_reg38_i <= rx_frame(37);
  reg_to_mt.rx_payld_reg39_i <= rx_frame(38);
  reg_to_mt.rx_payld_reg40_i <= rx_frame(39);
  reg_to_mt.rx_payld_reg41_i <= rx_frame(40);
  reg_to_mt.rx_payld_reg42_i <= rx_frame(41);
  reg_to_mt.rx_payld_reg43_i <= rx_frame(42);
  reg_to_mt.rx_payld_reg44_i <= rx_frame(43);
  reg_to_mt.rx_payld_reg45_i <= rx_frame(44);
  reg_to_mt.rx_payld_reg46_i <= rx_frame(45);
  reg_to_mt.rx_payld_reg47_i <= rx_frame(46);
  reg_to_mt.rx_payld_reg48_i <= rx_frame(47);
  reg_to_mt.rx_payld_reg49_i <= rx_frame(48);
  reg_to_mt.rx_payld_reg50_i <= rx_frame(49);
  reg_to_mt.rx_payld_reg51_i <= rx_frame(50);
  reg_to_mt.rx_payld_reg52_i <= rx_frame(51);
  reg_to_mt.rx_payld_reg53_i <= rx_frame(52);
  reg_to_mt.rx_payld_reg54_i <= rx_frame(53);
  reg_to_mt.rx_payld_reg55_i <= rx_frame(54);
  reg_to_mt.rx_payld_reg56_i <= rx_frame(55);
  reg_to_mt.rx_payld_reg57_i <= rx_frame(56);
  reg_to_mt.rx_payld_reg58_i <= rx_frame(57);
  reg_to_mt.rx_payld_reg59_i <= rx_frame(58);
  reg_to_mt.rx_payld_reg60_i <= rx_frame(59);
  reg_to_mt.rx_payld_reg61_i <= rx_frame(60);
  reg_to_mt.rx_payld_reg62_i <= rx_frame(61);
  reg_to_mt.rx_payld_reg63_i <= rx_frame(62);
  reg_to_mt.rx_payld_reg64_i <= rx_frame(63);
  reg_to_mt.rx_payld_reg65_i <= rx_frame(64);
  reg_to_mt.rx_payld_reg66_i <= rx_frame(65);
  reg_to_mt.rx_payld_reg67_i <= rx_frame(66);


---------------------------------------------------------------------------------------------------
--                                           CHIPSCOPE                                           --
---------------------------------------------------------------------------------------------------
  -- chipscope_ila_1 : chipscope_ila
  -- port map
    -- (CONTROL => CONTROL;
    -- CLK     => clk_i;
    -- TRIG0   => TRIG0;
    -- TRIG1   => TRIG1;
    -- TRIG2   => TRIG2;
    -- TRIG3   => TRIG3);

  -- chipscope_icon_1 : chipscope_icon
  -- port map ( CONTROL0 => CONTROL);

  -- TRIG0(8 downto 0)   <= reg_from_mt.tx_ctrl_bytes_num_o;
  -- TRIG0(18 downto 11) <= tx_ctrl_byte;
  -- TRIG0(27 downto 19) <= reg_to_mt.tx_stat_curr_byte_indx_i;
  -- TRIG0(28)           <= tx_completed_p;
  -- TRIG0(29)           <= fd_txena;
  -- TRIG0(30)           <= fd_txd;
  -- TRIG0(31)           <= fd_txck;
  -- TRIG1               <= tx_frame(0);
  -- TRIG1(15 downto 8)  <= rx_byte_index;
  -- TRIG2(8 downto 0)   <= rx_byte_index;
  -- TRIG2(9)            <= rx_fss_received_p;
  -- TRIG2(10)           <= rx_byte_ready_p;
  -- TRIG2(18 downto 11) <= rx_byte;
  -- TRIG2(26 downto 19) <= rx_ctrl_byte;
  -- TRIG2(27)           <= rx_frame_ok_p;
  -- TRIG2(28)           <= rx_crc_wrong_p;
  -- TRIG2(29)           <= core_rst;
  -- TRIG2(31 downto 30) <= speed_b1_i & speed_b0_i;


end rtl;
--=================================================================================================
--                                        architecture end
--=================================================================================================
---------------------------------------------------------------------------------------------------
--                                      E N D   O F   F I L E
---------------------------------------------------------------------------------------------------

.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


Welcome to ProFip's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   introduction
   sys_arch
   sw_arch
   modules/index
   glossary

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

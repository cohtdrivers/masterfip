.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _mfh_helper:

========================
MasterFip handler helper
========================

.. doxygenfile:: mfh_helper.h
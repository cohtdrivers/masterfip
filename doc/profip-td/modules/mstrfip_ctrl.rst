.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _mstrfip_ctrl:

=================
MasterFip control
=================

.. doxygenfile:: mstrfip_ctrl.h
.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _mf_handler:

=================
MasterFip handler
=================

.. doxygenfile:: mf_handler.h
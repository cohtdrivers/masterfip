.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _pf_err_m:

=============
Error Manager
=============

.. doxygenfile:: pf_err.h
.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _profip_bsp:

===
Bsp
===

.. doxygenfile:: profip_bsp.h
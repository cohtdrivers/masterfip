.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _spi_dma:

========
SPI DMA
========

.. doxygenfile:: spi_dma.h
.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+

.. _sys_arch:

===================
System Architecture
===================

ProFip is based on `SVEC <https://ohwr.org/project/svec/wikis/home>`_,
with two mezzanines:

    - `FMC-PROFINET <https://ohwr.org/project/fmc-profinet/wikis/home>`_ , 
    - `FMC-MasterFip <https://ohwr.org/project/fmc-masterfip/wikis/home>`_ .

| FMC-MasterFip shall be placed in SVEC slot 1.
| FMC-PROFINET shall be placed in SVEC slot 2.

.. graphviz::

    graph {
        splines=line;
        rankdir=LR;
        
        PLC [shape=rectangle]

        subgraph cluster_0 {
            label="SVEC";
            PF [shape=rectangle; label="FMC-Profinet"]
            MF [shape=rectangle; label="FMC-MasterFip"]
        }

        PLC -- PF
        PF -- MF

        NF  [shape=rectangle; label="NanoFip node(s)"]

        MF -- NF
    }

ProFip code consists of:
     - code for MasterFip (gateware and firmware)
     - code for Ertec (OS + software)

FMC-MasterFip
=============

FMC-MasterFIP is an interface card for the WorldFIP network in an LPC
FMC form-factor.

There is only a WorldFip transceiver on the FMC-MasterFip. The transceiver
is controlled by gateware and firmware programmed into the FPGA (located
on the SVEC board)

Gateware and Firmware for "MasterFip for ProFip" is based on basic MasterFip
(for SPEC, controlled by SBC).

The basic MasterFip consists of:
    - gateware with:
        - SPEC base,
        - Mock Turtle with two CPUs (μRV),
        - logic for WorldFIP (master),
    - firmware for: 
        - receiving configuration,
        - running macrocycle: sending and reveiving variables from nodes.

Compared to the basic MasterFip version, the following changes have been
applied:

    - in gateware:
        - SPEC base has been replaced with SVEC base.
        - another version of Mock Turtle (v.4.3.0) with other CPUs (μRV),
        - added SPI <-> RMQ translator, 
        - added SPI,
    - in firmware: 
        - additional frame header (for compatibility with Mock Turtle v.4.3.0)
        - option for communication via RMQ’s (so that the MasterFip can be
          controlled via SPI),
        - added CRC for communication through RMQ’s.

.. figure:: img/sch_01.svg
    :align: center
    :width: 600

So MasterFip for Profip can be controlled both through HMQ (Host) or RMQ (SPI).
After reset, frames from both Message Queues are accepted. After receiving
a valid frame from HMQ or RMQ, the other queue is blocked until it is reset
again.

Instructions how to program MasterFip are available in the User Manual.

FMC-PROFINET
============

FMC-PROFINET is a PROFINET IO device (“slave”) card based on the Siemens
ERTEC200-P chip in the FMC-LPC form-factor.

The Ertec software is responsible for:

    - connection to the Profinet network
    - receiving macrocycles configuration and programming MasterFip,
    - data translation between Profinet and MasterFip,
    - ProFip and MasterFip diagnostics.

A detailed description of the software architecture for Ertec can be found in :ref:`sw_arch`.

Communication between Ertec and MasterFip
=========================================

Ertec communicates with MasterFip throught Pins (GPIO) and SPI.

GPIO
----

GPIOs are used to reset MasterFip and check RMQ status.
All pins are described in the table below.

.. csv-table:: Table Title
   :file: ../profip_pin_mapping.csv
   :header-rows: 2

The signal for the LEDs comes from ERTEC 200-P and goes
to the FPGA on the SVEC, and from there it is connected
to FMC-Profinet where the LEDs are.

SPI
---

SPI is used to send/read messages to/from RMQs.

Messages that go to/from MasterFip are encapsulated as shown below:

.. graphviz::

    graph {
        splines=line;
        rankdir=LR;
        compound=true;
        
        SPIHEADER [shape=rectangle; label="MQOSPI header"]
        
        subgraph cluster_0 {
            label="MQOSPI payload (max 128 bytes)";

            RMQHEADER [shape=rectangle; label="RMQ header"]
            
            subgraph cluster_1 {
                label="RMQ data (max 124 bytes)";
            
                MASTERFIPHEADER [shape=rectangle; label="MasterFIP header"]
                MASTERFIPDATA [shape=rectangle; label="MasterFIP data"]
            }
        }

        SPIHEADER -- RMQHEADER [lhead=cluster_0];
        RMQHEADER -- MASTERFIPHEADER [lhead=cluster_1];
        MASTERFIPHEADER -- MASTERFIPDATA 
    }

MQSPI protocol
^^^^^^^^^^^^^^

MQOSPI (Message Queues over SPI) describes the format of SPI frames that are
sent between Ertec and the FPGA.

.. list-table:: 
   :widths: 25 25 50
   :header-rows: 1

   * - Name
     - Position
     - Description
   * - Direction
     - 0
     - 0 for writting to RMQ, 1 for reading from RMQ
   * - RMQ ID
     - 1 
     - index of RMQ = RMQ number + CPU number * 6
   * - Paylod size
     - 2
     - size in dwords (32 bits) (it is only relevant for sent frames)
   * - Reserved
     - 3
     - not used
   * - Payload
     - 4 - (size * 4 + 3)
     - RMQ frame

Ertec software does not know how large the MQOSPI payload will be when
requesting to read data, so information about RMQ payload size is taken
from the RMQ header.

Warning:

In Mockturtle, the header of HMQ messages is always the same (3x32 bits).
For RMQ messages, the RMQ header depends on the direction. In the case of
messages sent to MockTurtle, the size of the RMQ header is arbitrary.
For ProFip it is 3x32 bits in HMQ format. For messages received from the MT,
the RMQ header size is always 1x32 bits. The header contains the payload
size and sequence number.



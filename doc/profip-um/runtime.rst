.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


.. _runtime:description:

=======
Runtime
=======

ProFIP cycle
============

Data exchange cycle in ProFIP (see the diagram below):

#. after setting the new value of the variable in the PLC, it is sent
   in the next PROFINET data exchange cycle (line "PROFINET"),
#. the variable is received in ProFIP (ERTEC 200-P) and sent to
   the MasterFIP after the end of the current macrocycle (line "ProFIP").
#. the variable is physically sent to the node in the next macrocycle
   (line "MasterFIP"),
#. the variable is processed in nanoFIP - the processing length depends
   on the application running on the systemboard,
#. when the node prepares a variable, it will be sent in the next macrocycle,
#. ProFIP continuously reads variables from MasterFIP. When the last
   consumed variable is received, an interrupt is executed - the consumed
   variables are copied into internal buffer and the produced variables
   are sent to MasterFIP (line "ProFIP"),
#. in the next PROFINET cycle, the buffered variable is sent to the PLC.

Cyclic communication
====================

Cyclic communication is relatively simple - every value entered into
the module will be sent to the node (as long as the value does
not change faster than the duration of the macrocycles).

Variable will reach the node within 2 * macrocycle + 5 ms. Similarly,
if a variable is updated in the nanoFIP node, the change will be
visible in the PLC only after 2* macrocycle + 5 ms.

Data should only be processed if cons status
(available in :ref:`module-nanofip-node`) is equal to 0 (no error).

The data exchange frequency in PROFINET should be at least 3 times higher
than the FIP macrocycle frequency. Otherwise, some messages may be lost.
If the PROFINET cycle length is 2ms, then the macrocycle length (wait window)
should be greater than 6ms. The default PROFIP configuration will ensure
that these assumptions are met.

Aperiodic communication
=======================

To communicate in aperiodic mode - sending commands and reading responses,
user needs to react to events that indicate the arrival of a new message.

There are 3 variables available:
    - cons var cnt (available in :ref:`module-nanofip-node`) - increments
      after the end of reading new variables from MasterFIP (if the node
      does not respond, this variable does not increase),
    - prod var cnt (available in :ref:`module-nanofip-node`) - increments
      after the end of writing new variables to masterFAP.
    - macrocycle cnt (available in :ref:`module-control-status`) - increments
      after each macrocycle,

The response to the command may come up to 3 cycles after sending the command, 
so it's good to compare the command id for the sent messages (to be implemented
by the user) and the received response.

Data should only be processed if cons status
(available in :ref:`module-nanofip-node`) is equal to 0 (no error).

.. figure:: img/ProFIP_times.svg
    :align: center
    :width: 1200
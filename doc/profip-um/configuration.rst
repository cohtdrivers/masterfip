.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


=============
Configuration
=============

Configuration via TIA portal
============================

ProFIP and MasterFip macrocycle can be configured from the TIA Portal via
the PLC. ProFIP is represented as a single device in the PROFINET network. 
WorldFip nodes are represented as Modules. Configuration involves adding 
the appropriate modules to the ProFIP device and configuring the appropriate 
parameters in the moodules.

#. Firstly, the ProFIP GSDML file shall be installed in TIA. Download 
   the GSDML and PNG files from ProFIP GitLab: 
   https://gitlab.cern.ch/cohtdrivers/masterfip/-/tree/ProFIP_dev/software/ertec/gsdml. 
   Copy the files to: ``\<your_tia_project>\AdditionalFiles\GSD``. 
   Restart the TIA portal. (GSDML file was tested on TIA portal V17).

#. If the gsdml file has been imported correctly, the ProFIP device shall be 
   visible in "Hardware catalog" (PROFINET IO → I/O → CERN → DEVKIT → Head module
   → ProFIP translator → Standard, no MRP).

    .. figure:: img/catalog.PNG
        :align: center
        :width: 400
#. Drag & drop ProFIP device to "Topology view".

    .. figure:: img/img2.PNG
        :align: center
        :width: 400
#. Connect ProFIP to PLC in "Network View".

    .. figure:: img/img3.PNG
        :align: center
        :width: 400

#. Verify that the device name and IP address are the same
   as the name on the device. If not, set matching ones.

    .. figure:: img/configuration_name.PNG
        :align: center
        :width: 500

    .. code-block:: sh

        # open console (on FEC): 
        screen /dev/ttywbu-pf-0
        name set
        profip # (or other - matching the configuration in TIA Portal)
        ip

#. Drag & drop NanoFip modules to ProFIP Device. One agent can be represented 
   as one module (one module for incoming and outgoing data) or two modules 
   (separate modules for incoming and outgoing data - used when the sizes of data
   produced and consumed are different).

    NanoFip modules could be placed in slots 3 - 32.

    The ProFIP translator has 12 types of NanoFip modules depending on the size
    and direction of data:

    -	NanoFip node, 124 bytes, input,
    -	NanoFip node, 124 bytes, output,
    -	NanoFip node, 124 bytes, IO,
    -	NanoFip node, 64 bytes, input,
    -	NanoFip node, 64 bytes, output,
    -	NanoFip node, 64 bytes, IO,
    -	NanoFip node, 32 bytes, input,
    -	NanoFip node, 32 bytes, output,
    -	NanoFip node, 32 bytes, IO,
    -	NanoFip node, 16 bytes, input,
    -	NanoFip node, 16 bytes, output,
    -	NanoFip node, 16 bytes, IO.

    .. figure:: img/img5.PNG
        :align: center
        :width: 600

#. Configure NanoFip modules.

    In each "NanoFip node" you can configure parameters for each node. All
    parameters are described in detail here: :ref:`module-nanofip-node`.
    The most important thing is to set the correct agent ID. Each module
    should have a unique agent id (except when one node is represented by
    a separate module for data produced and consumed).

    .. figure:: img/configuration_nfnode.PNG
        :align: center
        :width: 400

#. Configure general parameters.

    In the Control & Status module you can set general parameters for
    the whole WorldFip network. All parameters are described in detail
    here: :ref:`module-control-status`. Setting window times to zero
    means that they will be automatically calculated by ProFIP.
    The calculated values can be read in the :ref:`module-control-status`.

    .. figure:: img/img7.PNG
        :align: center
        :width: 400

#. Save your changes and program the PLC.

    If the device is properly connected and the configuration is correct,
    the PLC status should be green.

    .. figure:: img/configuration_1.PNG
        :align: center
        :width: 400

Macrocycle
==========

The macrocycle is created automatically based on:
    -	selected modules and their order in slots,
    -	parameters entered in modules (described in detail for
        individual modules).

The order of the variables in the macrocycle is as follows:
    -	all produced variables for nanoFip nodes (the order of produced
        variables inside the window is the order of the modules in the slots),
    -	all consumed variables for nanoFip nodes (the order of consumed
        variables inside the window is the order if the modules in the slots), 
    -	periodic diagnostics produced variables (only if MasterFip diagnostics
        enabled in configuration), 
    -	periodic diagnostics consumed variables (only if MasterFip diagnostics
        enabled in configuration),
    -	aperiodic diagnostic (identification) variables (only if MasterFip
        diagnostics enabled in configuration),
    -	wait window.

In the :ref:`module-control-status`, you can set the length of each window.

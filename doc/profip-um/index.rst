.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+



Welcome to ProFip's User Manual!
=======================================

.. toctree::
   :maxdepth: 1
   :caption: Contents

   introduction
   installation
   configuration
   runtime
   diagnostics
   modules
   tools
   troubleshooting
   debugif
   glossary
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


=====
Tools
=====

.. _tools-tty:

TTY console
===========

The console is used to check the status, current configuration
and possible errors of the ProFIP device. It can also be used to
issue simple commands such as: reboot, macrocycle reset. For the
TTY console to work, PROFIP and WBUART drivers must be installed
correctly (see :ref:`installation-masterfip-drivers`).

| To start the console, select: ``screen /dev/ttywbu-pf-0``.
| Type ``?`` to see a list of all available commands.

.. code-block:: sh

    version
    ProFIP V3.0.0 63d30f70
    Mar 21 2024 - 16:06:53

    mcycle
    Macrocycle
    0.000 +- Periodic Var Windows
          | -> Prod Var 0x0501 124 bytes
          | -> Prod Var 0x0504 32 bytes
          | -> Prod Var 0x0505 32 bytes
          | -> Prod Var 0x0506 32 bytes
          | -> Prod Var 0x0507 32 bytes

.. _tools-pf-ertec-diag:

pf_ertec_diag
=============

This program allows to access full WorldFIP diagnostics on SBC (via SHM).
It creates shared memory (SHM) and periodically writes all MasterFIP
diagnostic parameters to it. For the program to work, PROFIP and WBUART
drivers must be installed correctly
(see :ref:`installation-masterfip-drivers`).

The program is located in ``./software/tools/pf_ertec_diag`` directory.

| To run the program, select:
| ``./pf_ertec_diag`` for shortened diagnostic
| or
| ``./pf_ertec_diag -x`` to have access to full diagnostics,
  but updated less frequently.

Shared memory default address is ``0x46444730``.

In the case of the basic diagnostics, the SHM is refreshed every 1 second,
in the case of the extended diagnostics, the SHM is refreshed every 5 seconds.

.. code-block:: sh

    user@pcname:~/masterfip/software/tools/pf_ertec_diag>./pf_ertec_diag -x
    ProFIP diagnostics shared memory
    SHM key: 0x46444730

.. _tools-pf-ertec-reprog:

pf_ertec_reprog
===============

Firmware updater for ERTEC.
For the program to work, PROFIP and WBUART drivers must be installed correctly.
The program communicates with ProFIP over UART (``/dev/ttywb-pfdata-0``).
The program is located in ``project/software/tools/pf_ertec_reprog``.

To run the program, select ``./pf_ertec_reprof -f <patch to PROFIP.bin>``.

Do not switch power off during reprogramming!

NOTE: Reprogramming with an incorrect .bin file will block the device
(JTAG will be needed).

.. code-block:: sh

    user@pcname:~/pf_ertec_reprog>./pf_ertec_reprog -f PROFIP_3.0.0.bin
    > Reprogram Ertec over UART
    file:        PROFIP_3.0.0.bin
    size:        2664 kB
    estimated:   5:07 min
    2.2 %

More on how to use the tool is described here: :ref:`pf_ertec_reprog_`.

.. _tools-ertec-reset:

ertec_reset
===========

| "ertec_reset" is a tool that resets the ERTEC 200-P processor by
| controlling its reset pin. It sets the reset pin to LOW for 40ms.
| It should only be used if ERTEC 200-P stops responding completely.
| 
| The "ProFIP reset" tool is located in ``software/ertec/tool/ertec_reset``
|
| To use it, it shall be built (it can be build on e.g. cs-ccr-dev[1-4].cern.ch)
| ``make ertec_reset``.
|
| To view help:
| ``ertec_reset -h``
|
| To reset ERTEC: 
| ``ertec_reset -a <svec address>``, where svec address is
  ``<slot number> << 19``, which is e.g. 0x180000 for slot 3,
  0x200000 for slot 4

.. _tools-tcp-reprogramming:

TCP reprogramming
=================

It is recommended to reprogram ERTEC (FMC-Profinet) using
the :ref:`tools-pf-ertec-reprog` tool. If this is not possible,
ERTEC 200-P can also be reprogrammed via Ethernet.

A detailed description of how to use the program is here:
:ref:`tcp_reprogram_`.

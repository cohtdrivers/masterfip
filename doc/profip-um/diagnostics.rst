.. SPDX-FileCopyrightText: 2023 CERN (home.cern)
..
.. SPDX-License-Identifier: CC-BY-SA-4.0+


===========
Diagnostics
===========
Diagnostics allows you to detect and determine the cause of the error.
Device diagnostic information can be checked in various ways:

	- Alarms,
	- TTY console, 
	- LEDs,
	- Diagnostic module.
	- Shared memory (SHM) for SBC, 

.. _diagnostics_alarms:

Alarms
======
Alarms are issued when the ProFIP device or at least one of the NanoFIP
nodes fails. The alarm status is visible on the PLC. Its details can be
read from "Diagnostics Buffer (TIA Portal)".

    .. figure:: img/diagnostics_alarm_2.PNG
        :align: center
        :width: 500

If any Alarm is active, the red LED on PROFINET-FMC is blinking
(see :ref:`diagnostics-leds`). If the ProFIP internal error occurs
or the ProFIP configuration is incorrect and it is not possible
to program the macrocycle, an alarm will appear on the
:ref:`module-control-status`. A detailed description of these alarms
can be found here: :ref:`module-control-status-alarms`.
  
    .. figure:: img/diagnostics_alarm_3.PNG
        :align: center
        :width: 500

If the alarm is related to incorrect or missing messages from the selected
agent, the alarm will appear on the selected :ref:`module-nanofip-node`.
More details about alarms for NanoFip nodes are described here:
:ref:`module-nanofip-alarms`.

    .. figure:: img/diagnostics_alarm_1.PNG
        :align: center
        :width: 400

.. _diagnostics_tty:

TTY console
===========
The console allows you to read the operating parameters of the ProFIP device.
A detailed description of the TTY console can be found here :ref:`tools-tty`.

.. _diagnostics-leds:

Leds
====
FMC-Profinet has two LEDs on the front panel (Green and Red) and one LED on
PCB (Yellow). They provide information about the device status.

    .. figure:: img/diagnostics_leds.PNG
        :align: center
        :width: 400

Green LED
---------
- Blinking fast (8Hz) - ProFIP is in ``CONNECTING`` state - PLC is not
  connected. Translation is not running.
- Blinking slow (1Hz) - ProFIP is in ``RUNNING`` state - PLC is connected
  and translation is running, 
- On - ProFIP is in ``STOPPED`` state - Prifinet is connected, Macrocycle
  is configured, but its stopped. Macrocycle can only be stopped by
  a command sent by the :ref:`module-control-status`.

By default, when the device is turned on, it flashes quickly (8 Hz) until
it connects to the PLC.

Red LED
-------
- Blinking slow (1Hz)- At least 1 alarm is active. Due to a device or
  configuration error, the macrocycle could not be programmed, or one
  or more NanoFIP nodes are not sending correct data. Check the diagnostic
  buffer to see the cause of the alarm.
- Off - no active alarms.

Yellow LED
----------
Yellow LED is not visible from the front (its on Profinet-FMC PCB).

- Blinking - ProFIP is in ``CONNECTING`` state.

.. _diagnostics_shm:

SHM Diagnostics
===============

WorldFIP network diagnostic tools use shared memory (SHM) to read all
diagnostic parameters. In the case of ProFIP all translation takes place
on the ERTEC 200-P processor. The :ref:`tools-pf-ertec-diag` tool reads
all diagnostics data from the ERTEC processor and makes them available
via shared memory (SHM) for diagnostic tools.

More about the tool: :ref:`tools-pf-ertec-diag`.

.. _diagnostics_module_diagnostics:

Module "Diagnostics"
====================
In the :ref:`module-diagnostics` module you can read all FIP diagnostic
parameters (the same as those available via shared memory (SHM) in the
case of MasterFIP).
